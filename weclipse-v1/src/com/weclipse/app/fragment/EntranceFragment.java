package com.weclipse.app.fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.widget.LoginButton;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.EntranceActivity;
import com.weclipse.app.activity.LoginActivity;
import com.weclipse.app.activity.SignupActivity;

public class EntranceFragment extends BaseFragment implements OnClickListener {
	
	Button bLogin,bSignup;
	LoginButton bFacebook;
	
	WApp app;
	EntranceActivity act;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		app = (WApp)getActivity().getApplication();
		act = (EntranceActivity)getActivity();
		
		View v = inflater.inflate(R.layout.fragment_entrance,container,false);
		
		initVariables(v);
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if(!act.hasGoogleServices){
			bLogin.setVisibility(View.INVISIBLE);
			bSignup.setVisibility(View.INVISIBLE);
			bFacebook.setVisibility(View.INVISIBLE);
		} else {
			bLogin.setVisibility(View.VISIBLE);
			bSignup.setVisibility(View.VISIBLE);
			bFacebook.setVisibility(View.VISIBLE);
		}
		
	}
	
	public void initVariables(View v){
		if(act.hasGoogleServices){
			bLogin = (Button)v.findViewById(R.id.bLogin);
			bSignup = (Button)v.findViewById(R.id.bSignup);
			bFacebook = (LoginButton)v.findViewById(R.id.bFacebook);
			bLogin.setVisibility(View.INVISIBLE);
			bSignup.setVisibility(View.INVISIBLE);
			bFacebook.setVisibility(View.INVISIBLE);
			
			bLogin.setOnClickListener(this);
			bSignup.setOnClickListener(this);
			
			ArrayList<String> permissions = new ArrayList<String>();
			permissions.add("email");
			permissions.add("public_profile");
			permissions.add("user_birthday");
			permissions.add("user_friends");
			bFacebook.setReadPermissions(permissions);
			
			bFacebook.setBackgroundResource(R.drawable.button_opaque_blue_pressed);
//			bFacebook.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
			
			((TextView)v.findViewById(R.id.tvLinks)).setMovementMethod(LinkMovementMethod.getInstance());
		} else {
			//TODO vai desinstalar os google play services
		}
		
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bLogin:
			startActivity(new Intent(act,LoginActivity.class));
			act.finish();
			break;
		case R.id.bSignup:
			startActivity(new Intent(act,SignupActivity.class));
			act.finish();
			break;
		case R.id.bFacebook:
			startActivity(new Intent(act,com.facebook.LoginActivity.class));
			break;
		}
	}
	
}
