package com.weclipse.app.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.CountryListActivity;
import com.weclipse.app.activity.SyncContactsActivity;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.tasks.RequestSMSCodeTask;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.RequestSMSEvent;

public class SyncContactsFragment extends BaseFragment implements OnClickListener {

	Button bSync;
	
	TextView tvCode;
	EditText etPhone;
	
	WApp app;
	SyncContactsActivity act;
	
	PhoneNumberUtil phoneUtil;
	
	private static final int CODE_GET_COUNTRY = 1;
	
	View v;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		app = (WApp)getActivity().getApplication();
		act = (SyncContactsActivity)getActivity();
		
		v = inflater.inflate(R.layout.fragment_sync_contacts,container,false);
		
		initVariables(v);
		
		return v;
		
	}
	
	public void initVariables(View v){
		bSync = (Button)v.findViewById(R.id.bSyncContacts);
		bSync.setOnClickListener(this);
		
		phoneUtil = PhoneNumberUtil.getInstance();
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bSyncContacts:
			new NumberDialogFragment().show(getActivity().getSupportFragmentManager(), "phone_number_dialog");
			break;
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == Activity.RESULT_OK){
			String code = data.getExtras().getString("code");
			PreferenceManager.getInstance(app).putPreference(PreferenceManager.PHONE_CODE, code);
			tvCode.setText("+"+code);
		}
	}

	@SuppressLint("ValidFragment")
	public class NumberDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			
						
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			View layout = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_phone_number, null);
			
			tvCode = (TextView)layout.findViewById(R.id.tvCode);
			etPhone = (EditText)layout.findViewById(R.id.etPhone);
			RelativeLayout etCodeContainer = (RelativeLayout)layout.findViewById(R.id.rlCodeContainer);
			
			String code = ""+phoneUtil.getCountryCodeForRegion(((TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE)).getNetworkCountryIso().toUpperCase());
			PreferenceManager.getInstance(app).putPreference(PreferenceManager.PHONE_CODE, code);
			tvCode.setText("+"+code);
			
			etCodeContainer.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent i = new Intent(getActivity(),CountryListActivity.class);
					SyncContactsFragment.this.startActivityForResult(i, CODE_GET_COUNTRY);
				}
			});
			
			builder.setView(layout);
			builder.setPositiveButton(R.string.veirify,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									//validify fields k?
									//if valid, send the sms k?
									if(validatePhone()){
										WLog.d(this,"Sending the code...");
										new RequestSMSCodeTask(app,tvCode.getText().toString().substring(1), etPhone.getText().toString());
									}
								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// do nothing
								}
							});
			return builder.create();
		}
	}
	
	public boolean validatePhone(){
		
		if(tvCode.getText().toString().equals("") || tvCode.getText().toString().equals("0")){
//			act.getToast().setText(R.string.toast_empty_country_code).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			Toast.makeText(act, getString(R.string.toast_empty_country_code), Toast.LENGTH_SHORT).show();
			WLog.d(this,getString(R.string.toast_empty_country_code));
			return false;
		}
		
		if(etPhone.getText().toString().equals("")){
//			act.getToast().setText(R.string.toast_empty_phone).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			Toast.makeText(act, getString(R.string.toast_empty_phone), Toast.LENGTH_SHORT).show();
			WLog.d(this,getString(R.string.toast_empty_phone));
			return false;
		}
		
		try {
			PhoneNumber phoneNo = phoneUtil.parse(etPhone.getText().toString(),phoneUtil.getRegionCodeForCountryCode(Integer.valueOf(tvCode.getText().toString().substring(1))));
			if(!phoneUtil.isValidNumber(phoneNo)){
//				act.getToast().setText(R.string.toast_invalid_phone).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
				Toast.makeText(act, getString(R.string.toast_invalid_phone), Toast.LENGTH_SHORT).show();
				WLog.d(this,getString(R.string.toast_invalid_phone));
				return false;
			}
		} catch (NumberFormatException e) {
//			act.getToast().setText(R.string.toast_invalid_phone).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			Toast.makeText(act, getString(R.string.toast_invalid_phone), Toast.LENGTH_SHORT).show();
			WLog.d(this,getString(R.string.toast_invalid_phone));
			return false;
		} catch (NumberParseException e) {
//			act.getToast().setText(R.string.toast_invalid_phone).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			Toast.makeText(act, getString(R.string.toast_invalid_phone), Toast.LENGTH_SHORT).show();
			WLog.d(this,getString(R.string.toast_invalid_phone));
			return false;
		}
		
		return true;
	}
	
	/* 
	 * 
	 * Subscriptions and the like
	 * 
	 */
	@Subscribe
	public void onAsyncTaskResult(RequestSMSEvent event){
		if(event.getSuccess()){
			//Next fragment
			getActivity().getSupportFragmentManager().beginTransaction().remove(SyncContactsFragment.this).replace(R.id.fragment, new SyncContactsCodeFragment()).commit();
		} else {
			//Erro e fazer bajoras
		}
	}
	
}
