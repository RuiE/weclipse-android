package com.weclipse.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.SettingsActivity;
import com.weclipse.app.managers.AlertManager;

public class NotificationSettingsFragment extends Fragment implements OnClickListener {

	WApp app;
	SettingsActivity act;

	CheckBox cbVideoAlert,cbVideoSound,cbVideoVibration,cbVideoLED;
	
	CheckBox cbCommentAlert,cbCommentSound,cbCommentVibration,cbCommentLED;
	
	CheckBox cbFriendAlert,cbFriendSound,cbFriendVibration,cbFriendLED;
	
	AlertManager mAlertManager;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		app = (WApp)getActivity().getApplication();
		act = (SettingsActivity)getActivity();
		View v = inflater.inflate(R.layout.fragment_settings_notification,container,false);
		initVariables(v);
		return v;
	}
	
	public View initVariables(View v){
		
		//Click listeners on layouts
		v.findViewById(R.id.rlVideoAlert).setOnClickListener(this);
		v.findViewById(R.id.rlVideoVibration).setOnClickListener(this);
		v.findViewById(R.id.rlVideoSound).setOnClickListener(this);
		v.findViewById(R.id.rlVideoLED).setOnClickListener(this);
		
		v.findViewById(R.id.rlCommentAlert).setOnClickListener(this);
		v.findViewById(R.id.rlCommentVibration).setOnClickListener(this);
		v.findViewById(R.id.rlCommentSound).setOnClickListener(this);
		v.findViewById(R.id.rlCommentLED).setOnClickListener(this);
		
		v.findViewById(R.id.rlFriendAlert).setOnClickListener(this);
		v.findViewById(R.id.rlFriendVibration).setOnClickListener(this);
		v.findViewById(R.id.rlFriendSound).setOnClickListener(this);
		v.findViewById(R.id.rlFriendLED).setOnClickListener(this);
		
		mAlertManager = AlertManager.getInstance(app);
		
		//Checkboxes
		cbVideoAlert = (CheckBox)v.findViewById(R.id.cbVideoAlert);
		cbVideoAlert.setChecked(mAlertManager.getAlertPreference(AlertManager.VIDEO_ALERT));
		
		cbVideoVibration = (CheckBox)v.findViewById(R.id.cbVideoVibration);
		cbVideoVibration.setChecked(mAlertManager.getAlertPreference(AlertManager.VIDEO_VIBRATION));
		
		cbVideoSound = (CheckBox)v.findViewById(R.id.cbVideoSound);
		cbVideoSound.setChecked(mAlertManager.getAlertPreference(AlertManager.VIDEO_SOUND));
		
		cbVideoLED = (CheckBox)v.findViewById(R.id.cbVideoLED);
		cbVideoLED.setChecked(mAlertManager.getAlertPreference(AlertManager.VIDEO_LED));
		
		
		cbCommentAlert = (CheckBox)v.findViewById(R.id.cbCommentAlert);
		cbCommentAlert.setChecked(mAlertManager.getAlertPreference(AlertManager.COMMENT_ALERT));
		
		cbCommentVibration = (CheckBox)v.findViewById(R.id.cbCommentVibration);
		cbCommentVibration.setChecked(mAlertManager.getAlertPreference(AlertManager.COMMENT_VIBRATION));
		
		cbCommentSound = (CheckBox)v.findViewById(R.id.cbCommentSound);
		cbCommentSound.setChecked(mAlertManager.getAlertPreference(AlertManager.COMMENT_SOUND));
		
		cbCommentLED = (CheckBox)v.findViewById(R.id.cbCommentLED);
		cbCommentLED.setChecked(mAlertManager.getAlertPreference(AlertManager.COMMENT_LED));
		
		
		cbFriendAlert = (CheckBox)v.findViewById(R.id.cbFriendAlert);
		cbFriendAlert.setChecked(mAlertManager.getAlertPreference(AlertManager.FRIEND_ALERT));
		
		cbFriendVibration = (CheckBox)v.findViewById(R.id.cbFriendVibration);
		cbFriendVibration.setChecked(mAlertManager.getAlertPreference(AlertManager.FRIEND_VIBRATION));
		
		cbFriendSound = (CheckBox)v.findViewById(R.id.cbFriendSound);
		cbFriendSound.setChecked(mAlertManager.getAlertPreference(AlertManager.FRIEND_SOUND));
		
		cbFriendLED = (CheckBox)v.findViewById(R.id.cbFriendLED);
		cbFriendLED.setChecked(mAlertManager.getAlertPreference(AlertManager.FRIEND_LED));
		
		manageVideoCBs();
		manageCommentCBs();
		manageFriendCBs();
		
		return v;
	}
	
	public void manageVideoCBs(){
		cbVideoVibration.setEnabled(cbVideoAlert.isChecked());
		cbVideoSound.setEnabled(cbVideoAlert.isChecked());
		cbVideoLED.setEnabled(cbVideoAlert.isChecked());
	}
	
	public void manageCommentCBs(){
		cbCommentVibration.setEnabled(cbCommentAlert.isChecked());
		cbCommentSound.setEnabled(cbCommentAlert.isChecked());
		cbCommentLED.setEnabled(cbCommentAlert.isChecked());
	}
	
	public void manageFriendCBs(){
		cbFriendVibration.setEnabled(cbFriendAlert.isChecked());
		cbFriendSound.setEnabled(cbFriendAlert.isChecked());
		cbFriendLED.setEnabled(cbFriendAlert.isChecked());
	}
	
	public void changePreference(CheckBox cb,String pref){
		if(cb.isEnabled()){
			cb.setChecked(!cb.isChecked());
			mAlertManager.setAlertPreference(pref, cb.isChecked());
		}
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.rlVideoAlert:
			changePreference(cbVideoAlert,AlertManager.VIDEO_ALERT);
			manageVideoCBs();
			break;
		case R.id.rlVideoVibration:
			changePreference(cbVideoVibration,AlertManager.VIDEO_VIBRATION);
			
			break;
		case R.id.rlVideoSound:
			changePreference(cbVideoSound,AlertManager.VIDEO_SOUND);
			break;
		case R.id.rlVideoLED:
			changePreference(cbVideoLED,AlertManager.VIDEO_LED);
			break;
		case R.id.rlCommentAlert:
			changePreference(cbCommentAlert,AlertManager.COMMENT_ALERT);
			manageCommentCBs();
			break;
		case R.id.rlCommentVibration:
			changePreference(cbCommentVibration,AlertManager.COMMENT_VIBRATION);
			break;
		case R.id.rlCommentSound:
			changePreference(cbCommentSound,AlertManager.COMMENT_SOUND);
			break;
		case R.id.rlCommentLED:
			changePreference(cbCommentLED,AlertManager.COMMENT_LED);
			break;
		case R.id.rlFriendAlert:
			changePreference(cbFriendAlert,AlertManager.FRIEND_ALERT);
			manageFriendCBs();
			break;
		case R.id.rlFriendVibration:
			changePreference(cbFriendVibration,AlertManager.FRIEND_VIBRATION);
			break;
		case R.id.rlFriendSound:
			changePreference(cbFriendSound,AlertManager.FRIEND_SOUND);
			break;
		case R.id.rlFriendLED:
			changePreference(cbFriendLED,AlertManager.FRIEND_LED);
			break;
		}
	}
	
}