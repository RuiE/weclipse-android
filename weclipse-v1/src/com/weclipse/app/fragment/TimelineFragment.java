package com.weclipse.app.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.TheMainActivity;
import com.weclipse.app.drawer.TimelineVideoDrawer;
import com.weclipse.app.drawer.TimelineVideoDrawer.ViewHolderVideo;
import com.weclipse.app.managers.DownloadManager;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.tasks.DownloadVideoTask;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.fetch.FetchTimelineTask;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.EndlessScrollListener;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DeleteVideoEvent;
import com.weclipse.app.utils.eventbus.FetchTimelineEvent;
import com.weclipse.app.utils.eventbus.InlineVideoStopEvent;
import com.weclipse.app.utils.eventbus.PageChangedEvent;
import com.weclipse.app.utils.eventbus.ReportEvent;
import com.weclipse.app.utils.eventbus.TimelineUpdateEvent;
import com.weclipse.app.utils.eventbus.WBus;
import com.weclipse.app.widget.InlineVideoPlayer;

public class TimelineFragment extends BaseFragment implements OnRefreshListener {

	WApp app;
	TheMainActivity act;
	Me me;

	Cursor cursorTimeline;
	ListView lvTimeline;
	TimelineAdapter mTimelineAdapter;

	SwipeRefreshLayout swipeTimeline;

	RelativeLayout rlNoTimeline;
	
	InlineVideoPlayer currentIVP;
	public int position = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		app = (WApp) getActivity().getApplication();
		act = (TheMainActivity) getActivity();

		View v = inflater.inflate(R.layout.fragment_timeline, container, false);

		me = Me.getInstance(act);

		initVariables(v);

		return v;
	}

	public void initVariables(View v) {
		swipeTimeline = (SwipeRefreshLayout) v.findViewById(R.id.swipeLayout);
		swipeTimeline.setColorSchemeResources(R.color.weclipse_green_light,
				R.color.weclipse_green,
				R.color.weclipse_yellow,
				R.color.weclipse_red);

		swipeTimeline.setOnRefreshListener(this);

		lvTimeline = (ListView) v.findViewById(R.id.lv);
		lvTimeline.setDividerHeight(0);

		lvTimeline.setOnScrollListener(new EndlessScrollListener() {

			@Override
			public void onLoadMore(int page, int totalItemsCount) {
				WLog.d(this, "Feel like loading more...: " + page);
				new FetchTimelineTask(app, FetchTimelineTask.LOAD_MORE);
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
				
				
//				WLog.d(this,"firstVisible: "+firstVisibleItem+" | visibleItemCount: "+visibleItemCount+" | position: "+position);
				if(position != -1 && (position < firstVisibleItem || position >= (firstVisibleItem+visibleItemCount))){
					currentIVP.stop();
					currentIVP = null;
					position = -1;
				}
			}
			
			
		});

		cursorTimeline = null;
		
		rlNoTimeline = (RelativeLayout) v.findViewById(R.id.rlNoTimeline);
	}

	@Override
	public void onResume() {
		super.onResume();
		updateTimeline();

		if (app.getFeedLoadingManager().feedRefreshIntent) {
			app.getFeedLoadingManager().feedRefreshIntent = false;
			onRefresh();
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		if(currentIVP != null){
			currentIVP.stop();
			currentIVP = null;
			position = -1;
		}
	}

	@Override
	public void onRefresh() {
		new FetchTimelineTask(app, SimpleTask.FRESH);
	}

	public void notifyTimeline() {
		if (mTimelineAdapter != null) {
			mTimelineAdapter.swapCursor(TimelineVideo.getTimeline(app));
			mTimelineAdapter.notifyDataSetChanged();
		}
	}

	public void scrollTop() {
		if (lvTimeline == null) {
			WLog.d(this, "lvtimeline a nul....");
		} else {
			lvTimeline.smoothScrollToPosition(0);
		}
	}

	public void updateTimeline() {

		if (cursorTimeline != null)
			cursorTimeline.close();

		cursorTimeline = TimelineVideo.getTimeline(app);

		if (cursorTimeline.getCount() == 0) {
			rlNoTimeline.setVisibility(View.VISIBLE);
			return;
		} else {
			rlNoTimeline.setVisibility(View.GONE);

			if (mTimelineAdapter == null) {
				mTimelineAdapter = new TimelineAdapter(getActivity(), cursorTimeline);
				lvTimeline.setAdapter(mTimelineAdapter);
			} else {
				mTimelineAdapter.changeCursor(cursorTimeline);
				mTimelineAdapter.notifyDataSetChanged();
			}

		}

	}
	
	public static void printViewHierarchy(ViewGroup $vg, String $prefix)
	{
	    for (int i = 0; i < $vg.getChildCount(); i++) {
	        View v = $vg.getChildAt(i);
	        String desc = $prefix + " | " + "[" + i + "/" + ($vg.getChildCount()-1) + "] "+ v.getClass().getSimpleName() + " " + v.getId();
	        WLog.v("x", desc);

	        if (v instanceof ViewGroup) {
	            printViewHierarchy((ViewGroup)v, desc);
	        }
	    }
	}

	class TimelineAdapter extends CursorAdapter {

		LayoutInflater mInflater;
		Context context;

		public TimelineAdapter(Context context, Cursor c) {
			super(context, c);
			this.context = context;
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public void bindView(View view, Context context,Cursor c) {
			
			final int cursor_position = c.getPosition();

			final long vid = c.getLong(c.getColumnIndex(DBHelper.C_ID));

			ViewHolderVideo holder = (ViewHolderVideo) view.getTag();

			String title = c.getString(c.getColumnIndex(DBHelper.C_TITLE));
			long owner_id = c.getLong(c.getColumnIndex(DBHelper.C_USER_ID));
			long group_id = c.getLong(c.getColumnIndex(DBHelper.C_GROUP_ID));

			int video_status = c.getInt(c.getColumnIndex(DBHelper.C_VIDEO_STATUS));

			String updated_at = c.getString(c.getColumnIndex(DBHelper.C_UPDATED_AT));
			String created_at = c.getString(c.getColumnIndex(DBHelper.C_CREATED_AT));
			String ready_at = c.getString(c.getColumnIndex(DBHelper.C_READY_AT));

			int n_likes = c.getInt(c.getColumnIndex(DBHelper.C_N_LIKES));
			int n_views = c.getInt(c.getColumnIndex(DBHelper.C_N_VIEWS));
			int n_comments = c.getInt(c.getColumnIndex(DBHelper.C_N_COMMENTS));

			int private_video = c.getInt(c.getColumnIndex(DBHelper.C_PRIVATE_VIDEO));
			int editors_choice = c.getInt(c.getColumnIndex(DBHelper.C_EDITORS_CHOICE));
			long reweclipsed_id = c.getLong(c.getColumnIndex(DBHelper.C_REWECLIPSED_BY));

			String[] user_ids = c.getString(c.getColumnIndex(DBHelper.C_USERS)).split(",");
			String[] replieds = c.getString(c.getColumnIndex(DBHelper.C_REPLIED_AT)).split(",");

			String thumbnail_url = c.getString(c.getColumnIndex(DBHelper.C_THUMBNAIL_URL));
			String share_url = c.getString(c.getColumnIndex(DBHelper.C_SHARE_URL));

			ArrayList<Long> users = new ArrayList<Long>();
			ArrayList<String> replied_ats = new ArrayList<String>();

			for (int i = 0; i < user_ids.length; i++) {
				users.add(Long.valueOf(user_ids[i]));
				replied_ats.add(replieds[i]);
			}

			final TimelineVideo video = new TimelineVideo(context, vid, title, owner_id, group_id, video_status, ready_at, updated_at, created_at, users, replied_ats, n_likes, n_views, n_comments, private_video, editors_choice,thumbnail_url, share_url,reweclipsed_id);

			TimelineVideoDrawer drawer = new TimelineVideoDrawer(getActivity(), app, video, holder, AbstractVideo.SOURCE_TIMELINE);
			drawer.liked = c.getString(c.getColumnIndex("like")) != null;
			drawer.draw();
			
			if(WApp.INLINE){
				if(holder.rlContainer.getChildAt(holder.rlContainer.getChildCount()-1) instanceof InlineVideoPlayer){
					holder.rlContainer.removeViewAt(holder.rlContainer.getChildCount()-1);
				}
				
				holder.rlContainer.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(TimelineFragment.this.position != cursor_position){
							
							if(!DownloadManager.getInstance(app).containsKey(video.getId())){
								DownloadVideoTask mTask = new DownloadVideoTask(app,video);
								DownloadManager.getInstance(app).put(video.getId(), mTask);
							}
							
							if(TimelineFragment.this.position != -1){
								currentIVP.stop();
								currentIVP = null;
								position = -1;
							}
							
							TimelineFragment.this.position = cursor_position;
							
							InlineVideoPlayer ivp = new InlineVideoPlayer(video, app, (RelativeLayout)v);
							ivp.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									WLog.d(this,"Clickei no IVP lolol");
								}
							});
							currentIVP = ivp;
							WBus.getInstance().register(ivp);
							
							RelativeLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
							ivp.setLayoutParams(params);
							
							((RelativeLayout)v).addView(ivp);
							((RelativeLayout)v).findViewById(R.id.videoProgress).setVisibility(View.VISIBLE);
						} else {
							WLog.d(this,"Already doing the stuff");
						}
					}
				});
			}

		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			return TimelineVideoDrawer.newView(mInflater.inflate(R.layout.row_timeline, parent, false));
		}

	}
	
	@Subscribe
	public void onAsyncTaskResult(TimelineUpdateEvent event) {
		if (mTimelineAdapter != null) {
			mTimelineAdapter.swapCursor(TimelineVideo.getTimeline(app));
			mTimelineAdapter.notifyDataSetChanged();
		}
	}

	@Subscribe
	public void onAsyncTaskResult(FetchTimelineEvent event) {
		swipeTimeline.setRefreshing(false);

		if (event.getSuccess()) {
			if (event.getMethod() == FetchTimelineTask.FRESH) {
				updateTimeline();
			} else {
				notifyTimeline();
			}
		}

	}

	@Subscribe
	public void onAsyncTaskResult(ReportEvent event) {
		if (event.getSuccess()) {
			Toast.makeText(getActivity(), getString(R.string.toast_report), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(InlineVideoStopEvent event){
		position = -1;
		currentIVP = null;
		
		if(event.getType() == InlineVideoStopEvent.CORRUPTED){
			Toast.makeText(act, "Corrupted file. Sorry for the incovenience", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(PageChangedEvent event){
		position = -1;
		if(currentIVP != null){
			currentIVP.stop();
			currentIVP = null;
		}
	}
	
}
