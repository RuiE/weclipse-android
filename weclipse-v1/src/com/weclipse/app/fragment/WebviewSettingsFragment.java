package com.weclipse.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.SettingsActivity;

public class WebviewSettingsFragment extends Fragment {
	
	WApp app;
	SettingsActivity act;
	WebView wv;
	WebViewClient wvc;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		act = (SettingsActivity)getActivity();
		app = (WApp)act.getApplication();
		View v = inflater.inflate(R.layout.fragment_settings_webview,container,false);
		initVariables(v);
		return v;
	}
	
	public View initVariables(View v){
		String url = getArguments().getString("url");
		wv = (WebView)v.findViewById(R.id.wvBrowser);
		wv.getSettings().setJavaScriptEnabled(true);
		wvc = new WebViewClient(){
			@Override
		    public boolean shouldOverrideUrlLoading(WebView  view, String  url){
				wv.loadUrl(url);
		        return true;
		    }
		};
		
		wv.setWebViewClient(wvc);
		wv.loadUrl(url);
		return v;
	}
	
}
