package com.weclipse.app.fragment;

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.CameraActivity;
import com.weclipse.app.activity.FeedPendingDetailActivity;
import com.weclipse.app.activity.GroupActivity;
import com.weclipse.app.activity.ProfileActivity;
import com.weclipse.app.activity.TheMainActivity;
import com.weclipse.app.activity.VideoDetailActivity;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.FollowingButtonDrawer;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.PendingVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.UserActivity;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.AcceptFollowTask;
import com.weclipse.app.tasks.FollowTask;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.UnfollowTask;
import com.weclipse.app.tasks.fetch.FetchActivityTask;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.DefaultHashMap;
import com.weclipse.app.utils.ReplyPendingCache;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.AcceptFollowEvent;
import com.weclipse.app.utils.eventbus.ActivityUpdateEvent;
import com.weclipse.app.utils.eventbus.CreatePendingVideoFinishedEvent;
import com.weclipse.app.utils.eventbus.CreatePendingVideoStartedEvent;
import com.weclipse.app.utils.eventbus.FeedUpdateEvent;
import com.weclipse.app.utils.eventbus.FollowEvent;
import com.weclipse.app.utils.eventbus.ProgressCreateVideoEvent;
import com.weclipse.app.utils.eventbus.ProgressPendingCreateVideoEvent;
import com.weclipse.app.utils.eventbus.ProgressReplyVideoEvent;
import com.weclipse.app.utils.eventbus.ProgressVideoDownload;
import com.weclipse.app.utils.eventbus.ReplyPendingVideoFinishedEvent;
import com.weclipse.app.utils.eventbus.UnfollowEvent;
import com.weclipse.app.utils.eventbus.VideoCreatedEvent;
import com.weclipse.app.utils.eventbus.VideoDownloadEvent;
import com.weclipse.app.utils.eventbus.VideoInviteEvent;
import com.weclipse.app.widget.ProgressWheel;

public class FeedFragment extends BaseFragment implements OnRefreshListener {

	WApp app;
	TheMainActivity act;
	Me me;

	Cursor cursorActivity;

	ActivityAdapter mActivityAdapter;
	ListView lvMyFeed;

	HashMap<Long, ProgressWheel> mWheels;
	DefaultHashMap<Long, Integer> mStatuses;

	LinearLayout header;
	LinearLayout headerCreate;

	SwipeRefreshLayout swipeFeed;

	RelativeLayout rlNoActivity;

	boolean refreshing;

	OnClickListener avatarListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent i = new Intent(getActivity(), ProfileActivity.class);
			i.putExtra("user_id", (Long) v.getTag());
			startActivity(i);
		}
	};

	OnClickListener follow = new OnClickListener() {

		@Override
		public void onClick(View v) {
			long user_id = (Long) v.getTag();
			WLog.d(this, "Clicked follow!: " + mStatuses.get(user_id));
			switch (mStatuses.get(user_id)) {
			case User.CLEAN_FOLLOW:
			case User.PENDING_ACCEPT:
				new UnfollowTask(app, user_id);
				v.setBackgroundResource(R.drawable.button_following);
				break;
			case User.NOTHING:
				new FollowTask(app, user_id);
				if (UserCache.getInstance(app).getUser(user_id).isPrivate()) {
					((Button) v).setText(getString(R.string.profile_button_follow_pending));
					v.setBackgroundResource(R.drawable.button_following_pending);
				} else {
					v.setBackgroundResource(R.drawable.button_following);
				}
				break;
			}
		}
	};

	ClickableSpan mUserClickSpan = new ClickableSpan() {

		@Override
		public void onClick(View v) {
			Bundle b = (Bundle) v.getTag();
			Intent i = new Intent(getActivity(), ProfileActivity.class);
			i.putExtra("user_id", b.getLong("user_id"));
			startActivity(i);
		}

		public void updateDrawState(android.text.TextPaint ds) {
			ds.setUnderlineText(false);
		};

	};

	ClickableSpan mVideoClickSpan = new ClickableSpan() {

		@Override
		public void onClick(View v) {
			Bundle b = (Bundle) v.getTag();
			Intent i = new Intent(getActivity(), VideoDetailActivity.class);
			i.putExtra("vid", b.getLong("video_id"));
			startActivity(i);
		}

		public void updateDrawState(android.text.TextPaint ds) {
			ds.setUnderlineText(false);
		};
	};

	ClickableSpan mPendingVideoClickSpan = new ClickableSpan() {

		@Override
		public void onClick(View v) {
			WLog.d(this, "Click pending video start activtiy");
			Intent i = new Intent(getActivity(), FeedPendingDetailActivity.class);
			i.putExtra("pid", (Long) v.getTag());
			startActivity(i);
		}

		public void updateDrawState(android.text.TextPaint ds) {
			ds.setUnderlineText(false);
		};
	};

	ClickableSpan mGroupClickSpan = new ClickableSpan() {

		@Override
		public void onClick(View widget) {
			Bundle b = (Bundle) widget.getTag();
			Intent i = new Intent(getActivity(), GroupActivity.class);
			WLog.d(this, "Group id: " + b.getLong("group_id"));
			i.putExtra("group_id", b.getLong("group_id"));
			startActivity(i);
		}

		public void updateDrawState(android.text.TextPaint ds) {
			ds.setUnderlineText(false);
		};
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		app = (WApp) getActivity().getApplication();
		act = (TheMainActivity) getActivity();

		View v = inflater.inflate(R.layout.fragment_feed, container, false);

		me = Me.getInstance(act);

		initVariables(v);
		
		mStatuses = new DefaultHashMap<Long, Integer>(0);

		return v;
	}

	public void initVariables(View v) {

		swipeFeed = (SwipeRefreshLayout) v.findViewById(R.id.swipeLayout);
		swipeFeed.setColorSchemeResources(R.color.weclipse_green_light, R.color.weclipse_green, R.color.weclipse_yellow, R.color.weclipse_red);

		swipeFeed.setOnRefreshListener(this);

		header = (LinearLayout) act.getLayoutInflater().inflate(R.layout.header_feed, null);
		headerCreate = (LinearLayout) act.getLayoutInflater().inflate(R.layout.header_feed_create, null);

		lvMyFeed = (ListView) v.findViewById(R.id.lv);
		lvMyFeed.setDividerHeight(0);
		

		// lvMyFeed.setOnScrollListener(new EndlessScrollListener() {
		//
		// @Override
		// public void onLoadMore(int page, int totalItemsCount) {
		// WLog.d(this,"Feel like loading more...: "+page);
		// new FetchActivityTask(app, FetchTimelineTask.LOAD_MORE,
		// UserActivity.getLastId(app));
		// }
		// });

		lvMyFeed.addHeaderView(headerCreate);
		lvMyFeed.addHeaderView(header);

		cursorActivity = null;

		mWheels = new HashMap<>();

		rlNoActivity = (RelativeLayout) v.findViewById(R.id.rlNoActivity);
	}

	@Override
	public void onRefresh() {
		new FetchActivityTask(app, SimpleTask.FRESH, 0);
	}

	public void scrollTop() {
		lvMyFeed.smoothScrollToPosition(0);
	}

	public void sortPending() {
		Cursor headerVideos = app.getDB().getPendingVideos(PendingVideo.CREATE);
		if (headerVideos.getCount() > 0) {
			drawPendingVideos(headerVideos);
		} else {
			header.removeAllViews();
			headerVideos.close();
		}
	}

	public void sortCreating() {
		headerCreate.removeAllViews();
		for (int i = 0; i < app.getFeedLoadingManager().getCreatingVideos().size(); i++) {
			LinearLayout vid = (LinearLayout) act.getLayoutInflater().inflate(R.layout.row_pending_video_send, null);
			vid.setTag(app.getFeedLoadingManager().getCreatingVideos().get(i).getId());
			String video_name = app.getFeedLoadingManager().getCreatingVideos().get(i).getCaption();
			TextView tvFirst = (TextView) vid.findViewById(R.id.tvFirstRow);
			
			String text = getString(R.string.activity_challenge).replace("_caption_", video_name);
			
			int index_yourself_challenge = text.indexOf("_yourself_");
			text = text.replace("_yourself_",getString(R.string.activity_yourself_word));
			
			SpannableString ss1 = new SpannableString(text);
			
			ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_yourself_challenge, index_yourself_challenge+getString(R.string.activity_yourself_word).length(), 0);
			ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), ss1.length() - video_name.length() - 1, ss1.length() - 1, 0);
			
			tvFirst.setText(ss1);
			tvFirst.setMovementMethod(LinkMovementMethod.getInstance());
			
			RelativeLayout avatarContainer = (RelativeLayout) vid.findViewById(R.id.srlAvatarContainer);
			new AvatarDrawer(app, (ImageView) vid.findViewById(R.id.ivAvatar), Me.getInstance(app).getUser()).draw();
			avatarContainer.setTag(Me.getInstance(app).getId());
			avatarContainer.setOnClickListener(avatarListener);

			((TextView) vid.findViewById(R.id.tvSecondRow)).setText("just now");

			headerCreate.addView(vid);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		updateMyFeed();
		
		onRefresh();

		sortPending();
		sortCreating();

	}

	public void drawPendingVideos(Cursor c) {
		header.removeAllViews();

		while (c.moveToNext()) {
			LinearLayout vid = (LinearLayout) act.getLayoutInflater().inflate(R.layout.row_pending_video_create, null);

			long id = c.getLong(c.getColumnIndex(DBHelper.C_ID));

			vid.setTag(id);

			new AvatarDrawer(app, (ImageView) vid.findViewById(R.id.ivAvatar), Me.getInstance(app).getUser()).draw();

			TextView tvFirst = (TextView) vid.findViewById(R.id.tvFirstRow);
			TextView tvSecond = (TextView) vid.findViewById(R.id.tvSecondRow);
			RelativeLayout avatarContainer = (RelativeLayout) vid.findViewById(R.id.srlAvatarContainer);
			final RelativeLayout pwContainer = (RelativeLayout) vid.findViewById(R.id.progressContainer);

			String video_name = c.getString(c.getColumnIndex(DBHelper.C_TITLE));

			Button bRetry = (Button) vid.findViewById(R.id.bRetry);

			SpannableString ss1 = new SpannableString(getString(R.string.activity_challenge).replace("_caption_", video_name));

			ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, 3, 0);
			ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), ss1.length() - video_name.length() - 1, ss1.length() - 1, 0);
			ss1.setSpan(mPendingVideoClickSpan, ss1.length() - video_name.length() - 1, ss1.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

			tvFirst.setTag(id);

			tvFirst.setText(ss1);
			tvFirst.setMovementMethod(LinkMovementMethod.getInstance());
			tvSecond.setText("batata");

			avatarContainer.setTag(Me.getInstance(app).getId());
			avatarContainer.setOnClickListener(avatarListener);

			bRetry.setTag(id);

			if (app.getFeedLoadingManager().getPendingLoadingIds().contains(id)) {
				bRetry.setVisibility(View.INVISIBLE);
				vid.findViewById(R.id.progressContainer).setVisibility(View.VISIBLE);
			} else {
				bRetry.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						app.getOfflineManager().retryCreate((Long) v.getTag());
					}
				});
			}

			header.addView(vid);
		}
		c.close();

	}

	public void clearHeaderCreateVideos() {
		headerCreate.removeAllViews();
	}

	public void updateMyFeed() {

		sortCreating();
		sortPending();

		if (cursorActivity != null)
			cursorActivity.close();

		cursorActivity = UserActivity.getActivity(app);

		if (cursorActivity.getCount() == 0 && app.getFeedLoadingManager().getCreatingVideos().size() == 0 && app.getDB().getPendingVideos(PendingVideo.CREATE).getCount() == 0) {
			rlNoActivity.setVisibility(View.VISIBLE);
		} else {
			rlNoActivity.setVisibility(View.GONE);

			if (mActivityAdapter == null) {
				mActivityAdapter = new ActivityAdapter(getActivity(), cursorActivity);
				lvMyFeed.setAdapter(mActivityAdapter);
			} else {
				WLog.d(this, "Apenas a mudar o cursor");
				mActivityAdapter.changeCursor(cursorActivity);
				mActivityAdapter.notifyDataSetChanged();
			}

		}

		refreshing = false;
		swipeFeed.setRefreshing(refreshing);

	}

	public void updateProgressDownload(long id, int progress) {
		if (mWheels.get(id) != null) {
			mWheels.get(id).setProgress((int) ((float) progress * 3.6));

			if (progress == Utils.PROGRESS_COMPLETE) {
				mWheels.get(id).setBarColor(getResources().getColor(R.color.weclipse_green));
			}
		}
	}

	public void updateProgressCreate(int id, int progress) {
		for (int i = 0; i < headerCreate.getChildCount(); i++) {
			if (id == (Integer) headerCreate.getChildAt(i).getTag()) {

				final ProgressWheel pw = ((ProgressWheel) headerCreate.getChildAt(i).findViewById(R.id.pw));

				pw.setProgress((int) ((float) progress * 3.6));

				if (progress == Utils.PROGRESS_FILE_UPLOAD) {
					pw.postDelayed(new Runnable() {

						@Override
						public void run() {
							pw.setProgress(pw.getProgress() + Utils.PROGRESS_STEP);

							if (pw.getProgress() >= (int) ((float) Utils.PROGRESS_PROCESS_FULL_OUTPUT * 3.6)) {
								pw.removeCallbacks(this);
							} else {
								pw.postDelayed(this, 125);
							}
						}
					}, 125);
				}

				if (progress == Utils.PROGRESS_COMPLETE) {
					pw.setBarColor(getResources().getColor(R.color.weclipse_green));
				}

			}
			;
		}
	}

	public void updateProgressReply(long id, int progress) {

		final ProgressWheel pw = mWheels.get(id);

		if (pw != null) {

			pw.setProgress((int) ((float) progress * 3.6));

			if (progress == Utils.PROGRESS_FILE_UPLOAD) {
				pw.postDelayed(new Runnable() {

					@Override
					public void run() {
						pw.setProgress(pw.getProgress() + Utils.PROGRESS_STEP);

						if (pw.getProgress() >= (int) ((float) Utils.PROGRESS_PROCESS_FULL_OUTPUT * 3.6)) {
							pw.removeCallbacks(this);
						} else {
							pw.postDelayed(this, 125);
						}
					}
				}, 125);
			}

			if (progress == Utils.PROGRESS_COMPLETE) {
				pw.setBarColor(getResources().getColor(R.color.weclipse_green));
			}

		}
	}

	public void updateProgressCreatePending(long id, int progress) {
		WLog.d(this, "Receiving progress: " + progress);
		for (int i = 0; i < header.getChildCount(); i++) {
			if (id == (long) header.getChildAt(i).getTag()) {

				final ProgressWheel pw = ((ProgressWheel) header.getChildAt(i).findViewById(R.id.pw));

				pw.setProgress((int) ((float) progress * 3.6));

				if (progress == Utils.PROGRESS_FILE_UPLOAD) {
					pw.postDelayed(new Runnable() {

						@Override
						public void run() {
							pw.setProgress(pw.getProgress() + Utils.PROGRESS_STEP);

							if (pw.getProgress() >= (int) ((float) Utils.PROGRESS_PROCESS_FULL_OUTPUT * 3.6)) {
								pw.removeCallbacks(this);
							} else {
								pw.postDelayed(this, 125);
							}
						}
					}, 125);
				}

				if (progress == Utils.PROGRESS_COMPLETE)
					pw.setBarColor(getResources().getColor(R.color.weclipse_green));
			}
		}
	}

	public void notifyListView() {
		notifyMyFeed();
	}

	public void notifyMyFeed() {
		if (mActivityAdapter != null) {
			mActivityAdapter.swapCursor(UserActivity.getActivity(app));
			mActivityAdapter.notifyDataSetChanged();
		}
	}

	public class ActivityAdapter extends CursorAdapter {

		Context context;
		LayoutInflater mInflater;

		public ActivityAdapter(Context context, Cursor c) {
			super(context, c);
			this.context = context;
			this.mInflater = LayoutInflater.from(context);
		}

		@Override
		public void bindView(View view, Context c, Cursor cursor) {

			// WLog.d(this,"Conta colunas: "+cursor.getColumnCount());
			// WLog.d(this,"Conta linhas: "+cursor.getCount());

			long id = cursor.getLong(cursor.getColumnIndex(DBHelper.C_ID));
			int type = cursor.getInt(cursor.getColumnIndex(DBHelper.C_TYPE));
			String updated_at = cursor.getString(cursor.getColumnIndex(DBHelper.C_UPDATED_AT));
			final long from_id = cursor.getLong(cursor.getColumnIndex(DBHelper.C_USER_ID));
			String from_username = cursor.getString(cursor.getColumnIndex(DBHelper.C_USERNAME));
			String from_name = cursor.getString(cursor.getColumnIndex(DBHelper.C_NAME));
			final long video_id = cursor.getLong(cursor.getColumnIndex(DBHelper.C_VIDEO_ID));
			final String video_name = cursor.getString(cursor.getColumnIndex(DBHelper.C_TITLE));
			final long group_id = cursor.getLong(cursor.getColumnIndex(DBHelper.C_GROUP_ID));
			String group_name = cursor.getString(cursor.getColumnIndex(DBHelper.C_GROUP_NAME));
			String thumbnail_url = cursor.getString(cursor.getColumnIndex(DBHelper.C_THUMBNAIL_URL));
			String video_expires_at = cursor.getString(cursor.getColumnIndex(DBHelper.C_EXPIRES_AT));
			String from_avatar_url = cursor.getString(cursor.getColumnIndex(DBHelper.C_AVATAR_URL));
			int private_video = cursor.getInt(cursor.getColumnIndex(DBHelper.C_PRIVATE_VIDEO));
			int priority = cursor.getInt(cursor.getColumnIndex(DBHelper.C_PRIORITY));

			final UserActivity a = new UserActivity(app, id, type, updated_at, from_id, from_username, from_name,from_avatar_url, video_id, video_name, group_id, group_name, thumbnail_url,video_expires_at,private_video,priority);

			User u = null;

			if (type == UserActivity.CHALLENGE || type == UserActivity.CHALLENGE_GROUP) {
				u = Me.getInstance(app).getUser();
			} else {
				u = UserCache.getInstance(app).getUser(from_id);
			}
			
			if((u.getAvatar() == null || !u.getAvatar().equals("") || !u.getAvatar().equals("null")) && !from_avatar_url.equals("")){
				u.setAvatar(from_avatar_url);
			}
			
//			WLog.d(this,"Avatar: "+u.getAvatar());

			ViewHolder holder = (ViewHolder) view.getTag();

			SpannableString ss1;
			String tempText = "";
			
			int index_caption = 0;
			int index_user = 0;

			switch (type) {
			case UserActivity.REPLIED:
			case UserActivity.CHALLENGED:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				tempText = getString(R.string.activity_video_challenged).replace("_fromuser_", from_username).replace("_caption_", video_name);
				
				if(type == UserActivity.REPLIED){
					ss1 = new SpannableString(tempText+" "+getString(R.string.activity_video_challenged_replied));
				} else {
					
					if(private_video == Video.PRIVATE){
						ss1 = new SpannableString(tempText+" "+Utils.getTimeLeft(app,video_expires_at)+" "+getString(R.string.activity_challenged_time_to_reply));
						Drawable d = getResources().getDrawable(R.drawable.activity_lock); 
			            d.setBounds(0, 0, d.getIntrinsicWidth()/2, d.getIntrinsicHeight()/2);
						ImageSpan imageSpan = new ImageSpan(d,ImageSpan.ALIGN_BASELINE); 
						ss1.setSpan(imageSpan,ss1.length()-1,ss1.length(),SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
					} else {
						ss1 = new SpannableString(tempText+" "+Utils.getTimeLeft(app,video_expires_at)+" "+getString(R.string.activity_challenged_time_to_reply));
					}
				}

				// Links
				ss1.setSpan(mUserClickSpan, 0, from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ss1.setSpan(mVideoClickSpan, tempText.length() - video_name.length(), tempText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				//
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, from_username.length(), 0);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), tempText.length() - video_name.length()-1, tempText.length()-1, 0);
				break;
			case UserActivity.CHALLENGED_GROUP:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				tempText = getString(R.string.activity_video_challenged_group).replace("_fromuser_", from_username).replace("_group_",group_name).replace("_caption_",video_name);
				ss1 = new SpannableString(tempText+" "+Utils.getTimeLeft(app,video_expires_at)+" "+getString(R.string.activity_challenged_time_to_reply));
				ss1.setSpan(mUserClickSpan, 0, from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ss1.setSpan(mVideoClickSpan, tempText.length() - video_name.length()-1, tempText.length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, from_username.length(), 0);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), tempText.length() - video_name.length()-1, tempText.length()-1, 0);
				
				if(private_video == Video.PRIVATE){
					Drawable d = getResources().getDrawable(R.drawable.activity_lock); 
		            d.setBounds(0, 0, d.getIntrinsicWidth()/2, d.getIntrinsicHeight()/2);
					ImageSpan imageSpan = new ImageSpan(d,ImageSpan.ALIGN_BASELINE); 
					ss1.setSpan(imageSpan,ss1.length()-1,ss1.length(),SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
				}
				
				break;
			case UserActivity.VIDEO_READY:
				holder.rlThumbnailContainer.setVisibility(View.VISIBLE);
				
				if(from_id == me.getId()){
					tempText = getString(R.string.activity_video_challenge_ready_self);
					index_caption = tempText.indexOf("_caption_");
					tempText = tempText.replace("_caption_", video_name);
					
					ss1 = new SpannableString(tempText);
					
					ss1.setSpan(mVideoClickSpan, index_caption, index_caption+video_name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_caption, index_caption+video_name.length(), 0);
					ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, getString(R.string.activity_yourself_word_possessive).length(), 0);
					
				} else {
					tempText = getString(R.string.activity_video_challenge_ready);
					
					index_caption = tempText.indexOf("_caption_"); 
					index_user = tempText.indexOf("_fromuser_");
					
					if(index_caption < index_user){
						//caption vem primeiro
						tempText = tempText.replace("_caption_",video_name);
						index_user = tempText.indexOf("_fromuser_");
						tempText = tempText.replace("_fromuser_", from_username);
					} else {
						//user vem primeiro
						tempText = tempText.replace("_fromuser_",from_username);
						index_caption = tempText.indexOf("_caption_");
						tempText = tempText.replace("_caption_", video_name);
					}
					
					ss1 = new SpannableString(tempText);
					
					ss1.setSpan(mUserClickSpan, index_user, index_user+from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					ss1.setSpan(mVideoClickSpan, index_caption, index_caption+video_name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_user, index_user+from_username.length(), 0);
					ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_caption, index_caption+video_name.length(), 0);
				}
				break;
			case UserActivity.LIKED:
				holder.rlThumbnailContainer.setVisibility(View.VISIBLE);
				tempText = getString(R.string.activity_like);
				
				index_user = tempText.indexOf("_fromuser_");		
				tempText = tempText.replace("_fromuser_",from_username);

				index_caption = tempText.indexOf("_caption_");
				tempText = tempText.replace("_caption_", video_name);
				
				ss1 = new SpannableString(tempText);
				ss1.setSpan(mUserClickSpan, index_user, index_user+from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ss1.setSpan(mVideoClickSpan, index_caption,index_caption+video_name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_user, index_user+from_username.length(), 0);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_caption,index_caption+video_name.length(), 0);
				break;
			case UserActivity.COMMENTED:
				holder.rlThumbnailContainer.setVisibility(View.VISIBLE);
				tempText = getString(R.string.activity_comment);
				
				index_user = tempText.indexOf("_fromuser_");		
				tempText = tempText.replace("_fromuser_",from_username);
				
				index_caption = tempText.indexOf("_caption_");
				tempText = tempText.replace("_caption_", video_name);
				
				ss1 = new SpannableString(tempText);
				ss1.setSpan(mUserClickSpan, index_user, index_user+from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ss1.setSpan(mVideoClickSpan, index_caption,index_caption+video_name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_user, index_user+from_username.length(), 0);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_caption,index_caption+video_name.length(), 0);
				break;
			case UserActivity.REQUEST_FOLLOW:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				ss1 = new SpannableString(getString(R.string.activity_request_follow).replace("_fromuser_",from_username));
				ss1.setSpan(mUserClickSpan, 0, from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, from_username.length(), 0);
				break;
			case UserActivity.ACCEPT_FOLLOW:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				ss1 = new SpannableString(getString(R.string.activity_request_follow_accept).replace("_fromuser_",from_username));
				ss1.setSpan(mUserClickSpan, 0, from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, from_username.length(), 0);
				break;
			case UserActivity.B_FOLLOWS:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				ss1 = new SpannableString(getString(R.string.activity_follow_you).replace("_fromuser_",from_username));
				ss1.setSpan(mUserClickSpan, 0, from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, from_username.length(), 0);
				break;
			case UserActivity.FRIENDED:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				ss1 = new SpannableString(getString(R.string.activity_friends).replace("_fromuser_",from_username));
				ss1.setSpan(mUserClickSpan, 0, from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, from_username.length(), 0);
				break;
			case UserActivity.GROUP_ADDED:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				ss1 = new SpannableString(getString(R.string.activity_group_add).replace("_fromuser_", from_username).replace("_group_",group_name));
				ss1.setSpan(mUserClickSpan, 0, from_username.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ss1.setSpan(mGroupClickSpan, ss1.length() - group_name.length()-1, ss1.length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), ss1.length() - group_name.length()-1, ss1.length()-1, 0);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, from_username.length(), 0);
				break;
			case UserActivity.CHALLENGE:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				String text_challenge = getString(R.string.activity_challenge).replace("_caption_", video_name);
				
				int index_yourself_challenge = text_challenge.indexOf("_yourself_");
				
				text_challenge = text_challenge.replace("_yourself_", getString(R.string.activity_yourself_word));
				
				ss1 = new SpannableString(text_challenge);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_yourself_challenge, index_yourself_challenge+getString(R.string.activity_yourself_word).length(), 0);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), ss1.length() - video_name.length() - 1, ss1.length() - 1, 0);
				ss1.setSpan(mVideoClickSpan, ss1.length() - video_name.length() - 1, ss1.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				break;
			case UserActivity.CHALLENGE_GROUP:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				String text = getString(R.string.activity_challenge_group);
				text = text.replace("_group_", group_name);
				text = text.replace("_caption_", video_name);
				
				int index_yourself_challenge_group = text.indexOf("_yourself_");
				text = text.replace("_yourself_", getString(R.string.activity_yourself_word));
				
				ss1 = new SpannableString(text);

				ss1.setSpan(mVideoClickSpan, text.length() - video_name.length() - 1, text.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), index_yourself_challenge_group, index_yourself_challenge_group+getString(R.string.activity_yourself_word).length(), 0);
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), ss1.length() - video_name.length() - 1, ss1.length() - 1, 0);
				break;
			case UserActivity.JOINED_WECLIPSE_CONTACT:
				String text_c = getString(R.string.activity_joined_contact).replace("_fromname_", a.getFrom_name()).replace("_fromuser_", a.getFrom_username());
				ss1 = new SpannableString(text_c);

				// 1 pq é p ponto final
				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), text_c.length()-1-a.getFrom_username().length(), text_c.length()-1, 0);
				ss1.setSpan(mUserClickSpan, text_c.length()-1-a.getFrom_username().length(), text_c.length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				break;
			case UserActivity.JOINED_WECLIPSE_FB:
				String text_f = getString(R.string.activity_joined_facebook).replace("_fromname_", a.getFrom_name()).replace("_fromuser_", a.getFrom_username());

				ss1 = new SpannableString(text_f);

				ss1.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), text_f.length()-1-a.getFrom_username().length(), text_f.length()-1, 0);
				ss1.setSpan(mUserClickSpan, text_f.length()-1-a.getFrom_username().length(), text_f.length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				break;
			default:
				holder.rlThumbnailContainer.setVisibility(View.GONE);
				ss1 = new SpannableString("Type: " + type);
				break;
			}
			
			holder.tvTime.setText(Utils.getTimePast(app,updated_at));

			Bundle ids = new Bundle();
			ids.putLong("user_id", u.getId());
			ids.putLong("video_id", video_id);
			ids.putLong("group_id", group_id);
			holder.tvActivityText.setTag(ids);
			holder.tvActivityText.setMovementMethod(LinkMovementMethod.getInstance());
			holder.tvActivityText.setText(ss1);

			// WLog.d(this,"Type: "+type+" - "+video_name+" strlen:"+video_name.length());
			if (type == UserActivity.CHALLENGED || type == UserActivity.CHALLENGED_GROUP) {
				
				if(app.getFeedLoadingManager().getLoadingIds().contains(video_id)){
					holder.rlProgressContainer.setVisibility(View.VISIBLE);
					
					
					if(!mWheels.containsKey(video_id)){
						ProgressWheel pw = (ProgressWheel)holder.rlProgressContainer.getChildAt(0);
						
						pw.setBarColor(getResources().getColor(R.color.weclipse_yellow));
						
						mWheels.put(video_id, pw);
					}
					
					holder.bReply.setVisibility(View.GONE);
				} else {
					mWheels.remove(video_id);
					holder.rlProgressContainer.setVisibility(View.GONE);
					holder.bReply.setVisibility(View.VISIBLE);
					
					if (ReplyPendingCache.getInstance(app).contains(video_id)) {
						holder.bReply.setText(getString(R.string.retry));
						holder.bReply.setBackgroundResource(R.drawable.button_retry_video);
	
						try {
							XmlResourceParser parser = getResources().getXml(R.drawable.button_white_red_text);
							ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
							holder.bReply.setTextColor(colors);
						} catch (Exception e) {
							// handle exceptions
						}
	
						holder.bReply.setOnClickListener(new OnClickListener() {
	
							@Override
							public void onClick(View v) {
								app.getOfflineManager().retry(video_id);
							}
						});
	
					} else {
						holder.bReply.setText(getString(R.string.reply));
						holder.bReply.setBackgroundResource(R.drawable.button_reply_video);
	
						try {
							XmlResourceParser parser = getResources().getXml(R.drawable.button_white_green_text);
							ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
							holder.bReply.setTextColor(colors);
						} catch (Exception e) {
							// handle exceptions
						}
	
						holder.bReply.setOnClickListener(new OnClickListener() {
	
							@Override
							public void onClick(View v) {
								Intent i = new Intent(getActivity(), CameraActivity.class);
								i.putExtra("isReplying", true);
								i.putExtra("caption", video_name);
								i.putExtra("user_id", from_id);
								i.putExtra("video_id", video_id);
								i.putExtra("group_id", group_id);
								i.putExtra("type", group_id == 0 ? WApp.TARGET_FRIENDS : WApp.TARGET_GROUPS);
								getActivity().startActivityForResult(i, TheMainActivity.CODE_REPLY);
							}
						});
					}
				}

			} else {
				holder.bReply.setVisibility(View.GONE);
				holder.bReply.setOnClickListener(null);
			}
			
			/* Follow button */
			if (type == UserActivity.B_FOLLOWS || type == UserActivity.JOINED_WECLIPSE_CONTACT || type == UserActivity.JOINED_WECLIPSE_FB) {
				holder.bAction.setTextOn(getString(R.string.profile_button_follow_pressed));
				holder.bAction.setTextOff(getString(R.string.profile_button_follow));

				holder.rlActionContainer.setVisibility(View.VISIBLE);
				new FollowingButtonDrawer(getActivity(), mStatuses.get(from_id), holder.bAction).draw();

				holder.bAction.setTag(from_id);
				holder.bAction.setOnClickListener(follow);
			} else if (type == UserActivity.REQUEST_FOLLOW) {
				holder.bAction.setTextOn(getString(R.string.activity_button_accept));
				holder.bAction.setTextOff(getString(R.string.activity_button_accept));
				holder.bAction.setText(getString(R.string.activity_button_accept));
				holder.bAction.setBackgroundResource(R.drawable.button_following);

				holder.rlActionContainer.setVisibility(View.VISIBLE);

				holder.bAction.setTag(from_id);
				holder.bAction.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						new AcceptFollowTask(app, (Long) v.getTag());
					}
				});
			} else {
				holder.rlActionContainer.setVisibility(View.GONE);
			}

			/* Clickers and other stuff, but mostly clickers */
			if (type == UserActivity.CHALLENGED || type == UserActivity.CHALLENGED_GROUP || type == UserActivity.VIDEO_READY
					|| type == UserActivity.LIKED || type == UserActivity.COMMENTED || type == UserActivity.REPLIED) {

				holder.rlThumbnailContainer.setTag(video_id);
				holder.rlThumbnailContainer.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent i = new Intent(getActivity(), VideoDetailActivity.class);
						i.putExtra("vid", (Long) v.getTag());
						startActivity(i);
					}
				});
			} else if (type == UserActivity.GROUP_ADDED) {
				holder.rlContainer.setOnClickListener(null);
			} else {
				// Que caso é este?
			}

			/* Images */
			if (thumbnail_url != null && !thumbnail_url.equals("null") && !thumbnail_url.equals("")) {
				Picasso.with(app).load(thumbnail_url).into(holder.ivThumbnail);
			} else {
				holder.ivThumbnail.setImageBitmap(null);
			}

			new AvatarDrawer(app, holder.ivAvatar, u).draw();

			holder.rlAvatarContainer.setTag(u.getId());
			holder.rlAvatarContainer.setOnClickListener(avatarListener);

		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {

			View v = mInflater.inflate(R.layout.row_activity, parent, false);
			ViewHolder holder = new ViewHolder();

			holder.tvActivityText = (TextView) v.findViewById(R.id.tvActivityText);
			holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
			/* Images */
			holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
			holder.ivThumbnail = (ImageView) v.findViewById(R.id.ivThumbnail);
			/* Containers */
			holder.rlAvatarContainer = (RelativeLayout) v.findViewById(R.id.srlAvatarContainer);
			holder.rlThumbnailContainer = (RelativeLayout) v.findViewById(R.id.rlThumbnailContainer);
			holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
			holder.filler = (RelativeLayout) v.findViewById(R.id.filler);
			holder.rlActionContainer = (RelativeLayout) v.findViewById(R.id.rlActionContainer);
			holder.rlProgressContainer = (RelativeLayout) v.findViewById(R.id.progressContainer);
			/* */
			holder.bReply = (Button) v.findViewById(R.id.bReply);
			holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

			v.setTag(holder);
			return v;
		}

		public class ViewHolder {
			TextView tvActivityText;
			TextView tvTime;
			/* Images */
			ImageView ivAvatar;
			ImageView ivThumbnail;
			/* Containers */
			RelativeLayout rlThumbnailContainer;
			RelativeLayout rlContainer;
			RelativeLayout rlAvatarContainer;
			RelativeLayout filler;
			RelativeLayout rlActionContainer;
			RelativeLayout rlProgressContainer;
			/* Buttons */
			Button bReply;
			ToggleButton bAction;
		}

	}

	@Subscribe
	public void onAsyncTaskResult(ActivityUpdateEvent event) {
		swipeFeed.setRefreshing(false);

		if (event.getSuccess()) {
			mStatuses = event.getStatuses();
		}
		
		updateMyFeed();
	}

	@Subscribe
	public void onAsyncTaskResult(ProgressCreateVideoEvent event) {
		updateProgressCreate(event.getId(), event.getProgress());
	}

	@Subscribe
	public void onAsyncTaskResult(ProgressReplyVideoEvent event) {
		updateProgressReply(event.getId(), event.getProgress());
	}

	@Subscribe
	public void onAsyncTaskResult(ProgressPendingCreateVideoEvent event) {
		updateProgressCreatePending(event.getId(), event.getProgress());
	}

	@Subscribe
	public void onAsyncTaskResult(ProgressVideoDownload event) {
		updateProgressDownload(event.getId(), event.getProgress());
	}

	@Subscribe
	public void onAsyncTaskResult(FeedUpdateEvent event) {
		WLog.d(this, "Feed Update Event");
		updateMyFeed();
	}

	@Subscribe
	public void onAsyncTaskResult(VideoInviteEvent event) {
		WLog.d(this, "Video Invite Event");
		updateMyFeed();
	}

	@Subscribe
	public void onAsyncTaskResult(FollowEvent event) {
		WLog.d(this, "A por: " + event.getStatus());
		mStatuses.remove(event.getUser_id());
		mStatuses.put(event.getUser_id(), event.getStatus());
	}

	@Subscribe
	public void onAsyncTaskResult(UnfollowEvent event) {
		mStatuses.remove(event.getUser_id());
		mStatuses.put(event.getUser_id(), User.NOTHING);
	}

	@Subscribe
	public void onAsyncTaskResult(AcceptFollowEvent event) {
		if (event.getSuccess()) {
			new FetchActivityTask(app, SimpleTask.FRESH, 0);
		}
	}

	@Subscribe
	public void onAsyncTaskResult(VideoCreatedEvent event) {
		if (event.getSuccess()) {
			onRefresh();
		} else {
			sortPending();
			sortCreating();
		}

	}

	@Subscribe
	public void onAsyncTaskResult(CreatePendingVideoFinishedEvent event) {
		if (event.getSuccess()) {
			onRefresh();
		} else {
			sortPending();
			sortCreating();
		}
	}

	@Subscribe
	public void onAsyncTaskResult(CreatePendingVideoStartedEvent event) {
		updateMyFeed();
	}

	// This VideoDownloadEvent is also used for uploading (replying)
	@Subscribe
	public void onAsyncTaskResult(VideoDownloadEvent event) {
		updateMyFeed();
	}

	@Subscribe
	public void onAsyncTaskResult(ReplyPendingVideoFinishedEvent event) {
		if (event.getSuccess()) {
			onRefresh();
		} else {
			sortPending();
			sortCreating();
		}
	}

}
