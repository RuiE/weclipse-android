package com.weclipse.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.EntranceActivity;
import com.weclipse.app.managers.TutorialManager;

public class WalkthroughFragment extends BaseFragment {
	
	WApp app;
	EntranceActivity act;
	
	private ViewPager viewPager;
	
	RelativeLayout container;
	
	Button bGetStarted;
	
    public int currentFrag;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		app = (WApp)getActivity().getApplication();
		act = (EntranceActivity)getActivity();
		
		View v = inflater.inflate(R.layout.fragment_walkthrough,container,false);
		
		initVariables(v);
		
		return v;
	}
	
	public void initVariables(View v){
		
		container = (RelativeLayout)v.findViewById(R.id.container);
		
		viewPager = (ViewPager)v.findViewById(R.id.pager);
		viewPager.setAdapter(new TutorialAdapter(act.getSupportFragmentManager()));
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				@Override
				public void onPageSelected(int p) {
					container.getChildAt(currentFrag).setBackgroundResource(R.drawable.circle_tutorial);
					container.getChildAt(p).setBackgroundResource(R.drawable.circle_tutorial_green);
					
					currentFrag = p;
				}


				@Override
				public void onPageScrolled(int arg0, float arg1,int arg2) {
					
				}

				@Override
				public void onPageScrollStateChanged(int p) {

				}
			});
		
		
		bGetStarted = (Button)v.findViewById(R.id.bGetStarted);
		bGetStarted.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				TutorialManager.getInstance(app).markWalkthrough();
				act.changeFragment();
			}
		});
	}
	
	public class TutorialAdapter extends FragmentPagerAdapter {

		public TutorialAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			Fragment tf = new TutorialFragment();
			Bundle b = new Bundle();
			b.putInt("layout", i);
			tf.setArguments(b);
//			Log.d(TAG,"Setting: "+i);
			return tf;
		}

		@Override
		public int getCount() {
			return 5;
		}
	}

	public static class TutorialFragment extends Fragment {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			
			View v = inflater.inflate(R.layout.pre_tutorial, container, false);
			
			
			TextView tvText = (TextView)v.findViewById(R.id.tvText);
			ImageView ivIcon = (ImageView)v.findViewById(R.id.ivIcon);
			ImageView ivImage = (ImageView)v.findViewById(R.id.ivImage);
			String text = "";
			int length = 0;
			switch (getArguments().getInt("layout")) {
			case 0:
				length = getString(R.string.pre_tutorial_1_key).length();
				text = getString(R.string.pre_tutorial_1);
				ivIcon.setBackgroundResource(R.drawable.pre_tutorial_icon_1);
				ivImage.setImageResource(R.drawable.pre_tutorial_1);
				break;
			case 1:
				length = getString(R.string.pre_tutorial_2_key).length();
				text = getString(R.string.pre_tutorial_2);
				ivIcon.setBackgroundResource(R.drawable.pre_tutorial_icon_2);
				ivImage.setImageResource(R.drawable.pre_tutorial_2);
				break;
			case 2:
				length = getString(R.string.pre_tutorial_3_key).length();
				text = getString(R.string.pre_tutorial_3);
				ivIcon.setBackgroundResource(R.drawable.pre_tutorial_icon_3);
				ivImage.setImageResource(R.drawable.pre_tutorial_3);
				break;
			case 3:
				length = getString(R.string.pre_tutorial_4_key).length();
				text = getString(R.string.pre_tutorial_4);
				ivIcon.setBackgroundResource(R.drawable.pre_tutorial_icon_4);
				ivImage.setImageResource(R.drawable.pre_tutorial_4);
				break;
			case 4:
				length = getString(R.string.pre_tutorial_5_key).length();
				text = getString(R.string.pre_tutorial_5);
				ivIcon.setBackgroundResource(R.drawable.pre_tutorial_icon_5);
				ivImage.setImageResource(R.drawable.pre_tutorial_5);
				break;
			}
			SpannableString s = new SpannableString(text);
			s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.weclipse_green)), 0, length, 0);
			tvText.setText(s);
			return v;
		}
	}

}
