package com.weclipse.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.SyncContactsActivity;
import com.weclipse.app.tasks.ValidateSMSTask;
import com.weclipse.app.utils.eventbus.ValidateSMSEvent;

public class SyncContactsCodeFragment extends BaseFragment implements OnClickListener {

	Button bVerify;
	
	EditText etCodeFirst,etCodeSecond,etCodeThird,etCodeFourth;
	String[] code;
	
	InputMethodManager imm;
	
	WApp app;
	SyncContactsActivity act;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		app = (WApp)getActivity().getApplication();
		act = (SyncContactsActivity)getActivity();
		
		View v = inflater.inflate(R.layout.fragment_sync_code_contacts,container,false);
		
		getActivity().setTitle(getString(R.string.find_header_contacts_sms));
		
		initVariables(v);
		
		return v;	
	}
	
	public void initVariables(View v){
		bVerify = (Button)v.findViewById(R.id.bVerify);
		bVerify.setOnClickListener(this);
		
		OnFocusChangeListener focusListener = new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					imm.showSoftInput((EditText)v, InputMethodManager.SHOW_IMPLICIT);
					((EditText)v).setText("");
				}
			}
		};
		
		etCodeFirst = (EditText)v.findViewById(R.id.etCodeFirst);
		etCodeSecond = (EditText)v.findViewById(R.id.etCodeSecond);
		etCodeThird = (EditText)v.findViewById(R.id.etCodeThird);
		etCodeFourth = (EditText)v.findViewById(R.id.etCodeFourth);
		
		etCodeFirst.setOnFocusChangeListener(focusListener);
		etCodeSecond.setOnFocusChangeListener(focusListener);
		etCodeThird.setOnFocusChangeListener(focusListener);
		etCodeFourth.setOnFocusChangeListener(focusListener);
		
		etCodeFirst.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(s.length()==1){
					etCodeSecond.requestFocus();
					code[0]=s.toString();
				}
				validateColorChange(1);
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		
		etCodeSecond.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(s.length()==1){
					etCodeThird.requestFocus();
					code[1]=s.toString();
				}
				validateColorChange(2);
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		
		etCodeThird.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(s.length()==1){
					etCodeFourth.requestFocus();
					code[2]=s.toString();
				}
				validateColorChange(3);
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {	
			}
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		
		etCodeFourth.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(s.length()==1){
					hideSoftKeyboard();
					code[3]=s.toString();
				}
				validateColorChange(4);
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {	
			}
		});
		etCodeFourth.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
//					imm.hideSoftInputFromWindow(etCodeFourth.getApplicationWindowToken(), 0);
					onClick(bVerify);
		            return true;
		        }
				return false;
			}
		});
	
		imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
		code = new String[4];
		for(int i=0;i<4;i++){
			code[i] = "";
		}
		
	}
	
	public boolean validateCode(){
		
		if(etCodeFirst.getText().toString().equals("") || etCodeSecond.getText().toString().equals("") || etCodeThird.getText().toString().equals("") || etCodeFourth.getText().toString().equals("")){			
			Toast.makeText(act, getString(R.string.toast_code_incomplete), Toast.LENGTH_SHORT).show();
			return false;
		}
		
		return true;
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bVerify:
			if(validateCode()){
				String temp = "";
				for(int i = 0 ; i < 4 ; i++)
					temp += code[i];
				
				new ValidateSMSTask(app, temp);
			}
			break;
		}
	}
	
	public void validateColorChange(int position){
		
		switch(position){
		case 1:
			if(etCodeFirst.getText().toString().length() == 1){
				etCodeFirst.setBackgroundResource(R.drawable.circle_edittext_green);
			} else {
				etCodeFirst.setBackgroundResource(R.drawable.circle_edittext_gray);
			}
			return;
		case 2:
			if(etCodeSecond.getText().toString().length() == 1){
				etCodeSecond.setBackgroundResource(R.drawable.circle_edittext_green);		
			} else {
				etCodeSecond.setBackgroundResource(R.drawable.circle_edittext_gray);
			}
			return;
		case 3:
			if(etCodeThird.getText().toString().length() == 1){
				etCodeThird.setBackgroundResource(R.drawable.circle_edittext_green);
			} else {
				etCodeThird.setBackgroundResource(R.drawable.circle_edittext_gray);
			}
			return;
		case 4:
			if(etCodeFourth.getText().toString().length() == 1){
				etCodeFourth.setBackgroundResource(R.drawable.circle_edittext_green);
			} else {
				etCodeFourth.setBackgroundResource(R.drawable.circle_edittext_gray);
			}
			return;
		}
		
		
	}
	
	/**
	 * Hides the soft keyboard
	 */
	public void hideSoftKeyboard() {
	    if(act.getCurrentFocus()!=null) {
	        imm.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), 0);
	    }
	}

	/**
	 * Shows the soft keyboard
	 */
	public void showSoftKeyboard(View view) {
	    view.requestFocus();
	    imm.showSoftInput(view, 0);
	}
	
	@Subscribe
	public void onAsyncTaskResult(ValidateSMSEvent event){
		if(event.getSuccess()){
			getActivity().getSupportFragmentManager().beginTransaction().remove(SyncContactsCodeFragment.this).replace(R.id.fragment, new SyncContactsFriendsFragment()).commit();
			act.updateMenu();
		} else {
			Toast.makeText(act, getString(R.string.toast_validate_code_error), Toast.LENGTH_SHORT).show();
		}
	}

	
}
