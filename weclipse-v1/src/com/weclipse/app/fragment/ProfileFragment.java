package com.weclipse.app.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.DetailActivity;
import com.weclipse.app.activity.TheMainActivity;
import com.weclipse.app.drawer.TimelineVideoDrawer;
import com.weclipse.app.drawer.TimelineVideoDrawer.ViewHolderVideo;
import com.weclipse.app.managers.DownloadManager;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.tasks.DownloadVideoTask;
import com.weclipse.app.tasks.fetch.FetchMyVideosTask;
import com.weclipse.app.utils.CircleTransform;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.EndlessScrollListener;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DeleteVideoEvent;
import com.weclipse.app.utils.eventbus.FetchMyVideosEvent;
import com.weclipse.app.utils.eventbus.InlineVideoStopEvent;
import com.weclipse.app.utils.eventbus.LikedVideoEvent;
import com.weclipse.app.utils.eventbus.PageChangedEvent;
import com.weclipse.app.utils.eventbus.SendAvatarEvent;
import com.weclipse.app.utils.eventbus.UnlikedVideoEvent;
import com.weclipse.app.utils.eventbus.WBus;
import com.weclipse.app.widget.InlineVideoPlayer;

public class ProfileFragment extends BaseFragment implements OnClickListener, OnRefreshListener {

	WApp app;
	TheMainActivity act;

	ListView lvVideos;
	LinearLayout header;

	ImageView ivAvatar;
	TextView tvNFriends, tvNVideos, tvNGroups, tvNFollowers, tvNFollowing;
	TextView tvName;

	VideoAdapter mVideoAdapter;
	RelativeLayout container;

	Cursor mVideosCursor;
	SwipeRefreshLayout mRefresh;

	Me me;

	int position = -1;
	InlineVideoPlayer currentIVP = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		app = (WApp) getActivity().getApplication();
		act = (TheMainActivity) getActivity();

		View v = inflater.inflate(R.layout.fragment_profile, container, false);

		me = Me.getInstance(act);

		initVariables(v);

		return v;
	}

	public void initVariables(View v) {
		header = (LinearLayout) act.getLayoutInflater().inflate(R.layout.header_my_profile, null);

		// find header elements
		tvName = (TextView) header.findViewById(R.id.tvName);
		tvNVideos = (TextView) header.findViewById(R.id.tvNVideos);
		tvNGroups = (TextView) header.findViewById(R.id.tvNGroups);
		tvNFollowers = (TextView) header.findViewById(R.id.tvNFollowers);
		tvNFollowing = (TextView) header.findViewById(R.id.tvNFollowing);
		tvNFriends = (TextView) header.findViewById(R.id.tvNFriends);

		ivAvatar = (ImageView) header.findViewById(R.id.ivAvatar);

		lvVideos = (ListView) v.findViewById(R.id.lvVideos);
		lvVideos.setDividerHeight(0);
		lvVideos.addHeaderView(header);

		lvVideos.setOnScrollListener(new EndlessScrollListener() {

			@Override
			public void onLoadMore(int page, int totalItemsCount) {
				new FetchMyVideosTask(app, FetchMyVideosTask.LOAD_MORE);
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

				if (position != -1 && ((position + 1) < firstVisibleItem || (position + 1) >= (firstVisibleItem + visibleItemCount))) {
					currentIVP.stop();
					currentIVP = null;
					position = -1;
				}
			}
		});

		container = (RelativeLayout) v.findViewById(R.id.container);

		/* Draw the stuff */
		drawNumbers();

		/* Avatar */
		Picasso.with(app).load(me.getAvatar()).transform(new CircleTransform()).into(ivAvatar);

		tvNVideos.setOnClickListener(this);
		tvNFriends.setOnClickListener(this);
		tvNFollowers.setOnClickListener(this);
		tvNFollowing.setOnClickListener(this);
		tvNGroups.setOnClickListener(this);

		mRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swipeLayout);
		mRefresh.setColorSchemeResources(R.color.weclipse_green_light,
				R.color.weclipse_green,
				R.color.weclipse_yellow,
				R.color.weclipse_red);

		mRefresh.setOnRefreshListener(this);
	}

	public void drawNumbers() {
		tvName.setText(me.getName());
		tvNVideos.setText(me.getN_videos() + " " + getString(R.string.profile_label_n_videos));
		tvNFollowers.setText(me.getN_followers() + " " + getString(R.string.profile_label_n_followers));
		tvNGroups.setText(me.getN_groups() + " " + getString(R.string.profile_label_n_groups));
		tvNFollowing.setText(me.getN_following() + " " + getString(R.string.profile_label_n_following));
		tvNFriends.setText(me.getN_friends() + " " + getString(R.string.profile_label_n_friends));
	}

	@Override
	public void onRefresh() {
		new FetchMyVideosTask(app, FetchMyVideosTask.FRESH);
	}

	public void scrollTop() {
		lvVideos.smoothScrollToPosition(0);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvNVideos:
			lvVideos.smoothScrollBy(header.getHeight(), 1000);
			break;
		case R.id.tvNFriends:
			Intent i = new Intent(getActivity(), DetailActivity.class);
			i.putExtra("user_id", me.getId());
			i.putExtra("type", DetailActivity.TYPE_FRIENDS);
			act.startActivity(i);
			break;
		case R.id.tvNFollowers:
			Intent i2 = new Intent(getActivity(), DetailActivity.class);
			i2.putExtra("user_id", me.getId());
			i2.putExtra("type", DetailActivity.TYPE_FOLLOWERS);
			act.startActivity(i2);
			break;
		case R.id.tvNFollowing:
			Intent i3 = new Intent(getActivity(), DetailActivity.class);
			i3.putExtra("user_id", me.getId());
			i3.putExtra("type", DetailActivity.TYPE_FOLLOWING);
			act.startActivity(i3);
			break;
		case R.id.tvNGroups:
			Intent i4 = new Intent(getActivity(), DetailActivity.class);
			i4.putExtra("user_id", me.getId());
			i4.putExtra("type", DetailActivity.TYPE_GROUPS);
			act.startActivity(i4);
			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		new FetchMyVideosTask(app, FetchMyVideosTask.FRESH);

		Picasso.with(app).load(me.getAvatar()).transform(new CircleTransform()).into(ivAvatar);
		
		updateListView();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (currentIVP != null) {
			currentIVP.stop();
			currentIVP = null;
			position = -1;
		}
	}

	public void updateListView() {

		if (mVideosCursor != null)
			mVideosCursor.close();

		mVideosCursor = TimelineVideo.getMyVideos(app);
		// WLog.d(this, "Há "+mVideosCursor.getCount()+" videos na db");

		if (mVideoAdapter == null) {
			mVideoAdapter = new VideoAdapter(act, mVideosCursor);
			lvVideos.setAdapter(mVideoAdapter);
		} else {
			mVideoAdapter.swapCursor(mVideosCursor);
			mVideoAdapter.notifyDataSetChanged();
		}

	}

	class VideoAdapter extends CursorAdapter {

		LayoutInflater mInflater;
		Context context;

		public VideoAdapter(Context context, Cursor c) {
			super(context, c);
			this.context = context;
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public void bindView(View view, Context context, Cursor c) {
			final long vid = c.getLong(c.getColumnIndex(DBHelper.C_ID));

			final int cursor_position = c.getPosition();

			ViewHolderVideo holder = (ViewHolderVideo) view.getTag();

			String title = c.getString(c.getColumnIndex(DBHelper.C_TITLE));
			long owner_id = c.getLong(c.getColumnIndex(DBHelper.C_USER_ID));
			long group_id = c.getLong(c.getColumnIndex(DBHelper.C_GROUP_ID));

			int video_status = c.getInt(c.getColumnIndex(DBHelper.C_VIDEO_STATUS));

			String updated_at = c.getString(c.getColumnIndex(DBHelper.C_UPDATED_AT));
			String created_at = c.getString(c.getColumnIndex(DBHelper.C_CREATED_AT));
			String ready_at = c.getString(c.getColumnIndex(DBHelper.C_READY_AT));

			int n_likes = c.getInt(c.getColumnIndex(DBHelper.C_N_LIKES));
			int n_views = c.getInt(c.getColumnIndex(DBHelper.C_N_VIEWS));
			int n_comments = c.getInt(c.getColumnIndex(DBHelper.C_N_COMMENTS));

			int private_video = c.getInt(c.getColumnIndex(DBHelper.C_PRIVATE_VIDEO));
			int editors_choice = c.getInt(c.getColumnIndex(DBHelper.C_EDITORS_CHOICE));
			long reweclipsed_id = c.getLong(c.getColumnIndex(DBHelper.C_REWECLIPSED_BY));

			String[] user_ids = c.getString(c.getColumnIndex(DBHelper.C_USERS)).split(",");
			String[] replieds = c.getString(c.getColumnIndex(DBHelper.C_REPLIED_AT)).split(",");

			String thumbnail_url = c.getString(c.getColumnIndex(DBHelper.C_THUMBNAIL_URL));
			String share_url = c.getString(c.getColumnIndex(DBHelper.C_SHARE_URL));

			ArrayList<Long> users = new ArrayList<Long>();
			ArrayList<String> replied_ats = new ArrayList<String>();

			for (int i = 0; i < user_ids.length; i++) {
				users.add(Long.valueOf(user_ids[i]));
				replied_ats.add(replieds[i]);
			}

			final TimelineVideo video = new TimelineVideo(context, vid, title,
					owner_id, group_id, video_status, ready_at, updated_at,
					created_at, users, replied_ats, n_likes, n_views,
					n_comments, private_video, editors_choice, thumbnail_url, share_url,reweclipsed_id);

			TimelineVideoDrawer drawer = new TimelineVideoDrawer(getActivity(), app, video, holder, AbstractVideo.SOURCE_PROFILE);
			drawer.liked = c.getString(c.getColumnIndex("like")) != null;
			drawer.draw();

			if (WApp.INLINE) {

				if (holder.rlContainer.getChildAt(holder.rlContainer.getChildCount() - 1) instanceof InlineVideoPlayer) {
					holder.rlContainer.removeViewAt(holder.rlContainer.getChildCount() - 1);
				}

				holder.rlContainer.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (ProfileFragment.this.position != cursor_position) {

							if (!DownloadManager.getInstance(app).containsKey(video.getId())) {
								DownloadVideoTask mTask = new DownloadVideoTask(app, video);
								DownloadManager.getInstance(app).put(video.getId(), mTask);
							}

							if (ProfileFragment.this.position != -1) {
								currentIVP.stop();
								currentIVP = null;
								position = -1;
							}

							ProfileFragment.this.position = cursor_position;

							InlineVideoPlayer ivp = new InlineVideoPlayer(video, app, (RelativeLayout) v);
							ivp.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									WLog.d(this, "Clickei no IVP lolol");
								}
							});
							currentIVP = ivp;
							WBus.getInstance().register(ivp);

							RelativeLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
							ivp.setLayoutParams(params);

							((RelativeLayout) v).addView(ivp);
							((RelativeLayout) v).findViewById(R.id.videoProgress).setVisibility(View.VISIBLE);
						} else {
							WLog.d(this, "Already doing the stuff");
						}
					}
				});

			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			return TimelineVideoDrawer.newView(mInflater.inflate(R.layout.row_timeline, parent, false));
		}

	}

	@Subscribe
	public void onAsyncTaskResult(FetchMyVideosEvent event) {
		mRefresh.setRefreshing(false);
		if (event.getSuccess()) {
			if (mVideoAdapter != null) {
				mVideoAdapter.swapCursor(TimelineVideo.getMyVideos(app));
				mVideoAdapter.notifyDataSetChanged();
			} else {
				updateListView();
			}
			drawNumbers();
		} else {
			// TODO Falhou, reload? show smth
		}

	}

	@Subscribe
	public void onAsyncTaskResult(LikedVideoEvent event) {
		mVideosCursor = TimelineVideo.getMyVideos(app);
		mVideoAdapter.swapCursor(mVideosCursor);
		mVideoAdapter.notifyDataSetChanged();
	}

	@Subscribe
	public void onAsyncTaskResult(UnlikedVideoEvent event) {
		mVideosCursor = TimelineVideo.getMyVideos(app);
		mVideoAdapter.swapCursor(mVideosCursor);
		mVideoAdapter.notifyDataSetChanged();
	}

	@Subscribe
	public void onAsyncTaskResult(InlineVideoStopEvent event) {
		position = -1;
		currentIVP = null;

		if (event.getType() == InlineVideoStopEvent.CORRUPTED) {
			Toast.makeText(act, "Corrupted file. Sorry for the incovenience", Toast.LENGTH_SHORT).show();
		}

	}

	@Subscribe
	public void onAsyncTaskResult(PageChangedEvent event) {
		position = -1;
		if (currentIVP != null) {
			currentIVP.stop();
			currentIVP = null;
		}
	}

	@Subscribe
	public void onAsyncTaskResult(DeleteVideoEvent event) {
		if (event.getSuccess()) {
			updateListView();
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(SendAvatarEvent event){
		if(event.getSuccess()){
			
			Me mMe = Me.getInstance(app);
			
			Picasso.with(app)
				.load(mMe.getAvatar())
				.memoryPolicy(MemoryPolicy.NO_CACHE)
				.networkPolicy(NetworkPolicy.NO_CACHE)
				.transform(new CircleTransform())
				.into(ivAvatar);
			
		}
	}

}
