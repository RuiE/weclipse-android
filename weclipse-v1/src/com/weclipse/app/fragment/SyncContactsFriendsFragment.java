package com.weclipse.app.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.ProfileActivity;
import com.weclipse.app.activity.SyncContactsActivity;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.FollowingButtonDrawer;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.FollowTask;
import com.weclipse.app.tasks.SyncContactsTask;
import com.weclipse.app.tasks.UnfollowTask;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FollowEvent;
import com.weclipse.app.utils.eventbus.SyncContactsEvent;
import com.weclipse.app.utils.eventbus.UnfollowEvent;
import com.weclipse.app.utils.eventbus.UnmergeContactsEvent;

public class SyncContactsFriendsFragment extends BaseFragment {

	WApp app;
	SyncContactsActivity act;
	
	ListView lvContacts;
	ArrayList<User> mUsers;
	ContactsAdapter mAdapter;
	RelativeLayout rlNoUsers;
	
	OnClickListener openProfile = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent i = new Intent(getActivity(), ProfileActivity.class);
			i.putExtra("user_id", (Long) v.getTag());
			startActivity(i);
		}
	};

	OnClickListener follow = new OnClickListener() {

		@Override
		public void onClick(View v) {
			int position = (Integer) v.getTag();
			WLog.d(this, "Clicked follow!");
			switch (mUsers.get(position).getFollow_status()) {
			case User.CLEAN_FOLLOW:
			case User.PENDING_ACCEPT:
				new UnfollowTask(app, mUsers.get(position).getId());
				v.setBackgroundResource(R.drawable.button_following);
				break;
			case User.NOTHING:
				new FollowTask(app, mUsers.get(position).getId());
				if(mUsers.get(position).isPrivate()){
					((Button)v).setText(getString(R.string.profile_button_follow_pending));
					v.setBackgroundResource(R.drawable.button_following_pending);
				} else {
					v.setBackgroundResource(R.drawable.button_following);
				}
				break;
			}
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		app = (WApp)getActivity().getApplication();
		act = (SyncContactsActivity)getActivity();
		
		View v = inflater.inflate(R.layout.fragment_sync_friends_contacts,container,false);
		
		getActivity().setTitle(getString(R.string.find_header_contacts));
		
		initVariables(v);
		
		return v;
		
	}
	
	public void initVariables(View v){
		lvContacts = (ListView)v.findViewById(R.id.lvContacts);
		
		rlNoUsers = (RelativeLayout)v.findViewById(R.id.rlNoContacts);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		new SyncContactsTask(app);
	}
	
	public void updateListView(){
			
		if(mUsers.size() == 0){
			rlNoUsers.setVisibility(View.VISIBLE);
			lvContacts.setVisibility(View.GONE);
			return;
		} else {
			lvContacts.setVisibility(View.VISIBLE);
			rlNoUsers.setVisibility(View.GONE);
		}
		
		mAdapter = new ContactsAdapter(getActivity(), mUsers);
		lvContacts.setAdapter(mAdapter);
	}
	
	private class ContactsAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<User> values;

		public ContactsAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.row_list_following, values);
			mInflater = LayoutInflater.from(context);
			this.values = values;
		}

		public void swapData() {
			this.values = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_following, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			User user = values.get(position);

			new AvatarDrawer(app,holder.ivAvatar, user).draw();
			holder.rlContainer.setTag(user.getId());
			holder.rlContainer.setOnClickListener(openProfile);

			holder.tvName.setText(user.getUsername());
			holder.tvTime.setText(user.getName());

			if (user.getId() == Me.getInstance(getActivity()).getId()) {
				holder.bAction.setVisibility(View.INVISIBLE);
			} else {
				holder.bAction.setVisibility(View.VISIBLE);

				new FollowingButtonDrawer(getActivity(),user.getFollow_status(), holder.bAction).draw();

				holder.bAction.setTag(position);
				holder.bAction.setOnClickListener(follow);
			}

			return v;
		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlContainer;
			ToggleButton bAction;
		}

	}

	@Subscribe
	public void onAsyncTaskResult(SyncContactsEvent event){
		if(event.getSuccess()){
			mUsers = event.getUsers();
			updateListView();
		} else {
			
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(FollowEvent event) {

		for (int i = 0; i < mUsers.size(); i++) {
			if (mUsers.get(i).getId() == event.getUser_id()) {
				mUsers.get(i).setFollow_status(event.getStatus());
				break;
			}
		}

		mAdapter.swapData();
		mAdapter.notifyDataSetChanged();

	}

	@Subscribe
	public void onAsyncTaskResult(UnfollowEvent event) {
		for (int i = 0; i < mUsers.size(); i++) {
			if (mUsers.get(i).getId() == event.getUser_id()) {
				mUsers.get(i).setFollow_status(User.NOTHING);
			}
		}

		mAdapter.swapData();
		mAdapter.notifyDataSetChanged();
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnmergeContactsEvent event){
		if(event.getSuccess()){
			getActivity().getSupportFragmentManager().beginTransaction().remove(SyncContactsFriendsFragment.this).replace(R.id.fragment, new SyncContactsFragment()).commit();
			act.updateMenu();
		} else {
			
		}
	}
	
}
