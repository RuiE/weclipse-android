package com.weclipse.app.fragment;

import com.weclipse.app.utils.eventbus.WBus;

import android.support.v4.app.Fragment;


public abstract class BaseFragment extends Fragment {
	
	
	@Override
	public void onResume() {
		super.onResume();
		WBus.getInstance().register(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		WBus.getInstance().unregister(this);
	}

}
