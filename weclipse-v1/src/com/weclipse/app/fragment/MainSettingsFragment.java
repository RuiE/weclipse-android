package com.weclipse.app.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.EditProfileActivity;
import com.weclipse.app.activity.SettingsActivity;
import com.weclipse.app.activity.SyncContactsActivity;
import com.weclipse.app.activity.SyncFacebookActivity;
import com.weclipse.app.tasks.DeleteAccountTask;
import com.weclipse.app.tasks.UpdateMeTask;

public class MainSettingsFragment extends Fragment implements OnClickListener {

	private static final String TAG = "MainSettings Screen";
	SettingsActivity act;
	WApp app;
	ScrollView container;
	TextView tvMyAccount, tvAdvanced, tvInformation;
	boolean deleting;
	int position;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		act = (SettingsActivity) getActivity();
		app = (WApp) act.getApplication();
		View v = inflater.inflate(R.layout.fragment_settings_main, container,false);
		initVariables(v);
		return v;
	}

	private View initVariables(View v) {

		container = (ScrollView) v.findViewById(R.id.svContainer);

		((TextView) v.findViewById(R.id.tvVersion)).setText("Version " + WApp.APP_VERSION + " - 2015 Weclipse" + (WApp.DEBUG ? ".dev" : ""));
		
		v.findViewById(R.id.bFacebook).setOnClickListener(this);
		v.findViewById(R.id.bContacts).setOnClickListener(this);
		v.findViewById(R.id.bInvite).setOnClickListener(this);
		
		v.findViewById(R.id.bEditProfile).setOnClickListener(this);
		v.findViewById(R.id.bNotification).setOnClickListener(this);
		v.findViewById(R.id.bChangePassword).setOnClickListener(this);
		
		v.findViewById(R.id.bTOS).setOnClickListener(this);
		v.findViewById(R.id.bPrivacy).setOnClickListener(this);
		v.findViewById(R.id.bBlog).setOnClickListener(this);
		v.findViewById(R.id.bFeedback).setOnClickListener(this);
		
		v.findViewById(R.id.bLogout).setOnClickListener(this);
		v.findViewById(R.id.bDeleteAccount).setOnClickListener(this);
		
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		container.post(new Runnable() {
			public void run() {
				container.scrollTo(0, position);
			}
		});

	}

	@Override
	public void onClick(View v) {
		position = container.getScrollY();
		switch (v.getId()) {
		case R.id.bFacebook:
			startActivity(new Intent(act, SyncFacebookActivity.class));
			break;
		case R.id.bContacts:
			startActivity(new Intent(act, SyncContactsActivity.class));
			break;
		case R.id.bInvite:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.setType("text/plain");
			sendIntent.putExtra(Intent.EXTRA_TEXT,getString(R.string.sms_invite));
			startActivity(Intent.createChooser(sendIntent, null));
			break;
		case R.id.bLogout:
			deleting = false;
			new ConfirmDialogFragment().show(act.getSupportFragmentManager(),
					"confirm_logout");
			break;
		case R.id.bEditProfile:
			startActivity(new Intent(act,EditProfileActivity.class));
			break;
		case R.id.bNotification:
			act.mStackManager.swapFragment(SettingsActivity.NOTIFICATION_TAG);
			act.swapHeader(getString(R.string.header_settings_notification));
			break;
		case R.id.bDeleteAccount:
			deleting = true;
			new ConfirmDialogFragment().show(act.getSupportFragmentManager(),"confirm_delete_account");
			break;
		case R.id.bChangePassword:
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = act.getLayoutInflater();
			final LinearLayout mView = (LinearLayout) inflater.inflate(R.layout.dialog_change_password, null);
			builder.setView(mView)
					.setTitle(R.string.dialog_change_password_title)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,int id) {
									new UpdateMeTask(app, SettingsActivity.PASSWORD, ((EditText)mView.findViewById(R.id.password)).getText().toString());
									
									dialog.dismiss();
								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									dialog.cancel();
								}
							});
			builder.create().show();
			break;
		case R.id.bFeedback:
			Bundle b1 = new Bundle();
			b1.putString("url", WApp.URL_FEEDBACK);
			act.mStackManager.swapFragment(SettingsActivity.WEBVIEW_TAG, b1);
			act.swapHeader(getString(R.string.header_feedback));
			break;
		case R.id.bPrivacy:
			Bundle b2 = new Bundle();
			b2.putString("url", WApp.URL_PRIVACY);
			act.mStackManager.swapFragment(SettingsActivity.WEBVIEW_TAG, b2);
			act.swapHeader(getString(R.string.header_privacy_policy));
			break;
		case R.id.bTOS:
			Bundle b3 = new Bundle();
			b3.putString("url", WApp.URL_TOS);
			act.mStackManager.swapFragment(SettingsActivity.WEBVIEW_TAG, b3);
			act.swapHeader(getString(R.string.header_terms_of_service));
			break;
		case R.id.bBlog:
			Bundle b4 = new Bundle();
			b4.putString("url", WApp.URL_BLOG);
			act.mStackManager.swapFragment(SettingsActivity.WEBVIEW_TAG, b4);
			act.swapHeader(getString(R.string.header_blog));
			break;
		}
	}

	@SuppressLint("ValidFragment")
	public class ConfirmDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			if (deleting) {
				builder.setTitle(R.string.delete_account);
			} else {
				builder.setTitle(R.string.logout);
			}
			
			builder.setMessage(R.string.are_you_sure)
					.setPositiveButton(R.string.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									if (deleting) {
										new DeleteAccountTask(app);
									} else {
										app.logout(true);
										act.logout();
									}
									deleting = false;
								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// do nothing
								}
							});
			return builder.create();
		}
	}

}
