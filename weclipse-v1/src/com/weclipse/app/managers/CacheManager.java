package com.weclipse.app.managers;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import android.os.Environment;

import com.weclipse.app.WApp;
import com.weclipse.app.utils.WLog;

public class CacheManager {

	WApp app;
	static CacheManager mCacheManager;
	public static final int MAX_CACHE_SIZE = 1024*1024*75; // 75MB
	public static final String CACHE_DIR = "cache/";
	
	private CacheManager(WApp app){
		this.app = app;
	}
	
	public static CacheManager getInstance(WApp app){
		if(mCacheManager == null)
			mCacheManager = new CacheManager(app);
		
		return mCacheManager;
	}
	
	public static void checkSpace(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				File mCacheDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + WApp.BASE_DIR + CACHE_DIR);
//				WLog.d(CacheManager.class, "cache size atm: "+getFolderSize(mCacheDir)+" bytes ----: "+((getFolderSize(mCacheDir)/1024/1024))+"MB");
				WLog.d(this,"Running...");
				if(getFolderSize(mCacheDir) >= MAX_CACHE_SIZE){
					File[] fichas = mCacheDir.listFiles();
					int total = fichas.length;
					
					Arrays.sort(fichas, new Comparator<File>() {
						
						public int compare(File f1,File f2){
							return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());							
						}
						
					});
					
					int i = 0 ;
					for(File f : fichas){
						if(f.getPath().endsWith(".zip")){
							if(i < (total / 2)){
								f.delete();
							}
						}
						i++;
					}
					//
				} else {
					WLog.d(CacheManager.class, "NO NEED to free space");
				}
			}
		}).run();
		
	}
	
	public static long getFolderSize(File dir) {
		long size = 0;
		for (File file : dir.listFiles()) {
		    if (file.isFile()) {
		        // System.out.println(file.getName() + " " + file.length());
		        size += file.length();
		    } else if(file.isDirectory()) {
		        size += getFolderSize(file);
		    } else {
		    	WLog.d(CacheManager.class,"Wtf is this?");
		    }
		}
		
		return size;
	}
	
}
