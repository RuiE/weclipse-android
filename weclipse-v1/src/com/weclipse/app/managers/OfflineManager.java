package com.weclipse.app.managers;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import android.database.Cursor;
import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.models.OfflineAction;
import com.weclipse.app.models.PendingVideo;
import com.weclipse.app.tasks.CreatePendingVideoTask;
import com.weclipse.app.tasks.ReplyPendingVideoTask;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;

public class OfflineManager {
	
	private static final String TAG = "OfflineManager";
	
	WApp app;
	boolean working;
	ArrayList<Long> workingIds;
	
	public OfflineManager(WApp app){
		this.app=app;
		working = false;
		workingIds = new ArrayList<Long>();
	}
	
	public void addVideo(PendingVideo pv){
		pv.create();
		app.saveOfflineVideo();
	}
	
	public void addAction(OfflineAction oa){
		oa.create();
		app.saveOfflineActions();
	}
	
	public void setWorking(boolean working){
		this.working=working;
	}
	
	public void sendOfflineActions(){
		Cursor c = app.getDB().getOfflineActions();
		if(c.moveToFirst()){
			int id;
			OfflineAction oa;
			do {
				id = c.getInt(c.getColumnIndex(DBHelper.C_ID));
				oa = new OfflineAction(app,id);
				oa.load();
				switch(oa.getType()){
				case OfflineAction.VIDEO_VIEWED:
					new MarkViewed(oa).execute();
					break;
				case OfflineAction.CLEAR_VIDEO:
					new ClearVideo(oa).execute();
					break;
				}
			} while(c.moveToNext());
		}
	}
	
	public void sendOfflineVideo(){
		Cursor c = app.getDB().getPendingVideos();
		if(c.moveToFirst()){
			long id;
			PendingVideo pv;
			do{
				id = c.getLong(c.getColumnIndex(DBHelper.C_ID));
				pv = new PendingVideo(app,id);
				try {
					pv.load();
					if(pv.getType()==PendingVideo.REPLY){
						if(!hasWorkingId(pv.getVideoId())){
							addWorkingId(pv.getVideoId());
							new ReplyPendingVideoTask(app, pv);
						}
					} else {
						if(!hasWorkingId(id)){
							new CreatePendingVideoTask(app, pv);
						}
						
					}
				} catch(Exception e){
					if(WApp.DEBUG)
						e.printStackTrace();
				}
			} while(c.moveToNext());
		}
		c.close();
	}
	
	public void retry(long vid){
		if(!working){
			Cursor c = app.getDB().getPendingVideo(vid);
			if(c.moveToFirst()){
				long id = c.getLong(c.getColumnIndex(DBHelper.C_ID));
				PendingVideo pv = new PendingVideo(app,id);
				pv.load();
				new ReplyPendingVideoTask(app, pv);
			}
		} else {
			//Let it finish the work he is doing
		}
	}
	
	public void retryCreate(long id){
		if(!hasWorkingId(id)){
			PendingVideo pv = new PendingVideo(app,id);
			pv.load();
			new CreatePendingVideoTask(app, pv);
		}
	}
	
	public void clearActions(){
		if(app.getDB().getOfflineActions().getCount()==0){
			app.clearOfflineActions();
		}
	}
	
	public void clearVideos(){
		if(app.getDB().getPendingVideos().getCount()==0){
			app.clearOfflineVideos();
		}
	}
	
	public void addWorkingId(long id){
		workingIds.add(id);
	}
	
	public void removeWorkingId(long id){
		boolean res = workingIds.remove(id);
	}
	
	public boolean hasWorkingId(long id){
		return workingIds.contains(id);
	}
	
	public class ClearVideo extends AsyncTask<String, String, String> {
	
		OfflineAction oa;
		
		public ClearVideo(OfflineAction oa){
			this.oa = oa;
		}

		@Override
		protected String doInBackground(String... params) {
			MyHttpClient httpclient = new MyHttpClient(app);
			String endpoint = WApp.BASE+WApp.CLEAR_VIDEO;
			endpoint = endpoint.replace("__vid__",""+oa.getProvidedId());
			endpoint += "?android_version="+WApp.APP_VERSION;
			int code = 0;
//			Log.d(TAG,"Ednpoint: "+endpoint);
	        HttpGet get = new HttpGet(endpoint);
	        get.setHeader("Authorization", "access_token="+app.getAccessToken());
	        try {
	            HttpResponse response = httpclient.execute(get);
	            code = response.getStatusLine().getStatusCode();
	        } catch (ClientProtocolException e) {
	        	if(WApp.DEBUG)
	        		e.printStackTrace();
	        } catch (IOException e) {
	        	if(WApp.DEBUG)
	        		e.printStackTrace();
	        }
	        
//	        WLog.d(TAG,"Code/Result From Clear Video: "+code);
	        
	        if(code==HTTPStatus.OK || code == HTTPStatus.BAD_REQUEST || code == HTTPStatus.NOT_FOUND){
	        	oa.destroy();
	        }
	        
			return null;
		}
		
	}
	
	public class MarkViewed extends AsyncTask<String,String,String> {

		OfflineAction oa;
		
		public MarkViewed(OfflineAction oa){
			this.oa=oa;
		}
		
		@Override
		protected String doInBackground(String... params) {
			//Marking as Viewed
			MyHttpClient httpclient = new MyHttpClient(app);
            String postEndoint = WApp.BASE+WApp.VIDEO_VIEWED;
            postEndoint = postEndoint.replace("__vid__",""+oa.getProvidedId());
            HttpPost post = new HttpPost(postEndoint);
            int code=0;
			HttpResponse response;
			try {
				MultipartEntity postEntity = new MultipartEntity();
				postEntity.addPart("android_version",new StringBody(WApp.APP_VERSION));
				post.setEntity(postEntity);
				post.setHeader("Authorization", "access_token="+app.getAccessToken());
				response = httpclient.execute(post);
				code = response.getStatusLine().getStatusCode();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if(code==HTTPStatus.OK){
				oa.destroy();
			}
			
			clearActions();
			
			return null;
		}
		
	}

}
