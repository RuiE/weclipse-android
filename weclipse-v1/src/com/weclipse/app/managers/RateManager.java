package com.weclipse.app.managers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

import com.weclipse.app.R;
import com.weclipse.app.utils.WLog;

public class RateManager {
	
	private static final String TAG = RateManager.class.getSimpleName();
	
	private static final String PREF_NAME = "RateThisApp";
	private static final String KEY_INSTALL_DATE = "rta_install_date";
//	private static final String KEY_LAUNCH_TIMES = "rta_launch_times";
	private static final String KEY_OPT_OUT = "rta_opt_out";
	
	private static long mInstallDate = 0;
	
//	private static int mLaunchTimes = 0;
	private static boolean mOptOut = false;
	
	/**
	 * Days after installation until showing rate dialog
	 */
	public static final int INSTALL_DAYS = 3;
	/**
	 * App launching times until showing rate dialog
	 */
//	public static final int LAUNCH_TIMES = 10;
	
	/**
	 * Call this API when the launcher activity is launched.<br>
	 * It is better to call this API in onStart() of the launcher activity.
	 */
	public static void onStart(Context context) {
		SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		// If it is the first launch, save the date in shared preference.
		if (pref.getLong(KEY_INSTALL_DATE, 0) == 0L) {
			editor.putLong(KEY_INSTALL_DATE, System.currentTimeMillis());
			WLog.d(RateManager.class,"First install: " + System.currentTimeMillis());
		}
		// Increment launch times
		
//		int launchTimes = pref.getInt(KEY_LAUNCH_TIMES, 0);
//		launchTimes++;
//		editor.putInt(KEY_LAUNCH_TIMES, launchTimes);
//		WLog.d(RateManager.class,"Launch times; " + launchTimes);
		
		editor.commit();
		
		mInstallDate = pref.getLong(KEY_INSTALL_DATE, 0);
//		mLaunchTimes = pref.getInt(KEY_LAUNCH_TIMES, 0);
		mOptOut = pref.getBoolean(KEY_OPT_OUT, false);
		
//		if(WApp.DEBUG)
//			printStatus(context);
	}
	
	/**
	 * Show the rate dialog if the criteria is satisfied
	 * @param context
	 */
	public static void showRateDialogIfNeeded(final Context context) {
		if (shouldShowRateDialog()) {
			showRateDialog(context);
		}
	}
	
	public static void resetRate(Context context){
		setOptOut(context, false);
		Editor editor = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit();
		editor.putLong(KEY_INSTALL_DATE, System.currentTimeMillis()-5*24*60*60*1000L);
		editor.commit();
	}
	
	/**
	 * Check whether the rate dialog shoule be shown or not
	 * @return
	 */
	private static boolean shouldShowRateDialog() {
//		WLog.d(RateManager.class,"Showing if needed: Optout? "+mOptOut);
		if (mOptOut) {
			
			return false;
		} else {
//			if (mLaunchTimes >= LAUNCH_TIMES) {
//				return true;
//			}
			long threshold = INSTALL_DAYS * 24 * 60 * 60 * 1000L;	// msec
//			WLog.d(RateManager.class, ">= threshold"+(System.currentTimeMillis() - mInstallDate >= threshold));
			if (System.currentTimeMillis() - mInstallDate >= threshold) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * Show the rate dialog
	 * @param context
	 */
	public static void showRateDialog(final Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(R.string.rta_dialog_title);
		builder.setMessage(R.string.rta_dialog_message);
		builder.setPositiveButton(R.string.rta_dialog_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.weclipse.app"));
				context.startActivity(intent);
				setOptOut(context, true);
			}
		});
		builder.setNeutralButton(R.string.rta_dialog_cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				clearSharedPreferences(context);
			}
		});
		builder.setNegativeButton(R.string.rta_dialog_no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				setOptOut(context, true);
			}
		});
		builder.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				clearSharedPreferences(context);
			}
		});
		builder.create().show();
	}
	
	public static void optIn(Context context){
		context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit().putBoolean(KEY_OPT_OUT,false).commit();
	}
	
	/**
	 * Clear data in shared preferences.<br>
	 * This API is called when the rate dialog is approved or canceled.
	 * @param context
	 */
	private static void clearSharedPreferences(Context context) {
		SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.remove(KEY_INSTALL_DATE);
//		editor.remove(KEY_LAUNCH_TIMES);
		editor.commit();
	}
	
	/**
	 * Set opt out flag. If it is true, the rate dialog will never shown unless app data is cleared.
	 * @param context
	 * @param optOut
	 */
	private static void setOptOut(final Context context, boolean optOut) {
		WLog.d(RateManager.class,"Opting out: "+optOut);
		SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putBoolean(KEY_OPT_OUT, optOut);
		editor.commit();
	}
	
	/**
	 * Print values in SharedPreferences (used for debug)
	 * @param context
	 */
	public static void printStatus(final Context context) {
		SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		WLog.d(RateManager.class,"*** RateThisApp Status ***");
		WLog.d(RateManager.class,"Install Date: " + pref.getLong(KEY_INSTALL_DATE, 0));
//		WLog.d(RateManager.class,"Launch Times: " + pref.getInt(KEY_LAUNCH_TIMES, 0));
		WLog.d(RateManager.class,"Opt out: " + pref.getBoolean(KEY_OPT_OUT, false));
	}

}
