package com.weclipse.app.managers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;
import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.activity.BaseActivity;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.eventbus.FeedUpdateEvent;
import com.weclipse.app.utils.eventbus.PendingVideosCleanEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class ResourceManager {
	
	private static final String TAG ="ResourceManager";
	WApp app;
	boolean working;
	
	public ResourceManager(WApp app){
		this.app = app;
	}
	
	public boolean isWorking(){
		return working;
	}
	
	public void cleanComments(){
		new AsyncTask<String, String, String>(){
			@Override
			protected String doInBackground(String... params) {
//				Log.d(TAG,"Cleaning comments("+app.getDB().getComments().getCount()+")...");
				ArrayList<Long> vids;
				Cursor c = app.getDB().getVideosId();
				if(c.moveToFirst()){
					vids = new ArrayList<Long>();
					do{
						vids.add(c.getLong(c.getColumnIndex(DBHelper.C_ID)));
					} while(c.moveToNext());
					app.getDB().cleanComments(vids);
				};
//				Log.d(TAG,"Done cleaning comments");
				return null;
			}
			
		}.execute();
	}
	
	public void cleanPendingVideos(ArrayList<Long> uploadedPins){
		app.getDB().clearPendingVideos(uploadedPins);
		WBus.getInstance().post(new PendingVideosCleanEvent());
	}
	
	public void updateVideo(final long vid,final long user_id,final BaseActivity activity){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				int code = 0;
				String endpoint=WApp.BASE+WApp.VIDEO;
				endpoint = endpoint.replace("__vid__",""+vid);
				endpoint = endpoint.replace("__uid__",""+user_id);
				
				endpoint += "?android_version="+WApp.APP_VERSION;
				
				//Log.d(TAG,"Endpoint: "+endpoint);
				String result = null;
				MyHttpClient httpclient = new MyHttpClient(activity.app);
				HttpGet get = new HttpGet(endpoint);
				get.setHeader("Authorization","access_token="+app.getAccessToken());
				HttpResponse  response;
				try {
					response = httpclient.execute(get);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if(entity != null){
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream); 
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
//				Log.d(TAG,"Result: "+result);
				try {
					Video.parse(app,new JSONObject(result)).create();
					
				} catch(JSONException e){
					e.printStackTrace();
				}
				
				WBus.getInstance().post(new FeedUpdateEvent());
			}
		}).start();
	}

}
