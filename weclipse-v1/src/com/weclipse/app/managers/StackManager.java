package com.weclipse.app.managers;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.weclipse.app.R;
import com.weclipse.app.activity.EntranceActivity;
import com.weclipse.app.activity.SettingsActivity;
import com.weclipse.app.fragment.MainSettingsFragment;
import com.weclipse.app.fragment.NotificationSettingsFragment;
import com.weclipse.app.fragment.WebviewSettingsFragment;

public class StackManager {
	
	Activity activity;
	ArrayList<Integer> stack;
	ArrayList<String> tags;
	boolean poping;
	String currentTag;
	
	public StackManager(Activity activity,ArrayList<String> tags){
		stack = new ArrayList<Integer>();
		this.tags = tags;
		this.activity = activity;
		poping = false;
	}
	
	/*
	 * Return the current fragment tag
	 */
	public String getCurrentTag(){
		return currentTag;
	}

	/*
	 * Push the fragment to the stack
	 */
	private void push(int frag){
		stack.add(frag);
	}
	
	/*
	 * Silently remove the last fragment of the stack.
	 */
	private void silentPop(){
//		Log.d(TAG,"Silent poping");
		stack.remove(stack.size()-1);
	}
	
	/*
	 * Remove the current fragment from the stack and add the previous one
	 */
	private void pop(){
		poping=true;
//		Log.d(TAG,"Stack Size: "+stack.size());
		stack.remove(stack.size()-1);
		swapFragment(stack.get(stack.size()-1));
	}
	
	/*
	 * Initializes the stack manager. 
	 * The first fragment to be displayed needs to be the 0th position in the List.
	 */
	public void init(){
		currentTag = tags.get(0);
		swapFragment(0);
	}
	
	/*
	 * swapFragment function wrapper when a Bundle is not provided
	 */
	public void swapFragment(int frag){
		swapFragment(frag,null);
	}
	
	public void swapFragment(String frag){
		swapFragment(getIndex(frag),null);
	}
	
	public void swapFragment(String frag,Bundle b){
		swapFragment(getIndex(frag),b);
	}
	
	/*
	 * Swaps the current fragment with the next one pressed.
	 * Clears the stack before and handles the animations if any.
	 */
	public void swapFragment(int frag,Bundle b) {
		String fragmentTag = "";
		String previousTag = "";
		boolean addToBackStack=false;
		Fragment selectedFragment = null;
		
//		if(activity instanceof MainActivity){
//			
//			/* Clearing the stack before anything */
//			if(!poping){
////				if(currentTag.equals(MainActivity.SEARCH_USER_TAG)){
////					silentPop();
////					Fragment f = ((MainActivity)activity).getSupportFragmentManager().findFragmentByTag(currentTag);
////					if(f!=null){
////						((MainActivity)activity).getSupportFragmentManager().beginTransaction().remove(f).commit();
////					}
////				}
//			}
//			
//			/* Lets find/create the fragment to be added */
//			fragmentTag = tags.get(frag);
////			Log.d(TAG,"fragmentTag: "+fragmentTag);
//			selectedFragment = ((MainActivity) activity).getSupportFragmentManager().findFragmentByTag(fragmentTag);
//			if(selectedFragment==null){
//				addToBackStack=true;
//				selectedFragment = newFragmentByTag(fragmentTag);
//			}
//			
//			/* These fragments dont deserve back stack */
////			if(fragmentTag.equals(MainActivity.SEARCH_USER_TAG))
////				addToBackStack=false;
//			
//			/* Set arguments to detailed fragments */
//			if(b != null)
//				selectedFragment.setArguments(b);
//			
//			/* Update current/previous tags to better control */
//			previousTag = currentTag;
//			currentTag = fragmentTag;
//			
//			/* Start the transaction */
//			FragmentTransaction ft = ((MainActivity) activity).getSupportFragmentManager().beginTransaction();
//			
//			if(poping){
//				/* Do the reverse animation if its the case */
////				if(previousTag.equals(MainActivity.FEED_DETAIL_TAG)){
////					ft.setCustomAnimations(R.anim.slide_in_right,R.anim.slide_out_right);
////				}
//				
//				poping=false;
//			} else {
//				/* Push the currentTag to the stack */
//				push(getIndex(currentTag));
//				/* Do the 'Slide Left' animation if entering the detail fragments */
////				if(currentTag.equals(MainActivity.FEED_DETAIL_TAG)){
////					ft.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
////				}	
//			}
//			
//			if(addToBackStack){
//				ft.replace(R.id.fragment,selectedFragment,currentTag).addToBackStack(null).commit();
//			} else {
//				ft.replace(R.id.fragment,selectedFragment,currentTag).commit();
//			}
//			
//		} else 
			if(activity instanceof EntranceActivity){
			
		} else if(activity instanceof SettingsActivity){
			
			/* Clearing the stack before anything */
			//No need to clear task
			
			/* Lets find/create the fragment to be added */
			fragmentTag = tags.get(frag);
			selectedFragment = ((SettingsActivity) activity).getSupportFragmentManager().findFragmentByTag(fragmentTag);
			if(selectedFragment==null){
				addToBackStack=true;
				selectedFragment = newFragmentByTag(fragmentTag);
			}
			
			/* These fragments dont deserve back stack */
			if(fragmentTag.equals(SettingsActivity.WEBVIEW_TAG) || fragmentTag.equals(SettingsActivity.NOTIFICATION_TAG)){
				addToBackStack=false;
			}
			
			/* Set arguments to detailed fragments */
			if(b!=null)
				selectedFragment.setArguments(b);
			
			/* Update current/previous tags to better control */
			previousTag = currentTag;
			currentTag = fragmentTag;
			
			/* Start the transaction */
			FragmentTransaction ft = ((SettingsActivity) activity).getSupportFragmentManager().beginTransaction();
			
			if(poping){
				/* Reverse animation (go from right to Left) */
				if(previousTag.equals(SettingsActivity.WEBVIEW_TAG) || previousTag.equals(SettingsActivity.NOTIFICATION_TAG)){
					ft.setCustomAnimations(R.anim.slide_in_right,R.anim.slide_out_right);
				}
				poping=false;
			} else {
				/* Go from left to right */
				push(getIndex(currentTag));
				if(currentTag.equals(SettingsActivity.WEBVIEW_TAG) || currentTag.equals(SettingsActivity.NOTIFICATION_TAG)){
					ft.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
				}
			}
			
			if(addToBackStack){
				ft.replace(R.id.fragment,selectedFragment,currentTag).addToBackStack(null).commit();
			} else {
				ft.replace(R.id.fragment,selectedFragment,currentTag).commit();
			}
			
		}
	}

	/*
	 * Returns a new fragment according to the provided tag
	 */
	public Fragment newFragmentByTag(String tag){
//		if(activity instanceof MainActivity){
//			if(tag.equals(MainActivity.FEED_TAG))
//				return new FeedsFragment();
//			else if(tag.equals(MainActivity.PROFILE_TAG))
//				return new ProfileFragment();
//		} else
			if(activity instanceof EntranceActivity){
			//
		} else if(activity instanceof SettingsActivity){
			if(tag.equals(SettingsActivity.MAIN_TAG)){
				return new MainSettingsFragment();
			} else if(tag.equals(SettingsActivity.NOTIFICATION_TAG)){
				return new NotificationSettingsFragment();
			} else if(tag.equals(SettingsActivity.WEBVIEW_TAG)){
				return new WebviewSettingsFragment();
			}
		}
		return null;
	}
	
	/*
	 * Returns the index in the stack of the provided tag
	 * This allows for non-hardcoded int values for each fragment.
	 */
	public int getIndex(String tag){
		for(int i = 0 ; i < tags.size() ; i++){
			if(tags.get(i).equals(tag))
				return i;
		}
		return 0;
	}
	
	/*
	 * 'Overrides' the activities onBackPressed.
	 * If its the main, quits, else pops to the fragment before
	 */
	public void onBackPressed(){
//		if(currentTag.equals(MainActivity.FEED_TAG)){
//			activity.finish();
//			return;
//		} else 
			if(currentTag.equals(SettingsActivity.MAIN_TAG)){
			activity.finish();
			return;
		}
		pop();
	}
	
}
