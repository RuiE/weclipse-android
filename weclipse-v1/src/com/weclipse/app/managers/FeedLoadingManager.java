package com.weclipse.app.managers;

import java.util.ArrayList;

import com.weclipse.app.models.CreatingVideo;
import com.weclipse.app.utils.WLog;

public class FeedLoadingManager {

	ArrayList<Long> loadingIds;
	ArrayList<Long> pendingLoadingIds;
	ArrayList<Long> pendingLoadingIdsReply;
	ArrayList<CreatingVideo> creatingVideos;
	int creatingVideoId;
	public boolean feedRefreshIntent = false;
	public boolean avatarSpinner = false;

	public FeedLoadingManager() {
		loadingIds = new ArrayList<Long>();
		pendingLoadingIds = new ArrayList<Long>();
		pendingLoadingIdsReply = new ArrayList<Long>();
		creatingVideos = new ArrayList<CreatingVideo>();
		creatingVideoId = 1;
		feedRefreshIntent = true;
	}

	public ArrayList<CreatingVideo> getCreatingVideos() {
		return creatingVideos;
	}

	public ArrayList<Long> getLoadingIds() {
		return loadingIds;
	}

	public ArrayList<Long> getPendingLoadingIds() {
		return pendingLoadingIds;
	}

	public ArrayList<Long> getPendingLoadingIdsReply() {
		return pendingLoadingIdsReply;
	}

	public int generateId() {
		return creatingVideoId++;
	}

	public void addLoading(long vid) {
		
		if (!loadingIds.contains(vid)) {
			WLog.d(this,"Putting: "+vid);
			loadingIds.add(vid);
		}
	}

	public void removeLoading(long vid) {
		if (loadingIds.contains(vid)) {
			WLog.d(this,"Removing: "+vid);
			loadingIds.remove(vid);
		}
	}

	public void addCreating(CreatingVideo cv) {
		if (!creatingVideos.contains(cv)) {
			creatingVideos.add(cv);
		}
	}

	public void removeCreating(CreatingVideo cv) {
		if (creatingVideos.contains(cv)) {
			creatingVideos.remove(cv);
		}
	}

}
