package com.weclipse.app.managers;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import android.os.Environment;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.DownloadVideoTask;
import com.weclipse.app.utils.WLog;

public class DownloadManager {

	WApp app;
	static DownloadManager mDownloadManager;
	HashMap<Long, DownloadVideoTask> downloading;
	
	private DownloadManager(WApp app){
		this.app = app;
		downloading = new HashMap<Long, DownloadVideoTask>();
	}
	
	public static DownloadManager getInstance(WApp app){
		if(mDownloadManager == null)
			mDownloadManager = new DownloadManager(app);
		
		return mDownloadManager;
	}
	
	public boolean containsKey(long key){
		return downloading.containsKey(key);
	}
	
	public void put(long key,DownloadVideoTask task){
		downloading.put(key, task);
	}
	
	public DownloadVideoTask get(long key){
		return downloading.get(key);
	}
	
	public void remove(long key){
		downloading.remove(key);
	}
	
	
	
	
}
