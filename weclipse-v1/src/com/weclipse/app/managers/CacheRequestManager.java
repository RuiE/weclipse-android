package com.weclipse.app.managers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class CacheRequestManager {

	Context context;
	static CacheRequestManager mCacheRequestManager;
	SharedPreferences requestPrefs;
	
	public static String REQUEST_KEY_ACTIVITY = "activity_request_key";
	public static String REQUEST_KEY_TIMELINE = "timeline_request_key";
	public static String REQUEST_KEY_PROFILE = "profile_request_key";
	
	private CacheRequestManager(Context context){
		this.context = context;
		requestPrefs = context.getSharedPreferences("request_preferences", Activity.MODE_PRIVATE);
	}
	
	public static CacheRequestManager getInstance(Context context){
		if(mCacheRequestManager == null)
			mCacheRequestManager = new CacheRequestManager(context);
		
		return mCacheRequestManager;
	}
	
	public void putKey(String key,String value){
		requestPrefs.edit().putString(key, value).commit();
	}
	
	public String getValue(String key){
		return requestPrefs.getString(key, "");
	}
	
	public void reset(){
		requestPrefs.edit().clear().commit();
	}
	
	
}
