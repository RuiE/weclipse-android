package com.weclipse.app.managers;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Environment;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Group;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.WLog;

public class FileManager {
	
	public static final String FILE_PREF = "file_preferences";
	
	private static final String CLEAN_GROUP = "last_group_cleaning";
	private static final String CLEAN_USER = "last_user_cleaning";
	
	public static final long GROUP_ERASE = 432000000; // 5 days
	public static final long USER_ERASE = 432000000; // 5 days
	
	WApp app;
	SharedPreferences filePrefs;
	private static FileManager mFileManager;
	
	private FileManager(WApp app){
		this.app = app;
		filePrefs = app.getSharedPreferences(FILE_PREF, Activity.MODE_PRIVATE);
	}
	
	public static FileManager getInstance(WApp app){
		if(mFileManager == null)
			mFileManager = new FileManager(app);
		
		return mFileManager;
	}
	
	public void checkFileCleaning(){
	
		if(isTimeToCleanGroups())
			cleanGroupPictures();
		
		if(isTimeToCleanUsers())
			cleanUserPictures();
		
	}

	
	/*
	 * Cleans the videos that this device created (not the zip files)
	 * This is being called from:
	 * 1) FeedDetailActivity - On the result of the CameraActivity
	 * 
	 * (keep on adding as you make more calls to this function)
	 */
	public void cleanVideos(){
		new Thread(new Runnable() {
			@Override
			public void run() {
//				Log.d(TAG,"######### Cleaning videos ##########");
				File[] files = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+CacheManager.CACHE_DIR).listFiles();
				for(File file : files){
//					Log.d(TAG,"File name: "+file.getName());
					if(app.getDB().isVideoPending(Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+CacheManager.CACHE_DIR+file.getName())){
//						Log.d(TAG,"Video is pending sending");
					} else {
						if(!file.getPath().endsWith(".zip")){
//							Log.d(TAG,"Vou apagar");
							file.delete();
						}
					}
				}
			}
		}).start();
	}
	
	public void cleanGroupPicture(final long id){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				File thumb = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+WApp.GRP_DIR+"1/"+id+".png.nomedia");
				File banner = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+WApp.GRP_DIR+"2/"+id+".png.nomedia");
				if(thumb.exists()){
					thumb.delete();
				}
				
				if(banner.exists()){
					banner.delete();
				}
				
			}
		}).start();
	}
	
	public void cleanGroupPictures(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				ArrayList<Long> activeIds = new ArrayList<Long>();
				/* Grabbing the groups im inserted in */
				Cursor c = Group.getGroups(app);
				if(c.moveToFirst()){
					do {
						activeIds.add(c.getLong(c.getColumnIndex(DBHelper.C_ID)));
					} while(c.moveToNext());
				} else {
					c.close();
				}
				
				//At this point, ids are in activeIds. Need to delete everything not in there.
				File[] files = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+WApp.GRP_DIR+"1/").listFiles();
				for(File file : files){
					Long id = Long.parseLong(file.getName().split("\\.")[0]);
					if(activeIds.contains(id)){
						//Do nothing
					} else {
						file.delete();
						File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+WApp.GRP_DIR+"2/"+id+".png.nomedia");
						if(f.exists())
							f.delete();
						
					}
				}
				
				updateTimeToCleanGroups();
			}
		}).start();
	}
	
	public void cleanUserPictures(){
		//TODO Redo this. Properly with new scheme.
//		new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				ArrayList<Long> activeIds = new ArrayList<Long>();
//				activeIds.add(app.getId());
//				/* Grabbing the users im somehow connected to */
//				Cursor c = app.getDB().getUsers();
//				if(c.moveToFirst()){
//					do {
//						activeIds.add(c.getLong(c.getColumnIndex(DBHelper.C_ID)));
////						Log.d(TAG,"Id: "+c.getLong(c.getColumnIndex(DBHelper.C_ID))+" User: "+c.getString(c.getColumnIndex(DBHelper.C_USERNAME)));
//					} while(c.moveToNext());
//					c.close();
//				} else {
//					c.close();
//				}
//				
//				/* Lets grab the contacts now */
//				c = app.getDB().getWeclipseContacts();
//				if(c.moveToFirst()){
//					do {
//						if(!activeIds.contains(c.getLong(c.getColumnIndex(DBHelper.C_WECLIPSE_ID)))){
//							activeIds.add(c.getLong(c.getColumnIndex(DBHelper.C_WECLIPSE_ID)));
//						}
////						Log.d(TAG,"id: "+c.getLong(c.getColumnIndex(DBHelper.C_WECLIPSE_ID))+" nome: "+c.getString(c.getColumnIndex(DBHelper.C_NAME)));
//					} while(c.moveToNext());
//					c.close();
//				} else {
//					c.close();
//				}
//				
//				File[] files = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+WApp.BASE_DIR+WApp.PPL_DIR).listFiles();
//				for(File f : files){
//					Long id = Long.parseLong(f.getName().split("\\.")[0]);
//					if(!activeIds.contains(id)){
//						f.delete();
//					}
//				}
//				
//				updateTimeToCleanUsers();
//			}
//		}).start();
	}
	
	/*
	 * Time management for cleaning the group pictures 
	 */
	public boolean isTimeToCleanGroups(){
		return System.currentTimeMillis()-getLastCleanGroup() >= GROUP_ERASE;
	}
	
	public long getLastCleanGroup(){
		return filePrefs.getLong(CLEAN_GROUP,0);
	}
	
	public void updateTimeToCleanGroups(){
		filePrefs.edit().putLong(CLEAN_GROUP, System.currentTimeMillis()).commit();
	}
	
	/*
	 * Time management for cleaning the user pictures
	 */
	public boolean isTimeToCleanUsers(){
		return System.currentTimeMillis()-getLastCleanUsers() >= USER_ERASE;
	}
	
	public long getLastCleanUsers(){
		return filePrefs.getLong(CLEAN_USER,0);
	}
	
	public void updateTimeToCleanUsers(){
		filePrefs.edit().putLong(CLEAN_USER,System.currentTimeMillis()).commit();
	}
	
}
