package com.weclipse.app.managers;

import android.app.Activity;
import android.content.SharedPreferences;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.models.WNotification;
import com.weclipse.app.utils.WLog;

public class NotificationBarManager {
	
	public static final String NOTIFICATION_PREF = "notification_preferences";
	
	WApp app;
	SharedPreferences notificationPrefs;
	static NotificationBarManager mManager;
	
	private NotificationBarManager(WApp app){
		this.app = app;
		notificationPrefs = app.getSharedPreferences(NOTIFICATION_PREF,Activity.MODE_PRIVATE);
	}
	
	public static NotificationBarManager getInstance(WApp app){
		if(mManager == null)
			mManager = new NotificationBarManager(app);
		
		return mManager;
	}
	
	public void clear(){
		notificationPrefs.edit().clear().commit();
	}
	
	public void savePush(int type){
		notificationPrefs.edit().putInt(getLabel(type), getNPush(type)+1).commit();
	}
	
	public int getNPush(int type){
		return notificationPrefs.getInt(getLabel(type), 0);
	}
	
	public String getMultipleText(int type){
		WLog.d(this,"Type: "+type);
		switch (type) {
		case WNotification.VIDEO_COMMENT:
			return app.getString(R.string.notification_video_comment_m).replace("_n_", ""+getNPush(type));
		case WNotification.VIDEO_LIKE:
			return app.getString(R.string.notification_video_like_m).replace("_n_", ""+getNPush(type));
		case WNotification.VIDEO_READY:
			return app.getString(R.string.notification_video_ready_m).replace("_n_", ""+getNPush(type));
		case WNotification.VIDEO_INVITE:
			return app.getString(R.string.notification_video_invite_m).replace("_n_", ""+getNPush(type));
		case WNotification.GROUP_INVITE:
			return app.getString(R.string.notification_group_invite_m).replace("_n_", ""+getNPush(type));
		case WNotification.USER_FRIENDS:
			return app.getString(R.string.notification_user_friends_m).replace("_n_", ""+getNPush(type));
		case WNotification.CONTACT_JOINED_WECLIPSE:
			return app.getString(R.string.notification_contact_joined_weclipse_m).replace("_n_", ""+getNPush(type));
		case WNotification.USER_ACCEPT_FOLLOW:
			return app.getString(R.string.notification_user_accept_follow_m).replace("_n_", ""+getNPush(type));
		case WNotification.USER_REQUEST_FOLLOW:
			return app.getString(R.string.notification_user_request_follow_m).replace("_n_", ""+getNPush(type));
		case WNotification.USER_FOLLOW:
			return app.getString(R.string.notification_user_follow_m).replace("_n_", ""+getNPush(type));
		}
		return "";
	}
	
	private String getLabel(int type){
		switch(type){
		case WNotification.VIDEO_COMMENT:
			return "video_comment";
		case WNotification.VIDEO_LIKE:
			return "video_like";
		case WNotification.VIDEO_READY:
			return "video_ready";
		case WNotification.VIDEO_INVITE:
			return "video_invite";
		case WNotification.GROUP_INVITE:
			return "group_invite";
		case WNotification.USER_FRIENDS:
			return "user_friends";
		case WNotification.CONTACT_JOINED_WECLIPSE:
			return "contact_joined_weclipse";
		case WNotification.USER_ACCEPT_FOLLOW:
			return "user_accept_follow";
		case WNotification.USER_REQUEST_FOLLOW:
			return "user_request_follow";
		case WNotification.USER_FOLLOW:
			return "user_follow";
		default:
			return "";
		}
	}
	
}
