package com.weclipse.app.managers;

import android.app.Activity;
import android.content.SharedPreferences;

import com.weclipse.app.WApp;

public class TutorialManager {

	public static final String WALKTHROUGH = "walkthrough";
	public static final String DIALOG_PRIVATE_VIDEO = "dialog_private_video";
	
	WApp app;
	static TutorialManager mTutorialManager;
	
	public String regid;
	SharedPreferences mTutorialPreferences;
	
	private TutorialManager(WApp app){
		this.app = app;
		mTutorialPreferences = app.getSharedPreferences("tutorial_preferences",Activity.MODE_PRIVATE);
	}
	
	public static TutorialManager getInstance(WApp app){
		if(mTutorialManager == null)
			mTutorialManager = new TutorialManager(app);
		
		return mTutorialManager;
	}
	
	/* Specific */
	/* For the walkthrough when the app is first booted */
	public boolean hasDoneWalkthrough(){
		return mTutorialPreferences.getBoolean(WALKTHROUGH, false);
	}
	
	public void markWalkthrough(){
		mTutorialPreferences.edit().putBoolean(WALKTHROUGH,true).commit();
	}
	
	public void demarkWalkthrough(){
		mTutorialPreferences.edit().putBoolean(WALKTHROUGH,false).commit();
	}
	
	/* For private video dialog on camera activity */
	public boolean hasShownPrivateDialog(){
		return mTutorialPreferences.getBoolean(DIALOG_PRIVATE_VIDEO, false);
	}
	
	public void markPrivateDialog(){
		mTutorialPreferences.edit().putBoolean(DIALOG_PRIVATE_VIDEO,true).commit();
	}
	
	public void demarkPrivateDialog(){
		mTutorialPreferences.edit().putBoolean(DIALOG_PRIVATE_VIDEO,false).commit();
	}
	
	/* Generic */
	public boolean hasShown(String desc){
		return mTutorialPreferences.getBoolean(desc, false);
	}
	
	public void mark(String desc){
		mTutorialPreferences.edit().putBoolean(desc,true).commit();
	}
	
	
	
}
