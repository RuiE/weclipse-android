package com.weclipse.app.managers;

import android.app.Activity;
import android.content.SharedPreferences;

import com.weclipse.app.WApp;

public class AlertManager {
	
	public static final String VIDEO_ALERT = "video_alert";
	public static final String VIDEO_SOUND = "video_sound";
	public static final String VIDEO_VIBRATION = "video_vibration";
	public static final String VIDEO_LED = "video_led";
	
	public static final String COMMENT_ALERT = "comment_alert";
	public static final String COMMENT_SOUND = "comment_sound";
	public static final String COMMENT_VIBRATION = "comment_vibration";
	public static final String COMMENT_LED = "comment_led";
	
	public static final String FRIEND_ALERT = "friend_alert";
	public static final String FRIEND_SOUND = "friend_sound";
	public static final String FRIEND_VIBRATION = "friend_vibration";
	public static final String FRIEND_LED = "friend_led";
	
	public WApp app;
	SharedPreferences alertPrefs;
	private static AlertManager mAlertManager;
	
	private AlertManager(WApp app){
		this.app = app;
		alertPrefs = app.getSharedPreferences("alert_preferences",Activity.MODE_PRIVATE);
	}
	
	public static AlertManager getInstance(WApp app){
		if(mAlertManager == null)
			mAlertManager = new AlertManager(app);
		
		return mAlertManager;
	}
	
	/* Here is the default value for when getting the preference values */
	public boolean getAlertPreference(String alert_type){
		return alertPrefs.getBoolean(alert_type, true);
	}
	
	public void setAlertPreference(String alert_type,boolean value){
		alertPrefs.edit().putBoolean(alert_type, value).commit();
	}

}
