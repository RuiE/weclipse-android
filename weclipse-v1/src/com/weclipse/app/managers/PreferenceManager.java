package com.weclipse.app.managers;

import android.app.Activity;
import android.content.SharedPreferences;

import com.weclipse.app.WApp;

public class PreferenceManager {
	
	//List of all present preferences
	public static final String FACEBOOK = "has_facebook";
	public static final String PHONE_CODE = "phone_code";
	
	public static final String HAS_PHONE = "has_phone";
	
	WApp app;
	SharedPreferences mPrefs;
	private static PreferenceManager mPreferenceManager;
	
	private PreferenceManager(WApp app){
		this.app = app;
		mPrefs = app.getSharedPreferences("app_preferences", Activity.MODE_PRIVATE);
	}
	
	public static PreferenceManager getInstance(WApp app){
		if(mPreferenceManager == null)
			mPreferenceManager = new PreferenceManager(app);
		
		return mPreferenceManager;
	}
	
	public void putPreference(String key,boolean value){
		mPrefs.edit().putBoolean(key, value).commit();
	}
	
	public void putPreference(String key,String value){
		mPrefs.edit().putString(key, value).commit();
	}
	
	public String getStringPreference(String key){
		return mPrefs.getString(key,"0");
	}
	
	public boolean getPreference(String key){
		return mPrefs.getBoolean(key,false);
	}
	
	public static void clearPreferences(WApp app){
		getInstance(app).mPrefs.edit().clear().commit();
	}

}
