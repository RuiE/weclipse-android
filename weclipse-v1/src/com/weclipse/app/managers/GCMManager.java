package com.weclipse.app.managers;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.weclipse.app.WApp;
import com.weclipse.app.tasks.RegisterDeviceTask;
import com.weclipse.app.utils.WLog;

public class GCMManager {

	WApp app;
	static GCMManager mGCMManager;
	
	public GoogleCloudMessaging gcm;
	public String regid;
	SharedPreferences mGCMPreferences;
	private static String REG_TOKEN_REFRESH = "registration_id_refersh";
	
	public static final int REG_ID_REFRESH = 604800000;
	
	private GCMManager(WApp app){
		this.app = app;
	}
	
	public static GCMManager getInstance(WApp app){
		if(mGCMManager == null)
			mGCMManager = new GCMManager(app);
		
		return mGCMManager;
	}
	
	public void checkRegId(){
		if(isTimeToRefreshRegId() || appVersionChanged()){
			registerInBackground();
		}
	}
	
	private boolean appVersionChanged(){
        return getGCMPreferences(app).getInt(WApp.PROPERTY_APP_VERSION, Integer.MIN_VALUE) != getAppVersion(app);
	}
	
	private boolean isTimeToRefreshRegId(){
		return System.currentTimeMillis()-getLastRegisterDevice() >= REG_ID_REFRESH;
	}
	
	public long getLastRegisterDevice(){
		return getGCMPreferences(app).getLong(REG_TOKEN_REFRESH,0);
	}
	
	public void updateTimeRegisterDevice(){
		getGCMPreferences(app).edit().putLong(REG_TOKEN_REFRESH, System.currentTimeMillis()).commit();
	}
	
	public void registerInBackground() {
        new AsyncTask<String, String, String>() {

			@Override
			protected String doInBackground(String... params) {
				String msg = "";
				
                try {
                    if(gcm == null){
                        gcm = GoogleCloudMessaging.getInstance(app);
                    }
                    
                    regid = gcm.register(WApp.SENDER_ID);
                    updateTimeRegisterDevice();
                    msg = "Device registered, registration ID=" + regid;
                    
                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    // Persist the regID - no need to register again.
//                    WLog.d(this,"-----------------");
//                    WLog.d(this,"Reg id do serv de GCM: "+regid);
//                    WLog.d(this,"-----------------");
//                    WLog.d(this, "Reg id na app e no weclipse serv :"+getRegistrationId(app));
//                    WLog.d(this,"-----------------");
                    if(!regid.equals(getRegistrationId(app))){
                    	storeRegistrationId(app.getApplicationContext(), regid);
                    	new RegisterDeviceTask(app);
                    }   
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                
                return msg;
			}
			
			@Override
            protected void onPostExecute(String msg) {
				
            }
			
        }.execute(null, null, null);
    }
	
	public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(app);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//                GooglePlayServicesUtil.getErrorDialog(resultCode,pointer,WApp.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
        		WLog.i(this, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    public String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(WApp.PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            WLog.i(this, "Registration not found.");
            return "";
        }
        
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        
        int registeredVersion = prefs.getInt(WApp.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            WLog.i(this, "App version changed.");
            return "";
        }
        return registrationId;
    }
    
    public SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return app.getSharedPreferences(WApp.class.getSimpleName(),Context.MODE_PRIVATE);
    }
    
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(WApp.PROPERTY_REG_ID, regId);
        editor.putInt(WApp.PROPERTY_APP_VERSION, getAppVersion(context));
        editor.commit();
    }
	
}
