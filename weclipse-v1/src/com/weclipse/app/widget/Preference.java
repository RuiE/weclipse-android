package com.weclipse.app.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weclipse.app.R;
import com.weclipse.app.utils.WLog;

public class Preference extends LinearLayout implements OnClickListener {

	private TextView secondary;
	private String key;
	private int defaultValue;
	RelativeLayout container;
	private String dialogTitle;
	private int elements;
	SharedPreferences myPrefs;
	Context ctx;
	
	public Preference(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    
	    TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.preference, 0, 0);
	    String title = a.getString(R.styleable.preference_ttitle);
	    key = a.getString(R.styleable.preference_key);
	    defaultValue = a.getInt(R.styleable.preference_default_value, 0);
	    dialogTitle = a.getString(R.styleable.preference_dialog_title);
	    elements = a.getResourceId(R.styleable.preference_elements, 0);
	    a.recycle();
	    
	    ctx=context;
	    
	    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View v  = inflater.inflate(R.layout.preference, this, true);
	    
	    ((TextView)v.findViewById(R.id.tvTitle)).setText(title);
	    
	    container = (RelativeLayout)v.findViewById(R.id.rlContainer);
	    
	    myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
	    secondary = (TextView)v.findViewById(R.id.tvValue); 
	    updateSecondText();
	    container.setOnClickListener(this);
	}
	
	public Preference(Context context){
		super(context,null);
	}
	
	public void updateSecondText(){
		if(key.equals("default_players")){
			secondary.setText(""+myPrefs.getInt(key,defaultValue));
		} else if(key.equals("default_expire_time")){
			String eta = myPrefs.getString(key, ""+24);
			if(eta.equals("0.5")){
				secondary.setText(getResources().getStringArray(R.array.expire_time)[0]);
			} else if(eta.equals("1")) {
				secondary.setText(getResources().getStringArray(R.array.expire_time)[1]);
			} else {
				secondary.setText(eta+" hours");
			}
			
		} else if(key.equals("default_video_private")){
			secondary.setText(getResources().getStringArray(R.array.video_privacy)[myPrefs.getInt(key,defaultValue)]);
		}
	}
	
	@Override
	public void onClick(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
	    builder.setTitle(dialogTitle)
	           .setItems(elements, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	            	   SharedPreferences.Editor editor = myPrefs.edit();
	            	   if(key.equals("default_players")){
	            		   editor.putInt(key, which+2);
	            	   } else if(key.equals("default_video_private")) {
	            		   WLog.d(this,"Putting: "+which);
            		   		editor.putInt(key, which);
	            	   } else {
	            		   editor.putString(key, getResources().getStringArray(R.array.expire_time_value)[which]);
	            	   }
	           		   editor.commit();
	            	   updateSecondText();
	           }
	    }).create().show();
	}
	
}
