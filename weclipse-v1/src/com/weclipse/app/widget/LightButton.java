package com.weclipse.app.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

public class LightButton extends Button {
	
	Typeface light;

	public LightButton(Context context) {
		super(context);
	}
	
	public LightButton(Context context,AttributeSet attrs){
		super(context,attrs);
		light = Typeface.createFromAsset(context.getAssets(),"Roboto-Light.ttf");
		this.setTypeface(light);
	}

}
