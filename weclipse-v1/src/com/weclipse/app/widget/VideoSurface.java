package com.weclipse.app.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.weclipse.app.utils.WLog;

public class VideoSurface extends SurfaceView implements SurfaceHolder.Callback {
	
	Context context;

	public VideoSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }
	
	public void init(){
		getHolder().addCallback(this);
//		enableDraw();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawARGB(255, 255, 255, 255);
	}
	
	public void enableDraw(){
		setWillNotDraw(false);
	}
	
	public void disableDraw(){
		setWillNotDraw(true);
	}

	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
//		 Log.d("media player", "play next video");
//		 mediaPlayer.release();
	}
	
}
