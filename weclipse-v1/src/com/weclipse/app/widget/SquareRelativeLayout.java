package com.weclipse.app.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class SquareRelativeLayout extends RelativeLayout{

	public SquareRelativeLayout(Context context) {
		super(context);
	}
	
	public SquareRelativeLayout(Context context, AttributeSet attributeSet){
	    super(context, attributeSet);
	}
	
	public SquareRelativeLayout(Context context, AttributeSet attrs, int defStyle){
		super(context,attrs,defStyle);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	}

}
