package com.weclipse.app.widget;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.activity.CameraActivity;
import com.weclipse.app.R;
import com.weclipse.app.managers.CacheManager;
import com.weclipse.app.managers.DownloadManager;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.tasks.MarkViewedTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.PlayPack;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DownloadVideoCompleteEvent;
import com.weclipse.app.utils.eventbus.InlineVideoStopEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class InlineVideoPlayer extends TextureView implements SurfaceTextureListener {

	SurfaceTexture s;
	Context context;
	MediaPlayer mp;
	AbstractVideo v;
	Surface surface;
	RelativeLayout parent;
	WApp app;

	boolean hasZip;
	boolean extracted = false;
	ArrayList<PlayPack> playpack;

	String path, zipPath;
	
	boolean wanted_start = false;
	boolean surface_available = false;

	boolean extracting = false;

	private int currentVideo = 0;
	int replied;

	public InlineVideoPlayer(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public InlineVideoPlayer(AbstractVideo v, WApp app, RelativeLayout parent) {
		super(app);
		this.app = app;
		this.v = v;
		this.parent = parent;
		this.setTag(System.currentTimeMillis());
		setSurfaceTextureListener(this);
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int arg1, int arg2) {
		this.s = surface;
		surface_available = true;
		
		if(wanted_start){
			this.surface = new Surface(s);
			this.bringToFront();
			parent.findViewById(R.id.videoProgress).setVisibility(View.GONE);
			playNext();
		}
		
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture arg0) {
//		WLog.d(this,"Destroying");
		try {
			WBus.getInstance().unregister(this);
		} catch(Exception e){
			if(WApp.DEBUG){
//				WLog.d(this,"Init crash -- could not unregister!");
				e.printStackTrace();
			}
		}
		return true;
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture arg0, int arg1, int arg2) {
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

	}

	public void stop() {
		if (extracting) {
			WLog.d(this, "Está Extracting!!!!! --- CORRUPTED FILE?");
			WBus.getInstance().post(new InlineVideoStopEvent(InlineVideoStopEvent.CORRUPTED));
		} else {
			WBus.getInstance().post(new InlineVideoStopEvent(InlineVideoStopEvent.NORMAL));
			WLog.d(this, "Não está extracing");
		}

//		if (extracted) {
//			new Thread(new Runnable() {
//
//				@Override
//				public void run() {
//					
//					
//				}
//			}).start();
//		}

		try {
			File f = new File(path);
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; i++) {
				files[i].delete();
			}
			f.delete();
		} catch (Exception e) {
			 WLog.d(this,"Exception on file delete");
		}
		
		if (DownloadManager.getInstance(app).containsKey(v.getId())) {
			WLog.d(this, "Requesting a cancel download");
			DownloadManager.getInstance(app).get(v.getId()).cancel(true);
			DownloadManager.getInstance(app).remove(v.getId());
		}

		if (mp != null) {
			mp.release();
			mp = null;
		}
		
		parent.findViewById(R.id.videoProgress).setVisibility(View.GONE);
		parent.removeView(InlineVideoPlayer.this);
		
	}

	public void playNext() {
		// Log.d(TAG,"CurrentVideo: "+currentVideo);
		// Log.d(TAG,"Get Repied: "+replied);
		if (currentVideo < replied) {
			if (playpack.get(currentVideo).media_type == CameraActivity.MEDIA_TYPE_IMAGE) {
				// playImage(path+playpack.get(currentVideo).fileName);
			} else {
				playVideo(path + playpack.get(currentVideo).fileName);
			}
			currentVideo++;
		} else {
			stop();
		}
	}

	// public void startVideo(){
	// if(s == null){
	// Log.d("Start Viddeo","Surface Texture ainda está a null (not available) se calhar meter callback para funcionar assim q estiver");
	// return;
	// }
	// this.surface = new Surface(s);
	// this.mp = new MediaPlayer();
	// this.mp.setSurface(this.surface);
	// try {
	// this.mp.setDataSource(file);
	// this.mp.prepareAsync();
	// this.mp.setOnCompletionListener(new OnCompletionListener() {
	//
	// @Override
	// public void onCompletion(MediaPlayer mp) {
	// playNext();
	// }
	// });
	// this.mp.setOnPreparedListener(new OnPreparedListener() {
	// public void onPrepared(MediaPlayer mp) {
	// mp.start();
	// }
	// });
	//
	// } catch (IllegalArgumentException e1) {
	// e1.printStackTrace();
	// } catch (SecurityException e1) {
	// e1.printStackTrace();
	// } catch (IllegalStateException e1) {
	// e1.printStackTrace();
	// } catch (IOException e1) {
	// e1.printStackTrace();
	// }
	//
	// }

	private void playVideo(String videoPath) {
		// WLog.d(this,"Playing path: "+videoPath);
		try {
			mp = new MediaPlayer();
			this.mp.setSurface(this.surface);
			mp.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
					playNext();
				}
			});
			// mediaPlayer.setOnInfoListener(new OnInfoListener() {
			//
			// @Override
			// public boolean onInfo(MediaPlayer mp, int what, int extra) {
			// if(what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START){
			// image.setVisibility(View.GONE);
			// }
			// return false;
			// }
			// });
			this.mp.setDataSource(videoPath);
			this.mp.prepare();
			this.mp.start();

		} catch (IllegalArgumentException e) {
			if (WApp.DEBUG)
				Log.d("MEDIA_PLAYER", e.getMessage() + "");
		} catch (IllegalStateException e) {
			if (WApp.DEBUG)
				Log.d("MEDIA_PLAYER", e.getMessage() + "");
		} catch (SecurityException e) {
			if (WApp.DEBUG)
				e.printStackTrace();
		} catch (IOException e) {
			if (WApp.DEBUG)
				e.printStackTrace();

			// Bug do iOS
			currentVideo++;
			playNext();
		}
	}

	public void unzipAndPlay() {
//		WLog.d(this, "Unziping from "+this.getTag());
		new MarkViewedTask(app, v);

		path = Environment.getExternalStorageDirectory().getAbsolutePath() + WApp.BASE_DIR + CacheManager.CACHE_DIR + v.getId() + "/";
		zipPath = Environment.getExternalStorageDirectory().getAbsolutePath() + WApp.BASE_DIR + CacheManager.CACHE_DIR + v.getId() + ".zip";

		hasZip = true;

		try {
			extracting = true;
			File zip = new File(zipPath);
			ZipFile zf = new ZipFile(zip);
			zf.extractAll(path);
			extracting = false;
			extracted = true;

		} catch (ZipException e1) {
			hasZip = false;
			e1.printStackTrace();
		}

		JSONObject json = null;

		if (hasZip) {
			try {
				json = new JSONObject(WApp.getStringFromFile(path + v.getId() + ".json"));
			} catch (Exception e) {
				e.printStackTrace();
			}

			playpack = new ArrayList<PlayPack>();
			// Log.d(TAG,"Logging durations...");
			try {
				JSONArray jarray = json.getJSONArray("clips");
				JSONObject clip;
				replied = jarray.length();

				for (int i = 0; i < replied; i++) {
					clip = jarray.getJSONObject(i);
					playpack.add(new PlayPack(clip.getString("filename"), clip.getInt("duration"), clip.getInt("media_type")));
				}

			} catch (JSONException e) {
				if (WApp.DEBUG) {
					e.printStackTrace();
				}
			}

			currentVideo = 0;
		} else {

			Tracker t = app.getTracker(TrackerName.APP_TRACKER);
			t.send(new HitBuilders.EventBuilder()
					.setCategory(AnalyticsHelper.C_VIDEO)
					.setAction(AnalyticsHelper.A_VIDEO_NOT_FOUND)
					.build());
			stop();
			return;//No play
		}

		if(surface_available){
			this.surface = new Surface(s);
			this.bringToFront();
			parent.findViewById(R.id.videoProgress).setVisibility(View.GONE);
			playNext();
		} else {
			wanted_start = true;
		}
		
	}

	@Subscribe
	public void onAsyncTaskResult(DownloadVideoCompleteEvent event) {
		if (v.getId() == event.getId()) {
			if (event.getSuccess()) {
				DownloadManager.getInstance(app).remove(v.getId());
				// loading.setVisibility(View.GONE);
				unzipAndPlay();
			} else {
				WLog.d(this,"Download fail? no internet connection maybe?");
				DownloadManager.getInstance(app).remove(v.getId());
				parent.findViewById(R.id.videoProgress).setVisibility(View.GONE);
				stop();
			}
		}
	}

}
