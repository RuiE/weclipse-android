package com.weclipse.app.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedImageView extends ImageView {
	
	public static float radius = 12.0f;
	
	public RoundedImageView(Context context) {
	    super(context);
	}
	
	public RoundedImageView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	}
	
	public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		Path clipPath = new Path();
        RectF rect = new RectF(0, 0, this.getWidth(), this.getHeight());
        clipPath.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(clipPath);
        super.onDraw(canvas);
//	    Drawable drawable = getDrawable();
//	
//	    if (drawable == null) {
//	        return;
//	    }
//	
//	    if (getWidth() == 0 || getHeight() == 0) {
//	        return; 
//	    }
//	    Bitmap b =  ((BitmapDrawable)drawable).getBitmap() ;
//	    Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
//	
//	    int w = getWidth(), h = getHeight();
//	
//	    Bitmap roundBitmap =  getCroppedBitmap(bitmap, w);
//	    canvas.drawBitmap(roundBitmap, 0,0, null);
	}
	
	public static Bitmap getCroppedBitmap(Bitmap bmp, int radius) {
	    Bitmap sbmp;
	    if(bmp.getWidth() != radius || bmp.getHeight() != radius)
	        sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false);
	    else
	        sbmp = bmp;
	    Bitmap output = Bitmap.createBitmap(sbmp.getWidth(),
	            sbmp.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final int color = 0xffa19774;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());
	
	    paint.setAntiAlias(true);
	    paint.setFilterBitmap(true);
	    paint.setDither(true);
	    canvas.drawARGB(0, 0, 0, 0);
//	    paint.setColor(color.parseColor("#BAB399"));
	    canvas.drawCircle(sbmp.getWidth() / 2+0.7f, sbmp.getHeight() / 2+0.7f,
            sbmp.getWidth() / 2+0.1f, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(sbmp, rect, rect, paint);

    return output;
	}

}