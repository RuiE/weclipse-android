package com.weclipse.app.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

public class LightEditText extends EditText {
	
	Typeface light;

	public LightEditText(Context context) {
		super(context);
	}
	
	public LightEditText(Context context,AttributeSet attrs){
		super(context,attrs);
		light = Typeface.createFromAsset(context.getAssets(),"Roboto-Light.ttf");
		this.setTypeface(light);
	}

}
