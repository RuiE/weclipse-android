package com.weclipse.app.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MediumTextView extends TextView {
	
	Typeface light;

	public MediumTextView(Context context) {
		super(context);
	}
	
	public MediumTextView(Context context,AttributeSet attrs){
		super(context,attrs);
		light = Typeface.createFromAsset(context.getAssets(),"Roboto-Medium.ttf");
		this.setTypeface(light);
	}

}

