package com.weclipse.app;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ImageView;
import com.facebook.Session;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.weclipse.app.activity.BaseActivity;
import com.weclipse.app.activity.EntranceActivity;
import com.weclipse.app.activity.GroupActivity;
import com.weclipse.app.activity.VideoDetailActivity;
import com.weclipse.app.R;
import com.weclipse.app.managers.CacheManager;
import com.weclipse.app.managers.FeedLoadingManager;
import com.weclipse.app.managers.FileManager;
import com.weclipse.app.managers.GCMManager;
import com.weclipse.app.managers.NotificationBarManager;
import com.weclipse.app.managers.OfflineManager;
import com.weclipse.app.managers.ResourceManager;
import com.weclipse.app.receiver.WeclipseNetworkReceiver;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.FavoriteCache;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.ReplyPendingCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;

public class WApp extends Application {

	DBHelper dbHelper;
	SharedPreferences myPrefs;
	public BaseActivity pointer;
	public boolean isActive;

	public static boolean DEBUG = true;
	
	public static boolean INLINE = true; 

	/* Base ENDPOINT */
//	public static final String DB_NAME = "weclipsedev.db";
//	public static final String BASE_DIR = "/Android/data/com.weclipse.app.dev/";
//	public static final String BASE = "http://dev.weclipse.co/v4.1/";
//	public static final String BASE_NOSSL = "http://dev.weclipse.co/v4.1/";

	 public static final String DB_NAME = "weclise.db";
	 public static final String BASE_DIR = "/Android/data/com.weclipse.app/";
	 public static final String BASE = "https://api.weclipse.co/v4.1/";
	 public static final String BASE_NOSSL = "http://api.weclipse.co/v4.1/";

	public static final String APP_VERSION = "2.0.3";

	public static final String AUTH = "login";

	public static final String CREATE_CLIP = "users/__uid__/videos";
	public static final String VIDEO = "users/__uid__/videos/__vid__";
	public static final String REPLY_CLIP = "users/__uid__/videos/__vid__/clips";

	public static final String VIDEO_DOWNLOAD = "videos/__vid__/download";
	public static final String VIDEO_VIEWED = "videos/__vid__/viewed";
	public static final String UPLOAD_PIN = "videos/check_retry/__pin__";

	public static final String FRIENDS = "friends";
	public static final String FOLLOWERS = "followers";
	public static final String FOLLOWING = "following";
	public static final String VALIDATE = "users/validate";
	public static final String PERSON_AVA = "users/__username__/avatar";
	public static final String USER_UPDATE = "update_settings";

	/* Account ENDPOINTS */
	public static final String LOGOUT = "logout";

	/* World ENDPOINTS */
	public static final String WORLD = "world";
	public static final String WORLD_SEARCH = "world/__query__/__page__";

	/* Groups ENDPOINTS */
	public static final String CREATE_GROUP = "groups";
	public static final String GROUP = "groups/__gid__";
	public static final String EDIT_GROUP = "groups/__gid__";
	public static final String GROUP_MEMBERS = "groups/__gid__";
	public static final String GROUP_AVA = "groups/__gid__/__uid__/avatar/__type__";

	/* Clear from feed ENDPOINTS */
	public static final String CLEAR_VIDEO = "clear_video/__vid__";

	/* Comments ENDPOINTS */
	public static final String COMMENTS = "videos/__vid__/comments";
	public static final String COMMENT = "videos/__vid__/comments/__cid__";

	public static final String CLIENT_ID = "weclipse";
	public static final String CLIENT_SECRET = "BKc6uHB8jxIUoB6yMKOjjjUrfvekOY5uYtG32JLY";

	/* weclipse directories */
	public static final String GRP_DIR = "files/groups/";
	public static final String PPL_DIR = "files/people/";
	public static final String TEMP_DIR = "files/temp/";
	public static final String MEDIA_DIR = "/Weclipse/pictures";
	public static final String CLIP_DIR = "/Weclipse/";

	/* Links for the Feedback, Privacy and Terms */
	public static final String URL_FEEDBACK = "http://weclipse.co/feedback_mobile";
	public static final String URL_PRIVACY = "http://weclipse.co/privacy_mobile";
	public static final String URL_TOS = "http://weclipse.co/terms_mobile";
	public static final String URL_BLOG = "http://weclipse.co/blog";
	// public static final String URL_FEEDBACK =
	// "http://web-dev.weclipse.co/feedback_mobile";
	// public static final String URL_PRIVACY =
	// "http://web-dev.weclipse.co/privacy_mobile";
	// public static final String URL_TOS =
	// "http://web-dev.weclipse.co/terms_mobile";

	public static final String TEMP_GROUP_LOC = Environment.getExternalStorageDirectory().getAbsolutePath() + MEDIA_DIR + "/temp_group.jpeg";
	public static final String TEMP_AVA_LOC = Environment.getExternalStorageDirectory().getAbsolutePath() + MEDIA_DIR + "/temp_avatar.png";

	public static final int GRP_PIC = 0;
	public static final int PPL_PIC = 1;

	public static final int TARGET_FRIENDS = 1;
	public static final int TARGET_GROUPS = 2;
	public static final int TARGET_WORLD = 3;
	public static final int TARGET_BOTH = 4;

	public static final int TYPE_FAVORITE = 0;
	public static final int TYPE_GROUPS = 1;
	public static final int TYPE_FRIEND = 2;

	public static final int MAX_FAVORITES = 4;

	private static final String TAG = "WApp";

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public final static String SENDER_ID = "494612536092";

	public static final int MESSAGE_REQUEST_THRESHOLD = 60000;

	private static final int EXPLORE_UPDATE_TRESHOLD = 1;
	private static final int MILLIS_IN_HOUR = 3600000;

	public static final String MIXPANEL_TOKEN = "707fcd8985d34aa74f133c848e77ede5";

	public static final String PROPERTY_ID = "UA-52095118-1";

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
						// roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
							// company.
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			if (DEBUG) {
				analytics.setDryRun(true);
			}
			Tracker t = analytics.newTracker(PROPERTY_ID);
			mTrackers.put(trackerId, t);
		}
		
		return mTrackers.get(trackerId);
	}

	private OfflineManager om;
	private NotificationBarManager nm;
	private ResourceManager rm;
	private FeedLoadingManager flm;

	@Override
	public void onCreate() {
		super.onCreate();
		
		WLog.d(this,"Creating..");

		dbHelper = new DBHelper(this);
		myPrefs = PreferenceManager.getDefaultSharedPreferences(this);

		File dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + GRP_DIR + "1/");
		dir.mkdirs();
		dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + GRP_DIR + "2/");
		dir.mkdir();
		dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + PPL_DIR);
		dir.mkdir();
		dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + TEMP_DIR);
		dir.mkdir();
		dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + CacheManager.CACHE_DIR);
		dir.mkdir();
		dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + MEDIA_DIR);
		dir.mkdirs();

		try {
			Class.forName("android.os.AsyncTask");
		} catch (ClassNotFoundException e) {
			if (WApp.DEBUG)
				e.printStackTrace();
		}

		/*
		 * Managers for all sorts of things: OfflineManager.java - Offline video
		 * handling, automatically send when connectivity is back up
		 * TutorialManager.java - Tutorial handling for first time comers
		 * NotificationManager.java - In-app Notification handling. Red dots.
		 * FileManager.java - File cleaning and maintenance NetworkManager.java
		 * - Handle opererations through the network, ideally, every operation
		 * should be here. reply videos, add friends, ... ResourceManager.java -
		 * Handle updates from feed, groups, sync contacts etc.
		 * FeedLoadingManager.java - Handle the loading spins on the
		 * 'FeedFragment' and 'FeedDetail' screens
		 */
		om = new OfflineManager(this);
		rm = new ResourceManager(this);
		flm = new FeedLoadingManager();

		/*
		 * These jobs clean any pictures from both groups and users that are
		 * still in the phone but no matching user on the database has been
		 * found
		 */
		FileManager.getInstance(this).checkFileCleaning();

		/* Job to check if need to register device again */
		if(isLogged())
			GCMManager.getInstance(this).checkRegId();

		/* Force updates on database. (if needed, do them here) */
		// if(WApp.DEBUG){
		// dbHelper.rebasePendingComments();
		// }

		/* Job to free cache or whatever */
		CacheManager.checkSpace();
	}
	
	@Override
	public void onTerminate() {
		super.onTerminate();
		getDB().close();
	}

	public ResourceManager getResourceManager() {
		return rm;
	}

	public FeedLoadingManager getFeedLoadingManager() {
		return flm;
	}

	/* Methods concerning the offline manager */
	public OfflineManager getOfflineManager() {
		return om;
	}

	public DBHelper getDB() {
		return dbHelper;
	}

	public void putBooleanDetail(String key, boolean value) {
		SharedPreferences.Editor editor = myPrefs.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public void putStringDetail(String key, String value) {
		SharedPreferences.Editor editor = myPrefs.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public void putIntDetail(String key, int value) {
		SharedPreferences.Editor editor = myPrefs.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public void login() {
		putBooleanDetail("isLogged", true);
	}

	public void activelyLogout() {
		if (isActive) {
			pointer.startActivity(new Intent(pointer, EntranceActivity.class));
		}
	}

	public void clearDB() {
		new AsyncTask<String, String, String>() {
			@Override
			protected String doInBackground(String... params) {
				dbHelper.clearVideos();
				dbHelper.clearUsers();
				// dbHelper.clearContacts();
				dbHelper.clearGroups();
				dbHelper.clearComments();
				dbHelper.clearActions();
				dbHelper.clearPendingVideos();
				FavoriteCache.clearFavorites(WApp.this);
				ReplyPendingCache.clear(WApp.this);
				dbHelper.rebasePendingComments();
				dbHelper.clearLikesActivity();
				dbHelper.clearLikesTimeline();
				dbHelper.clearTimeline();
				dbHelper.clearMyVideos();
				dbHelper.close();
				return null;
			}
		}.execute();
	}

	public void logout(boolean letServerKnow) {
		if (letServerKnow)
			logoutFromServer(getAccessToken());

		myPrefs.edit().clear().commit();
		com.weclipse.app.managers.PreferenceManager.clearPreferences(WApp.this);

		if (Session.getActiveSession() != null && Session.getActiveSession().isOpened()) {
			Session.getActiveSession().closeAndClearTokenInformation();
		}

	}

	public String getAccessToken() {
		return myPrefs.getString("access_token", null);
	}

	public String getRefreshToken() {
		return myPrefs.getString("refresh_token", null);
	}

	public boolean isLogged() {
		return myPrefs.getBoolean("isLogged", false);
	}

	public String getUserCode() {
		return myPrefs.getString("phone_code", "");
	}

	public long getUID() {
		return myPrefs.getLong("id", 0);
	}

	public long getId() {
		return myPrefs.getLong("id", 0);
	}

	public int getDefaultPlayers() {
		return myPrefs.getInt("default_players", 5);
	}

	public String getDefaultVideoTime() {
		return myPrefs.getString("default_expire_time", "" + 24);
	}
	
	public int getDefaultVideoType(){
		return myPrefs.getInt("default_video_private", 0);
	}

	public int getDefaultPictureTime() {
		return myPrefs.getInt("default_picture_time", 3);
	}

	public boolean getPreferenceDownloadWifiOnly() {
		return myPrefs.getBoolean("download_on_wifi_only", true);
	}

	public boolean isTimeToUpdate(long mills) {
		// Log.d(TAG,"Last time: "+myPrefs.getLong("last_explore_update", 0));
		// Log.d(TAG,"Now: "+mills);
		// Log.d(TAG,"Dif: "+(mills-myPrefs.getLong("last_explore_update", 0)));
		return (mills - myPrefs.getLong("last_explore_update", 0)) > (MILLIS_IN_HOUR * EXPLORE_UPDATE_TRESHOLD);
	}

	/* Preference related to the offline mode */
	public void saveOfflineActions() {
		myPrefs.edit().putBoolean("offline_actions", true).commit();
	}

	public boolean hasOfflineActions() {
		return myPrefs.getBoolean("offline_actions", false);
	}

	public void clearOfflineActions() {
		myPrefs.edit().putBoolean("offline_actions", false).commit();
	}

	public void saveOfflineVideo() {
		myPrefs.edit().putBoolean("offline_video", true).commit();
	}

	public boolean hasOfflineVideo() {
		return myPrefs.getBoolean("offline_video", false);
	}

	public void clearOfflineVideos() {
		myPrefs.edit().putBoolean("offline_video", false).commit();
	}

	public NotificationBarManager getNotificationManager() {
		return nm;
	}

	public void updateGroupMembers() {
		if (isActive) {
			if (pointer instanceof GroupActivity) {
				((GroupActivity) pointer).drawMembers();
			}
		}
	}

	public void updateUserDoesntExist() {
		if (isActive) {
			if (pointer instanceof VideoDetailActivity) {
				((VideoDetailActivity) pointer).getToast()
						.setText(getString(R.string.toast_user_does_not_exist))
						.setBackground(R.color.weclipse_red_75)
						.show(WToast.MEDIUM);
			} else if (pointer instanceof GroupActivity) {
				((GroupActivity) pointer).getToast()
						.setText(getString(R.string.toast_user_does_not_exist))
						.setBackground(R.color.weclipse_red_75)
						.show(WToast.MEDIUM);
			}
		}
	}

	/* Manages toast showing there isn\'t internet */
	public void connectedToInternet(int connection_type) {
		if (isActive) {
			((BaseActivity) pointer).getToast().hide();
		}

		if (hasOfflineVideo()) {
			om.sendOfflineVideo();
		}

		if (hasOfflineActions()) {
			om.sendOfflineActions();
		}

		if (connection_type == WeclipseNetworkReceiver.CONNECTION_WIFI) {
			// TODO anything to do ?
		}

	}

	// public void showNoInternet() {
	// if (isActive) {
	// ((BaseActivity) pointer).getToast()
	// .setText(getString(R.string.toast_no_internet))
	// .setBackground(R.color.dark_grey_75).show();
	// }
	// }

	public void showNoInternet(int duration) {
		if (isActive) {
			((BaseActivity) pointer).getToast()
					.setText(getString(R.string.toast_no_internet))
					.setBackground(R.color.dark_grey_75).show(duration);
		}
	}

	public BaseActivity getBaseActivity() {
		if (isActive)
			return pointer;

		return null;
	}

	/* Variables to manage the sync contacts upon contact list change */
	public void contactsChanged() {
		myPrefs.edit().putBoolean("sync_contacts", true).commit();
	}

	public boolean needToSyncContacts() {
		return myPrefs.getBoolean("sync_contacts", false);
	}

	public void syncedContacts() {
		myPrefs.edit().putBoolean("sync_contacts", false).commit();
	}

	public void setPointer(BaseActivity act) {
		pointer = act;
	}

	public static Bitmap getAvatar(long uid) {
		return getImage(uid, WApp.PPL_PIC, 0);
	}

	public static void deleteAvatar(long id) {
		new File(Environment.getExternalStorageDirectory().getAbsolutePath()
				+ BASE_DIR + PPL_DIR + id + ".png.nomedia").delete();
	}

	public static Bitmap getImage(long uid, int target, int type) {
		Bitmap bitmap = null;
		String img = uid + ".png.nomedia";
		String path = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR;

		switch (target) {
		case GRP_PIC:
			path += GRP_DIR + type + "/";
			break;
		case PPL_PIC:
			path += PPL_DIR;
			break;
		}
		path += "" + img;
		// Log.d(TAG,"Total path: "+path);
		bitmap = BitmapFactory.decodeFile(path);
		return bitmap;
	}

	public static Bitmap getTempAvatar() {
		Bitmap bitmap = null;
		bitmap = BitmapFactory.decodeFile(TEMP_AVA_LOC);
		return bitmap;
	}

	public static boolean storeImage(Bitmap bitmap, int type, String name) {
		boolean res = false;
		File dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + GRP_DIR + type + "/");
		File image = new File(dir, name);
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			FileOutputStream outStream = new FileOutputStream(image);
			if (outStream != null) {
				outStream.write(out.toByteArray());
				outStream.flush();
				outStream.close();
				res = true;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static void deleteTempGroup() {
		File image = new File(TEMP_GROUP_LOC);
		image.delete();
	}

	public static File storeTempImage(Bitmap bitmap) {
		File image = new File(TEMP_GROUP_LOC);
		if (image.exists()) {
			image.delete();
			image = new File(TEMP_GROUP_LOC);
		}
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			FileOutputStream outStream = new FileOutputStream(image);
			if (outStream != null) {
				outStream.write(out.toByteArray());
				outStream.flush();
				outStream.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return image;
	}

	public static File storeTempAvatar(Bitmap bitmap) {
		File image = new File(TEMP_AVA_LOC);
		if (image.exists()) {
			image.delete();
			image = new File(TEMP_AVA_LOC);
		}
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			FileOutputStream outStream = new FileOutputStream(image);
			if (outStream != null) {
				outStream.write(out.toByteArray());
				outStream.flush();
				outStream.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return image;
	}

	public static boolean storeAvatar(final Bitmap bitmap, final String name) {
		boolean res = false;
		if (bitmap == null)
			return res;

		File dir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + PPL_DIR);
		File image = new File(dir, name);
		if (image.exists()) {
			image.delete();
			image = new File(dir, name);
		}
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			FileOutputStream outStream = new FileOutputStream(image);
			if (outStream != null) {
				outStream.write(out.toByteArray());
				outStream.flush();
				outStream.close();
				res = true;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Bitmap cropImage(Bitmap bitmap) {
		if (bitmap == null) {
			Log.d(TAG, "Oops, null bitmap");
			return null;
		}
		final int width = bitmap.getWidth();
		final int height = bitmap.getHeight();
		final Bitmap outputBitmap = Bitmap.createBitmap(width, height,
				Config.ARGB_8888);

		final Canvas canvas = new Canvas(outputBitmap);
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

		paint.setAntiAlias(true);
		Rect rect = new Rect(0, 0, width, height);
		RectF rectF = new RectF(rect);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(0xff424242);

		canvas.drawCircle(width / 2, height / 2, width / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rectF, paint);

		return outputBitmap;
	}

	public static Bitmap squareImage(Bitmap bitmap) {
		Bitmap resBitmap;
		if (bitmap.getWidth() >= bitmap.getHeight()) {

			resBitmap = Bitmap.createBitmap(bitmap, bitmap.getWidth() / 2
					- bitmap.getHeight() / 2, 0, bitmap.getHeight(),
					bitmap.getHeight());
		} else {
			resBitmap = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() / 2
					- bitmap.getWidth() / 2, bitmap.getWidth(),
					bitmap.getWidth());
		}
		return resBitmap;

	}

	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public static int dpToPx(int dp) {
		return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static int pxToDp(int px) {
		return (int) (px / Resources.getSystem().getDisplayMetrics().density);
	}

	public static void deleteMediaFile(String path) {
		try {
			File f = new File(path);
			f.delete();
		} catch (NullPointerException e) {
			if (WApp.DEBUG) {
				Log.d(TAG, "No file. No problem.");
			}
		}
	}

	public static boolean avatarExist(long id) {
		String dir = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + PPL_DIR;
		File image = new File(dir, id + ".png.nomedia");
		return image.exists();
	}

	public static boolean imageExist(long gid) {
		String dir = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + GRP_DIR + "2/";
		File image = new File(dir, gid + ".png.nomedia");
		return image.exists();
	}

	public static boolean bannerExis(long gid) {
		String dir = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + BASE_DIR + GRP_DIR + "1/";
		File image = new File(dir, gid + ".png.nomedia");
		return image.exists();
	}

	public static Bitmap rotate(Bitmap bitmap, int degree) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Matrix mtx = new Matrix();
		mtx.postRotate(degree);
		return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
	}

	public static class ImageLoaderTask extends
			AsyncTask<String, String, Bitmap> {

		ImageView v;
		long uid;
		int capTime = 500;
		int sleepTime = 50;
		int total;

		public ImageLoaderTask(ImageView v, long uid) {
			this.v = v;
			this.uid = uid;
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			total = 0;
			boolean avatarExist = WApp.avatarExist(uid);

			while (!avatarExist) {
				try {
					total += sleepTime;
					Thread.sleep(sleepTime);
					avatarExist = WApp.avatarExist(uid);
					if (total > capTime) {
						break;
					}
				} catch (InterruptedException e) {
					// Do nothing
				}
			}

			Bitmap bm = getAvatar(uid);

			return bm;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			if (v != null && (Long) v.getTag() == uid && result != null)
				v.setImageBitmap(result);
		}
	}

	public static class GroupImageLoaderTask extends
			AsyncTask<String, String, Bitmap> {

		ImageView v;
		long gid;
		int capTime = 500;
		int sleepTime = 50;
		int total;

		public GroupImageLoaderTask(ImageView v, long gid) {
			this.v = v;
			this.gid = gid;
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			total = 0;
			boolean avatarExist = WApp.imageExist(gid);
			while (!avatarExist) {
				try {
					if (total > capTime)
						break;
					total += sleepTime;
					Thread.sleep(sleepTime);
					avatarExist = WApp.imageExist(gid);
				} catch (InterruptedException e) {
					// Do nothing
				}
			}

			Bitmap bm = WApp.getImage(gid, WApp.GRP_PIC, 2);

			return bm;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			if (v != null && (Long) v.getTag() == gid)
				v.setImageBitmap(result);
		}

	}

	public void logoutFromServer(final String access_token) {
		new AsyncTask<String, String, String>() {
			int code;

			@Override
			protected String doInBackground(String... params) {
				String endpoint = WApp.BASE + WApp.LOGOUT;
				// Log.d(TAG,"Endpoint: "+endpoint);
				endpoint += "?android_version=" + WApp.APP_VERSION;
				String result = null;
				MyHttpClient httpclient = new MyHttpClient(WApp.this);
				HttpGet get = new HttpGet(endpoint);
				get.setHeader("Authorization", "access_token="+ access_token);
				HttpResponse response;
				try {
					response = httpclient.execute(get);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if (entity != null) {
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream);
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return result;
			}

			protected void onPostExecute(String result) {
				if (code == HTTPStatus.OK) {
					// logged out from server
				}
			};

		}.execute();
	}

	public static String getStringFromFile(String filePath) throws Exception {
		File fl = new File(filePath);
		FileInputStream fin = new FileInputStream(fl);
		String ret = convertStreamToString(fin);
		fin.close();
		return ret;
	}

	/*
	 * Methods to control when was the last sms request sent
	 */
	public long getLastCodeRequest() {
		return myPrefs.getLong("last_sms_request", 0);
	}

	public void setLastRequestSent() {
		myPrefs.edit().putLong("last_sms_request", System.currentTimeMillis())
				.commit();
	}

	public void resetLastRequestSent() {
		myPrefs.edit().putLong("last_sms_request", 0).commit();
	}

	public boolean canRequestSms() {
		return System.currentTimeMillis() - getLastCodeRequest() >= MESSAGE_REQUEST_THRESHOLD ? true
				: false;
	}

}
