package com.weclipse.app.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.fetch.FetchPossiblePlayers;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.StringArrayAlphabetIndexer;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.FetchPossiblePlayersEvent;
import com.weclipse.app.widget.PinnedHeaderListView;
import com.weclipse.app.widget.PinnedHeaderListView.PinnedHeaderAdapter;

public class FriendChoserActivity extends BaseActivity implements OnClickListener {
	
	private static final String TAG ="FriendChoser Screen";
	
	RelativeLayout bSend;
	
	FriendAdapter adapter;
	TextView tvSend;
	PinnedHeaderListView lv;
	Cursor cursor;
	ArrayList<Long> selected;
	ArrayList<Long> idsInGroup;
	
	String[] names;
	boolean canSend;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/* Setup action bar */
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.header_choose_friends_activity));
		
		setContentView(R.layout.activity_friend_selector);
		selected = new ArrayList<Long>();
		initVariables();	
	}
	
	private void initVariables(){
		bSend = (RelativeLayout)findViewById(R.id.bSend);
		bSend.setOnClickListener(this);
		tvSend = (TextView)findViewById(R.id.tvSend);
		tvSend.setText(getString(R.string.choose));
		lv = (PinnedHeaderListView)findViewById(R.id.lvFriends);
		lv.setOverScrollMode(View.OVER_SCROLL_NEVER);
		lv.setDividerHeight(0);
		
		idsInGroup = (ArrayList<Long>)getIntent().getExtras().get("ids");
		canSend = false;
//		idsInGroup = null;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bSend:
			if(canSend){
//				Log.d(TAG,"Cliquei: "+selected.size());
				Intent i = new Intent();
				long[] temp = new long[selected.size()];
				for(int j = 0 ; j < selected.size() ; j++){
					temp[j] = selected.get(j);
				}
				i.putExtra("sending", temp);
				setResult(RESULT_OK,i);
				if(cursor!=null)
					cursor.close();
				this.finish();
			} else {
				getToast().setText(R.string.toast_choose_group).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			}
			break;
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		new FetchPossiblePlayers(app);
		updateListView();
	}
	
	public void updateListView(){
		
		if(cursor != null)
			cursor.close();

//		
//		for(int i = 0 ; i < idsInGroup.size() ; i++){
//			WLog.d(this,"Id "+i+" no array: "+idsInGroup.get(i));
//		}
		
		cursor = app.getDB().getFriends(idsInGroup);
		names = app.getDB().getFriendsSectionArray(idsInGroup);
			
		
//		WLog.d(this,"Cursor Dump--------------");
//		for(int i = 0 ; i < cursor.getColumnNames().length ; i++){
//			WLog.d(this,cursor.getColumnNames()[i]);
//			
//		}
//		
//		while(cursor.moveToNext()){
//			WLog.d(this,"Id: "+cursor.getString(0)+" username: "+cursor.getString(1));
//		}
		
		
		if(cursor.getCount() == 0){
			findViewById(R.id.rlNoFriendsCreateGroupContainer).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.rlNoFriendsCreateGroupContainer).setVisibility(View.GONE);
			if(adapter == null){
				adapter = new FriendAdapter(this, cursor);
				lv.setAdapter(adapter);
			} else {
				adapter.swapCursor(cursor);
				adapter.notifyDataSetChanged();
			}
		}
		
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED,null);
		this.finish();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(cursor!=null){
			cursor.close();
		}
	}
	
	public void incrementSelected(long id){
		selected.add(id);
		if(!canSend){
			canSend=true;
			bSend.setBackgroundResource(R.color.weclipse_green);
		}
//		Log.d(TAG,selected.toString());
	}
	
	public void decrementSelected(long id){
		for(int i = 0 ; i < selected.size() ; i++){
			if(selected.get(i)==id){
				selected.remove(i);
				
				if(selected.size()==0){
					canSend=false;
					bSend.setBackgroundResource(R.color.comment_gray);
				}
				
				return;
			}
		}
		
	}
	
	private class FriendAdapter extends CursorAdapter implements OnClickListener,SectionIndexer,OnScrollListener,PinnedHeaderAdapter {

		LayoutInflater mInflater;
		private SectionIndexer mIndexer;
		Cursor c;
		
		public FriendAdapter(Context context, Cursor c) {
			super(context, c);
			mInflater = LayoutInflater.from(context);
			this.c = c;
			mIndexer = new StringArrayAlphabetIndexer(names,"ABCDEFGHIJKLMNOPQRSTUVWXYZ_");
		}
		
		@Override
		public Cursor swapCursor(Cursor newCursor) {
			this.c = cursor;
			return super.swapCursor(newCursor);
		}
		
		@Override
		public void bindView(View arg0, Context arg1, Cursor arg2) {
		}

		@Override
		public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
			return null;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			c.moveToPosition(position);
			
			String name = c.getString(c.getColumnIndex(DBHelper.C_NAME));
			String username = c.getString(c.getColumnIndex(DBHelper.C_USERNAME));
			long id = c.getLong(c.getColumnIndex(DBHelper.C_ID));
			String avatar_url = c.getString(c.getColumnIndex(DBHelper.C_AVATAR_URL));
			int follow_status = c.getInt(c.getColumnIndex(DBHelper.C_FOLLOW_STATUS));
			int private_user = c.getInt(c.getColumnIndex(DBHelper.C_PRIVATE_USER));
			
			User u = new User(FriendChoserActivity.this, id, username, name, avatar_url, follow_status,private_user == User.PRIVATE);
			
			ViewHolder holder;
			if(convertView==null){
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.row_friend_selector, null);
				holder.name = (TextView)convertView.findViewById(R.id.tvName);
				holder.container = (RelativeLayout)convertView.findViewById(R.id.rlContainer);
				holder.ivAvatar = (ImageView)convertView.findViewById(R.id.ivAvatar);
				holder.cb = (CheckBox)convertView.findViewById(R.id.cSelector);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}
			
			holder.name.setText(username);
			
			holder.container.setTag(id);
			holder.container.setOnClickListener(this);
			
			
			new AvatarDrawer(app,holder.ivAvatar,u).draw();
			
			if(!selected.contains(id)){
				holder.cb.setChecked(false);
			} else {
				holder.cb.setChecked(true);
			}
			
			bindSectionHeader(convertView, position);
			
			return convertView;
		}

		@Override
		public void onClick(View v) {
			CheckBox cb = (CheckBox)v.findViewById(R.id.cSelector);
			cb.setChecked(!cb.isChecked());
			if(cb.isChecked())
				incrementSelected((Long)v.getTag());
			else
				decrementSelected((Long)v.getTag());
		}
		
		private class ViewHolder {
			TextView name;
			RelativeLayout container;
			ImageView ivAvatar;
			CheckBox cb;
		}
		
		private void bindSectionHeader(View itemView, int position) {
            final RelativeLayout headerView = (RelativeLayout) itemView.findViewById(R.id.rlHeader);
            final View dividerView = itemView.findViewById(R.id.list_divider);
            
            final int section = getSectionForPosition(position);
            if (getPositionForSection(section) == position) {
                String title = (String) mIndexer.getSections()[section];
                ((TextView)headerView.findViewById(R.id.tvHeader)).setText(title);
                headerView.setVisibility(View.VISIBLE);
            } else {
                headerView.setVisibility(View.GONE);
                dividerView.setVisibility(View.VISIBLE);
            }
    
            // move the divider for the last item in a section
            if (getPositionForSection(section + 1) - 1 == position) {
                dividerView.setVisibility(View.GONE);
            } else {
                dividerView.setVisibility(View.VISIBLE);
            }
		}

		public int getPinnedHeaderState(int position) {
			if (mIndexer == null || getCount() == 0) {
                return PINNED_HEADER_GONE;
            }

            if (position < 0) {
                return PINNED_HEADER_GONE;
            }

            // The header should get pushed up if the top item shown
            // is the last item in a section for a particular letter.
            int section = getSectionForPosition(position);
            int nextSectionPosition = getPositionForSection(section + 1);
            
            if (nextSectionPosition != -1 && position == nextSectionPosition - 1) {
                return PINNED_HEADER_PUSHED_UP;
            }

            return PINNED_HEADER_VISIBLE;
		}

		@Override
		public void configurePinnedHeader(View v, int position, int alpha) {
			TextView header = (TextView)v.findViewById(R.id.tvHeader);
            
            final int section = getSectionForPosition(position);
            final String title = (String) getSections()[section];
            
            header.setText(title);
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if (view instanceof PinnedHeaderListView) {
                ((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
            }  
		}

		@Override
		public Object[] getSections() {
			if (mIndexer == null) {
                return new String[] { " " };
            } else {
                return mIndexer.getSections();
            }
		}

		public int getPositionForSection(int sectionIndex) {
            if (mIndexer == null)
                return -1;
            return mIndexer.getPositionForSection(sectionIndex);
            
        }
                
        public int getSectionForPosition(int position) {
            if (mIndexer == null)
                return -1;
            return mIndexer.getSectionForPosition(position);
        }
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(FetchPossiblePlayersEvent event){
		WLog.d(this,"chegou evento fetch possible players");
		if(event.getSuccess()){
			updateListView();
		}
	}
	
}
