package com.weclipse.app.activity;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.fragment.SyncContactsFragment;
import com.weclipse.app.fragment.SyncContactsFriendsFragment;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.UnmergeContactsTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;

public class SyncContactsActivity extends BaseActivity {

	FragmentManager mFragmentManager;
	ArrayList<User> mUsers;
	
	Menu mMenu;

	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);

		setContentView(R.layout.activity_sync_contacts);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.find_header_contacts));

		initVariables();
	}

	public void setTitle(String title) {
		getSupportActionBar().setTitle(title);
	}

	public void initVariables() {

		mFragmentManager = getSupportFragmentManager();
		Fragment mFragment = null;

		if (PreferenceManager.getInstance(app).getPreference(PreferenceManager.HAS_PHONE)) {
			mFragment = new SyncContactsFriendsFragment();
		} else {
			mFragment = new SyncContactsFragment();
		}

		mFragmentManager.beginTransaction().replace(R.id.fragment, mFragment).commit();
		
		Tracker t = ((WApp)getApplication()).getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_CONTACTS);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		mMenu = menu;
		updateMenu();
		return true;
	}
	
	public void updateMenu(){
		if(mMenu != null){
			mMenu.clear();
			
			if(PreferenceManager.getInstance(app).getPreference(PreferenceManager.HAS_PHONE)){
				getMenuInflater().inflate(R.menu.menu_unmerge_contacts, mMenu);
			} else {
				
			}
		} else {
			invalidateOptionsMenu();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.onBackPressed();
			break;
		case R.id.action_unmerge_contact:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.unmerge_contacts_dialog_title));
			
			builder.setMessage(R.string.are_you_sure)
					.setPositiveButton(R.string.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									new UnmergeContactsTask(app);
								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// do nothing
								}
							});
			builder.create().show();
			
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void setUsers(ArrayList<User> users) {
		this.mUsers = users;
	}

	public ArrayList<User> getUsers() {
		return mUsers;
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}
	
}
