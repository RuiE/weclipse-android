package com.weclipse.app.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.fragment.FeedFragment;
import com.weclipse.app.fragment.ProfileFragment;
import com.weclipse.app.fragment.TimelineFragment;
import com.weclipse.app.managers.FileManager;
import com.weclipse.app.managers.NotificationBarManager;
import com.weclipse.app.managers.RateManager;
import com.weclipse.app.models.Me;
import com.weclipse.app.tasks.CreateVideoTask;
import com.weclipse.app.tasks.ReplyVideoTask;
import com.weclipse.app.utils.CircleTransform;
import com.weclipse.app.utils.ReplyPendingCache;
import com.weclipse.app.utils.VideoPack;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.PageChangedEvent;
import com.weclipse.app.utils.eventbus.SendAvatarEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class TheMainActivity extends BaseActivity implements OnLongClickListener {
	
	private static final int LOGOUT_CODE = 1;
	public static final int GROUP_CREATE = 7;

	private static final int CODE_CAMERA = 10;
	public static final int CODE_REPLY = 11;
	public static final int CODE_VIDEO = 12;
	
	public static final int CODE_SMS_SENT = 20;
	public static final int CODE_EMAIL_SENT = 21;

	public static final int CREATE = 0;
	public static final int REPLY = 1;
	
	public static final int TIMELINE = 0;
	public static final int FEED = 1;
	public static final int PROFILE = 2;
	
	int currentPage;
	
	public String lastFile;
	
	ViewPager mPager;
	MyPagerAdapter mPagerAdapter;
	
	ActionBar mActionBar;
	
	View stripTimeline,stripFeed,stripProfile;
	ImageView ivFeed,ivTimeline,ivProfile;
	
	public UiLifecycleHelper uiHelper;
	
	RelativeLayout bFeed,bTimeline,bProfile;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		setContentView(R.layout.activity_the_main);
		
		initVariables();
		
		ReplyPendingCache.getInstance(app).load();
		
		 uiHelper = new UiLifecycleHelper(this, null);
		 uiHelper.onCreate(b);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_add_friend:
			startActivity(new Intent(this,FindFriendsActivity.class));
			break;
		case R.id.action_open_camera:
			startActivityForResult(new Intent(getBaseContext(),CameraActivity.class), CODE_CAMERA);
			break;
		case R.id.action_settings:
			startActivityForResult(new Intent(getBaseContext(),SettingsActivity.class), LOGOUT_CODE);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	    
	    if(getIntent().getExtras() !=null && getIntent().getExtras().getBoolean("from_push",false)){
			getIntent().removeExtra("from_push");
			onFeedClick(null);
			NotificationBarManager.getInstance(app).clear();
		}
	    
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		RateManager.onStart(this);
		RateManager.showRateDialogIfNeeded(this);
	}
	
	@Override
	public boolean onLongClick(View v) {
		
		Toast t = null;
		
		int height = bProfile.getHeight()+getActionBar().getHeight();
		
		switch(v.getId()){
		case R.id.bTimeline:
			t = Toast.makeText(this, R.string.menu_desc_timeline, Toast.LENGTH_SHORT);
			t.setGravity(Gravity.TOP|Gravity.START, 0, height);
			break;
		case R.id.bFeed:
			t = Toast.makeText(this, R.string.menu_desc_feed, Toast.LENGTH_SHORT);
			t.setGravity(Gravity.TOP|Gravity.CENTER, 0, height);
			break;
		case R.id.bProfile:
			t = Toast.makeText(this, R.string.menu_desc_profile, Toast.LENGTH_SHORT);
			t.setGravity(Gravity.TOP|Gravity.END, 0, height);
			break;
		}
		
		t.show();
		
		return true;
	}
	
	public void initVariables(){
		mPager = (ViewPager)findViewById(R.id.pager);
		mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
		mPager.setAdapter(mPagerAdapter);
		mPager.setOffscreenPageLimit(2);
		
		mActionBar = getSupportActionBar();
		mActionBar.setIcon(getResources().getDrawable(R.drawable.logo));
		mActionBar.setTitle("");
		
		stripTimeline = findViewById(R.id.stripTimeline);
		stripProfile = findViewById(R.id.stripProfile);
		stripFeed = findViewById(R.id.stripFeed);
		
		ivFeed = (ImageView)findViewById(R.id.ivFeed);
		ivTimeline = (ImageView)findViewById(R.id.ivTimeline);
		ivProfile = (ImageView)findViewById(R.id.ivProfile);
		
		mPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int page) {
				currentPage = page;
				drawStrip();
				
//				WLog.d(this,"need to stop any videos playing around");
				WBus.getInstance().post(new PageChangedEvent());
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		bProfile = (RelativeLayout)findViewById(R.id.bProfile);
		bFeed = (RelativeLayout)findViewById(R.id.bFeed);
		bTimeline = (RelativeLayout)findViewById(R.id.bTimeline);
		
		bProfile.setOnLongClickListener(this);
		bTimeline.setOnLongClickListener(this);
		bFeed.setOnLongClickListener(this);
		
	}
	
	public void drawStrip(){
		switch(currentPage){
		case FEED:
			stripTimeline.setVisibility(View.GONE);
			stripFeed.setVisibility(View.VISIBLE);
			stripProfile.setVisibility(View.GONE);
			
			ivFeed.setBackgroundResource(R.drawable.main_feed_selected);
			ivTimeline.setBackgroundResource(R.drawable.main_timeline);
			ivProfile.setBackgroundResource(R.drawable.main_profile);
			break;
		case TIMELINE:
			stripTimeline.setVisibility(View.VISIBLE);
			stripFeed.setVisibility(View.GONE);
			stripProfile.setVisibility(View.GONE);
			
			ivFeed.setBackgroundResource(R.drawable.main_feed);
			ivTimeline.setBackgroundResource(R.drawable.main_timeline_selected);
			ivProfile.setBackgroundResource(R.drawable.main_profile);
			break;
		case PROFILE:
			stripTimeline.setVisibility(View.GONE);
			stripFeed.setVisibility(View.GONE);
			stripProfile.setVisibility(View.VISIBLE);
			
			ivFeed.setBackgroundResource(R.drawable.main_feed);
			ivTimeline.setBackgroundResource(R.drawable.main_timeline);
			ivProfile.setBackgroundResource(R.drawable.main_profile_selected);
			break;
		}
	}
	
	public void openNotificationDetails(long vid) {
		Intent i = new Intent(this,VideoDetailActivity.class);
		i.putExtra("vid",vid);
		startActivity(i);
	}
	
	public void editProfile(View v){
		Intent i = new Intent(this,EditProfileActivity.class);
		startActivity(i);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		WLog.d(this,"Entering Act Result: "+requestCode);
		switch(requestCode){
		case LOGOUT_CODE:
//			Log.d(TAG,"Returned log out");
			if (resultCode == RESULT_OK) {
//				Log.d(TAG,"Result okay sir");
				if (data.getBooleanExtra("logout", false)) {
					app.logout(false);
					Intent i = new Intent(getBaseContext(), EntranceActivity.class);
					startActivity(i);
					this.finish();
				}
			}
			break;
		case GROUP_CREATE:
			if(resultCode == RESULT_OK){
				getToast().setText(getString(R.string.toast_group_created)).setBackground(R.color.weclipse_green_75).show(WToast.MEDIUM);
			} else {
				WApp.deleteTempGroup();
			}
			break;
		case CODE_CAMERA:
			if (resultCode == RESULT_OK) {
				int target = data.getIntExtra("target", 0);
			
				ArrayList<Long> groupIds = new ArrayList<Long>();
				ArrayList<Long> contactIds = new ArrayList<Long>();
				
				long[] temp = data.getLongArrayExtra("sendingGroup");
				if(temp != null){
					for (int j = 0; j < temp.length; j++)
						groupIds.add(temp[j]);
				}
				
				
				long[] temp2 = data.getLongArrayExtra("sendingContact");
				if(temp2 != null){
					for (int j = 0; j < temp2.length; j++)
						contactIds.add(temp2[j]);
				}
				
				new CreateVideoTask(app,
						new VideoPack(
						data.getIntExtra("players",0),
						data.getStringExtra("eta"),
						data.getStringExtra("filePath"),
						data.getStringExtra("caption"),
						data.getLongExtra("duration", 0),
						data.getIntExtra("media_type",0),
						data.getIntExtra("private_video",0)), groupIds,contactIds, target).execute();
				
			} else {
				FileManager.getInstance(app).cleanVideos();
			}
			break;
		case CODE_REPLY:
			WLog.d(this,"Code reply!");
			if (resultCode == RESULT_OK) {
				WLog.d(this,"Result OK!");
				new ReplyVideoTask(app, this,
						data.getLongExtra("user_id", 0),
						data.getLongExtra("video_id", 0),
						data.getStringExtra("filePath"),
						data.getLongExtra("duration", 0),
						data.getIntExtra("media_type",0));

			} else {
				FileManager.getInstance(app).cleanVideos();
			}
			break;
		case CODE_VIDEO:
			if(resultCode == RESULT_OK){
				
			} else {
				
			}
			break;
		}
		
		uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
	        @Override
	        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
	            Log.e("Activity", String.format("Error: %s", error.toString()));
	        }

	        @Override
	        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
	            Log.i("Activity", "Success!");
	        }
	    });
	}
	
	public void onFeedClick(View v){
		if(currentPage == FEED){
			if(((FeedFragment)mPagerAdapter.getCurrentFragment()) != null){
				((FeedFragment)mPagerAdapter.getCurrentFragment()).scrollTop();
			} else {
				//Do nothing? null?
			}
		}
		
		currentPage = FEED;
		drawStrip();
		mPager.setCurrentItem(FEED,true);
	}
	
	public void onTimelineClick(View v){
		if(currentPage == TIMELINE){
			mPagerAdapter.getCurrentFragment();	
			((TimelineFragment)mPagerAdapter.getCurrentFragment()).scrollTop();
		}
		
		currentPage = TIMELINE;
		drawStrip();
		mPager.setCurrentItem(TIMELINE,true);
	}
	
	public void onProfileClick(View v){
		if(currentPage == PROFILE){
			((ProfileFragment)mPagerAdapter.getCurrentFragment()).scrollTop();
		}
		currentPage = PROFILE;
		drawStrip();
		mPager.setCurrentItem(PROFILE,true);
	}
	
	public class MyPagerAdapter extends FragmentPagerAdapter {
		
		Fragment mCurrentFragment;
		
		public MyPagerAdapter(FragmentManager fm){
			super(fm);
		}
		
		public Fragment getCurrentFragment(){
			return mCurrentFragment;
		}

		@Override
		public Fragment getItem(int position) {
			Fragment mReturnFragment = null;
			switch(position){
			case TIMELINE:
				mReturnFragment = new TimelineFragment();
				break;
			case FEED:
				mReturnFragment = new FeedFragment();
				break;
			case PROFILE:
				mReturnFragment = new ProfileFragment();
				break;
			}
			return mReturnFragment;
		}
		
		@Override
		public void setPrimaryItem(ViewGroup container, int position,Object object) {
			
			if(getCurrentFragment() != object)
				mCurrentFragment = (Fragment)object;
			
			super.setPrimaryItem(container, position, object);
		}

		@Override
		public int getCount() {
			return 3;
		}
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}
	
}
