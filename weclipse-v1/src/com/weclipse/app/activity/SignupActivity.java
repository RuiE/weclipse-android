package com.weclipse.app.activity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.tasks.SignupTask;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.LoginEvent;
import com.weclipse.app.utils.eventbus.PreLoginEvent;

public class SignupActivity extends BaseActivity implements TextWatcher {
	
	private static final int SEARCH_USERNAME = 0;
	private static final int SEARCH_EMAIL = 1;
	private static final int MINIMUM_PASSWORD_SIZE = 6;
	private static final int MAX_USERNAME_SIZE = 20;
	
	ProgressDialog dialog;
	Context context;
	
	public boolean isSigningUp;
	long tempId;
	EditText etPass,etEmail,etUsername,etName;
	CheckBox age;
	Button bSignup;
	
	boolean usernameAlreadyExists,emailAlreadyExists;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		context = this;
		setContentView(R.layout.activity_signup);
		
		getSupportActionBar().hide();
		
		initVariables();
	}
	
	public void initVariables(){
		
		dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.logging_in));
        
        etPass = (EditText)findViewById(R.id.etPass);
        etName = (EditText)findViewById(R.id.etName);
		etEmail = (EditText)findViewById(R.id.etEmail);
		etUsername = (EditText)findViewById(R.id.etUsername);
		
		etPass.addTextChangedListener(this);
		etName.addTextChangedListener(this);
		etEmail.addTextChangedListener(this);
		etUsername.addTextChangedListener(this);
		
		etUsername.setOnFocusChangeListener(new View.OnFocusChangeListener(){
	        @Override
	        public void onFocusChange(View v, boolean hasFocus){
	        	if(!hasFocus){
	        		if(!etUsername.getText().toString().equals("") && etUsername.getText().toString().matches("^[a-zA-Z0-9_]*$")){
	        			search(SEARCH_USERNAME);
	        		} else if(etUsername.getText().toString().length() > MAX_USERNAME_SIZE){
	        			getToast().setText(R.string.toast_long_username).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
	        		} else {
	        			getToast().setText(R.string.toast_invalid_username).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
	        		}
	        			
	        	}
	        }
	    });
		
		etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener(){
	        @Override
	        public void onFocusChange(View v, boolean hasFocus){
	        	if(!hasFocus){
	        		if(!etEmail.getText().toString().equals("") && etEmail.getText().toString().matches("^[a-zA-Z0-9\\._\\+-]+@[a-zA-Z0-9\\.-]+\\.[a-zA-Z]{2,4}$")){
	        			search(SEARCH_EMAIL);
	        		} else {
	        			getToast().setText(R.string.toast_invalid_email).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
	        		}
	        	}
	        }
	    });
		
		age = (CheckBox)findViewById(R.id.cbAge);
		
		age.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				validateColorChange();
			}
		});
		
		bSignup = (Button)findViewById(R.id.bNext);
		
        Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_SIGNUP);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		validateColorChange();
	}
	
	@Override
	public void onBackPressed() {
		startActivity(new Intent(this,EntranceActivity.class));
		this.finish();
	}
	
	public void login(){
		app.login();
		startActivity(new Intent(this,TheMainActivity.class));
		startActivity(new Intent(this,FindFriendsActivity.class));
		this.finish();
	}
	
	public void signUp(View v){
		if(validateSignup()){
			dialog.show();
			new SignupTask(app, etName.getText().toString(), 
								etUsername.getText().toString(),
								etEmail.getText().toString(),
								etPass.getText().toString());
		}
	}
	
	public void validateColorChange(){
		if(age.isChecked() && !etPass.getText().toString().equals("") && !etUsername.getText().toString().equals("") && !etEmail.getText().toString().equals("")){
			bSignup.setBackgroundResource(R.drawable.button_opaque_green);
			
			try {
				XmlResourceParser parser = getResources().getXml(R.drawable.button_green_white_text);
				ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
				bSignup.setTextColor(colors);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			bSignup.setBackgroundResource(R.drawable.button_opaque_gray);
			
			try {
				XmlResourceParser parser = getResources().getXml(R.drawable.button_gray_white_text);
				ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
				bSignup.setTextColor(colors);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	}
	
	public boolean validateSignup(){
		if(etEmail.getText().toString().equals("") || etUsername.getText().toString().equals("") || etPass.getText().toString().equals("") || etName.getText().toString().equals("")){
			getToast().setText(R.string.toast_empty_credentials).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		if(!etUsername.getText().toString().matches("^[a-zA-Z0-9_]*$")){
			getToast().setText(R.string.toast_invalid_username).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		if(!etEmail.getText().toString().matches("^[a-zA-Z0-9\\._\\+-]+@[a-zA-Z0-9\\.-]+\\.[a-zA-Z]{2,4}$")){
			getToast().setText(R.string.toast_invalid_email).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		if(etPass.getText().toString().length()<MINIMUM_PASSWORD_SIZE){
			getToast().setText(R.string.toast_password_length).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		if(usernameAlreadyExists){
			getToast().setText(R.string.toast_username_already).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		if(emailAlreadyExists){
			getToast().setText(R.string.toast_email_already).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		if(etUsername.getText().toString().length()>MAX_USERNAME_SIZE){
			getToast().setText(R.string.toast_long_username).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		if(!age.isChecked()){
			getToast().setText(R.string.age_warning).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		return true;
	}
	
	public void search(final int searchType){
		new AsyncTask<Integer, String, String>() {
			int code;

			@Override
			protected String doInBackground(Integer... params) {
				String result = "";
				MyHttpClient httpclient = new MyHttpClient(app);
				HttpResponse response;
	            HttpPost post = new HttpPost(WApp.BASE+WApp.VALIDATE);
	            HttpEntity entity;
	            try {
	            	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	            	if(searchType==SEARCH_USERNAME){
	            		nameValuePairs.add(new BasicNameValuePair("field","username"));
	            		nameValuePairs.add(new BasicNameValuePair("value",etUsername.getText().toString()));
	            	} else if(searchType==SEARCH_EMAIL){
	            		nameValuePairs.add(new BasicNameValuePair("field","email"));
	            		nameValuePairs.add(new BasicNameValuePair("value",etEmail.getText().toString()));
	            	}
	                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	                response = httpclient.execute(post);
	                entity = response.getEntity();
	
	                code = response.getStatusLine().getStatusCode();
	                if(entity!=null){
	                    InputStream instream = entity.getContent();
	                    result = WApp.convertStreamToString(instream);
	                }
	            }catch (ClientProtocolException e) {
	                e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
				return result;
			}
			
			protected void onPostExecute(String result) {
//				Log.d(TAG,"Result: "+result+"("+code+")");
				if(code==HTTPStatus.OK){
					if(searchType==SEARCH_USERNAME){
						usernameAlreadyExists=false;
						if(emailAlreadyExists){
							getToast().setText(R.string.toast_email_already);
						} else {
							getToast().hide();
						}
					} else {
						emailAlreadyExists=false;
						if(usernameAlreadyExists){
							getToast().setText(R.string.toast_username_already);
						} else {
							getToast().hide();
						}
					}
				} else if(code==HTTPStatus.BAD_REQUEST){
					getToast().setBackground(R.color.weclipse_red);
					if(searchType==SEARCH_USERNAME){
						getToast().setText(getString(R.string.toast_username_already));
						usernameAlreadyExists=true;
					} else {
						getToast().setText(getString(R.string.toast_email_already));
						emailAlreadyExists=true;
					}
					getToast().show();
				} else {
					
				}
			};
			
		}.execute();
	}
	
	@Subscribe
	public void onAsyncTaskResult(PreLoginEvent event){
		getToast().setText(R.string.toast_signingup).setBackground(R.color.dark_grey_75).show();
	}
	
	@Subscribe
	public void onAsyncTasksResult(LoginEvent event){
		dialog.dismiss();
		
		switch(event.getType()){
		case LoginEvent.TYPE_SUCCESS:
			login();
			break;
		}
	
	}	

}
