package com.weclipse.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.utils.AnalyticsHelper;

public class RecoverPasswordActivity extends BaseActivity {
	
	public static final String RESET_PASSWORD_URL = "http://api.weclipse.co/password/remind";
	
	WebView wv;
	WebViewClient wvc;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		setContentView(R.layout.activity_recover_password);
		initVariables();
	}
	
	public void initVariables(){		
		wv = (WebView)findViewById(R.id.webView);
		wvc = new WebViewClient(){
			@Override
		    public boolean shouldOverrideUrlLoading(WebView  view, String  url){
				wv.loadUrl(url);
		        return true;
		    }	
		};
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.header_recover_password_activity));
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_RECOVER_PASSWORD);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Intent i = getIntent();
		if(i.getData() != null){
			wv.loadUrl(getIntent().getData().toString());
		} else {
			if(WApp.DEBUG){
				wv.loadUrl("http://dev.weclipse.co/password/remind");
			} else {
				wv.loadUrl(RESET_PASSWORD_URL);
			}
			
		}
	}
	
}
