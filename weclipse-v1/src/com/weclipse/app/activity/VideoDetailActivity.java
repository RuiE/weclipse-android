package com.weclipse.app.activity;

import java.util.ArrayList;
import java.util.Collections;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.R;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.managers.DownloadManager;
import com.weclipse.app.managers.FileManager;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.MyLike;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.DeleteVideoTask;
import com.weclipse.app.tasks.DownloadVideoTask;
import com.weclipse.app.tasks.LikeVideoTask;
import com.weclipse.app.tasks.ReplyVideoTask;
import com.weclipse.app.tasks.ReportTask;
import com.weclipse.app.tasks.UnlikeVideoTask;
import com.weclipse.app.tasks.fetch.FetchVideoTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DeleteVideoEvent;
import com.weclipse.app.utils.eventbus.FetchVideoEvent;
import com.weclipse.app.utils.eventbus.InlineVideoStopEvent;
import com.weclipse.app.utils.eventbus.LikedVideoEvent;
import com.weclipse.app.utils.eventbus.ReportEvent;
import com.weclipse.app.utils.eventbus.UnlikedVideoEvent;
import com.weclipse.app.utils.eventbus.VideoDownloadEvent;
import com.weclipse.app.utils.eventbus.VideoReadyEvent;
import com.weclipse.app.utils.eventbus.WBus;
import com.weclipse.app.widget.InlineVideoPlayer;

public class VideoDetailActivity extends BaseActivity {
	
	public static final int CODE_REPLY = 11;
	
	ListView lvUsers;
	Cursor cursor;
	NotificationUsersAdapter adapter;
	
	ArrayList<DetailUser> detailUsers;
	
	OnClickListener playVideo;
	
	/* TextViews */
	TextView tvLikes,tvComments,tvViews;
	TextView tvTime,tvHost,tvCaption;
	TextView tvDetail,tvAvatar;
	
	/* Images */
	ImageView ivLike;
	ImageView ivThumbnail;
	ImageView ivHost;
	
	/* Containers */
	RelativeLayout rlContainer,rlHostContainer;
	RelativeLayout rlIconsContainer,rlReplyContainer,rlDetailContainer,rlDetailReadyContainer;
	RelativeLayout rlCommentContainer,rlLikeContainer,rlShareContainer;
	LinearLayout llRepliedContainer;
	
	View header;
	
	MyOnClickListener mOnClickListener;
	
	OnClickListener openProfile = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent i = new Intent(VideoDetailActivity.this, ProfileActivity.class);
			i.putExtra("user_id", (Long) v.getTag());
			startActivity(i);
		}
	};
	
	InlineVideoPlayer currentIVP;
	public int position = -1;
	
	long vid;
	Video video;
	boolean liked;
	int replied;
	int total;
	
	ProgressBar loading;
	
	private UiLifecycleHelper uiHelper;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		vid = getIntent().getLongExtra("vid", 0);
		
		//Fazer o pedido do video
		new FetchVideoTask(app,vid);

//		WLog.d(this,"Users? "+video.getUsers().toString());
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.activity_video_detail);
		
		initVariables();
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_VIDEO_DETAIL);
	    t.send(new HitBuilders.AppViewBuilder().build());	
	    
	    uiHelper = new UiLifecycleHelper(this, null);
	    uiHelper.onCreate(b);
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    
	    if(currentIVP != null){
	    	currentIVP.stop();
	    	currentIVP = null;
	    }
	    
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			Intent i = new Intent(this,TheMainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(i);
	    }
		return super.onOptionsItemSelected(item);
	}
	
	private void initVariables(){
		lvUsers = (ListView)findViewById(R.id.lvUsers);
		lvUsers.setDividerHeight(0);
		
		//Add header to listview
		header = LayoutInflater.from(this).inflate(R.layout.header_detail, null);
		
		/* Newly design */
		tvLikes = (TextView)header.findViewById(R.id.tvLikes);
		tvComments = (TextView)header.findViewById(R.id.tvComments);
		tvViews = (TextView)header.findViewById(R.id.tvViews);
		tvHost = (TextView)header.findViewById(R.id.tvHost);
		tvTime = (TextView)header.findViewById(R.id.tvTime);
		tvCaption = (TextView)header.findViewById(R.id.tvCaption);
		tvDetail = (TextView)header.findViewById(R.id.tvDetail);
		tvAvatar = (TextView)header.findViewById(R.id.tvAvatar);
		
		ivLike = (ImageView)header.findViewById(R.id.ivLikes);
		ivThumbnail = (ImageView)header.findViewById(R.id.ivImage);
		ivHost = (ImageView)header.findViewById(R.id.ivHost); 
		
		rlContainer = (RelativeLayout)header.findViewById(R.id.rlContainer);
		rlLikeContainer = (RelativeLayout)header.findViewById(R.id.rlLikeContainer);
		rlCommentContainer = (RelativeLayout)header.findViewById(R.id.rlCommentsContainer);
		rlShareContainer = (RelativeLayout)header.findViewById(R.id.rlShareContainer);
		rlIconsContainer = (RelativeLayout)header.findViewById(R.id.rlIconsContainer);
		rlReplyContainer = (RelativeLayout)header.findViewById(R.id.rlReplyContainer);
		rlDetailContainer = (RelativeLayout)header.findViewById(R.id.rlDetailContainer);
		rlDetailReadyContainer = (RelativeLayout)header.findViewById(R.id.rlDetailReadyContainer);
		rlHostContainer = (RelativeLayout)header.findViewById(R.id.rlHost);
		llRepliedContainer = (LinearLayout)header.findViewById(R.id.llRepliedContainer);
	
		lvUsers.addHeaderView(header);
		
		playVideo = new OnClickListener() {	
			@Override
			public void onClick(View v) {
				
				if(WApp.INLINE){
					
					if(currentIVP == null){
						if(!DownloadManager.getInstance(app).containsKey(video.getId())){
							DownloadVideoTask mTask = new DownloadVideoTask(app,video);
							DownloadManager.getInstance(app).put(video.getId(), mTask);
						}
						
						InlineVideoPlayer ivp = new InlineVideoPlayer(video, app, (RelativeLayout)v);
						ivp.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								WLog.d(this,"Clickei no IVP lolol");
							}
						});
						currentIVP = ivp;
						WBus.getInstance().register(ivp);
						
						RelativeLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
						ivp.setLayoutParams(params);
						
						((RelativeLayout)v).addView(ivp);
						((RelativeLayout)v).findViewById(R.id.videoProgress).setVisibility(View.VISIBLE);
						
					} else {
						WLog.d(this, "Already doing stuff, just wait");
					}
					
				} else {
					
					Intent i = new Intent(VideoDetailActivity.this, VideoActivity.class);
					i.putExtra("vid", video.getId());
					i.putExtra("caption",video.getTitle());
					startActivity(i);
					
				}
			}
		};
		
		loading = (ProgressBar)findViewById(R.id.pbLoading);
		
	}
	
	public void refreshUI(final boolean update){
//		WLog.d(this,"--Refreshing FeedDetailActii UI--");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				drawTop(update);
			}
		});
	}
	
	public void notifyContacts(){
//		WLog.d(this,"Notifying Contacts: "+showingUsers);
		notifyListView();
	}
	
	public void notifyListView(){
		updateListView();	
	}
	
	public void drawTop(boolean update){
		liked = new MyLike(this, vid, DBHelper.TABLE_LIKES_ACTIVITY).exists();
		User host = UserCache.getInstance(app).getUser(video.getOwner_id());
		String sname = host.getUsername();
		
		total = video.getPlayers();
		replied = video.getReplied();
		
		/* Private/Public icon */
		if(video.getPrivate_video() == Video.PRIVATE){
			findViewById(R.id.ivPrivate).setVisibility(View.VISIBLE);
		}
		
		tvHost.setText(sname + ( (video.getReplied()-1)>0?" + "+(video.getReplied()-1) : "" ));
		tvCaption.setText(video.getTitle());
		tvTime.setText(Utils.getTimePast(this,video.getCreated_at()));
		
		new AvatarDrawer(app, ivHost, host).draw();
		
		/* Assemble the detail text */
		String detail = "";
		
		if(video.getPrivate_video() == Video.PRIVATE){
			detail = getString(R.string.video_private)+" - ";
		} else {
			detail = getString(R.string.video_public)+" - ";
		}
		
		detail += Utils.etaToTime(video.getEta())+" "+getString(R.string.to_reply);
		
		tvDetail.setText(detail);
		
		/* Like button */
		if(liked){
			ivLike.setBackgroundResource(R.drawable.timeline_likes_pink);
		} else {
			ivLike.setBackgroundResource(R.drawable.timeline_likes_white);
		}
	
		WLog.d(this,"Este video - Status: "+video.getStatus());
		WLog.d(this,"Este video - Video Status: "+video.getVideo_status());
		WLog.d(this,"Este video - User Status: "+video.getUser_status());
		if(video.getStatus() == Video.ACTIVE){
			rlIconsContainer.setVisibility(View.GONE);
			if(video.getUser_status() == Video.REPLIED){
				llRepliedContainer.setVisibility(View.VISIBLE);
				int missingParticipants = total-replied;
				((TextView)(header.findViewById(R.id.tvNParticipants))).setText(getString(R.string.video_waiting_n).replace("_n_", ""+missingParticipants)+(missingParticipants>1?"s":""));
				rlReplyContainer.setVisibility(View.GONE);
			} else {
				llRepliedContainer.setVisibility(View.GONE);
				rlReplyContainer.setVisibility(View.VISIBLE);
			}
			rlDetailContainer.setVisibility(View.VISIBLE);
			rlDetailReadyContainer.setVisibility(View.GONE);
			getSupportActionBar().setTitle(getString(R.string.title_video_challenge));
			switch(video.getUser_status()){
			case Video.INVITED:
				break;
			case Video.REPLIED:
				break;
			}
		} else if(video.getStatus() == Video.READY){
			rlIconsContainer.setVisibility(View.VISIBLE);
			rlReplyContainer.setVisibility(View.GONE);
			llRepliedContainer.setVisibility(View.GONE);
			rlDetailContainer.setVisibility(View.GONE);
			rlDetailReadyContainer.setVisibility(View.VISIBLE);
			getSupportActionBar().setTitle(getString(R.string.title_video_ready));
			
			if(!update){
				Picasso.with(app)
						.load(video.getThumbnail())
						.into(ivThumbnail);
			}
			
			/* Verbose */
			tvLikes.setText(video.getN_likes()+" "+getString(R.string.timeline_video_likes));
			tvComments.setText(video.getN_comments()+" "+getString(R.string.timeline_video_comments));
			tvViews.setText(video.getN_views()+" "+getString(R.string.timeline_video_views));
			
			rlContainer.setOnClickListener(playVideo);
			
			//Listeners to open new DetailActivity
			OnClickListener commentListener = new MyOnClickListener(DetailActivity.TYPE_COMMENT);
			rlCommentContainer.setOnClickListener(commentListener);
			tvComments.setOnClickListener(commentListener);
			tvLikes.setOnClickListener(new MyOnClickListener(DetailActivity.TYPE_LIKE));
			rlHostContainer.setOnClickListener(new MyOnClickListener(DetailActivity.TYPE_PLAYERS));
			
			//That like
			rlLikeContainer.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(liked){
						ivLike.setBackgroundResource(R.drawable.timeline_likes_white);
						new UnlikeVideoTask(app,video,AbstractVideo.SOURCE_DETAIL);
					} else {
						ivLike.setBackgroundResource(R.drawable.timeline_likes_pink);
						new LikeVideoTask(app, video,AbstractVideo.SOURCE_DETAIL);
					}
				}
			});
			
			//That dialgo to report and do other actions
			rlShareContainer.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					videoOptions(video).show();
				}
			});
		}
		
	}
	
	public void onReply(View v){
		Intent i = new Intent(getBaseContext(), CameraActivity.class);
		i.putExtra("isReplying", true);
		i.putExtra("user_id", video.getOwner_id());
		i.putExtra("video_id", vid);
		i.putExtra("group_id", video.getGroup_id());
		i.putExtra("type", video.getTarget());
		startActivityForResult(i, CODE_REPLY);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		WLog.d(this,"Entering Act Result: "+requestCode);
		switch(requestCode){
		case CODE_REPLY:
			if (resultCode == RESULT_OK) {
				new ReplyVideoTask(app, this,
						data.getLongExtra("user_id", 0),
						data.getLongExtra("video_id", 0),
						data.getStringExtra("filePath"),
						data.getLongExtra("duration", 0),
						data.getIntExtra("media_type",0));
			} else {
				FileManager.getInstance(app).cleanVideos();
			}
		}
		
		uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
	        @Override
	        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
	            Log.e("Activity", String.format("Error: %s", error.toString()));
	        }

	        @Override
	        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
	            Log.i("Activity", "Success!");
	        }
	    });
	}
	
	public void updateListView(){
		fillArray();
		if(detailUsers != null){
			adapter = new NotificationUsersAdapter(this,detailUsers);
			lvUsers.setAdapter(adapter);
		}
	}
	
	public void fillArray(){
		detailUsers = new ArrayList<DetailUser>();
		
		for(int i = 0 ; i < video.getUsers().size() ; i++){
			User u = UserCache.getInstance(app).getUser(video.getUsers().get(i));
			int status = 1;
			status = video.getStatuses().get(i);
			detailUsers.add(new DetailUser(u,status,video.getRepliedAts().get(i)));
			
			if(status == Video.REPLIED)
				replied++;
		}
		
		Collections.sort(detailUsers);
	}
	
	private class DetailUser implements Comparable<DetailUser> {
		
		User u;
		int user_status;
		String replied_at;
		
		public DetailUser(User u,int user_status,String replied_at){
			this.u = u;
			this.user_status = user_status;
			this.replied_at = replied_at;
		}

		@Override
		public int compareTo(DetailUser another) {
			if((getRepliedAt().equals("null") || getRepliedAt() == null) && (another.getRepliedAt().equals("null") || another.getRepliedAt() == null))
				return 0;
			
			if(getRepliedAt() == null || getRepliedAt().equals("null"))
				return 1;
			
			if(another.getRepliedAt() == null || another.getRepliedAt().equals("null"))
				return -1;
			
			return Utils.isAfter(this.getRepliedAt(), another.getRepliedAt())?1:-1;
		}
		
		public User getUser(){
			return u;
		}
		
		public int getUserStatus(){
			return this.user_status;
		}
		
		public String getRepliedAt(){
			return this.replied_at;
		}
		
		public String toString(){
			return u.getUsername()+"("+getUserStatus()+") replied at "+getRepliedAt();
		}
		
	}
	
	private class NotificationUsersAdapter extends ArrayAdapter<DetailUser> {

		LayoutInflater mInflater;
		
		public NotificationUsersAdapter(Context context,ArrayList<DetailUser> values) {
			super(context, R.layout.row_explore_second,values);
			mInflater = LayoutInflater.from(context);
		}

		@Override
		 public View getView(int position, View convertView, ViewGroup parent) {
			 User u = detailUsers.get(position).getUser();
			 int stat = detailUsers.get(position).getUserStatus();
			 View rowView = mInflater.inflate(R.layout.row_explore_second, parent,false);
			 
			 ImageView avatar = (ImageView)rowView.findViewById(R.id.ivAvatar);
			 TextView tvAvatar = (TextView)rowView.findViewById(R.id.tvAvatar);
			 RelativeLayout rlAvatarContainer = (RelativeLayout)rowView.findViewById(R.id.srlNumberBox);
			 avatar.setTag(u.getId());
			 String sname;
			 
			 int status = 0;
			 
			 if(u.getId() == app.getId()){
				 sname = Me.getInstance(VideoDetailActivity.this).getUsername();
				 ((TextView)rowView.findViewById(R.id.tvName)).setText(getString(R.string.you));	 
			 } else {
				 sname = u.getUsername();
				 ((TextView)rowView.findViewById(R.id.tvName)).setText(sname);
			 }
			 
			 TextView tvAux = (TextView)rowView.findViewById(R.id.tvAux);
			 tvAux.setTextColor(getResources().getColor(R.color.weclipse_green));
			 tvAux.setVisibility(View.VISIBLE);
			 if(u.getId() == video.getOwner_id()){
				 tvAux.setText(getString(R.string.host));
			 } else {
				 if(stat == 1){
					 tvAux.setText(getString(R.string.replied)+" "+Utils.getTimePast(VideoDetailActivity.this,detailUsers.get(position).getRepliedAt()));
				 } else {
					 if(u.getId() == app.getId()){//owner
						 tvAux.setText(getString(R.string.havent_replied));
					 } else {
						 tvAux.setText(getString(R.string.hasnt_replied));
					 }
					 tvAux.setTextColor(getResources().getColor(R.color.comment_gray));
				 }
			 }
			 
			 new AvatarDrawer(app, avatar, u).draw();
			 
			 rlAvatarContainer.setTag(u.getId());
			 rlAvatarContainer.setOnClickListener(openProfile);
			 
			 return rowView;
		 }	 
	}
	
	public Dialog videoOptions(final Video video) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    
	    String[] options = null;
	    
	    if(video.getPrivate_video() == Video.PUBLIC){
	    	options = getResources().getStringArray(R.array.timeline_options);
	    } else {
	    	options = getResources().getStringArray(R.array.timeline_options_private);
	    }
	    
	    builder.setItems(options, new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
            	   switch(which){
            	   case 0:// Report
            		   new ReportTask(app,video.getId());
            		   break;
            	   case 1://Delete
            		   new DeleteVideoTask(app,video.getId());
            		   break;
            	   case 2:// Share on facebook
            		   FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(VideoDetailActivity.this)
           	        		.setLink(video.getShare_url())
       	        			.build();
            		   uiHelper.trackPendingDialogCall(shareDialog.present());
            		   break;
            	   }
        	   }
	    });
	    builder.setTitle(video.getTitle());
	    return builder.create();
	}
	
	public class MyOnClickListener implements OnClickListener {

		int type;
		
		public MyOnClickListener(int type) {
			this.type = type;
		}
		
		@Override
		public void onClick(View v) {
			Intent i = new Intent(VideoDetailActivity.this,DetailActivity.class);
			i.putExtra("type",type);
			i.putExtra("vid",video.getId());
			i.putExtra("is_timeline",false);
			i.putExtra("caption",video.getTitle());
			startActivity(i);
		}
		
	}
	
	/*
	 * Subscriptions
	 */
	@Subscribe
	public void onAsyncTaskResult(VideoReadyEvent event){
		if(vid == event.getId()){
			refreshUI(true);
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(VideoDownloadEvent event){
		if(vid == event.getId()){
			refreshUI(true);
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(FetchVideoEvent event){
		loading.setVisibility(View.GONE);
		if(vid == event.getVideo().getId()){
			this.video = event.getVideo();
			updateListView();
			drawTop(false);
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(ReportEvent event){
		if(event.getSuccess()){
			Toast.makeText(this, getString(R.string.toast_report), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(LikedVideoEvent event){
		if(vid == event.getId()){
			video.setN_likes(video.getN_likes()+1);
			drawTop(false);
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnlikedVideoEvent event){
		if(vid == event.getId()){
			video.setN_likes(video.getN_likes()-1);
			drawTop(false);
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(InlineVideoStopEvent event){
		position = -1;
		currentIVP = null;
		
		if(event.getType() == InlineVideoStopEvent.CORRUPTED){
			Toast.makeText(this, "Corrupted file. Sorry for the incovenience", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(DeleteVideoEvent event){
		if(event.getId() == vid){
			if(event.getSuccess()){
				this.onBackPressed();
			} else {
				
			}
		}
	}

}
