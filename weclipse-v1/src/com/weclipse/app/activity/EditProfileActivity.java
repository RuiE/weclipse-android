package com.weclipse.app.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.weclipse.app.WApp;
import com.weclipse.app.R;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SendAvatarTask;
import com.weclipse.app.tasks.UpdateMeTask;
import com.weclipse.app.utils.CircleTransform;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.SendAvatarEvent;
import com.weclipse.app.utils.eventbus.UpdateMeEvent;

import eu.janmuller.android.simplecropimage.CropImage;

public class EditProfileActivity extends BaseActivity {

	public static final int PRIVATE = 3;
	public static final int NAME = 4;

	private static final int PICTURE_TAKE = 4;
	private static final int PICTURE_SELECT = 5;
	private static final int PICTURE_CROP = 6;
	Uri picUri;

	Switch togglePrivacy;
	Me mMe;

	TextView tvName, tvUsername;
	RelativeLayout rlNameContainer;

	AlertDialog.Builder builder;

	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);

		setContentView(R.layout.activity_edit_profile);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.profile_edit_header));

		initVariables();

	}

	public void initVariables() {
		mMe = Me.getInstance(app);
		
		new AvatarDrawer(app, (ImageView)findViewById(R.id.ivAvatar), mMe.getUser()).draw();
		
		togglePrivacy = (Switch) findViewById(R.id.togglePrivacy);
		togglePrivacy.setChecked(mMe.getPrivate_user() == User.PRIVATE);
		togglePrivacy.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// call the service that changes the user to private/public
				new UpdateMeTask(app, PRIVATE, togglePrivacy.isChecked() ? ""
						+ User.PRIVATE : "" + User.PUBLIC);
			}
		});

		tvName = (TextView) findViewById(R.id.tvName);
		tvUsername = (TextView) findViewById(R.id.tvUsername);

		tvName.setText(mMe.getName());
		tvUsername.setText(mMe.getUsername());

		builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.choose_from));
		builder.setItems(
				getResources().getStringArray(R.array.picture_options),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:// Camera
							takePicture();
							break;
						case 1:// Galeria
							selectAvatarPicture();
							break;
						}
					}
				});

		builder.create();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void changeName(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();
		final LinearLayout mView = (LinearLayout) inflater.inflate(
				R.layout.dialog_change_name, null);
		final EditText etName = (EditText) mView.findViewById(R.id.etName);
		builder.setView(mView)
				.setTitle(R.string.dialog_change_name_title)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						new UpdateMeTask(app, NAME, etName.getText().toString());
						dialog.dismiss();
					}
				})

				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		builder.create().show();
	}

	public void changeAvatar(View v) {
		builder.show();
	}

	public void selectAvatarPicture() {
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		pickPhoto.setType("image/*");
		pickPhoto.putExtra("crop", "true");
		pickPhoto.putExtra("aspectX", 1);
		pickPhoto.putExtra("aspectY", 1);
		pickPhoto.putExtra("outputX", 300);
		pickPhoto.putExtra("outputY", 300);
//		pickPhoto.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
//		pickPhoto.putExtra("outputFormat", Bitmap.CompressFormat.PNG.toString());
		// pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(pickPhoto, PICTURE_SELECT);
	}

	private Uri getTempUri() {
		return Uri.fromFile(getTempFile());
	}

	private File getTempFile() {
		File f = new File(WApp.TEMP_AVA_LOC);
		
		if(f.exists()){
			WLog.d(this,"getTempFile - Já existe!");
			//do nothing
		} else {
			WLog.d(this,"getTempFile - Está a criar!");
			try {
				f.createNewFile();
			} catch (IOException e) {
	
			}
		}
		
		return f;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case PICTURE_SELECT:
				Uri selectedImage = data.getData();
				
				String path = Utils.getPath(this, selectedImage);
				
				WLog.d(this,"Path: "+path);
				WLog.d(this,"Selected image: "+selectedImage.toString());
				if(path != null){
					Utils.copyFile(path, WApp.TEMP_AVA_LOC);
				} else {
					//Não está no device, vamos guarda-lo
					try {
						Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
						File f = new File(WApp.TEMP_AVA_LOC);
						if(f.exists()){
							f.delete();
							f.createNewFile();
						}
						FileOutputStream outStream = new FileOutputStream(f);
						bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
						outStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
//					Picasso.with(app)
//						.load(selectedImage)
//						.memoryPolicy(MemoryPolicy.NO_CACHE)
//						.networkPolicy(NetworkPolicy.NO_CACHE)
//						.transform(new CircleTransform())
//						.into((ImageView)findViewById(R.id.ivAvatar));		
//					WLog.d(this,"PAth Está a null!!!");
				}
			case PICTURE_CROP:
				new SendAvatarTask(app).execute();
				break;
			case PICTURE_TAKE:
				cropCapturedImage();
				break;
			}
		}
	}

	public void cropCapturedImage() {
		Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, WApp.TEMP_AVA_LOC);
		intent.putExtra(CropImage.SCALE, true);
		intent.putExtra(CropImage.ASPECT_X, 1);
		intent.putExtra(CropImage.ASPECT_Y, 1);
		startActivityForResult(intent, PICTURE_CROP);
	}

	public void takePicture() {
		File image = new File(WApp.TEMP_AVA_LOC);
		picUri = Uri.fromFile(image);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
		startActivityForResult(intent, PICTURE_TAKE);
	}

	/*
	 * Subscriptions Name - when name changes Avatar - when avatar changes
	 * privacy - when private user field changes
	 */
	@Subscribe
	public void onAsyncTaskResult(UpdateMeEvent event) {
		if (event.getSuccess()) {
			switch(event.getField()){
			case NAME:
				tvName.setText(mMe.getName());
				break;
			case PRIVATE:
				togglePrivacy.setChecked(mMe.getPrivate_user() == User.PRIVATE);
				break;
			}
		}

	}
	
	@Subscribe
	public void onAsyncTaskResult(SendAvatarEvent event){
		if(event.getSuccess()){
			
			mMe = Me.getInstance(app);
			
			Picasso.with(app)
				.load(mMe.getAvatar())
				.memoryPolicy(MemoryPolicy.NO_CACHE)
				.networkPolicy(NetworkPolicy.NO_CACHE)
				.transform(new CircleTransform())
				.into((ImageView)findViewById(R.id.ivAvatar));		
		}
	}

}
