package com.weclipse.app.activity;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.Session;
import com.facebook.SessionState;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.FollowingButtonDrawer;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.FacebookSynchronizeTask;
import com.weclipse.app.tasks.FollowTask;
import com.weclipse.app.tasks.UnfollowTask;
import com.weclipse.app.tasks.UnmergeFacebookTask;
import com.weclipse.app.tasks.fetch.FetchFacebookFriendsTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchFacebookFriendsEvent;
import com.weclipse.app.utils.eventbus.FollowEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.UnfollowEvent;
import com.weclipse.app.utils.eventbus.UnmergeFacebookEvent;

public class SyncFacebookActivity extends BaseActivity {

	RelativeLayout rlNoFacebook;
	RelativeLayout rlNoFacebookFriends;
	ListView lvFriends;
	Menu mMenu;
	
	boolean synced,hasFacebook;
	
	Session mSession;
	
	ArrayList<User> mUsers;
	FacebookFriendsAdapter mAdapter;
	
	OnClickListener openProfile = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent i = new Intent(SyncFacebookActivity.this, ProfileActivity.class);
			i.putExtra("user_id", (Long) v.getTag());
			startActivity(i);
		}
	};

	OnClickListener follow = new OnClickListener() {

		@Override
		public void onClick(View v) {
			int position = (Integer) v.getTag();
			WLog.d(this, "Clicked follow!");
			switch (mUsers.get(position).getFollow_status()) {
			case User.CLEAN_FOLLOW:
			case User.PENDING_ACCEPT:
				new UnfollowTask(app, mUsers.get(position).getId());
				v.setBackgroundResource(R.drawable.button_following);
				break;
			case User.NOTHING:
				new FollowTask(app, mUsers.get(position).getId());
				if(mUsers.get(position).isPrivate()){
					((Button)v).setText(getString(R.string.profile_button_follow_pending));
					v.setBackgroundResource(R.drawable.button_following_pending);
				} else {
					v.setBackgroundResource(R.drawable.button_following);
				}
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		
		setContentView(R.layout.activity_sync_facebook);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		initVariables();
		
		Tracker t = ((WApp)getApplication()).getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_FACEBOOK);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}
	
	public void initVariables(){
		
		rlNoFacebook = (RelativeLayout)findViewById(R.id.rlNoFacebook);
		rlNoFacebookFriends = (RelativeLayout)findViewById(R.id.rlNoFacebookFriends);
		lvFriends = (ListView)findViewById(R.id.lvFacebookFriends);
		
		hasFacebook = PreferenceManager.getInstance(app).getPreference(PreferenceManager.FACEBOOK);
		synced = Session.getActiveSession() != null && Session.getActiveSession().isOpened();

		if(hasFacebook){
			new FetchFacebookFriendsTask(app);
			getSupportActionBar().setTitle(getString(R.string.find_header_facebook_friends));
		} else {
			rlNoFacebook.setVisibility(View.VISIBLE);
			getSupportActionBar().setTitle(getString(R.string.find_header_facebook));
		}
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		hasFacebook = PreferenceManager.getInstance(app).getPreference(PreferenceManager.FACEBOOK);
		synced = Session.getActiveSession() != null && Session.getActiveSession().isOpened();
		
		updateMenu();
		
		if(mUsers != null){
			ArrayList<User> temp = new ArrayList<User>();
			for(int i = 0 ; i < mUsers.size() ; i++){
				WLog.d(this,"adding... "+i);
				temp.add(UserCache.getInstance(app).getUser(mUsers.get(i).getId()));
				WLog.d(this,"User: "+temp.get(i).toString());
			}
			mUsers = temp;
			
			if(mAdapter == null){
				mAdapter = new FacebookFriendsAdapter(this, mUsers);
				lvFriends.setAdapter(mAdapter);
			} else {
				mAdapter.swapData();
				mAdapter.notifyDataSetChanged();
			}
		} else {
			//This is a workaround.
			if(hasFacebook){
				new FetchFacebookFriendsTask(app);
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		case R.id.action_unmerge_fb:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.unmerge_fb_dialog_title));
			
			builder.setMessage(R.string.are_you_sure)
					.setPositiveButton(R.string.yes,
							new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog,int id) {
									new UnmergeFacebookTask(app);
								}
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// do nothing
								}
							});
			builder.create().show();
			
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		this.mMenu = menu;
		updateMenu();
		return true;
	}
	
	public void updateMenu(){
		if(mMenu != null){
			mMenu.clear();
			if(hasFacebook){
				getMenuInflater().inflate(R.menu.menu_unmerge_fb,mMenu);
				getSupportActionBar().setTitle(getString(R.string.find_header_facebook_friends));
			} else {
				getSupportActionBar().setTitle(getString(R.string.find_header_facebook));
			}
		} else {
			invalidateOptionsMenu();
		}
	}
	
	
	public void syncFacebook(View v){
		
		if(synced){
			Toast.makeText(this, "Lol synced already", Toast.LENGTH_SHORT).show();
		} else {
			if(hasFacebook){
				
			} else {
				ArrayList<String> permissions = new ArrayList<String>();
				permissions.add("email");
				permissions.add("public_profile");
				permissions.add("user_birthday");
				permissions.add("user_friends");
				
			    final Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, permissions);
			    Session openActiveSession = Session.openActiveSession(this, true, new Session.StatusCallback(){
			        @Override
			        public void call(Session session, SessionState state, Exception exception){
			        	
			        	if(state.isOpened()){
				        	if(mSession == null || isSessionChanged(session)){
				        		mSession = session;
				        		session.requestNewReadPermissions(newPermissionsRequest);
				                new FacebookSynchronizeTask(app,Session.getActiveSession().getAccessToken());
					    	}
			        	} else {
			        		WLog.d(this,"Logged out?");
			        	}
			        	
			        }
			        
			    });
			}
			
		}
	}
	
	private boolean isSessionChanged(Session session) {
	    // Check if session state changed
	    if (mSession.getState() != session.getState())
	        return true;

	    // Check if accessToken changed
	    if (mSession.getAccessToken() != null) {
	        if (!mSession.getAccessToken().equals(session.getAccessToken()))
	            return true;
	    }
	    else if (session.getAccessToken() != null) {
	        return true;
	    }

	    // Nothing changed
	    return false;
	}
	
	
	public void updateListView(){
		rlNoFacebook.setVisibility(View.GONE);
		
		if(mUsers.size() == 0){
			rlNoFacebookFriends.setVisibility(View.VISIBLE);
			lvFriends.setVisibility(View.GONE);
			return;
		} else {
			lvFriends.setVisibility(View.VISIBLE);
			rlNoFacebookFriends.setVisibility(View.GONE);
		}
		
		mAdapter = new FacebookFriendsAdapter(this, mUsers);
		lvFriends.setAdapter(mAdapter);
	}
	
	private class FacebookFriendsAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<User> values;

		public FacebookFriendsAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.row_list_following, values);
			mInflater = LayoutInflater.from(context);
			this.values = values;
		}

		public void swapData() {
			this.values = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_following, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			User user = values.get(position);

			new AvatarDrawer(app,holder.ivAvatar, user).draw();
			holder.rlContainer.setTag(user.getId());
			holder.rlContainer.setOnClickListener(openProfile);

			holder.tvName.setText(user.getUsername());
			holder.tvTime.setText(user.getName());

			if (user.getId() == Me.getInstance(SyncFacebookActivity.this).getId()) {
				holder.bAction.setVisibility(View.INVISIBLE);
			} else {
				holder.bAction.setVisibility(View.VISIBLE);

				new FollowingButtonDrawer(SyncFacebookActivity.this,user.getFollow_status(), holder.bAction).draw();

				holder.bAction.setTag(position);
				holder.bAction.setOnClickListener(follow);
			}

			return v;
		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlContainer;
			ToggleButton bAction;
		}

	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if(Session.getActiveSession() != null)
        	Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }
	
	@Subscribe
	public void onAsyncTaskResult(FetchFacebookFriendsEvent event){
		if(event.getSuccess()){
			PreferenceManager.getInstance(app).putPreference(PreferenceManager.FACEBOOK, true);
			mUsers = event.getUsers();
			updateListView();
		} else {
			//TODO alguma coisa a avisa q n deu ?
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(FollowEvent event) {

		for (int i = 0; i < mUsers.size(); i++) {
			if (mUsers.get(i).getId() == event.getUser_id()) {
				mUsers.get(i).setFollow_status(event.getStatus());
				break;
			}
		}

		mAdapter.swapData();
		mAdapter.notifyDataSetChanged();

	}

	@Subscribe
	public void onAsyncTaskResult(UnfollowEvent event) {
		for (int i = 0; i < mUsers.size(); i++) {
			if (mUsers.get(i).getId() == event.getUser_id()) {
				mUsers.get(i).setFollow_status(User.NOTHING);
			}
		}

		mAdapter.swapData();
		mAdapter.notifyDataSetChanged();
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnmergeFacebookEvent event){
		if(event.getSuccess()){
			mUsers = null;
			hasFacebook = PreferenceManager.getInstance(app).getPreference(PreferenceManager.FACEBOOK);
			synced = Session.getActiveSession() != null && Session.getActiveSession().isOpened();
			WLog.d(this,"Synced: "+synced+" hasFacebook: "+hasFacebook);
			rlNoFacebook.setVisibility(View.VISIBLE);
			rlNoFacebookFriends.setVisibility(View.GONE);
			lvFriends.setVisibility(View.GONE);
			updateMenu();
		} else {
			
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}
	
}
