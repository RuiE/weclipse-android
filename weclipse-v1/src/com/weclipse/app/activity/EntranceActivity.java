package com.weclipse.app.activity;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.squareup.otto.Subscribe;
import com.weclipse.app.WApp;
import com.weclipse.app.R;
import com.weclipse.app.fragment.EntranceFragment;
import com.weclipse.app.fragment.WalkthroughFragment;
import com.weclipse.app.managers.GCMManager;
import com.weclipse.app.managers.TutorialManager;
import com.weclipse.app.tasks.LoginFacebookTask;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.LoginEvent;
import com.weclipse.app.utils.eventbus.SignupEvent;

public class EntranceActivity extends BaseActivity {
	
	private static final int CODE_SELECT_USERNAME = 1;
	
    SharedPreferences prefs;
	
    public boolean hasGoogleServices;
    
	Session mSession;
	
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
    		onSessionStateChange(session, state, exception);
	    }
	};
	
	ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		try {
//	        PackageInfo info = getPackageManager().getPackageInfo(
//	        		this.getPackageName(), 
//	                PackageManager.GET_SIGNATURES);
//	        for (Signature signature : info.signatures) {
//	            MessageDigest md = MessageDigest.getInstance("SHA");
//	            md.update(signature.toByteArray());
//	            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//	            }
//	    } catch (NameNotFoundException e) {
//
//	    } catch (NoSuchAlgorithmException e) {
//
//	    }
		
		setContentView(R.layout.activity_entrance);
		hasGoogleServices = GCMManager.getInstance(app).checkPlayServices();
		
		if(((WApp)getApplication()).isLogged()){
			Intent i = new Intent(this,TheMainActivity.class);
			startActivity(i);
			this.finish();
		}
		
		if(TutorialManager.getInstance(app).hasDoneWalkthrough()){
			getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new EntranceFragment()).commit();			
		} else {
			getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new WalkthroughFragment()).commit();
		}
		
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		
		initVariables();
		
	}
	
	public void changeFragment(){
		getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new EntranceFragment()).commit();
	}
	
	public void initVariables(){
		dialog = new ProgressDialog(this);
		dialog.setIndeterminate(true);
		dialog.setMessage(getString(R.string.logging_in));
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(!hasGoogleServices){
			GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this),this,WApp.PLAY_SERVICES_RESOLUTION_REQUEST).show();
		} else {
			
		}
		
		AppEventsLogger.activateApp(this);
		
		Session session = Session.getActiveSession();
	    if (session != null &&
	           (session.isOpened() || session.isClosed()) ) {
	        onSessionStateChange(session, session.getState(), null);
	    }
		
		uiHelper.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		AppEventsLogger.deactivateApp(this);
		uiHelper.onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		
		switch(requestCode){
		case CODE_SELECT_USERNAME:
			if(resultCode == Activity.RESULT_OK){
				app.login();
				startActivity(new Intent(this,TheMainActivity.class));
				startActivity(new Intent(this,FindFriendsActivity.class));
				this.finish();
			} else {
				Session.getActiveSession().closeAndClearTokenInformation();
			}
			break;
		}
		
	}
	
	private boolean isSessionChanged(Session session) {
	    // Check if session state changed
	    if (mSession.getState() != session.getState())
	        return true;

	    // Check if accessToken changed
	    if (mSession.getAccessToken() != null) {
	        if (!mSession.getAccessToken().equals(session.getAccessToken()))
	            return true;
	    }
	    else if (session.getAccessToken() != null) {
	        return true;
	    }

	    // Nothing changed
	    return false;
	}
	
	@SuppressWarnings("deprecation")
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		WLog.d(this,"Entered one last time!?");
	    if (state.isOpened()) {
	    	
	    	if(mSession == null || isSessionChanged(session)){
	    		dialog.show();
	    		mSession = session;
		    	session.getPermissions();
		    	for(int i = 0 ; i < session.getPermissions().size() ; i++){
		    		Log.d("MainAct","Tenho esta permissao--------- "+session.getPermissions().get(i));
		    	}
		    	
		    	if(!session.getPermissions().contains("email")){
//		    		Session.getActiveSession().closeAndClearTokenInformation();
		    		Toast.makeText(this, "Email must be provided", Toast.LENGTH_SHORT).show();
//		    		WLog.d(this,"DESLIGADO ----------- NAO DESTE EMAILE?!?");
		    		List<String> permissions = Arrays.asList("email");
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(EntranceActivity.this, permissions);
		    		Session.getActiveSession().requestNewReadPermissions(newPermissionsRequest);
		    		dialog.dismiss();
		    		return;
		    	}
		        Log.i("MainAct", "Logged in...");
		        
		        new LoginFacebookTask(app, Session.getActiveSession().getAccessToken());
	    	} else {
	    		WLog.d(this,"Estado estranho");
	    		
	    		WLog.d(this,"Session null: "+(mSession == null));
	    		WLog.d(this,"Session open: "+(mSession.isOpened()));
	    		
	    		if(mSession != null && mSession.isOpened()){
	    			Session.getActiveSession().closeAndClearTokenInformation();
	    		}
	    	}
	    } else if (state.isClosed()) {
	        Log.i("MainAct", "Logged out...");
	    }
	}
	
	@Subscribe
	public void onAsyncTasksResult(LoginEvent event){
		dialog.cancel();
		switch(event.getType()){
		case LoginEvent.TYPE_SUCCESS:
			app.login();
			startActivity(new Intent(this,TheMainActivity.class));
			this.finish();
			break;
		case LoginEvent.TYPE_FAIL:
			if (Session.getActiveSession() != null && Session.getActiveSession().isOpened()) {
				Session.getActiveSession().closeAndClearTokenInformation();
			}
			break;
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(SignupEvent event){
		dialog.cancel();
		startActivityForResult(new Intent(this,InsertUsernameActivity.class), CODE_SELECT_USERNAME);
	}
}
