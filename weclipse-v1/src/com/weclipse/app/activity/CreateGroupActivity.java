package com.weclipse.app.activity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.CircleTransform;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.widget.LightTextView;
import com.weclipse.app.widget.SquareRelativeLayout;

import eu.janmuller.android.simplecropimage.CropImage;

public class CreateGroupActivity extends BaseActivity implements OnClickListener {
	
	private static final String TAG = "CreateGroup Screen";
	
	private static final int FRIEND_CHOSER_CREATE_CODE = 2;
	private static final int GROUP_PICTURE_TAKE = 4;
	private static final int GROUP_PICTURE_CROP = 5;
	private static final int GROUP_PICTURE_SELECT = 6;
	
	Button bAddMember,bUploadImage;
	RelativeLayout bCreate,rlBanner;
	EditText etGroupName;
	ScrollView svContainer;
	ArrayList<Long> weclipseIds;
	LinearLayout membersContainer;
	TextView adminName,tvUsername;
	boolean isCreatingGroup,hasAvatar;
	ImageView banner;
	LightTextView tvNrMembers;
	ImageView avatar;
	AlertDialog.Builder builder;
	Typeface light;
	Uri tempUri;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_create_group);
		initVariables();
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.header_create_group_activity));
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_CREATE_GROUP);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@SuppressLint("NewApi")
	public void initVariables(){
		light = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
		svContainer = (ScrollView)findViewById(R.id.svContainer);
		svContainer.setOverScrollMode(View.OVER_SCROLL_NEVER);
		
		bCreate = (RelativeLayout)findViewById(R.id.bCreateGroup);
		bCreate.setOnClickListener(this);
		
		bUploadImage = (Button)findViewById(R.id.bUploadImage);
		bUploadImage.setOnClickListener(this);
		
		bAddMember = (Button)findViewById(R.id.bAddMember);
		bAddMember.setOnClickListener(this);
		bAddMember.setTypeface(light);
		
		etGroupName = (EditText)findViewById(R.id.etGroupName);
		
		etGroupName.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				validateColorChange();
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
		
		membersContainer = (LinearLayout)findViewById(R.id.llMembersContainer);
		
		String username = Me.getInstance(this).getUsername();
		
		adminName = (TextView)findViewById(R.id.tvName);
		adminName.setText(username);
		
		tvUsername = (TextView)findViewById(R.id.tvUsername);
		tvUsername.setTypeface(light);
		
		tvNrMembers = (LightTextView)findViewById(R.id.tvNrMembers);
		avatar = (ImageView)findViewById(R.id.ivAvatar);
		
		Picasso.with(app)
				.load(Me.getInstance(this).getAvatar())
				.transform(new CircleTransform())
				.into(avatar);
		
		weclipseIds = new ArrayList<Long>();
		weclipseIds.add(Me.getInstance(this).getId());
		
		rlBanner = (RelativeLayout)findViewById(R.id.rlGroupBanner);
		
		Display dip = getWindowManager().getDefaultDisplay();
		int displayWidth;
		if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2){
			Point size = new Point();
			dip.getSize(size);
			displayWidth = size.x;
		} else {
			displayWidth = dip.getWidth();
		}
		float height = (float)9/16*displayWidth;
		LinearLayout.LayoutParams params = (LayoutParams) rlBanner.getLayoutParams();
		params.height = Math.round(height);
		
		banner = (ImageView)findViewById(R.id.ivBanner);
		
		isCreatingGroup = false;
		builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.choose_from));
        builder.setItems(getResources().getStringArray(R.array.picture_options),
        					new DialogInterface.OnClickListener() {
					            public void onClick(DialogInterface dialog, int which) {
					                switch (which){
					                    case 0://Camera
					                    	takeGroupPicture();
					                        break;
					                    case 1://Galeria
					                        selectGroupPicture();
					                        break;
					                }
					            }
					        });
        builder.create();
        
	    hasAvatar = false;
	    
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			switch(requestCode){
			case FRIEND_CHOSER_CREATE_CODE:
				ArrayList<Long> ids = new ArrayList<Long>();
				long[] temp = data.getLongArrayExtra("sending");
				for (int j = 0; j < temp.length; j++){
					WLog.d(this,"Este veio do friend choser "+temp[j]);
					ids.add(temp[j]);
				}

				updateMembers(ids);
				break;
			case GROUP_PICTURE_TAKE:
				cropCapturedImage();
				break;
			case GROUP_PICTURE_CROP:
			case GROUP_PICTURE_SELECT:
				updateGroupPicture();
				break;
			}
		} else {
			//do nothing
		}
		
	}
	
	public void validateColorChange(){
		if(!etGroupName.getText().toString().equals("") && weclipseIds.size()>=2 ){
			bCreate.setBackgroundResource(R.color.weclipse_green);
		} else {
			bCreate.setBackgroundResource(R.color.comment_gray);
		}
	}
	
	private Uri getTempUri() {
	    return Uri.fromFile(getTempFile());
    }
	
	private File getTempFile() {
	    File f = new File(WApp.TEMP_GROUP_LOC);
	    try {
	    	f.createNewFile();
	    } catch (IOException e) {
	    	
	    }
	    return f;
    }
		
	public void takeGroupPicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		tempUri = getTempUri();
		intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
		startActivityForResult(intent, GROUP_PICTURE_TAKE);
	}

	/*
	 * This function will select and crop
	 */
	public void selectGroupPicture() {
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		pickPhoto.setType("image/*");
		pickPhoto.putExtra("aspectX", 16);
		pickPhoto.putExtra("aspectY", 9);
		pickPhoto.putExtra("outputX", Group.GROUP_BANNER_WIDTH);
		pickPhoto.putExtra("outputY", Group.GROUP_BANNER_HEIGHT);
		pickPhoto.putExtra("crop","true");
		pickPhoto.putExtra(MediaStore.EXTRA_OUTPUT,getTempUri());
		pickPhoto.putExtra("outputFormat",Bitmap.CompressFormat.JPEG.toString());
//		pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(pickPhoto, GROUP_PICTURE_SELECT);
	}

	public void cropCapturedImage() {
		Intent intent = new Intent(this, CropImage.class);

	    // tell CropImage activity to look for image to crop
	    intent.putExtra(CropImage.IMAGE_PATH, WApp.TEMP_GROUP_LOC);

	    // allow CropImage activity to rescale image
	    intent.putExtra(CropImage.SCALE, true);

	    // if the aspect ratio is fixed to ratio 16/9
	    intent.putExtra(CropImage.ASPECT_X, 16);
	    intent.putExtra(CropImage.ASPECT_Y, 9);

	    // start activity CropImage with certain request code and listen
	    // for result
	    startActivityForResult(intent, GROUP_PICTURE_CROP);
//		Intent cropIntent = new Intent("com.android.camera.action.CROP");
//		cropIntent.setDataAndType(tempUri, "image/*");
//		cropIntent.putExtra("aspectX", 16);
//		cropIntent.putExtra("aspectY", 9);
//		cropIntent.putExtra("outputX", 320);
//		cropIntent.putExtra("outputY", 180);
//		cropIntent.putExtra("crop","true");
//		cropIntent.putExtra(MediaStore.EXTRA_OUTPUT,tempUri);
//		startActivityForResult(cropIntent, GROUP_PICTURE_CROP);
	}
	
	public void updateMembers(ArrayList<Long> ids){
		weclipseIds.clear();
		weclipseIds.add(Me.getInstance(this).getId());
		
		for(int i  = 0 ; i < ids.size() ; i++ ){
			User u = UserCache.getInstance(app).getUser(ids.get(i));
			RelativeLayout layout = (RelativeLayout)getLayoutInflater().inflate(R.layout.row_group_member, null);
			layout.setTag(u.getId());
			SquareRelativeLayout srlRemove = (SquareRelativeLayout)layout.findViewById(R.id.srlRemove);
			srlRemove.setVisibility(View.VISIBLE);
			srlRemove.setTag(ids.get(i));
			srlRemove.setOnClickListener(this);
			
			((TextView)layout.findViewById(R.id.tvName)).setText(u.getUsername());
			
			new AvatarDrawer(app, (ImageView)layout.findViewById(R.id.ivAvatar), u).draw();
			
			
			membersContainer.addView(layout);
			weclipseIds.add(u.getId());
			validateColorChange();
		}
	}
	
	public void updatePicture(Uri picUri){
		banner.setImageURI(picUri);
		bUploadImage.bringToFront();
	}
	
	/** Create group auxiliary methods */
	public void chooseMembers(ArrayList<Long> weclipseIds) {
		Intent i = new Intent(getBaseContext(), FriendChoserActivity.class);

		if (weclipseIds != null){
			i.putExtra("ids", weclipseIds);
		}
		startActivityForResult(i, FRIEND_CHOSER_CREATE_CODE);
	}

	public void updateGroupPicture() {
		hasAvatar = true;
		updatePicture(Uri.fromFile(new File(WApp.TEMP_GROUP_LOC)));
	}
	
	public boolean validateCreateGroup(){
		if(etGroupName.getText().toString().length() == 0){
			svContainer.scrollTo(0,(int)etGroupName.getY());
			etGroupName.requestFocus();
			InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
			imm.showSoftInput(etGroupName,InputMethodManager.SHOW_IMPLICIT);
			return false;
		}
		
		WLog.d(this, "Members inside");
		
		for(int i = 0 ; i < weclipseIds.size() ; i++){
			WLog.d(this,"Id: "+weclipseIds.get(i));
		}
		
		if(weclipseIds.size() < 2){
			getToast().setText(getString(R.string.toast_group_minimum_members)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		return true;
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bAddMember:
			chooseMembers(weclipseIds);	
			break;
		case R.id.bCreateGroup:
			if(!isCreatingGroup){
				if(validateCreateGroup()){
					new createGroup().execute();
				}
			}
			break;
		case R.id.srlRemove:
			User u = UserCache.getInstance(app).getUser((Long)v.getTag());
			for(int i = 0 ; i < membersContainer.getChildCount() ; i++ ){
				if((Long)membersContainer.getChildAt(i).getTag() == u.getId()){
					membersContainer.removeViewAt(i);
				}
			}
			for(int i = 0 ; i < weclipseIds.size() ; i++ ){
				if(weclipseIds.get(i) == u.getId())
					weclipseIds.remove(i);
			}
			validateColorChange();
			break;
		case R.id.bUploadImage:
			builder.show();
			break;
		}
	}
	
	public class createGroup extends AsyncTask<Integer, Integer, String>{

		int code;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			isCreatingGroup = true;
			getToast().setText(getString(R.string.toast_creating_group)).setBackground(R.color.dark_grey_75).show();
		}
		
		protected String doInBackground(Integer... params) {
			String endpoint = WApp.BASE+WApp.CREATE_GROUP;
//			Log.d(TAG,"Endpoint: "+endpoint);
			code = 0;
			String result = null;
			MyHttpClient httpclient = new MyHttpClient(app);
			HttpPost post = new HttpPost(endpoint);
			HttpResponse  response;
			try {
				File f = new File(WApp.TEMP_GROUP_LOC);
				MultipartEntity postEntity = new MultipartEntity();
				postEntity.addPart("name", new StringBody(""+etGroupName.getText().toString(),Charset.forName("UTF-8")));
				postEntity.addPart("android_version",new StringBody(WApp.APP_VERSION));
				
				if(hasAvatar){
					postEntity.addPart("avatar", new FileBody(f, "image/jpeg"));
				}
				
				JSONArray jarray = new JSONArray();
				for(int i = 0 ; i < weclipseIds.size() ; i++ ){
					jarray.put(weclipseIds.get(i));
				}
				postEntity.addPart("user_ids", new StringBody(""+jarray.toString()));
				
				post.setEntity(postEntity);
				post.setHeader("Authorization","access_token="+app.getAccessToken());
				
				response = httpclient.execute(post);
				HttpEntity entity = response.getEntity();
				code = response.getStatusLine().getStatusCode();
				if(entity!=null){
					InputStream inStream = entity.getContent();
					result = WApp.convertStreamToString(inStream); 
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch(IllegalArgumentException e){
				e.printStackTrace();
			}
			
			WLog.d(this, "Ananas("+code+"): "+result);
			
			if(code == HTTPStatus.CREATED){
				try {
					JSONObject group = new JSONObject(result);
//					Log.d(TAG,"Result: "+result);
					Group.parse(app, group).create();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
//			Log.d(TAG,"DONE create Groups: "+result);
			getToast().hide();
			isCreatingGroup=false;
			if(code == HTTPStatus.CREATED){
				WApp.deleteTempGroup();
				setResult(RESULT_OK,null);
				CreateGroupActivity.this.finish();
			} else {
				getToast().setText(getString(R.string.toast_group_fail)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			}
		}
	}//end of asynctask
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}

}
