package com.weclipse.app.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.PendingVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.eventbus.CreatePendingVideoFinishedEvent;
import com.weclipse.app.utils.eventbus.CreatePendingVideoStartedEvent;
import com.weclipse.app.utils.eventbus.ProgressPendingCreateVideoEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.widget.ProgressWheel;
import com.weclipse.app.widget.SquareRelativeLayout;

public class FeedPendingDetailActivity extends BaseActivity {
	
	private static final String TAG = "FeedDetail Screen";
	
	ListView lvUsers;
	SquareRelativeLayout srlIconContainer;
	Cursor cursor;
	RelativeLayout progressContainer;
	Button bRetry;
	NotificationUsersAdapter adapter;
	User[] array;
	long pid;
	ImageView bQuickSend,bPlay;
	TextView tvTarget,tvAction,tvCaption,tvNoComments;
	PendingVideo pv;
	ProgressWheel pw;
	Group g;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		
		pid = getIntent().getLongExtra("pid", 0);
		
		pv = new PendingVideo(this, pid);
		pv.load();
		
		getSupportActionBar().setTitle(pv.getTitle());
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.activity_feed_detail_pending);
		
		initVariables();
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(TAG+"_users");
	    t.send(new HitBuilders.AppViewBuilder().build());
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initVariables(){
		lvUsers = (ListView)findViewById(R.id.lvNotificationDetails);
		lvUsers.setDividerHeight(0);
		
		// Concerning the top header
		tvCaption = (TextView)findViewById(R.id.tvFirstRow);
		tvTarget = (TextView)findViewById(R.id.tvSecondRow);
		progressContainer = (RelativeLayout)findViewById(R.id.progressContainer);
		pw = (ProgressWheel) findViewById(R.id.pw);
		bRetry = (Button)findViewById(R.id.bRetry);
		bRetry.setTag(pv.getId());
		bRetry.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				app.getOfflineManager().retryCreate((Long)v.getTag());
			}
		});
		
	}
	
	public void videoSent(long id){
		//Need to update the ui
		if(!pv.exists()){
			bRetry.setVisibility(View.GONE);
			progressContainer.setVisibility(View.GONE);
			Video v = new Video(this,id);
			v.load();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		drawTop();
		updateListView();	
	}
	
	public void notifyListView(){
		updateListView();	
	}
	
	/* 
	 * Draw the top layout
	 */
	public void drawTop(){
		if(pv.exists()){
			pv.load();
			if(pv.getTarget() == WApp.TARGET_GROUPS){
				g = new Group(this, pv.getGroups().get(0));
				g.load();
			}
			
			tvCaption.setText(pv.getTitle());
			
			if(pv.getTarget() == WApp.TARGET_FRIENDS){
				if(pv.getUsers().size()==1){
					tvTarget.setText(getString(R.string.to)+" "+UserCache.getInstance(app).getUsername(pv.getUsers().get(0)));
					
				} else {
					tvTarget.setText(getString(R.string.sent_to_friends));
				}
			} else {//Groups
				tvTarget.setText(getString(R.string.to)+" "+g.getName());
			}
		} else {
			this.finish();
		}
		
		if(app.getFeedLoadingManager().getPendingLoadingIds().contains(pv.getId())){
			bRetry.setVisibility(View.INVISIBLE);
			progressContainer.setVisibility(View.VISIBLE);
		}
		
	}
	
	public void updateListView(){
		fillArray();
		if(array!= null){
			adapter = new NotificationUsersAdapter(this,array);
			lvUsers.setAdapter(adapter);
		}	
	}
	
	public void updateCreatePendingProgress(int progress){
		
		pw.setProgress((int)((float)progress*3.6));
		
		if(progress == Utils.PROGRESS_FILE_UPLOAD){
			pw.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					pw.setProgress(pw.getProgress()+Utils.PROGRESS_STEP);
					
					if(pw.getProgress() >= (int)((float)Utils.PROGRESS_PROCESS_FULL_OUTPUT*3.6)){
						pw.removeCallbacks(this);
					} else {
						pw.postDelayed(this, 125);
					}
				}
			}, 125);
		}
		
		if(progress==Utils.PROGRESS_COMPLETE){
			pw.setBarColor(getResources().getColor(R.color.weclipse_green));
		}
		
	}
	
	public void fillArray(){
		ArrayList<Long> users;
		if(pv.getTarget() == WApp.TARGET_FRIENDS){
			users = pv.getUsers();
			users.add(0, app.getId());
		} else {
			users = g.getUsers();
		}
		array = new User[users.size()];
		
		for(int i=0;i<users.size();i++){
			
			User u = UserCache.getInstance(app).getUser(users.get(i));
			array[i] = u;
		}
		
		if(array[0].getId()==(pv.getUserId()))
			return;
		
		User temp;
		for(int j=1;j<array.length;j++){
			if(array[j].getId()==(pv.getUserId())){
				temp = array[0];
				array[0]=array[j];
				array[j]=temp;
				
				return;
			}
		}
	}
	
	private class NotificationUsersAdapter extends ArrayAdapter<User> {

		LayoutInflater mInflater;
		
		public NotificationUsersAdapter(Context context,User[] values) {
			super(context, R.layout.row_explore_second,values);
			mInflater = LayoutInflater.from(context);
		}

		@Override
		 public View getView(int position, View convertView, ViewGroup parent) {
			 User u = array[position];
			 View rowView = mInflater.inflate(R.layout.row_explore_second, parent,false);
			 
			 ImageView avatar = (ImageView)rowView.findViewById(R.id.ivAvatar);
			 avatar.setTag(u.getId());
			 String sname;
			 int status=0;
			 
			 if(u.getId()==app.getId()){
				 sname = Me.getInstance(FeedPendingDetailActivity.this).getUsername();
				 ((TextView)rowView.findViewById(R.id.tvName)).setText(getString(R.string.you));
//			 }else if(u.isContact()){
//				 status = u.getContact().getStatus();
//				 sname = u.getContact().getDisplayName();
//				 ((TextView)rowView.findViewById(R.id.tvName)).setText(sname);
			 } else {
				 sname = u.getUsername();
				 ((TextView)rowView.findViewById(R.id.tvName)).setText(sname);
			 }
			 TextView tvAux = (TextView)rowView.findViewById(R.id.tvAux);
			 tvAux.setTextColor(getResources().getColor(R.color.weclipse_green));
			 tvAux.setVisibility(View.VISIBLE);
			 if(u.getId()==pv.getUserId()){
				 tvAux.setText(getString(R.string.host));
			 } else {
				 if(u.getId()==app.getId()){//owner
					 tvAux.setText(getString(R.string.havent_replied));
				 } else {
					 tvAux.setText(getString(R.string.hasnt_replied));
				 }
				 tvAux.setTextColor(getResources().getColor(R.color.comment_gray));
			 }
			 
//			 Log.d(TAG,"User addable: "+u.getAddable());
//			 Log.d(TAG,"Video target: "+video.getTarget());
//			 Log.d(TAG,"Status: "+status);
			 
			 new AvatarDrawer(app, avatar, u).draw();
			 
			 return rowView;
		 }	 
	}
	
	/*
	 * Subscribed
	 */
	@Subscribe
	public void onAsyncTaskResult(CreatePendingVideoFinishedEvent event){
		if(event.getPid()==pv.getId()){
			if(event.getSuccess()){
				videoSent(event.getVid());
			} else {
				progressContainer.setVisibility(View.GONE);
				bRetry.setVisibility(View.VISIBLE);
			}
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(CreatePendingVideoStartedEvent event){
		if(event.getPid()==pv.getId()){
			progressContainer.setVisibility(View.VISIBLE);
			bRetry.setVisibility(View.INVISIBLE);
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(ProgressPendingCreateVideoEvent event){
		if(event.getId() == pv.getId()){
			updateCreatePendingProgress(event.getProgress());
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}
	
}
