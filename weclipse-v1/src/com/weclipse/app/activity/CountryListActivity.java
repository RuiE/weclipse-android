package com.weclipse.app.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.weclipse.app.R;
import com.weclipse.app.models.Country;
import com.weclipse.app.utils.StringArrayAlphabetIndexer;
import com.weclipse.app.widget.PinnedHeaderListView;
import com.weclipse.app.widget.PinnedHeaderListView.PinnedHeaderAdapter;
import com.weclipse.app.widget.SquareRelativeLayout;

public class CountryListActivity extends ActionBarActivity implements OnItemClickListener,OnQueryTextListener {
	
	private static final String TAG = "CountryList Screen";
	
	PinnedHeaderListView lvCountries;
	ArrayList<Country> countriesList;
	String[] countriesArray;
	CountryAdapter mAdapter;
	SquareRelativeLayout back;
	Menu menu;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_countries);
		lvCountries = (PinnedHeaderListView)findViewById(R.id.lvCountries);
		lvCountries.setDividerHeight(0);
		lvCountries.setPinnedHeaderView(LayoutInflater.from(this).inflate(R.layout.pinned_header, lvCountries, false));
//		lvCountries.setFastScrollEnabled(true);
		countriesList = new ArrayList<Country>();
		countriesArray = new String[getResources().getStringArray(R.array.countries).length];
		int i = 0;
		for(String item : getResources().getStringArray(R.array.countries)){
			countriesList.add(new Country(item.split(";")[0],item.split(";")[1]));
			countriesArray[i++] = item.split(";")[0];
		}
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.header_country_activity));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_country, menu);
		this.menu=menu;
		
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
		
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchViewMenuItem);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getString(R.string.search_view_country));
        searchView.setOnQueryTextListener(this);
        ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setTextColor(getResources().getColor(R.color.white));
        ((EditText)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(getResources().getColor(R.color.white));
        ((ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_button)).setImageResource(R.drawable.action_search);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void filter(String query){
//		Log.d(TAG,"Query: "+query);
		countriesList.clear();
		for(String item : getResources().getStringArray(R.array.countries)){
			if(item.split(";")[0].toLowerCase().startsWith(query.toLowerCase())){
				countriesList.add(new Country(item.split(";")[0],item.split(";")[1]));
			}
		}
		
		countriesArray = new String[countriesList.size()];
		for(int i=0;i<countriesList.size();i++){
			countriesArray[i] = countriesList.get(i).country;
		}
		
		mAdapter.notifyDataSetChanged();
		mAdapter.swapIndexer(countriesArray);
	}
	
	@Override
	public boolean onQueryTextChange(String query) {
		filter(query);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		filter(query);
		return false;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		updateListView();
	}
	
	public void updateListView(){
		mAdapter = new CountryAdapter(this,R.layout.row_country,countriesList);
		lvCountries.setAdapter(mAdapter);
		lvCountries.setOnItemClickListener(this);
		lvCountries.setOnScrollListener(mAdapter);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		Intent resultIntent = new Intent();
		resultIntent.putExtra("country",countriesList.get(position).country);
		resultIntent.putExtra("code",countriesList.get(position).code);
		setResult(RESULT_OK,resultIntent);
		this.finish();
	}
	
	public class CountryAdapter extends ArrayAdapter<Country> implements SectionIndexer,OnScrollListener,PinnedHeaderAdapter {
		
		private SectionIndexer mIndexer;
		
		LayoutInflater mInflater;
		List<Country> objects;
		
		public CountryAdapter(Context context, int resource,List<Country> objects) {
			super(context, resource, objects);
			mInflater = LayoutInflater.from(context);
			this.objects=objects;
			mIndexer = new StringArrayAlphabetIndexer(countriesArray,"ABCDEFGHIJKLMNOPQRSTUVWXYZ_");
		}
		
		public void swapIndexer(String[] array){
			mIndexer = new StringArrayAlphabetIndexer(array, "ABCDEFGHIJKLMNOPQRSTUVWXYZ_");
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			String _country = objects.get(position).country;
			String _code = "+"+objects.get(position).code;
			
			ViewHolder viewHolder;
			if(convertView==null){
				viewHolder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.row_country, null);
				viewHolder.country = (TextView)convertView.findViewById(R.id.tvCountry);
				viewHolder.code = (TextView)convertView.findViewById(R.id.tvCode);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder)convertView.getTag();
			}
			
			viewHolder.country.setText(_country);
			viewHolder.code.setText(_code);
			
			bindSectionHeader(convertView, position);
			
			return convertView;
		}
		
		private void bindSectionHeader(View itemView, int position) {
            final RelativeLayout headerView = (RelativeLayout) itemView.findViewById(R.id.rlHeader);
            final View dividerView = itemView.findViewById(R.id.list_divider);
            
            final int section = getSectionForPosition(position);
            if (getPositionForSection(section) == position) {
                String title = (String) mIndexer.getSections()[section];
                ((TextView)headerView.findViewById(R.id.tvHeader)).setText(title);
                headerView.setVisibility(View.VISIBLE);
            } else {
                headerView.setVisibility(View.GONE);
                dividerView.setVisibility(View.VISIBLE);
            }
    
            // move the divider for the last item in a section
            if (getPositionForSection(section + 1) - 1 == position) {
                dividerView.setVisibility(View.GONE);
            } else {
                dividerView.setVisibility(View.VISIBLE);
            }
		}
		
		public int getPositionForSection(int sectionIndex) {
            if (mIndexer == null)
                return -1;
            return mIndexer.getPositionForSection(sectionIndex);
            
        }
                
        public int getSectionForPosition(int position) {
            if (mIndexer == null)
                return -1;
            return mIndexer.getSectionForPosition(position);
        }
		
		private class ViewHolder {
	        TextView country;
	        TextView code;
	    }

		@Override
		public int getPinnedHeaderState(int position) {
			if (mIndexer == null || getCount() == 0) {
                return PINNED_HEADER_GONE;
            }

            if (position < 0) {
                return PINNED_HEADER_GONE;
            }

            // The header should get pushed up if the top item shown
            // is the last item in a section for a particular letter.
            int section = getSectionForPosition(position);
            int nextSectionPosition = getPositionForSection(section + 1);
            
            if (nextSectionPosition != -1 && position == nextSectionPosition - 1) {
                return PINNED_HEADER_PUSHED_UP;
            }

            return PINNED_HEADER_VISIBLE;
		}

		@Override
		public void configurePinnedHeader(View v, int position, int alpha) {
			TextView header = (TextView) v.findViewById(R.id.tvHeader);
            
            final int section = getSectionForPosition(position);
            final String title = (String) getSections()[section];
            
            header.setText(title);
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			if (view instanceof PinnedHeaderListView) {
                ((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
            }  
		}

		 public Object[] getSections() {
            if (mIndexer == null) {
                return new String[] { " " };
            } else {
                return mIndexer.getSections();
            }
        }
		 
	}

}
