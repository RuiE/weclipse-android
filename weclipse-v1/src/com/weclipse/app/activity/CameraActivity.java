package com.weclipse.app.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.googlecode.mp4parser.BasicContainer;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.squareup.otto.Subscribe;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.R;
import com.weclipse.app.managers.CacheManager;
import com.weclipse.app.managers.TutorialManager;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.widget.SquareRelativeLayout;

public class CameraActivity extends FragmentActivity implements OnClickListener, OnTouchListener {

	public static final int VIDEO_W = 480;
	public static final int VIDEO_H = 640;

	private static final int VIDEO_FRAME_RATE = 30;
	private static final int VIDEO_BITRATE = 1700000;
	private static final int AUDIO_CHANNELS = 1;
	private static final int AUDIO_BITRATE = 128000;
	private static final int SAMPLE_RATE = 44100;

	// private static final int PHOTO_TIME = 500;
	private static final int VIDEO_TIME = 7000;

	public static final int PICTURE = 2;
	public static final int VIDEO = 1;

	public static final int MEDIA_TYPE_IMAGE = 2;
	public static final int MEDIA_TYPE_VIDEO = 1;
	
	private static final int FRIEND_SELECTOR_CODE = 1;

	private static final int STEP = 25;
	private static final int MAX_RECORDING_TIME = 7000;

	Camera mCamera;
	CameraPreview mPreview;
	PlayBack mPlayBack;
	private static final String TAG = "Camera Screen";
	FrameLayout preview;
	MediaRecorder mMediaRecorder;
	MediaPlayer mMediaPlayer;
	private boolean isRecording = false;
	private boolean isPlaying = false;
	// private boolean isTakingPhoto = false;
	private boolean isReleased = true;
	ImageView bVideo, bSwapCamera, bFlash;
	// Timer timer;
	String lastFile;
	RelativeLayout settings, actions, replyPanel;
	Button bSend;
	RelativeLayout container, recordingTutorial, sendingTutorial;
	SquareRelativeLayout window;
	SquareRelativeLayout flashContainer, swapContainer, backContainer, closeContainer;
	View secondCloseContainer;
	EditText etCaption;
	TextView tvCaptionPlaceholder;
	LinearLayout playersBox;
	int displayHeight, displayWidth;
	boolean drawn, flash, isShowingSetttings, isUsingBackCamera;
	Camera.Size previewSize;
	SeekBar progress;
	long duration;
	int media_type;
	WApp app;
	// Handler mHandler;
	public boolean isReplying;
	boolean canClickNext,isPreviewing;
	long userIdToSend, groupIdToSend, video_id, user_id, group_id;
	String video_name;
//	Contact c;
	Group g;
	int sum;
	Typeface light;
	View topView;
	boolean has2Cameras,hasFlash;
	long now;

	private CameraHandlerThread mThread = null;

	String selectedTime;
	
	int private_video;
	ImageView ivPrivateVideo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (WApp) getApplication();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_camera);
		
		initVariables();
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_CAMERA);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}

	@SuppressLint("NewApi")
	public void initVariables() {
		isReplying = getIntent().getBooleanExtra("isReplying", false);
		light = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");

		Display dip = getWindowManager().getDefaultDisplay();

		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			Point size = new Point();
			dip.getSize(size);
			displayHeight = size.y;
			displayWidth = size.x;
		} else {
			displayHeight = dip.getHeight();
			displayWidth = dip.getWidth();
		}

		// Log.d(TAG,"Height da V: "+v.getHeight());
		// Log.d(TAG,"W: "+displayWidth+" H: "+displayHeight);

		preview = (FrameLayout) findViewById(R.id.camera_preview);

		/* Settings Layout and its Buttons */
		settings = (RelativeLayout) findViewById(R.id.rlCameraSettings);
		// settings.bringToFront();
		bVideo = (ImageView) findViewById(R.id.bCaptureVideo);

		has2Cameras = Camera.getNumberOfCameras() >= 2; 
		hasFlash = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH); 
		
		if (has2Cameras) {
			swapContainer = (SquareRelativeLayout) findViewById(R.id.srlSwapContainer);
			bSwapCamera = (ImageView) findViewById(R.id.bSwapCamera);
			swapContainer.setVisibility(View.VISIBLE);
			swapContainer.setOnClickListener(this);
		}

		if (hasFlash) {
			bFlash = (ImageView) findViewById(R.id.bFlash);
			flashContainer = (SquareRelativeLayout) findViewById(R.id.srlFlashContainer);
			flashContainer.setVisibility(View.VISIBLE);
			flashContainer.setOnClickListener(this);
			flash = false;
		}

		bVideo.setOnTouchListener(this);
		bVideo.setOnClickListener(this);

		closeContainer = (SquareRelativeLayout) findViewById(R.id.srlCloseContainer);
		secondCloseContainer = (View)findViewById(R.id.vCloseContainer);
		backContainer = (SquareRelativeLayout) findViewById(R.id.srlBackContainer);
		backContainer.setOnClickListener(this);
		closeContainer.setOnClickListener(this);
		secondCloseContainer.setOnClickListener(this);

		/* Actions Layout and its Buttons */
		actions = (RelativeLayout) findViewById(R.id.rlCameraAction);
		bSend = (Button) findViewById(R.id.bSend);
		bSend.setOnClickListener(this);
		selectedTime = app.getDefaultVideoTime();

		isShowingSetttings = true;
		progress = (SeekBar) findViewById(R.id.sbVideo);
		container = (RelativeLayout) findViewById(R.id.cameraFragContainer);
		window = (SquareRelativeLayout) findViewById(R.id.rlWindow);

		topView = (RelativeLayout) findViewById(R.id.rlTopView);
		ViewTreeObserver vto = topView.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
//				Log.d(TAG,"Measured height TopView: "+ topView.getMeasuredHeight());
//				Log.d(TAG,"Measured height Settings: "+ settings.getMeasuredHeight());

				RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,(topView.getMeasuredHeight() / 2) + settings.getMeasuredHeight());
				rlParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
				rlParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				settings.setLayoutParams(rlParams);

				RelativeLayout.LayoutParams previewParams = new RelativeLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
				previewParams.setMargins(0, (topView.getMeasuredHeight() / 2),
						0, 0);
				preview.setLayoutParams(previewParams);

				if (Build.VERSION.SDK_INT < 16) {
					container.getViewTreeObserver()
							.removeGlobalOnLayoutListener(this);
				} else {
					container.getViewTreeObserver()
							.removeOnGlobalLayoutListener(this);
				}
			}
		});

		etCaption = (EditText) findViewById(R.id.etCaption);
		if(!isReplying){
			etCaption.addTextChangedListener(new TextWatcher() {
	
				@Override
				public void onTextChanged(CharSequence s, int start, int before,int count) {
	
					if (isPlaying) {
						mPlayBack.mMediaPlayer.stop();
					}
	
					if (!s.toString().equals("")) {
						
						if (canClickNext) {
							// do nothing, already did
						} else {
							try {
								XmlResourceParser parser = getResources().getXml(
										R.drawable.text_white_green_pressed);
								ColorStateList colors = ColorStateList
										.createFromXml(getResources(), parser);
								bSend.setTextColor(colors);
								bSend.setBackgroundResource(R.drawable.button_opaque_green);
							} catch (XmlPullParserException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							canClickNext = true;
						}
						
					} else {
						canClickNext = false;
						try {
							XmlResourceParser parser = getResources().getXml(R.drawable.button_white_green_text);
							ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
							bSend.setTextColor(colors);
							bSend.setBackgroundResource(R.drawable.button_opaque_white);
						} catch (XmlPullParserException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {
					// Do nothing
				}
	
				@Override
				public void afterTextChanged(Editable s) {
					// Do nothing
				}
			});
			
		} 

		/* Get vars from the intent create/reply */
		if (isReplying) {
			replyPanel = (RelativeLayout) findViewById(R.id.rlReplyPanel);
			try {
				XmlResourceParser parser = getResources().getXml(R.drawable.text_white_green_pressed);
				ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
				bSend.setTextColor(colors);
				bSend.setBackgroundResource(R.drawable.button_opaque_green);
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			user_id = getIntent().getLongExtra("user_id", 0);
			video_id = getIntent().getLongExtra("video_id", 0);
			group_id = getIntent().getLongExtra("group_id", 0);
			int type = getIntent().getIntExtra("type", 0);
			video_name = getIntent().getStringExtra("caption");
			
			((TextView) findViewById(R.id.tvReplyCaption)).setText(video_name);

			switch (type) {
			case WApp.TARGET_FRIENDS:
				User u = UserCache.getInstance(app).getUser(user_id);
				((TextView) replyPanel.findViewById(R.id.tvReplyTo)).setText(getString(R.string.reply_to) + " " + u.getUsername());
				break;
			case WApp.TARGET_GROUPS:
				g = new Group(this, group_id);
				if (g.imInGroup()) {
					g.load();
					((TextView) replyPanel.findViewById(R.id.tvReplyTo)).setText(getString(R.string.reply_to) + " " + g.getName());
				}
				break;
			}
			
		}

		// mPicture = new PictureCallback() {
		// @Override
		// public void onPictureTaken(byte[] data,Camera camera) {
		// File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
		// if (pictureFile == null){
		// Log.d(TAG, "Error creating media file, check storage permissions: ");
		// return;
		// }
		// lastFile = pictureFile.getAbsolutePath();
		// try {
		// FileOutputStream fos = new FileOutputStream(pictureFile);
		//
		// data = resizeAndSquareImage(data);
		//
		// Bitmap realImage = BitmapFactory.decodeByteArray(data, 0,
		// data.length);
		//
		// boolean bo = realImage.compress(Bitmap.CompressFormat.JPEG, 100,
		// fos);
		// fos.close();
		//
		// } catch (FileNotFoundException e) {
		// Log.d("Info", "File not found: " + e.getMessage());
		// } catch (IOException e) {
		// Log.d("TAG", "Error accessing file: " + e.getMessage());
		// }
		//
		// }
		//
		// };
		// isPicture=false;

		flash = false;
		canClickNext = false;
		duration = 0;
		isUsingBackCamera = true;
		
		ivPrivateVideo = (ImageView)findViewById(R.id.ivPrivateVideo);
		private_video = app.getDefaultVideoType();
		
		if(private_video == Video.PRIVATE){
			ivPrivateVideo.setBackgroundResource(R.drawable.camera_private_video);
		} else {
			ivPrivateVideo.setBackgroundResource(R.drawable.camera_public_video);
		}
	}
	
	public void togglePrivate(View v){
		if(private_video == Video.PRIVATE){
			private_video = Video.PUBLIC;
			ivPrivateVideo.setBackgroundResource(R.drawable.camera_public_video);
		} else {
			private_video = Video.PRIVATE;
			ivPrivateVideo.setBackgroundResource(R.drawable.camera_private_video);
		}
	}

	byte[] resizeAndSquareImage(byte[] input) {
		Bitmap original = BitmapFactory.decodeByteArray(input, 0, input.length);
		Bitmap scaled = null;

//		Log.d(TAG, "Original Width: " + original.getWidth()+ " Original Height: " + original.getHeight());

		if ((original.getWidth() > 480 && original.getHeight() > 640) || (original.getWidth() > 640 && original.getHeight() > 480)) {
//			Log.d(TAG, "Medidas grandes, vamos reduzir");
			if (original.getWidth() > original.getHeight()) { // Está em
																// landscape
//				Log.d(TAG, "Landscape");
				scaled = Bitmap.createScaledBitmap(original, 640, 480, true);
				Matrix m = new Matrix();
				m.postRotate(90);
				scaled = Bitmap.createBitmap(scaled, 0, 0, 640, 480, m, true);
				scaled = Bitmap.createBitmap(scaled, 0, 0, 480, 480);
			} else {// está em portrait
//				Log.d(TAG, "Portrait");
				scaled = Bitmap.createScaledBitmap(original, 480, 640, true);
				scaled = Bitmap.createBitmap(scaled, 0, 0, 480, 480);
			}
		} else {
//			Log.d(TAG, "Medidas fixes, vamos meter quadrado");
			if (original.getWidth() > original.getHeight()) { // Está em
																// landscape
//				Log.d(TAG, "Landscape");
				scaled = Bitmap.createScaledBitmap(original,
						original.getWidth(), original.getHeight(), true);
				Matrix m = new Matrix();
				m.postRotate(90);
				scaled = Bitmap.createBitmap(scaled, 0, 0, original.getWidth(),
						original.getHeight(), m, true);
				scaled = Bitmap.createBitmap(scaled, 0, 0,
						original.getHeight(), original.getWidth());
			} else {// está em portrait
//				Log.d(TAG, "Portrait");
				scaled = Bitmap.createScaledBitmap(original,
						original.getWidth(), original.getHeight(), true);
				scaled = Bitmap.createBitmap(scaled, 0, 0, original.getWidth(),
						original.getWidth());
			}
		}
		ByteArrayOutputStream blob = new ByteArrayOutputStream();
		scaled.compress(Bitmap.CompressFormat.JPEG, 100, blob);

		return blob.toByteArray();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == FRIEND_SELECTOR_CODE) {
			if (resultCode == RESULT_OK) {

				long[] temp = data.getLongArrayExtra("sendingGroup");
				long[] temp2 = data.getLongArrayExtra("sendingContact");

				cameraFinish(data.getIntExtra("players", 0),
						data.getStringExtra("eta"),
						data.getStringExtra("filePath"),
						data.getStringExtra("caption"),
						data.getIntExtra("target", 0), duration, temp, temp2,
						media_type,
						data.getIntExtra("private_video",Video.PUBLIC));
			} else {
				// canceled, did not send, it's okay
			}
		}
	}

	public void cameraFinish(int players, String eta, String filePath,
			String caption, int target, long duration, long[] groupIds,
			long[] contactIds, int media_type,int private_video) {
		Intent i = new Intent();
		i.putExtra("eta", eta);
		i.putExtra("players", players);
		i.putExtra("filePath", filePath);
		i.putExtra("caption", caption);
		i.putExtra("duration", duration);
		i.putExtra("media_type", media_type);

		if (target == WApp.TARGET_GROUPS) {
			i.putExtra("sendingGroup", groupIds);
		} else if (target == WApp.TARGET_FRIENDS) {
			i.putExtra("sendingContact", contactIds);
		} else {
			i.putExtra("sendingGroup", groupIds);
			i.putExtra("sendingContact", contactIds);
		}

		i.putExtra("target", target);
		i.putExtra("private_video", private_video);
		
		setResult(RESULT_OK, i);
		
		this.finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.srlFlashContainer:
			if (flash) {
				Parameters p = mCamera.getParameters();
				p.setFlashMode(Parameters.FLASH_MODE_OFF);
				
				if(mCamera.getParameters().getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
					p.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
				
				mCamera.setParameters(p);
				bFlash.setImageResource(R.drawable.camera_flash_off);
			} else {
				if (isUsingBackCamera) {
					Parameters p = mCamera.getParameters();
					p.setFlashMode(Parameters.FLASH_MODE_TORCH);
					
					if(mCamera.getParameters().getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
						p.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
					
					mCamera.setParameters(p);
					bFlash.setImageResource(R.drawable.camera_flash_on);
				}
			}
			flash = !flash;
			break;
		case R.id.srlSwapContainer:
			stopPreview(mCamera);
			releaseCamera();
			if (isUsingBackCamera) {// switch to front
				mCamera = openFrontFacingCamera();
			} else {// is using front, switch to back camera
				newOpenCamera();
			}
			mPreview = new CameraPreview(this, mCamera);
			alignView(mPreview);
			preview.removeAllViews();
			preview.addView(mPreview);
			break;
		case R.id.bSend:
			// if(isPlaying){
			//  mPlayBack.mMediaPlayer.stop();
			// }

			if (isReplying) {
//				Log.d(TAG, "Is replying!!");
				Intent i = new Intent();
				i.putExtra("user_id", user_id);
				i.putExtra("video_id", video_id);
				i.putExtra("filePath", lastFile);
				i.putExtra("duration", duration);
				i.putExtra("media_type", media_type);
				setResult(RESULT_OK, i);
				this.finish();
			} else {
				if (etCaption.getText().toString().equals("")) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
					imm.showSoftInput(etCaption,InputMethodManager.SHOW_IMPLICIT);
				} else {
//					Log.d(TAG, "Simply Sending");
					Intent i = new Intent(this, SelectTargetActivity.class);
					i.putExtra("filePath", lastFile);
					i.putExtra("eta", selectedTime);
					i.putExtra("players", app.getDefaultPlayers());
					i.putExtra("caption", etCaption.getText().toString());
					i.putExtra("duration", duration);
					i.putExtra("media_type", media_type);
					i.putExtra("private_video", private_video);
					startActivityForResult(i, FRIEND_SELECTOR_CODE);
				}
			}
			break;
		case R.id.srlCloseContainer:
		case R.id.vCloseContainer:
			this.onBackPressed();
			break;
		case R.id.srlBackContainer:
			etCaption.setText("");
			swapCameraLayout();
			try {
				new File(lastFile).delete();
			} catch (NullPointerException e) {
				// No file, no problem, lets move on
			}
			if(!isPreviewing){
				startPreview(mCamera);
			}
			lastFile = null;
			break;
		}
	}

	@Override
	public void onBackPressed() {
		
		if(backContainer.isShown()){
			onClick(backContainer);
			return;
		}
		
		super.onBackPressed();
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (v.getId()) {
		case R.id.bCaptureVideo:
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				if (!isRecording && isReleased) {
					startRecording();
				}
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				if (isRecording) {
					AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
					audio.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
					try {
						mMediaRecorder.stop();
					} catch (RuntimeException e) {
						Tracker t = app.getTracker(TrackerName.APP_TRACKER);
						t.send(new HitBuilders.EventBuilder()
								.setCategory(AnalyticsHelper.C_CAMERA)
								.setAction(AnalyticsHelper.A_VIDEO_RECORD_FAIL)
								.setValue(duration)
								.build());
						
						File f = new File(lastFile);
						f.delete();
						lastFile = null;
						Toast.makeText(this,getString(R.string.toast_keep_holding),Toast.LENGTH_SHORT).show();
						e.printStackTrace();
						releaseMediaRecorder();
						bVideo.setImageResource(R.drawable.camera_start_recording);
						mCamera.lock();
						isRecording = false;
						startPreview(mCamera);
						resetProgress();
						progress.setVisibility(View.GONE);
						if(hasFlash)
							flashContainer.setVisibility(View.VISIBLE);
						
						if(has2Cameras)
							swapContainer.setVisibility(View.VISIBLE);
						
						closeContainer.setVisibility(View.VISIBLE);
						secondCloseContainer.setVisibility(View.VISIBLE);
						return false;
					}
					bVideo.setImageResource(R.drawable.camera_start_recording);
					// timer.cancel(true);
					releaseMediaRecorder();
					mCamera.lock();
					// isTakingPhoto=false;
					isRecording = flash = false;
					stopPreview(mCamera);
					swapCameraLayout();
//					Log.d(TAG, "Parou de gravar");
				}
				// else if(isTakingPhoto){
				// isTakingPhoto=false;
				// mCamera.takePicture(null, null, mPicture);
				// isPicture=true;
				// swapCameraLayout();
				// }
				else {
					// what what
				}
			}
			break;
		}

		return false;
	}
	
	public void startPreview(Camera camera){
		camera.startPreview();
		isPreviewing = true;
	}
	
	public void stopPreview(Camera camera){
		isPreviewing=false;
		camera.stopPreview();
	}

	private void startRecording() {
		if (/* isTakingPhoto && */prepareVideoRecorder()) {
			AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			audio.setRingerMode(0);
//			Log.d(TAG, "prepared");
			mMediaRecorder.start();
			// timer = new Timer();
			// timer.execute();
			isRecording = true;
			new Duration().start();
			// isTakingPhoto=false;
			bVideo.setImageResource(R.drawable.camera_stop_recording);
			sum = 0;
			// Show and hide stuff
			if(hasFlash)
				flashContainer.setVisibility(View.GONE);
			
			if(has2Cameras)
				swapContainer.setVisibility(View.GONE);
			
			closeContainer.setVisibility(View.GONE);
			secondCloseContainer.setVisibility(View.GONE);
			progress.setVisibility(View.VISIBLE);
			now = System.currentTimeMillis();
			progress.postDelayed(new Runnable() {
				@Override
				public void run() {
					sum = (int)(System.currentTimeMillis()-now);
					progress.setProgress(sum);
					if(sum < MAX_RECORDING_TIME){
						if (isRecording) {
							progress.postDelayed(this, STEP);
						} else {
							sum = 0;
							progress.removeCallbacks(this);
						}
					} else {
						sum = 0;
						progress.removeCallbacks(this);
						stopRecording();
					}
				}
			}, STEP);
		} else {
			releaseMediaRecorder();
		}
	}

	private void swapCameraLayout() {
		if (isShowingSetttings) {
			if (isReplying) {
				bSend.setText(getString(R.string.camera_button_send));
				replyPanel.setVisibility(View.VISIBLE);
			} else {
				ivPrivateVideo.setVisibility(View.VISIBLE);
				bSend.setText(getString(R.string.camera_button_next));
				actions.setVisibility(View.VISIBLE);
				
//				TutorialManager.getInstance(app).demarkPrivateDialog();
				if(!TutorialManager.getInstance(app).hasShownPrivateDialog()){
					TutorialManager.getInstance(app).markPrivateDialog();
					new PrivateVideoDialog().show(getFragmentManager(), "dialog_private");
				}
				
			}
			settings.setVisibility(View.INVISIBLE);
			bSend.setVisibility(View.VISIBLE);
			// start loop
			// if(!isPicture){
			preview.removeView(mPreview);
			releaseCamera();
			mPreview = null;
			releaseMediaRecorder();
			mMediaPlayer = new MediaPlayer();
			mPlayBack = new PlayBack(this, mMediaPlayer);
			alignView(mPlayBack);
			preview.addView(mPlayBack);
			backContainer.setVisibility(View.VISIBLE);
			closeContainer.setVisibility(View.GONE);
			secondCloseContainer.setVisibility(View.GONE);
			progress.setVisibility(View.GONE);
//			WLog.d(this," stop the recording by lifting up");
			resetProgress();
			
		} else {
			if(has2Cameras)
				swapContainer.setVisibility(View.VISIBLE);
			
			if(hasFlash)
				flashContainer.setVisibility(View.VISIBLE);
			
			backContainer.setVisibility(View.GONE);

			closeContainer.setVisibility(View.VISIBLE);
			secondCloseContainer.setVisibility(View.VISIBLE);
			bSend.setVisibility(View.GONE);
			ivPrivateVideo.setVisibility(View.GONE);
			settings.setVisibility(View.VISIBLE);
			actions.setVisibility(View.INVISIBLE);
			if (isReplying)
				replyPanel.setVisibility(View.INVISIBLE);
			
			if(!isPreviewing)
				stopLoop();
			
		}
		isShowingSetttings = !isShowingSetttings;
	}

	private void stopLoop() {
		preview.removeView(mPlayBack);
		releaseMediaPlayer();
		mPlayBack = null;
		if(isUsingBackCamera){
			newOpenCamera();
		} else {
			mCamera = openFrontFacingCamera();
		}
		mPreview = new CameraPreview(this, mCamera);
		alignView(mPreview);
		preview.addView(mPreview);
	}

	// Foi chamado da task
	public void stopRecording() {
		if (isRecording) {
			AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			audio.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			resetProgress();
			bVideo.setImageResource(R.drawable.camera_start_recording);
			mMediaRecorder.stop();
			releaseMediaRecorder();
			mCamera.lock();
			isRecording = flash = false;
			stopPreview(mCamera);
			swapCameraLayout();
		}
	}

	public void alignView(SurfaceView view) {
		view.setBackgroundColor(Color.TRANSPARENT);
		float height = ((float) VIDEO_H / (float) VIDEO_W) * (float) displayWidth;
		final int heightInt = Math.round(height);
//		Log.d(TAG, "____" + displayWidth + "x" + heightInt);
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(displayWidth, heightInt);
		params.setMargins(0, 0, 0, 0);
		view.setLayoutParams(params);
	}

	public Size getPreviewSize() {
		Size res = null;
		List<Size> sizes = mCamera.getParameters().getSupportedPreviewSizes();
		if (sizes == null) {
//			Log.d(TAG, "no available sizes");
			return null;
		}

		for (Size s : sizes) {
			// Log.d(TAG,"Iterator: "+s.width+"x"+s.height);
			if (s.width == 640 && s.height == 480) {
				res = s;
				return res;
			}
		}

		if (res == null) {
//			Log.d(TAG, "640x480 not available");
		}

		return res;
	}

	private File getOutputMediaFile(int type) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

//		Log.d(TAG, "Type: " + type);
		File mediaStorageDir = new File(Environment
				.getExternalStorageDirectory().getAbsolutePath()
				+ WApp.BASE_DIR + CacheManager.CACHE_DIR);
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
//				Log.d("MyCameraApp", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator+ "IMG_" + timeStamp + ".jpg.nomedia");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator+ "VID_" + timeStamp + ".mp4.nomedia");
		} else {
			return null;
		}
		media_type = type;
//		Log.d(TAG, "Type set to: " + media_type);
//		Log.d(TAG, "Patho2: " + mediaFile.getAbsolutePath());
		return mediaFile;
	}

	/** A safe way to get an instance of the Camera object. */
	private void newOpenCamera() {
		if (mThread == null) {
			mThread = new CameraHandlerThread();
		}

		synchronized (mThread) {
			mThread.openCamera();
		}
	}

	private void oldOpenCamera() {
		try {
			mCamera = Camera.open();
			isUsingBackCamera = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Camera openFrontFacingCamera() {
		int cameraCount = 0;
		Camera cam = null;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		cameraCount = Camera.getNumberOfCameras();
		for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
			Camera.getCameraInfo(camIdx, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				try {
					cam = Camera.open(camIdx);
				} catch (RuntimeException e) {
//					Log.e(TAG,"Camera failed to open: " + e.getLocalizedMessage());
				}
			}
		}
		isUsingBackCamera = false;
		return cam;
	}

	private boolean prepareVideoRecorder() {
		// mCamera = getCameraInstance();
//		long now = System.currentTimeMillis();
		isReleased = false;
		mMediaRecorder = new MediaRecorder();
		// Step 1: Unlock and set camera to MediaRecorder
		mCamera.unlock();
		mMediaRecorder.setCamera(mCamera);
		// Step 2: Set sources
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		// Step 3: Set a CamcorderProfile (requires API Level 8 or higher)

		// mMediaRecorder.setProfile(CamcorderProfile.get(cameraId,CamcorderProfile.QUALITY_HIGH);
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		File temp = getOutputMediaFile(MEDIA_TYPE_VIDEO);
		lastFile = temp.getAbsolutePath();
		mMediaRecorder.setOutputFile(temp.toString());

		mMediaRecorder.setVideoSize(previewSize.width, previewSize.height);
		mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
		mMediaRecorder.setVideoEncodingBitRate(VIDEO_BITRATE);
		mMediaRecorder.setVideoFrameRate(VIDEO_FRAME_RATE);
		mMediaRecorder.setMaxDuration(VIDEO_TIME);

		// audio
		mMediaRecorder.setAudioChannels(AUDIO_CHANNELS);
		mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
		mMediaRecorder.setAudioEncodingBitRate(AUDIO_BITRATE);
		mMediaRecorder.setAudioSamplingRate(SAMPLE_RATE);
		// Step 5: Set the preview output
		mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());
		if (isUsingBackCamera) {
			mMediaRecorder.setOrientationHint(90);
		} else {
			mMediaRecorder.setOrientationHint(270);
		}
		// Step 6: Prepare configured MediaRecorder
		try {
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
//			Log.d(TAG,"IllegalStateException preparing MediaRecorder: "+ e.getMessage());
			releaseMediaRecorder();
			return false;
		} catch (IOException e) {
//			Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		}
//		Log.d(TAG,"Dif: "+((System.currentTimeMillis()-now))+"ms");
		return true;
	}
	
	public void resetProgress(){
		sum = 0;
		progress.post(new Runnable() {
			
			@Override
			public void run() {
				progress.setProgress(sum);
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		
		resetProgress();
		
		if(lastFile == null){
			if(isUsingBackCamera){
				newOpenCamera();
			} else {
				mCamera = openFrontFacingCamera();
			}

			if (mPreview == null) {
				mPreview = new CameraPreview(this, mCamera);
				alignView(mPreview);
				preview.addView(mPreview);
			}
			isPreviewing=true;
		} else {
			mMediaPlayer = new MediaPlayer();
			mPlayBack = new PlayBack(this, mMediaPlayer);
			alignView(mPlayBack);
			preview.addView(mPlayBack);
		}
		
	}

	@Override
	public void onPause() {
		super.onPause();
		releaseMediaRecorder(); // if you are using MediaRecorder, release it first
		releaseCamera(); // release the camera immediately on pause event
		releaseMediaPlayer();

		if (mPreview != null) {
			preview.removeView(mPreview);
			mPreview = null;
		}

		if (mPlayBack != null) {
			preview.removeView(mPlayBack);
			mPlayBack = null;
		}
	}

	private void releaseMediaRecorder() {
		if (mMediaRecorder != null) {
			mMediaRecorder.reset(); // clear recorder configuration
			mMediaRecorder.release(); // release the recorder object
			mMediaRecorder = null;
			mCamera.lock(); // lock camera for later use
		}
		isReleased = true;
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mPreview.getHolder().removeCallback(mPreview);
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	private void releaseMediaPlayer() {
		if (mMediaPlayer != null) {
			mPlayBack.getHolder().removeCallback(mPlayBack);
			mMediaPlayer.release();
			isPlaying = false;
			mMediaPlayer = null;
		}
	}

	public class PlayBack extends SurfaceView implements SurfaceHolder.Callback {
		private SurfaceHolder mHolder;
		private MediaPlayer mMediaPlayer;

		@SuppressLint("NewApi")
		public PlayBack(Context context, MediaPlayer mMediaPlayer) {
			super(context);
			this.mMediaPlayer = mMediaPlayer;
			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = getHolder();
			mHolder.addCallback(this);
			// deprecated setting, but required on Android versions prior to 3.0
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void stop() {
			isPlaying = false;
			mMediaPlayer.release();
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {

		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			try {
//				Log.d(TAG, "LAstfile path: " + lastFile);
				if (mMediaPlayer == null) {
//					Log.d(TAG, "mplayer a nulooo");
				}
				if (lastFile != null) {
					mMediaPlayer.setDataSource(lastFile);
					mMediaPlayer.setDisplay(mHolder);
					mMediaPlayer.setLooping(true);
					mMediaPlayer.prepare();
					mMediaPlayer.start();
					isPlaying = true;
					duration = mMediaPlayer.getDuration();
//					Log.d(TAG,"Duration: "+duration);
				} else {
					this.stop();
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			mMediaPlayer.release();
			mMediaPlayer = null;
			isPlaying = false;
		}
	}

	/** A basic Camera preview class */
	public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
		private SurfaceHolder mHolder;
		private Camera mCamera;

		public CameraPreview(Context context, Camera camera) {
			super(context);
			mCamera = camera;
			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = getHolder();
			mHolder.addCallback(this);
			// deprecated setting, but required on Android versions prior to 3.0
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void surfaceCreated(SurfaceHolder holder) {
			// The Surface has been created, now tell the camera where to draw
			// the preview.
			try {
				if(mCamera == null){
					Log.d(TAG,"null cam");
				}
				mCamera.setPreviewDisplay(holder);
				
				android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
				int cameraId;
				if (isUsingBackCamera) {
					cameraId = 0;
				} else {
					cameraId = 1;
				}
				android.hardware.Camera.getCameraInfo(cameraId, info);
				int rotation = CameraActivity.this.getWindowManager().getDefaultDisplay().getRotation();
				int degrees = 0;
				switch (rotation) {
				case Surface.ROTATION_0:
					degrees = 0;
					break;
				case Surface.ROTATION_90:
					degrees = 90;
					break;
				case Surface.ROTATION_180:
					degrees = 180;
					break;
				case Surface.ROTATION_270:
					degrees = 270;
					break;
				}

				int result;
				if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
					result = (info.orientation + degrees) % 360;
					result = (360 - result) % 360; // compensate the mirror
				} else { // back-facing
					result = (info.orientation - degrees + 360) % 360;
				}
				mCamera.setDisplayOrientation(result);

				int d;
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
					d = 90;
				} else {
					d = 0;
				}
				// mCamera.setDisplayOrientation(d);
				Camera.Parameters params = mCamera.getParameters();
				Size s = getPreviewSize();
				previewSize = s;
				params.setPreviewSize(s.width, s.height);
//				Log.d(TAG, "PREVIEW SIZE: " + s.width + "x" + s.height);
				params.setRotation(d);
				try {
					stopPreview(mCamera);
				} catch (Exception e) {
//					Log.d(TAG, "Culd not stop preview?");
				}
				
				if(mCamera.getParameters().getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
					params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
				
				mCamera.setParameters(params);
				startPreview(mCamera);
//				mCamera.autoFocus(null);
			} catch (IOException e) {
//				Log.d(TAG, "Error setting camera preview: " + e.getMessage());
			} catch (NullPointerException npe) {
//				Log.d(TAG,"Null pointer, provavelmente na linha do primeiro try");
				npe.printStackTrace();
			}
			
		}
		
		public void surfaceDestroyed(SurfaceHolder holder) {
			// empty. Take care of releasing the Camera preview in your
			// activity.
			mCamera.release();
			// mPreview.getHolder().removeCallback(mPreview);
			mCamera = null;
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w,
				int h) {
			// If your preview can change or rotate, take care of those events
			// here.
			// Make sure to stop the preview before resizing or reformatting it.

//			if (mHolder.getSurface() == null) {
//				// preview surface does not exist
//				return;
//			}
//
//			// stop preview before making changes
//			try {
//				stopPreview(mCamera);
//			} catch (Exception e) {
//				// ignore: tried to stop a non-existent preview
//			}
//
//			// set preview size and make any resize, rotate or
//			// reformatting changes here
//			// start preview with new settings
//			try {
//				mCamera.setPreviewDisplay(mHolder);
//				startPreview(mCamera);
//
//			} catch (Exception e) {
//				Log.d(TAG, "Error starting camera preview: " + e.getMessage());
//			}
		}
	}

	private class CameraHandlerThread extends HandlerThread {
		Handler mHandler = null;

		public CameraHandlerThread() {
			super("CameraHandlerThread");
			start();
			mHandler = new Handler(getLooper());
		}

		synchronized void notifyCameraOpened() {
			notify();
		}

		void openCamera() {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					oldOpenCamera();
					notifyCameraOpened();
				}
			});

			try {
				wait();
			} catch (InterruptedException e) {
//				Log.w(TAG, "wait was interrupted");
			}
		}

	}

	private class Duration extends Thread {

		long dur;

		@Override
		public void run() {
			dur = System.currentTimeMillis();
			while (isRecording) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
//			long temp = (System.currentTimeMillis() - dur) > 7000 ? 7000
//					: (System.currentTimeMillis() - dur);
//			Log.d(TAG,"Temp: "+temp);
			if(duration!=0){
				duration = (System.currentTimeMillis() - dur) > 7000 ? 7000
						: (System.currentTimeMillis() - dur);
			}
//			Log.d(TAG, "Duration: " + duration);
		}

	}
	
	public class PrivateVideoDialog extends DialogFragment {
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        
	        String string = getString(R.string.camera_dialog_private_video);
	        
	        int index = string.indexOf("_n_");
	        
	        string = string.replace("_n_", "   ");
			SpannableString s = new SpannableString(string);
			
			Drawable d = getResources().getDrawable(R.drawable.camera_private_video);
			Double w = d.getIntrinsicWidth()*.75;
			Double h = d.getIntrinsicHeight()*.75;
			
            d.setBounds(0, 0, w.intValue(),h.intValue());
            ImageSpan imageSpan = new ImageSpan(d,ImageSpan.ALIGN_BOTTOM);
            s.setSpan(imageSpan, index+1,index+2,SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
	        
	        builder.setMessage(s)
	               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       
	                   }
	               });
	               
	        return builder.create();
	    }
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}

}