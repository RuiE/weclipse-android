package com.weclipse.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.managers.ResourceManager;
import com.weclipse.app.managers.StackManager;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.WBus;

public abstract class BaseActivity extends ActionBarActivity {

	public StackManager mStackManager;
	public WToast toast;
	public WApp app;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		/* Pointer to the application */
		app = (WApp)getApplication();
		
	}
	
	public ResourceManager getResourceManager(){
		return app.getResourceManager();
	}
	
	public StackManager getStackManager(){
		return mStackManager;
	}
	
	public WToast getToast(){
		if(toast==null){
			toast = new WToast(this);
			return toast;
		} else {
			return toast;
		}
	}
	
	public void notifyContactLV(){
//		if(this instanceof AddFriendActivity) {
//			((AddFriendActivity)this).notifyContacts();
//		} else
//			if(this instanceof SearchUserActivity){
//			((SearchUserActivity)this).notifyContacts();
//		} else
			if(this instanceof VideoDetailActivity){
			((VideoDetailActivity)this).notifyContacts();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		WBus.getInstance().register(this);
		app.isActive=true;
		app.setPointer(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		WBus.getInstance().unregister(this);
		app.setPointer(null);
		app.isActive=false;
	}
	
	public WApp getApp(){
		return app;
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}
	
}
