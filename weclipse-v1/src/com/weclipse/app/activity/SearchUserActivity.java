package com.weclipse.app.activity;

import java.util.ArrayList;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.FollowingButtonDrawer;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.FollowTask;
import com.weclipse.app.tasks.SearchUserTask;
import com.weclipse.app.tasks.UnfollowTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.FollowEvent;
import com.weclipse.app.utils.eventbus.SearchEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.UnfollowEvent;

public class SearchUserActivity extends BaseActivity implements OnQueryTextListener {
	
	ListView lvUsers;
	SearchAdapter mAdapter;
	
	ArrayList<User> mUsers;
	
	SearchView mSearchView;
	
	RelativeLayout rlNoUsers;
	
	OnClickListener openProfile = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent i = new Intent(SearchUserActivity.this, ProfileActivity.class);
			i.putExtra("user_id", (Long) v.getTag());
			startActivity(i);
		}
	};
	
	OnClickListener follow = new OnClickListener() {

		@Override
		public void onClick(View v) {
			int position = (Integer) v.getTag();
			switch (mUsers.get(position).getFollow_status()) {
			case User.CLEAN_FOLLOW:
			case User.PENDING_ACCEPT:
				new UnfollowTask(app, mUsers.get(position).getId());
				v.setBackgroundResource(R.drawable.button_following);
				break;
			case User.NOTHING:
				new FollowTask(app, mUsers.get(position).getId());
				if(mUsers.get(position).isPrivate()){
					((Button)v).setText(getString(R.string.profile_button_follow_pending));
					v.setBackgroundResource(R.drawable.button_following_pending);
				} else {
					v.setBackgroundResource(R.drawable.button_following);
				}
				break;
			}
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_search);
	    getSupportActionBar().setTitle(getString(R.string.header_search_activity));
	    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	    
	    initVariables();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_search_user, menu);
		
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
		
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchViewMenuItem);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setQueryHint(getString(R.string.action_search));
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
            	
                if(!queryTextFocused)
                	searchViewMenuItem.collapseActionView();
            }
        });
        
        ((EditText)mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setTextColor(getResources().getColor(R.color.white));
        ((EditText)mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(getResources().getColor(R.color.white));
        ((ImageView)mSearchView.findViewById(android.support.v7.appcompat.R.id.search_button)).setImageResource(R.drawable.action_search);
		
		MenuItem searchMenuItem = menu.findItem(R.id.action_search);
		MenuItemCompat.expandActionView(searchMenuItem);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void initVariables(){
		lvUsers = (ListView)findViewById(R.id.lvUsers);
		lvUsers.setDividerHeight(0);
		
		rlNoUsers = (RelativeLayout)findViewById(R.id.rlNoUsers);
		
		Tracker t = ((WApp)getApplication()).getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_SEARCH);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}

	@Override
	public boolean onQueryTextChange(String query) {
		
		return false;
	}
	
	public void updateListView(){
		
		if(mUsers.size() == 0){
			rlNoUsers.setVisibility(View.VISIBLE);
		} else {
			rlNoUsers.setVisibility(View.GONE);
//			if (mAdapter == null) {
				mAdapter = new SearchAdapter(this,mUsers);
				lvUsers.setAdapter(mAdapter);
//			} else {
//				mAdapter.swapData();
//				mAdapter.notifyDataSetChanged();
//			}			
		}
	}
	
	@Override
	public boolean onQueryTextSubmit(String query) {
		
		if(validateSearch())
			new SearchUserTask(app, query);
		
		return onQueryTextChange(query);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(mUsers != null){
			UserCache mCache = UserCache.getInstance(app);
			for(int i = 0 ; i < mUsers.size(); i++){
				if(mCache.containsUser(mUsers.get(i).getId())){
					mUsers.get(i).setFollow_status(mCache.getUser(mUsers.get(i).getId()).getFollow_status());
				}
			}
			
			mAdapter.notifyDataSetChanged();
		}
		
	}
	
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
	
	
	
	private class SearchAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<User> values;

		public SearchAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.row_list_following, values);
			mInflater = LayoutInflater.from(context);
			this.values = values;
		}

		public void swapData() {
			this.values = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_following, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlAvatarContainer = (RelativeLayout)v.findViewById(R.id.srlAvatarContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			User user = values.get(position);

			AvatarDrawer drawer = new AvatarDrawer(app,holder.ivAvatar, user);
			drawer.draw();
			holder.rlAvatarContainer.setTag(user.getId());
			holder.rlAvatarContainer.setOnClickListener(openProfile);

			holder.tvName.setText(user.getUsername());
			holder.tvTime.setText(user.getName());

			if (user.getId() == Me.getInstance(SearchUserActivity.this).getId()) {
				holder.bAction.setVisibility(View.INVISIBLE);
			} else {
				holder.bAction.setVisibility(View.VISIBLE);

				new FollowingButtonDrawer(SearchUserActivity.this,user.getFollow_status(), holder.bAction).draw();

				holder.bAction.setTag(position);
				holder.bAction.setOnClickListener(follow);
			}

			return v;
		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlAvatarContainer;
			ToggleButton bAction;
		}

	}
	
	public boolean validateSearch(){
		
//		if(!(mSearchView.getQuery().toString().length() < 3)){
//			getToast().setText(getString(R.string.toast_search_min_char)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
//			return false;
//		}
		
		if(!mSearchView.getQuery().toString().matches("^[a-zA-Z0-9_]*$")){
			getToast().setText(getString(R.string.toast_invalid_username)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		
		return true;
	}
	
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}

	@Subscribe
	public void onAsyncTaskResult(SearchEvent event){
		if(event.getSuccess()){
			mUsers = event.getUsers();
			updateListView();
		} else {
			//TODO Não deu sucesso...
		}
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(FollowEvent event) {
		if(mUsers != null){
			for (int i = 0; i < mUsers.size(); i++) {
				if (mUsers.get(i).getId() == event.getUser_id()) {
					mUsers.get(i).setFollow_status(event.getStatus());
					break;
				}
			}
	
			updateListView();
		}
	}

	@Subscribe
	public void onAsyncTaskResult(UnfollowEvent event) {
		if(mUsers != null){
			for (int i = 0; i < mUsers.size(); i++) {
				if (mUsers.get(i).getId() == event.getUser_id()) {
					mUsers.get(i).setFollow_status(User.NOTHING);
				}
			}
	
			updateListView();
		}
	}
	
}
