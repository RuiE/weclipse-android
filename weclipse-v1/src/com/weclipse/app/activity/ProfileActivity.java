package com.weclipse.app.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.WApp;
import com.weclipse.app.R;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.FollowingButtonDrawer;
import com.weclipse.app.drawer.TimelineVideoDrawer;
import com.weclipse.app.drawer.TimelineVideoDrawer.ViewHolderVideo;
import com.weclipse.app.managers.DownloadManager;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.DownloadVideoTask;
import com.weclipse.app.tasks.FollowTask;
import com.weclipse.app.tasks.UnfollowTask;
import com.weclipse.app.tasks.fetch.FetchMyVideosTask;
import com.weclipse.app.tasks.fetch.FetchWallTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.EndlessScrollListener;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DeleteVideoEvent;
import com.weclipse.app.utils.eventbus.FollowEvent;
import com.weclipse.app.utils.eventbus.InlineVideoStopEvent;
import com.weclipse.app.utils.eventbus.LikedVideoEvent;
import com.weclipse.app.utils.eventbus.ProfileUpdateEvent;
import com.weclipse.app.utils.eventbus.ReportEvent;
import com.weclipse.app.utils.eventbus.UnfollowEvent;
import com.weclipse.app.utils.eventbus.UnlikedVideoEvent;
import com.weclipse.app.utils.eventbus.WBus;
import com.weclipse.app.widget.InlineVideoPlayer;

public class ProfileActivity extends BaseActivity {

	ListView lvVideos;
	LinearLayout header;
	long user_id;

	LinearLayout container;

	VideoAdapter mVideoAdapter;

	ArrayList<TimelineVideo> mVideos;
	ArrayList<Long> mLikes;

	String name;
	String username;
	int n_videos;
	int n_friends;
	int n_followers;
	int n_following;
	boolean private_user;

	ImageView ivAvatar;
	TextView tvAvatar;

	TextView tvNFriends, tvNVideos, tvNFollowers, tvNFollowing;
	TextView tvName;
	String avatar_url;
	int following_status;
	int temp_status;

	ToggleButton bFollow;

	boolean locked;

	public UiLifecycleHelper uiHelper;
	
	InlineVideoPlayer currentIVP;
	int position = -1;
	
	ProgressBar loading;

	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);

		user_id = getIntent().getLongExtra("user_id", 0);

		setContentView(R.layout.activity_profile);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		initVariables();
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_PROFILE);
	    t.send(new HitBuilders.AppViewBuilder().build());	

		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(b);
	}

	public void initVariables() {
		container = (LinearLayout) findViewById(R.id.llContainer);

		header = (LinearLayout) getLayoutInflater().inflate(R.layout.header_user_profile, null);

		// find header elements
		tvName = (TextView) header.findViewById(R.id.tvName);
		tvNVideos = (TextView) header.findViewById(R.id.tvNVideos);
		tvNFollowers = (TextView) header.findViewById(R.id.tvNFollowers);
		tvNFollowing = (TextView) header.findViewById(R.id.tvNFollowing);
		tvNFriends = (TextView) header.findViewById(R.id.tvNFriends);

		tvAvatar = (TextView) header.findViewById(R.id.tvAvatar);
		ivAvatar = (ImageView) header.findViewById(R.id.ivAvatar);

		bFollow = (ToggleButton) header.findViewById(R.id.bFollow);

		if (user_id == Me.getInstance(app).getId()) {
			bFollow.setVisibility(View.INVISIBLE);
		}

		lvVideos = (ListView) findViewById(R.id.lvVideos);
		lvVideos.setDividerHeight(0);
		lvVideos.addHeaderView(header);
		
		lvVideos.setOnScrollListener(new EndlessScrollListener() {

			@Override
			public void onLoadMore(int page, int totalItemsCount) {
				new FetchWallTask(app, user_id,FetchWallTask.LOAD_MORE,getLastVideoId());
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

				if (position != -1 && ((position + 1) < firstVisibleItem || (position + 1) >= (firstVisibleItem + visibleItemCount))) {
					currentIVP.stop();
					currentIVP = null;
					position = -1;
				}
			}
		});
		
		loading = (ProgressBar)findViewById(R.id.pbLoading);

		locked = false;

	}
	
	public long getLastVideoId(){
		TimelineVideo v = mVideos.get(mVideos.size()-1);
		
//		WLog.d(this,"A devolver id de : "+v.getTitle()+" ("+v.getId()+")");
		
		return v.getId();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
			@Override
			public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
				Log.e("Activity", String.format("Error: %s", error.toString()));
			}

			@Override
			public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
				Log.i("Activity", "Success!");
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
		
		if (user_id != 0) {
			new FetchWallTask(app, user_id,FetchWallTask.FRESH,0);
		}
		
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
		
		if(currentIVP != null){
			currentIVP.stop();
			currentIVP = null;
			position = -1;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	public void drawUI() {
		drawTop();

		if (private_user && following_status != User.CLEAN_FOLLOW) {
			lvVideos.setVisibility(View.GONE);
			container.removeAllViews();
			container.addView(header);
			container.addView(getLayoutInflater().inflate(R.layout.profile_private_user, null));
			return;
		}
		
		if(mVideos != null){
			if (mVideos.size() == 0) {
				WLog.d(this, "Não tem video masé");
				lvVideos.setVisibility(View.GONE);
				container.removeAllViews();
				container.addView(header);
				container.addView(getLayoutInflater().inflate(R.layout.profile_no_videos, null));
				return;
			}
		} else {
			
		}

		updateListView();

	}

	public void drawTop() {
		User u = new User(this, user_id, username, name, avatar_url, following_status,private_user);
		getSupportActionBar().setTitle(username);

		tvName.setText(name);
		tvNVideos.setText(n_videos + " " + getString(R.string.profile_label_n_videos));

		tvNFollowers.setText(n_followers + " " + getString(R.string.profile_label_n_followers));
		tvNFollowing.setText(n_following + " " + getString(R.string.profile_label_n_following));
		tvNFriends.setText(n_friends + " " + getString(R.string.profile_label_n_friends));

		if (!private_user || following_status == User.CLEAN_FOLLOW) {
			tvNFriends.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(ProfileActivity.this, DetailActivity.class);
					i.putExtra("user_id", user_id);
					i.putExtra("type", DetailActivity.TYPE_FRIENDS);
					ProfileActivity.this.startActivity(i);
				}
			});

			tvNFollowers.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(ProfileActivity.this, DetailActivity.class);
					i.putExtra("user_id", user_id);
					i.putExtra("type", DetailActivity.TYPE_FOLLOWERS);
					ProfileActivity.this.startActivity(i);
				}
			});

			tvNFollowing.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(ProfileActivity.this, DetailActivity.class);
					i.putExtra("user_id", user_id);
					i.putExtra("type", DetailActivity.TYPE_FOLLOWING);
					ProfileActivity.this.startActivity(i);
				}
			});

			tvNVideos.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					lvVideos.smoothScrollBy(header.getHeight(), 1000);
				}
			});
		}

		drawFollowButton();

		/* */
		new AvatarDrawer(app, ivAvatar, u).draw();

	}

	public void followButton(View v) {
		if (!locked) {
			locked = true;
			temp_status = following_status;
			switch (following_status) {
			case User.CLEAN_FOLLOW:
			case User.PENDING_ACCEPT:
				new UnfollowTask(app, user_id);
				following_status = User.NOTHING;
				break;
			case User.NOTHING:
				new FollowTask(app, user_id);
				if(private_user){
					following_status = User.PENDING_ACCEPT;
				} else {
					following_status = User.CLEAN_FOLLOW;
				}
				break;
			}
			drawFollowButton();
		}
	}

	public void drawFollowButton() {
		new FollowingButtonDrawer(this, following_status, bFollow).draw();
	}

	public void updateListView() {
		if (mVideoAdapter == null) {
			mVideoAdapter = new VideoAdapter(this, mVideos);
			lvVideos.setAdapter(mVideoAdapter);
		} else {
			mVideoAdapter.swapData();
			mVideoAdapter.notifyDataSetChanged();
		}

		
	}

	public class VideoAdapter extends ArrayAdapter<TimelineVideo> {

		LayoutInflater mInflater;
		ArrayList<TimelineVideo> videos;

		public VideoAdapter(Context context, ArrayList<TimelineVideo> values) {
			super(context, R.layout.row_timeline, values);
			mInflater = LayoutInflater.from(context);
			this.videos = values;
		}
		
		public void swapData(){
			this.videos = mVideos;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			if (mVideos == null) {
				WLog.d(this, "Está nulo, foi inserido à força, é pq n há!");
				return null;
			}

			View v = convertView;
			ViewHolderVideo holder; // to reference the child views for later
									// actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_timeline, null);
				
				// cache view fields into the holder

				holder = new ViewHolderVideo();
				/* Texts */
				holder.caption = (TextView) v.findViewById(R.id.tvCaption);
				holder.tvHost = (TextView) v.findViewById(R.id.tvHost);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvLikes = (TextView) v.findViewById(R.id.tvLikes);
				holder.tvViews = (TextView) v.findViewById(R.id.tvViews);
				holder.tvComments = (TextView) v.findViewById(R.id.tvComments);
				/* Icons */
				holder.ivHost = (ImageView) v.findViewById(R.id.ivHost);
				holder.ivImage = (ImageView) v.findViewById(R.id.ivImage);
				holder.ivPrivate = (ImageView) v.findViewById(R.id.ivPrivate);
				holder.ivLikes = (ImageView) v.findViewById(R.id.ivLikes);
				holder.ivEditor = (ImageView) v.findViewById(R.id.ivEditor);
				
				/* Containers */
				holder.rlLikeContainer = (RelativeLayout) v.findViewById(R.id.rlLikeContainer);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.rlCommentsContainer = (RelativeLayout) v.findViewById(R.id.rlCommentsContainer);
				holder.rlShareContainer = (RelativeLayout) v.findViewById(R.id.rlShareContainer);
				holder.rlHost = (RelativeLayout) v.findViewById(R.id.rlHost);

				v.setTag(holder);
			} else {
				holder = (ViewHolderVideo) v.getTag();
			}

			TimelineVideoDrawer drawer = new TimelineVideoDrawer(ProfileActivity.this, app, videos.get(position), holder, 0);
			drawer.liked = mLikes.contains(videos.get(position).getId());
			drawer.draw();
			
			if(WApp.INLINE){
				
				if(holder.rlContainer.getChildAt(holder.rlContainer.getChildCount()-1) instanceof InlineVideoPlayer){
					holder.rlContainer.removeViewAt(holder.rlContainer.getChildCount()-1);
				}
				
				holder.rlContainer.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(ProfileActivity.this.position != position){
							
							AbstractVideo video = videos.get(position);
							
							if(!DownloadManager.getInstance(app).containsKey(video.getId())){
								DownloadVideoTask mTask = new DownloadVideoTask(app,video);
								DownloadManager.getInstance(app).put(video.getId(), mTask);
							}
							
							if(ProfileActivity.this.position != -1){
								currentIVP.stop();
								currentIVP = null;
								ProfileActivity.this.position = -1;
							}
							
							ProfileActivity.this.position = position;
							
							InlineVideoPlayer ivp = new InlineVideoPlayer(video, app, (RelativeLayout)v);
							ivp.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									WLog.d(this,"Clickei no IVP lolol");
								}
							});
							currentIVP = ivp;
							WBus.getInstance().register(ivp);
							
							RelativeLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
							ivp.setLayoutParams(params);
							
							((RelativeLayout)v).addView(ivp);
							((RelativeLayout)v).findViewById(R.id.videoProgress).setVisibility(View.VISIBLE);
						} else {
							WLog.d(this,"Already doing the stuff");
						}
					}
				});
				
			}

			return v;
		}

	}

	@Subscribe
	public void onAsyncTaskResult(ProfileUpdateEvent event) {
		loading.setVisibility(View.GONE);
		if(event.isSuccess()){
			this.name = event.getName();
			this.username = event.getUsername();
			if(mVideos == null){
				this.mVideos = event.getVideos();
			} else {
				this.mVideos.addAll(event.getVideos());
			}
			
			if(mLikes == null){
				this.mLikes = event.getLikes();				
			} else {
				this.mLikes.addAll(event.getLikes());
			}
			this.n_followers = event.getN_followers();
			this.n_friends = event.getN_friends();
			this.n_following = event.getN_following();
			this.n_videos = event.getN_videos();
			this.private_user = event.getPrivate();
			this.avatar_url = event.getAvatar_url();
			this.following_status = event.getFollowing_status();
			drawUI();
		} else {
			if(event.getCode() == HTTPStatus.NOT_FOUND){
				findViewById(R.id.rlDeletedAccount).setVisibility(View.VISIBLE);
			}
		}
	}

	@Subscribe
	public void onAsyncTaskResult(LikedVideoEvent event) {
		mLikes.add(event.getId());
		mVideoAdapter.notifyDataSetChanged();
	}

	@Subscribe
	public void onAsyncTaskResult(UnlikedVideoEvent event) {
		mLikes.remove(event.getId());
		mVideoAdapter.notifyDataSetChanged();
	}

	@Subscribe
	public void onAsyncTaskResult(FollowEvent event) {
		locked = false;
		if (event.getUser_id() == user_id) {
			if (event.getSuccess()) {
				/* Little update on the UI */
				n_followers = event.getN_followers();
				tvNFollowers.setText(n_followers + " " + getString(R.string.profile_label_n_followers));

				following_status = event.getStatus();
			} else {
				// Falhou
				following_status = User.NOTHING;
			}
			drawFollowButton();
		}
	}

	@Subscribe
	public void onAsyncTaskResult(UnfollowEvent event) {
		locked = false;
		if (event.getUser_id() == user_id) {
			if (event.getSuccess()) {
				/* Little update on the UI */
				n_followers = event.getN_followers();
				tvNFollowers.setText(n_followers + " " + getString(R.string.profile_label_n_followers));

				following_status = User.NOTHING;
			} else {
				following_status = temp_status;
				temp_status = 0;
			}
			drawFollowButton();
		}
	}

	@Subscribe
	public void onAsyncTaskResult(ReportEvent event) {
		if (event.getSuccess()) {
			Toast.makeText(this, getString(R.string.toast_report), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(InlineVideoStopEvent event){
		position = -1;
		currentIVP = null;
		
		if(event.getType() == InlineVideoStopEvent.CORRUPTED){
			Toast.makeText(this, "Corrupted file. Sorry for the incovenience", Toast.LENGTH_SHORT).show();
		}
		
	}

}
