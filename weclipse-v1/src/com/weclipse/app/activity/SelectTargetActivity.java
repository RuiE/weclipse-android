package com.weclipse.app.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.GroupAvatarDrawer;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.fetch.FetchPossiblePlayers;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.VideoPack;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.FetchPossiblePlayersEvent;
import com.weclipse.app.widget.PinnedHeaderListView;
import com.weclipse.app.widget.PinnedHeaderListView.PinnedHeaderAdapter;

public class SelectTargetActivity extends BaseActivity implements OnClickListener {
	
	WApp app;
	VideoPack vp;
	RelativeLayout bSend;
	FriendAdapter adapter;
	PinnedHeaderListView lv;
	Cursor cursor;
	ArrayList<Long> selectedGroup;
	ArrayList<Long> selectedContact;
	String[] names;
	boolean isSendingToGroup = false;
	boolean isSendingToFriend = false;
	boolean canSend;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (WApp)getApplication();
		setContentView(R.layout.activity_friend_selector);
		
		/* Setup action bar */
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.header_select_friends_activity));
		
		Bundle b = getIntent().getExtras();
		vp = new VideoPack(b.getInt("players"), b.getString("eta"), b.getString("filePath"),b.getString("caption"),b.getLong("duration"),b.getInt("media_type"),b.getInt("private_video"));
		selectedGroup = new ArrayList<Long>();
		selectedContact = new ArrayList<Long>();
		initVariables();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initVariables(){
		bSend = (RelativeLayout)findViewById(R.id.bSend);
		bSend.setOnClickListener(this);
		lv = (PinnedHeaderListView)findViewById(R.id.lvFriends);
		lv.setOverScrollMode(View.OVER_SCROLL_NEVER);
		lv.setDividerHeight(0);
		canSend = false;
		
		Tracker t = ((WApp)getApplication()).getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_SEND_CLIP);
	    t.send(new HitBuilders.AppViewBuilder().build());
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bSend:
			
			if(canSend){
				if(selectedContact.size() > 4 ){
					getToast().setText(getString(R.string.toast_too_many_people)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
				} else if(selectedContact.size() <= 4 ){
					Intent i = new Intent();
					
					long[] temp = new long[selectedContact.size()];
					for(int j = 0 ; j < selectedContact.size() ; j++)
						temp[j]=selectedContact.get(j);
					
					long[] temp2 = new long[selectedGroup.size()];
					for(int j = 0 ; j < selectedGroup.size() ; j++)
						temp2[j]=selectedGroup.get(j);
					
					
					i.putExtra("sendingContact", temp);
					i.putExtra("sendingGroup",temp2);
					i.putExtra("caption", vp.caption);
					i.putExtra("eta",vp.eta);
					i.putExtra("players",vp.players);
					i.putExtra("filePath",vp.filePath);
					i.putExtra("private_video", vp.private_video);
					
					if(isSendingToFriend && !isSendingToGroup)
						i.putExtra("target", WApp.TARGET_FRIENDS);
					else if(!isSendingToFriend && isSendingToGroup)
						i.putExtra("target",WApp.TARGET_GROUPS);
					else
						i.putExtra("target", WApp.TARGET_BOTH);
					
					setResult(RESULT_OK,i);
					if(cursor!=null)
						cursor.close();
					this.finish();
				}
			} else {
				getToast().setText(R.string.toast_choose_select).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			}
			break;
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		new FetchPossiblePlayers(app);
		updateListView();
	}
	
	public void updateListView(){
		if(cursor!=null)
			cursor.close();
		
		cursor = app.getDB().getSendTo();
		
		if(cursor.getCount() == 0){
			findViewById(R.id.rlNoFriendsCameraContainer).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.rlNoFriendsCameraContainer).setVisibility(View.GONE);
			adapter = new FriendAdapter(this,cursor);
			lv.setOnScrollListener(adapter);
			lv.setAdapter(adapter);
//			lv.setPinnedHeaderView(LayoutInflater.from(this).inflate(R.layout.pinned_header, lv, false));
		}
		
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED,null);
		this.finish();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(cursor != null)
			cursor.close();
	}
	
	public void incrementSelected(long id,int target){
		if(target == WApp.TARGET_GROUPS){
			isSendingToGroup = true;
			if(!selectedGroup.contains(id)){
				selectedGroup.add(id);
			}
		} else {
			isSendingToFriend = true;
			if(!selectedContact.contains(id)){
				selectedContact.add(id);
			}
		}
		
		canSend = true;
		bSend.setBackgroundResource(R.color.weclipse_green);
	}
	
	public void decrementSelected(long id,int target){
		
		if(target == WApp.TARGET_GROUPS){
			for(int i = 0 ; i < selectedGroup.size() ; i++){
				if(selectedGroup.get(i) == id){
					selectedGroup.remove(i);
					
					if(selectedGroup.size() == 0)
						isSendingToGroup = false;
					
					changeSendButton();
					return;
				}
			}
		} else {
			for(int i = 0 ; i < selectedContact.size() ; i++){
				if(selectedContact.get(i) == id){
					selectedContact.remove(i);
					
					if(selectedContact.size() == 0){
						isSendingToFriend = false;
					}
					
					changeSendButton();
					return;
				}
			}
		}
		
	}
	
	public void changeSendButton(){
		if(selectedGroup.size() == 0 && selectedContact.size() == 0){
			canSend = false;
			bSend.setBackgroundResource(R.color.comment_gray);
		}
	}
	
	private class FriendAdapter extends CursorAdapter implements OnClickListener,SectionIndexer,OnScrollListener,PinnedHeaderAdapter {

		LayoutInflater mInflater;
		String[] headers = {getString(R.string.activity_select_target_header_favorites),
							getString(R.string.activity_select_target_header_groups),
							getString(R.string.activity_select_target_header_friends)};
		
		Cursor c;
		
		public FriendAdapter(Context context, Cursor c) {
			super(context, c);
			mInflater = LayoutInflater.from(context);
			this.c=c;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
				
			
			WLog.d(this, "Count: "+c.getCount()+" Posição: "+position);
			
			c.moveToPosition(position);
			
			User u;
			Group group;
			
			long id = c.getLong(0);
//			String username = c.getString(c.getColumnIndex(DBHelper.C_USERNAME));
//			String name = c.getString(c.getColumnIndex(DBHelper.C_NAME));
//			String avatar_url = c.getString(c.getColumnIndex(DBHelper.C_AVATAR_URL));
//			int follow_status = c.getInt(c.getColumnIndex(DBHelper.C_FOLLOW_STATUS));
			
			ViewHolder holder;
			if(convertView == null){
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.row_friend_selector, null);
				holder.name = (TextView)convertView.findViewById(R.id.tvName);
				holder.container = (RelativeLayout)convertView.findViewById(R.id.rlContainer);
				holder.avatar = (ImageView)convertView.findViewById(R.id.ivAvatar);
				holder.cb = (CheckBox)convertView.findViewById(R.id.cSelector);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}
			
//			Log.d(TAG,"Id: "+id);
			
			switch(c.getInt(c.getColumnIndex("type"))){
			case WApp.TYPE_FAVORITE:
			case WApp.TYPE_FRIEND:
				u = UserCache.getInstance(app).getUser(id);
				
				//Render views
				holder.name.setText(u.getUsername());
				Bundle bContact = new Bundle();
				bContact.putLong("id",u.getId());
				bContact.putBoolean("friend", true);
				holder.container.setTag(bContact);
				
				new AvatarDrawer(app, holder.avatar, u).draw();
				
				if(!selectedContact.contains(id)){
					holder.cb.setChecked(false);
				} else {
					holder.cb.setChecked(true);
				}
				
				break;
			case WApp.TYPE_GROUPS:
				//Load asset
				group = new Group(SelectTargetActivity.this,id);
				group.load();
				
				//Render views
				holder.name.setText(group.getName());
				Bundle bGroup = new Bundle();
				bGroup.putLong("id",group.getId());
				bGroup.putBoolean("group", true);
				holder.container.setTag(bGroup);
				holder.avatar.setTag(group.getId());
				
				new GroupAvatarDrawer(app, holder.avatar, group).drawAvatar();
				
				if(!selectedGroup.contains(id)){
					holder.cb.setChecked(false);
				} else {
					holder.cb.setChecked(true);
				}
				break;
			}
			
			holder.container.setOnClickListener(this);
			
			bindSectionHeader(convertView, position);
			
			return convertView;
		}

		@Override
		public void bindView(View view, Context context, Cursor c) {
			
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup arg2) {
			return null;
		}

		@Override
		public void onClick(View v) {
			CheckBox cb = (CheckBox)v.findViewById(R.id.cSelector);
			cb.setChecked(!cb.isChecked());
			
			Bundle b = (Bundle)v.getTag();
			boolean group = b.getBoolean("group",false);
			boolean friend = b.getBoolean("friend",false);
			
			if(group){
				if(cb.isChecked())
					incrementSelected(b.getLong("id"),WApp.TARGET_GROUPS);
				else
					decrementSelected(b.getLong("id"),WApp.TARGET_GROUPS);
			}
			
			if(friend){
				if(cb.isChecked())
					incrementSelected(b.getLong("id"),WApp.TARGET_FRIENDS);
				else
					decrementSelected(b.getLong("id"),WApp.TARGET_FRIENDS);
			}
		}
		
		private class ViewHolder {
			RelativeLayout container;
			TextView name;
			ImageView avatar;
			RelativeLayout header;
			CheckBox cb;
		}
		
		private void bindSectionHeader(View itemView, int position) {
            final RelativeLayout headerView = (RelativeLayout) itemView.findViewById(R.id.rlHeader);
            final View dividerView = itemView.findViewById(R.id.list_divider);
            
            final int section = getSectionForPosition(position);
            
            if (getPositionForSection(section) == position) {
                String title = (String) getSections()[section];
                ((TextView)headerView.findViewById(R.id.tvHeader)).setText(title);
                headerView.setVisibility(View.VISIBLE);
            } else {
                headerView.setVisibility(View.GONE);
                dividerView.setVisibility(View.VISIBLE);
            }
    
            //move the divider for the last item in a section
            if (getPositionForSection(section + 1) - 1 == position) {
                dividerView.setVisibility(View.GONE);
            } else {
                dividerView.setVisibility(View.VISIBLE);
            }
		}

		public int getPinnedHeaderState(int position) {
			if (getCount() == 0) {
                return PINNED_HEADER_GONE;
            }

            if (position < 0) {
                return PINNED_HEADER_GONE;
            }

            // The header should get pushed up if the top item shown
            // is the last item in a section for a particular letter.
            int section = getSectionForPosition(position);
            int nextSectionPosition = getPositionForSection(section + 1);
            
            if (nextSectionPosition != -1 && position == nextSectionPosition - 1) {
                return PINNED_HEADER_PUSHED_UP;
            }

            return PINNED_HEADER_VISIBLE;
		}

		@Override
		public void configurePinnedHeader(View v, int position, int alpha) {
			TextView header = (TextView)v.findViewById(R.id.tvHeader);
            
            final int section = getSectionForPosition(position);
            final String title = (String) getSections()[section];
            
            header.setText(title);
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if (view instanceof PinnedHeaderListView) {
                ((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
            }  
		}

		@Override
		public Object[] getSections() {
            return headers;
		}

		public int getPositionForSection(int sectionIndex) {
			int position = c.getPosition();
			
			c.moveToFirst();
			boolean found=true;
			int res=0;
			int currentSection;
			do {
				currentSection = c.getInt(1);
				
				if(currentSection==sectionIndex){
					found = false;
					res = c.getPosition();
				} else {
					if(c.getPosition()==c.getCount()-1){
						found = false;
						res = 0;						
					} else {
						c.moveToNext();
					}
				}
				
			} while(found);
			
			
			c.moveToPosition(position);
			
			return res;
        }
                
        public int getSectionForPosition(int position) {
        	int res = 0;
        	
        	int pos = c.getPosition();
        	
        	c.moveToPosition(position);
        	res = c.getInt(1);
        	c.moveToPosition(pos);
        	
        	return res;
        }
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(FetchPossiblePlayersEvent event){
		if(event.getSuccess()){
			updateListView();
		}
	}

}
