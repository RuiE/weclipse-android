package com.weclipse.app.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.tasks.LoginTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.NetworkUtils;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.LoginEvent;
import com.weclipse.app.utils.eventbus.PreLoginEvent;

public class LoginActivity extends BaseActivity implements OnClickListener {
	
	Context context;
	EditText etEmail,etPass;
	Button bLogin;
	InputMethodManager manager;
	ProgressDialog dialog;
	TextView tvForgotPassword;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		context = this;
		setContentView(R.layout.activity_login);
		
		getSupportActionBar().hide();
		
		initVariables();
	}
	
	public void initVariables(){
		bLogin = (Button)findViewById(R.id.bLogin);
		bLogin.setOnClickListener(this);
		etEmail = (EditText)findViewById(R.id.etEmail);
		etPass = (EditText)findViewById(R.id.etPass);
		etPass.setOnClickListener(this);
		manager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		etPass.setOnEditorActionListener(new TextView.OnEditorActionListener(){
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_GO){
					onClick(bLogin);
					manager.hideSoftInputFromWindow(etPass.getWindowToken(),0);
					return true;
				}
				return false;
			}
        });
		
		dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        
        tvForgotPassword = (TextView)findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(this);
        
        Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_LOGIN);
	    t.send(new HitBuilders.AppViewBuilder().build());
        
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		app.isActive = false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		app.isActive = true;
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bLogin:
			//TODO Login mechanism
//			if(!app.isLogging){
				if(!NetworkUtils.isInternetConnected(app)){
					getToast().showNoInternet();
				} else if(validateLogin()){
					new LoginTask(app,etEmail.getText().toString(),etPass.getText().toString());
				}
//			}
			break;
		case R.id.tvForgotPassword:
			startActivity(new Intent(this,RecoverPasswordActivity.class));
			break;
		}
	}
	
	public boolean validateLogin(){
		if(etEmail.getText().toString().equals("") || etPass.getText().toString().equals("")){
			getToast().setText(getString(R.string.toast_empty_credentials)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}
		return true;
	}
	
	public void login(){
		app.login();
		startActivity(new Intent(this,TheMainActivity.class));
		this.finish();
	}
	
	@Override
	public void onBackPressed() {
		startActivity(new Intent(this,EntranceActivity.class));
		this.finish();
	}
	

	/*
	 * Subscribers
	 */
	
	@Subscribe
	public void onAsyncTasksResult(LoginEvent event){
		dialog.dismiss();
		
		switch(event.getType()){
		case LoginEvent.TYPE_SUCCESS:
			login();
			break;
		case LoginEvent.TYPE_TOO_MANY_TIMES:
			getToast().setText(app.getString(R.string.toast_password_attempts)).setBackground(R.color.weclipse_red).show(WToast.MEDIUM);
			break;
		case LoginEvent.TYPE_UNAUTHTORIZED:
			getToast().setText(getString(R.string.toast_invalid_credentials)).setBackground(R.color.weclipse_red).show(WToast.MEDIUM);
			break;
		}
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(PreLoginEvent event){
		dialog.setMessage(getString(R.string.logging_in));
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

}
