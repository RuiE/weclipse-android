package com.weclipse.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.models.Me;
import com.weclipse.app.tasks.InsertUsernameTask;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.eventbus.InsertUsernameEvent;

public class InsertUsernameActivity extends BaseActivity {
	
	EditText etUsername;
	RelativeLayout bExit;
	TextView tvHeader;
	ImageView ivAvatar;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		
		setContentView(R.layout.activity_insert_username);
		
		getSupportActionBar().hide();
		
		initVariables();
	}
	
	public void initVariables(){
		etUsername = (EditText)findViewById(R.id.etUsername);	
		bExit = (RelativeLayout)findViewById(R.id.rlBackContainer);
		tvHeader = (TextView)findViewById(R.id.tvHeader);
		tvHeader.setText(tvHeader.getText().toString()+" "+Me.getInstance(app).getName());
		
//		Picasso.with(app)
//				.load("http://avatars.dev.weclipse.co/174079388504033135")
//				.into((ImageView)findViewById(R.id.ivAvatar));
		
		new AvatarDrawer(app, (ImageView)findViewById(R.id.ivAvatar), Me.getInstance(app).getUser()).draw();
	}
	
	public void onBackClick(View v){
		setResult(Activity.RESULT_CANCELED);
		this.finish();
	}
	
	public void submitUsername(View v){
		if(etUsername.getText().toString().equals("")){
			Toast.makeText(app, getString(R.string.toast_empty_username), Toast.LENGTH_SHORT).show();
		} else {
			new InsertUsernameTask(app, etUsername.getText().toString());
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(InsertUsernameEvent event){
		switch(event.getCode()){
		case HTTPStatus.OK:
			setResult(Activity.RESULT_OK);
			this.finish();
			break;
		case HTTPStatus.BAD_REQUEST:
			Toast.makeText(this, getString(R.string.toast_username_already), Toast.LENGTH_SHORT).show();
			break;
		}
	}

}
