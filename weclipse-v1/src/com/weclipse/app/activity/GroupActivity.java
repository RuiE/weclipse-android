package com.weclipse.app.activity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.GroupAvatarDrawer;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.DeleteGroupTask;
import com.weclipse.app.tasks.LeaveGroupTask;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.DeleteGroupFinishEvent;
import com.weclipse.app.utils.eventbus.DeleteGroupInitEvent;
import com.weclipse.app.utils.eventbus.GroupLeaveEvent;
import com.weclipse.app.utils.eventbus.GroupUpdateEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.widget.LightTextView;
import com.weclipse.app.widget.SquareRelativeLayout;

import eu.janmuller.android.simplecropimage.CropImage;

public class GroupActivity extends BaseActivity {

	private static final String TAG = "Group Screen";
	
	private static final int FRIEND_CHOSER_ADD_CODE = 3;
	private static final int GROUP_PICTURE_TAKE = 4;
	private static final int GROUP_PICTURE_CROP = 5;
	private static final int GROUP_PICTURE_SELECT = 6;
	
	private static final int DIALOG_LEAVE_GROUP = 1;
	private static final int DIALOG_DELETE_GROUP = 2;
	
	Group g;
	Typeface light;
	TextView tvGroupName,tvClips;
	LightTextView tvNrMembers;
	EditText etGroupName;
	ScrollView container;
	RelativeLayout rlBanner;
	SquareRelativeLayout editName,editImage;
	Button bAddMember;
	ImageView banner;
	LinearLayout membersContainer;
	long gid;
	boolean isAdmin;
	Uri tempUri;
	ArrayList<Long> members;
	AlertDialog.Builder builder;
	
	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);
		gid = getIntent().getExtras().getLong("group_id");
		g = new Group(this,gid);
		g.load();
		isAdmin = (app.getId() == g.getUserId());
		
		getSupportActionBar().setTitle(g.getName());
		getSupportActionBar().setSubtitle(getString(R.string.activity_group_subtitle)+" "+Utils.getTimePast(this,g.getUpdatedAt()));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.activity_group);
		
		initVariables();
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(TAG);
	    t.send(new HitBuilders.AppViewBuilder().build());
	    
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		if(isAdmin){
			inflater.inflate(R.menu.menu_group_admin, menu);
		} else {
			inflater.inflate(R.menu.menu_group, menu);
		}
		return super.onCreateOptionsMenu(menu);
	}
	
	public void initVariables(){
		light = Typeface.createFromAsset(getAssets(),"Roboto-Light.ttf");
		
		container = (ScrollView)findViewById(R.id.svContainer);
		container.setOverScrollMode(View.OVER_SCROLL_NEVER);
		membersContainer = (LinearLayout)findViewById(R.id.llMembersContainer);
		
		tvGroupName = (TextView)findViewById(R.id.tvGroupName);
		tvClips = (TextView)findViewById(R.id.tvClips);
		tvNrMembers = (LightTextView)findViewById(R.id.tvNrMembers);
		
		tvGroupName.setText(g.getName());
		tvNrMembers.setText(""+g.getUsers().size()+getString(R.string.max_members_string));
		tvClips.setText(""+g.getNrVideos());
		
		drawMembers(g.getUsers());
		
		rlBanner = (RelativeLayout)findViewById(R.id.rlBannerContainer);
		Display dip = getWindowManager().getDefaultDisplay();
		int displayWidth;
		Point size = new Point();
		dip.getSize(size);
		displayWidth = size.x;
		float height = (float)9/16*displayWidth;
		LinearLayout.LayoutParams params = (LayoutParams) rlBanner.getLayoutParams();
		params.height = Math.round(height);
		
//		Log.d(TAG,"Grupo: "+g.getId());
		banner = (ImageView)findViewById(R.id.ivBanner);
		
		new GroupAvatarDrawer(app, banner, g).drawCover();
		
		builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.choose_from));
        builder.setItems(getResources().getStringArray(R.array.picture_options),
        					new DialogInterface.OnClickListener() {
					            public void onClick(DialogInterface dialog, int which) {
					                switch (which){
					                    case 0://Camera
					                    	takeGroupPicture();
					                        break;
					                    case 1://Galeria
					                        selectGroupPicture();
					                        break;
					                }
					            }
					        });
        builder.create();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		case R.id.action_add_member:
			Intent i = new Intent(getBaseContext(), FriendChoserActivity.class);
			i.putExtra("ids", members);
			startActivityForResult(i, FRIEND_CHOSER_ADD_CODE);
			break;
		case R.id.action_leave_group:
			new ConfirmDialogFragment(DIALOG_LEAVE_GROUP).show(getSupportFragmentManager(), "confirm_leave_group");
			break;
		case R.id.action_change_avatar:
			builder.show();
			break;
		case R.id.action_change_name:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    LayoutInflater inflater = getLayoutInflater();
		    final LinearLayout mView = (LinearLayout) inflater.inflate(R.layout.dialog_change_name, null);
		    ((EditText)mView.findViewById(R.id.etName)).setText(g.getName());
		    builder.setView(mView).setTitle(R.string.dialog_change_name_title)
		           .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {
		            	   String name = ((EditText)mView.findViewById(R.id.etName)).getText().toString();
		            	   g.update(DBHelper.C_NAME,name);
		            	   g.setName(name);
		            	   g.updateName(app);
		                   dialog.dismiss();
		               }
		           })
		           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {	
		               public void onClick(DialogInterface dialog, int id) {
		            	   dialog.cancel();
		               }
		           });
		    builder.create().show();
			break;
		case R.id.action_delete_group:
			new ConfirmDialogFragment(DIALOG_DELETE_GROUP).show(getSupportFragmentManager(), "confirm_delete_group");
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode==RESULT_OK){
			switch(requestCode){
			case GROUP_PICTURE_TAKE:
				cropCapturedImage();
				break;
			case GROUP_PICTURE_CROP:
			case GROUP_PICTURE_SELECT:
				updateGroupPicture();
				break;
			case FRIEND_CHOSER_ADD_CODE:
				ArrayList<Long> ids = new ArrayList<Long>();
				long[] temp = data.getLongArrayExtra("sending");
				for (int j = 0 ; j < temp.length ; j++){
					ids.add(temp[j]);
				}
				addMembers(ids);
				break;
			}
		} else {
			//Do nothing
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		app.setPointer(this);
		app.isActive = true;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		app.setPointer(null);
		app.isActive = false;
	}
	
	public void updateUI(){
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				tvGroupName.setText(g.getName());
				getSupportActionBar().setTitle(g.getName());
				getSupportActionBar().setSubtitle(getString(R.string.activity_group_subtitle)+" "+Utils.getTimePast(GroupActivity.this,g.getUpdatedAt()));
				drawMembers();		
				tvNrMembers.setText(""+g.getUsers().size()+getString(R.string.max_members_string));
			}
		});
	}
	
	public void updateMembers(ArrayList<Long> ids) {
		ArrayList<Long> wids = ids;
		for(int i = 0 ; i < ids.size() ; i++){
			WLog.d(this,"id: "+ids.get(i));
			User u = UserCache.getInstance(app).getUser(ids.get(i));
			RelativeLayout layout = (RelativeLayout)getLayoutInflater().inflate(R.layout.row_group_member, null);
			String sname = u.getUsername();
			((TextView)layout.findViewById(R.id.tvName)).setText(sname);
			layout.setTag(u.getId());
			
			WLog.d(this,"drawing this guy: "+sname);
			
			new AvatarDrawer(app, (ImageView)layout.findViewById(R.id.ivAvatar), u).draw();
			
			membersContainer.addView(layout);
		}
		addMembers(wids);
	}
	
	public void drawMembers(){
		clearMembers();
		drawMembers(g.getUsers());
	}
	
	public void clearMembers(){
		membersContainer.removeAllViews();
	}
	
	public void drawMembers(ArrayList<Long> arrayList){
		members = arrayList;
		for(int i = 0 ; i < members.size();i++){
			
			User u = UserCache.getInstance(app).getUser(members.get(i));
			
			RelativeLayout layout = (RelativeLayout)getLayoutInflater().inflate(R.layout.row_group_member, null);
			TextView tvUsername = (TextView)layout.findViewById(R.id.tvUsername);
			if(g.getUserId() == members.get(i)){ 
				tvUsername.setText(getString(R.string.group_administrator_caps));
				tvUsername.setTypeface(light);
			} else {
				tvUsername.setText(u.getName());
				tvUsername.setTextColor(getResources().getColor(R.color.comment_gray));
				tvUsername.setTextSize(12);
			}
			
			
			layout.setTag(u.getId());
			String sname;
			TextView tvName = (TextView)layout.findViewById(R.id.tvName);
			sname = u.getUsername();
			tvName.setText(sname);
			
			new AvatarDrawer(app,(ImageView)layout.findViewById(R.id.ivAvatar), u).draw();
			
			if(isAdmin && u.getId() != app.getId()){
				layout.setOnLongClickListener(new View.OnLongClickListener() {
					
					@Override
					public boolean onLongClick(View v) {
						memberOptions((Long)v.getTag()).show();
						return true;
					}
				});
			}
			
			layout.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent i = new Intent(GroupActivity.this, ProfileActivity.class);
					i.putExtra("user_id", (Long) v.getTag());
					startActivity(i);
				}
			});
			
			membersContainer.addView(layout);
		}
	}
	
	public Dialog memberOptions(final Long id) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setItems(getResources().getStringArray(R.array.member_options), new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
            	   switch(which){
            	   case 0:// Remove from group
            		   if(members.size()>Group.MIN_GROUP_SIZE){
	            		   g.removeMember(id);
	            		   g.updateMembers(app);
	            		   members.remove(id);
	            		   removeLayout(id);
            		   } else {
            			   getToast().setText(getString(R.string.toast_group_minimum_members)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
            		   }
            		   break;
            	   }
        	   }
	    });
	    builder.setTitle(UserCache.getInstance(app).getUsername(id));
	    return builder.create();
	}
	
	public void addFriend(View v){
		User u = (User)v.getTag();
		//TODO É para fazer follow
//		if(u.isContact()){
//			new ContactAddTask(app, u.getContact());
//		} else {
//			u.friend(app);
//		}
		
	}
	
	public void removeLayout(long user_id){
		for(int i = 0 ; i < membersContainer.getChildCount() ; i++){
			if((Long)membersContainer.getChildAt(i).getTag() == user_id)
				membersContainer.removeViewAt(i);
		}
	}
	
	public void updatePicture(Uri picUri) {
		banner.setImageURI(picUri);
		g.updateAvatar(app);
	}
	
	/* This needs to be called only from  */
	public void addMembers(ArrayList<Long> ids){
		g.addMembers(ids);
		g.updateMembers(app);
	}
	
	public void removeMember(long user_id){
		if(members.size()>2){
			for(int i=0;i<members.size();i++){
				if(members.get(i)==user_id){
					members.remove(i);
				}
			}
			removeLayout(user_id);
			g.setUsers(members);
		} else {
			getToast().setText(getString(R.string.toast_group_minimum_members)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
		}
	}
	
	/*
	 * Concerning group picture
	 */
	private Uri getTempUri() {
	    return Uri.fromFile(getTempFile());
    }
	
	private File getTempFile() {
	    File f = new File(WApp.TEMP_GROUP_LOC);
	    try {
	    	f.createNewFile();
	    } catch (IOException e) {
	    	
	    }
	    return f;
    }
	
	public void takeGroupPicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		tempUri = getTempUri();
		intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
		startActivityForResult(intent, GROUP_PICTURE_TAKE);
	}
	
	public void selectGroupPicture() {
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		pickPhoto.setType("image/*");
		pickPhoto.putExtra("aspectX", 16);
		pickPhoto.putExtra("aspectY", 9);
		pickPhoto.putExtra("outputX", Group.GROUP_BANNER_WIDTH);
		pickPhoto.putExtra("outputY", Group.GROUP_BANNER_HEIGHT);
		pickPhoto.putExtra("crop","true");
		pickPhoto.putExtra(MediaStore.EXTRA_OUTPUT,getTempUri());
		pickPhoto.putExtra("outputFormat",Bitmap.CompressFormat.JPEG.toString());
		startActivityForResult(pickPhoto, GROUP_PICTURE_SELECT);
	}

	public void cropCapturedImage() {
		Intent intent = new Intent(this, CropImage.class);
	    intent.putExtra(CropImage.IMAGE_PATH, WApp.TEMP_GROUP_LOC);
	    intent.putExtra(CropImage.SCALE, true);
	    intent.putExtra(CropImage.ASPECT_X, 16);
	    intent.putExtra(CropImage.ASPECT_Y, 9);
	    startActivityForResult(intent, GROUP_PICTURE_CROP);
	}
	
	public void cropCapturedImage(Uri picUri) {
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		cropIntent.setDataAndType(picUri, "image/*");
		cropIntent.putExtra("aspectX", 16);
		cropIntent.putExtra("aspectY", 9);
		cropIntent.putExtra("outputX", 320);
		cropIntent.putExtra("outputY", 180);
		cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
		startActivityForResult(cropIntent, GROUP_PICTURE_CROP);
	}
	
	public void updateGroupPicture() {
		updatePicture(Uri.fromFile(new File(WApp.TEMP_GROUP_LOC)));
	}
	
	@SuppressLint("ValidFragment")
	public class ConfirmDialogFragment extends DialogFragment {
		
		int action;
		
		public ConfirmDialogFragment(int action){
			this.action = action;
		}
		
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        if(action == DIALOG_DELETE_GROUP){
	        	builder.setTitle(R.string.delete_group);
	        } else {
	        	builder.setTitle(R.string.leave_group);
	        }
	        builder.setMessage(R.string.are_you_sure)
	               .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	   if(action == DIALOG_DELETE_GROUP){
	                		   new DeleteGroupTask(app, g);
	                	   } else {
	                		   new LeaveGroupTask(app,g);	                		   
	                	   }
	                   }
	               })
	               .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // do nothing
	                   }
	               });
	        return builder.create();
	    }
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}
	
	@Subscribe
	public void onAsyncTaskResult(DeleteGroupInitEvent event){
		if(event.getId() == g.getId()){
			getToast().setText(getString(R.string.toast_delete_group)).setBackground(R.color.dark_grey_75).show();
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(DeleteGroupFinishEvent event){
		getToast().hide();
		if(event.getId() == g.getId()){
			if(event.getSuccess()){
				g.destroy();
				GroupActivity.this.onBackPressed();
			} else {
				
			}
		}
	}
	
//	@Subscribe
//	public void onAsyncTaskResult(ContactAddEvent event){
//		drawMembers();
//		if(event.getStatus() == Contact.FOLLOWING){
//			getToast().setText(getString(R.string.toast_friend_requested)).setBackground(R.color.weclipse_green_75).show(WToast.MEDIUM);
//		} else if(event.getStatus() == Contact.FRIEND) {
//			getToast().setText(getString(R.string.toast_friend_added)).setBackground(R.color.weclipse_green_75).show(WToast.MEDIUM);
//		}
//	}
//	
//	@Subscribe
//	public void onAsyncTaskResult(ContactNotFoundEvent event){
//		getToast().setText(getString(R.string.toast_user_not_found)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
//	}
//	
//	@Subscribe
//	public void onAsyncTaskResult(ContactActionErrorEvent event){
//		getToast().setText(getString(R.string.toast_undone)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
//	}
	
	@Subscribe
	public void onAsyncTaskResult(GroupLeaveEvent event){
		if(event.getSuccess() && event.getId() == g.getId()){
			g.destroy();
			GroupActivity.this.onBackPressed();
		} else {
			getToast().setText(getString(R.string.toast_leave_group_fail)).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
		}
		
	}
	
	@Subscribe
	public void onAsyncTaskResult(GroupUpdateEvent event){
		if(event.getId() == g.getId()){
			updateUI();
		}
	}
	
}
