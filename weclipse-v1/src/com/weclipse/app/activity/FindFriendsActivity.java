package com.weclipse.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.Session;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.WLog;

public class FindFriendsActivity extends BaseActivity {

	boolean isFetching;

	boolean syncedFB;
	boolean hasFacebook;

	TextView tvFacebook;
	TextView tvContacts;

	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);

		setContentView(R.layout.activity_find_friends);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.find_header));

		initVariables();

		Tracker t = ((WApp)getApplication()).getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_ADD_FRIEND);
	    t.send(new HitBuilders.AppViewBuilder().build());
		
		WLog.d(this, "Has Fb? " + hasFacebook + ", Synced Fb? " + syncedFB);
	}

	public void initVariables() {

		tvFacebook = (TextView) findViewById(R.id.tvFacebook);
		tvContacts = (TextView) findViewById(R.id.tvContacts);

		hasFacebook = PreferenceManager.getInstance(app).getPreference(PreferenceManager.FACEBOOK);
		syncedFB = Session.getActiveSession() != null && Session.getActiveSession().isOpened();

		if (hasFacebook) {
			tvFacebook.setText(getString(R.string.find_facebook_connect));
		}

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_find_friends, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.onBackPressed();
			break;
		case R.id.action_search:
			Intent i = new Intent(this,SearchUserActivity.class);
			startActivity(i);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void openSyncFacebookActivity(View v) {
		Intent i = new Intent(this, SyncFacebookActivity.class);
		startActivity(i);
	}

	public void openSyncContactsActivity(View v) {
		Intent i = new Intent(this, SyncContactsActivity.class);
		startActivity(i);
	}

	public void shareApplication(View v) {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		sendIntent.putExtra(Intent.EXTRA_TEXT,getString(R.string.sms_invite));
		startActivity(Intent.createChooser(sendIntent, null));
	}

}
