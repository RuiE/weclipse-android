package com.weclipse.app.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.R;
import com.weclipse.app.drawer.AvatarDrawer;
import com.weclipse.app.drawer.FollowingButtonDrawer;
import com.weclipse.app.drawer.GroupAvatarDrawer;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Comment;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.Like;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.FollowTask;
import com.weclipse.app.tasks.SendCommentTask;
import com.weclipse.app.tasks.UnfollowTask;
import com.weclipse.app.tasks.fetch.FetchCommentsTask;
import com.weclipse.app.tasks.fetch.FetchFollowersTask;
import com.weclipse.app.tasks.fetch.FetchFollowingTask;
import com.weclipse.app.tasks.fetch.FetchFriendsTask;
import com.weclipse.app.tasks.fetch.FetchGroupsTask;
import com.weclipse.app.tasks.fetch.FetchLikesTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.FavoriteCache;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.FetchFollowersEvent;
import com.weclipse.app.utils.eventbus.FetchFollowingEvent;
import com.weclipse.app.utils.eventbus.FetchFriendsEvent;
import com.weclipse.app.utils.eventbus.FetchGroupsEvent;
import com.weclipse.app.utils.eventbus.FetchLikeEvent;
import com.weclipse.app.utils.eventbus.FollowEvent;
import com.weclipse.app.utils.eventbus.NewCommentEvent;
import com.weclipse.app.utils.eventbus.SendCommentInitEvent;
import com.weclipse.app.utils.eventbus.SendCommentResultEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.UnfollowEvent;
import com.weclipse.app.utils.eventbus.UserNotExistEvent;

public class DetailActivity extends BaseActivity {

	public static final int TYPE_PLAYERS = 0;
	public static final int TYPE_LIKE = 1;
	public static final int TYPE_COMMENT = 2;

	public static final int TYPE_FRIENDS = 3;
	public static final int TYPE_FOLLOWERS = 4;
	public static final int TYPE_FOLLOWING = 5;

	public static final int TYPE_GROUPS = 6;

	FavoriteCache mFavoriteCache;

	int type;

	AbstractVideo video;
	long user_id;

	ListView lvList;

	TextView tvNoText;
	RelativeLayout rlNoUsers;

	/* Video Adapters */
	LikeAdapter mLikeAdapter;
	CommentAdapter mCommentAdapter;
	PlayersAdapter mPlayerAdapter;
	/* User Adapters */
	FriendsAdapter mFriendsAdapter;
	FollowersAdapter mFollowersAdapter;
	FollowingAdapter mFollowingAdapter;
	GroupsAdapter mGrouspAdapter;

	ProgressBar loading;

	Cursor mCursor;
	boolean isTimeline;

	RelativeLayout footerGroup;

	/* Comments Stuff */
	RelativeLayout rlCommentBox;
	EditText etComment;
	Button bPost;
	LinearLayout footerViewPendingComments;

	ArrayList<User> mUsers;
	ArrayList<Integer> mStatuses;
	ArrayList<Like> mLikes;
	
	OnClickListener openProfile = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent i = new Intent(DetailActivity.this, ProfileActivity.class);
			i.putExtra("user_id", (Long) v.getTag());
			startActivity(i);
		}
	};

	OnClickListener follow = new OnClickListener() {

		@Override
		public void onClick(View v) {
			int position = (Integer) v.getTag();
			WLog.d(this, "Clicked follow!");
			switch (mUsers.get(position).getFollow_status()) {
			case User.CLEAN_FOLLOW:
			case User.PENDING_ACCEPT:
				new UnfollowTask(app, mUsers.get(position).getId());
				v.setBackgroundResource(R.drawable.button_following);
				break;
			case User.NOTHING:
				new FollowTask(app, mUsers.get(position).getId());
				if (mUsers.get(position).isPrivate()) {
					((Button) v).setText(getString(R.string.profile_button_follow_pending));
					v.setBackgroundResource(R.drawable.button_following_pending);
				} else {
					v.setBackgroundResource(R.drawable.button_following);
				}
				break;
			}
		}
	};

	OnClickListener favorite = new OnClickListener() {

		@Override
		public void onClick(View v) {
			long id = (Long) v.getTag();
			if (mFavoriteCache.contains(id)) {
				mFavoriteCache.removeFavorite(id);
			} else {
				mFavoriteCache.addFavorite(id);
			}
			mFriendsAdapter.notifyDataSetChanged();
		}
	};

	@Override
	protected void onCreate(Bundle b) {
		super.onCreate(b);

		type = getIntent().getIntExtra("type", 0);
		String title = "";

		if (type == TYPE_COMMENT || type == TYPE_LIKE || type == TYPE_PLAYERS) {
			title = getIntent().getStringExtra("caption");
			isTimeline = getIntent().getBooleanExtra("is_timeline", false);

			if (isTimeline) {
				video = new TimelineVideo(this, getIntent().getLongExtra("vid", 0));

			} else {
				video = new Video(this, getIntent().getLongExtra("vid", 0));
			}

			video.load();
			getSupportActionBar().setTitle(title);

		} else if (type == TYPE_FOLLOWERS || type == TYPE_FOLLOWING
				|| type == TYPE_FRIENDS || type == TYPE_GROUPS) {
			user_id = getIntent().getLongExtra("user_id", 0);
		}

		setContentView(R.layout.activity_detail_list);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    
		switch (type) {
		case TYPE_PLAYERS:
			getSupportActionBar().setSubtitle(getString(R.string.detail_header_players));
			t.setScreenName(AnalyticsHelper.S_PARTICIPANTS);
			break;
		case TYPE_LIKE:
			getSupportActionBar().setSubtitle(getString(R.string.detail_header_likes));
			t.setScreenName(AnalyticsHelper.S_LIKES);
			break;
		case TYPE_COMMENT:
			getSupportActionBar().setSubtitle(getString(R.string.detail_header_comments));
			t.setScreenName(AnalyticsHelper.S_COMMENTS);
			break;
		case TYPE_FRIENDS:
			getSupportActionBar().setTitle(getString(R.string.detail_header_friends));
			t.setScreenName(AnalyticsHelper.S_FRIENDS);
			break;
		case TYPE_FOLLOWERS:
			getSupportActionBar().setTitle(getString(R.string.detail_header_followers));
			t.setScreenName(AnalyticsHelper.S_FOLLOWERS);
			break;
		case TYPE_FOLLOWING:
			getSupportActionBar().setTitle(getString(R.string.detail_header_following));
			t.setScreenName(AnalyticsHelper.S_FOLLOWING);
			break;
		case TYPE_GROUPS:
			getSupportActionBar().setTitle(getString(R.string.detail_header_groups));
			t.setScreenName(AnalyticsHelper.S_GROUPS);
			break;
		}
		
		t.send(new HitBuilders.AppViewBuilder().build());

		initVariables();
	}

	public void initVariables() {

		lvList = (ListView) findViewById(R.id.lvList);
		lvList.setDividerHeight(0);

		if (type == TYPE_COMMENT) {
			rlCommentBox = (RelativeLayout) findViewById(R.id.rlCommentBox);
			rlCommentBox.setVisibility(View.VISIBLE);
			etComment = (EditText) findViewById(R.id.etComment);
			bPost = (Button) findViewById(R.id.bPost);

			etComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event) {

					if (actionId == EditorInfo.IME_ACTION_SEND) {
						postComment(null);
						return true;
					}
					return false;
				}
			});

			etComment.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// Do nothing
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// Do nothing
				}

				@Override
				public void afterTextChanged(Editable s) {
					if (s.toString().equals("")) {
						bPost.setTextColor(getResources().getColor(
								R.color.comment_gray));
					} else {
						bPost.setTextColor(getResources().getColor(
								R.color.weclipse_green));
					}
				}
			});

			footerViewPendingComments = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.footer_pending_comments, null);
			lvList.addFooterView(footerViewPendingComments);
			lvList.addFooterView(LayoutInflater.from(this).inflate(R.layout.footer_white, null));

		} else if (type == TYPE_GROUPS) {
			footerGroup = (RelativeLayout) getLayoutInflater().inflate(R.layout.footer_create_group, null);
			lvList.addFooterView(footerGroup);
		} else if (type == TYPE_FRIENDS) {
			if (user_id == Me.getInstance(this).getId()) {
				mFavoriteCache = FavoriteCache.getInstance(app);
				mFavoriteCache.load();
			}
		}

		tvNoText = (TextView) findViewById(R.id.tvNoText);
		rlNoUsers = (RelativeLayout) findViewById(R.id.rlNoUsers);

		loading = (ProgressBar) findViewById(R.id.pbLoading);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateListView();
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (type == TYPE_FRIENDS && mFavoriteCache != null) {
			WLog.d(this, "Saving");
			mFavoriteCache.save();
		}
	}

	public void updateListView() {
		long before_id = 0;
		switch (type) {
		case TYPE_COMMENT:
			loading.setVisibility(View.GONE);
			updateCommentUI();
			new FetchCommentsTask(app, video.getId(), FetchCommentsTask.SOURCE_FEED_DETAIL);
			break;
		case TYPE_LIKE:
			new FetchLikesTask(app, FetchLikesTask.FRESH, video);
			break;
		case TYPE_PLAYERS:
			WLog.d(this, "Updating Players UI");
			loading.setVisibility(View.GONE);
			updatePlayersUI();
			break;
		case TYPE_FRIENDS:
			new FetchFriendsTask(app, user_id, FetchFriendsTask.FRESH, before_id);
			break;
		case TYPE_FOLLOWERS:
			new FetchFollowersTask(app, user_id, FetchFollowersTask.FRESH, before_id);
			break;
		case TYPE_FOLLOWING:
			new FetchFollowingTask(app, user_id, FetchFollowingTask.FRESH, before_id);
			break;
		case TYPE_GROUPS:
			new FetchGroupsTask(app, FetchGroupsTask.FRESH, before_id);
			break;
		}
	}

	public void updateFriends(ArrayList<User> users) {
		mUsers = users;

		if (mUsers.size() == 0) {
			tvNoText.setText(getString(R.string.detail_no_friends));
			rlNoUsers.setVisibility(View.VISIBLE);
			lvList.setVisibility(View.GONE);
		} else {
			rlNoUsers.setVisibility(View.GONE);
			lvList.setVisibility(View.VISIBLE);
			mFriendsAdapter = new FriendsAdapter(this, users);
			lvList.setAdapter(mFriendsAdapter);
		}
	}

	public void updateFollowers(ArrayList<User> users) {
		this.mUsers = users;

		if (mUsers.size() == 0) {
			tvNoText.setText(getString(R.string.detail_no_followers));
			rlNoUsers.setVisibility(View.VISIBLE);
			lvList.setVisibility(View.GONE);
		} else {
			rlNoUsers.setVisibility(View.GONE);
			lvList.setVisibility(View.VISIBLE);
			mFollowersAdapter = new FollowersAdapter(this, mUsers);
			lvList.setAdapter(mFollowersAdapter);
		}

	}

	public void updateFollowing(ArrayList<User> users) {
		this.mUsers = users;

		if (mUsers.size() == 0) {
			tvNoText.setText(getString(R.string.detail_no_following));
			rlNoUsers.setVisibility(View.VISIBLE);
			lvList.setVisibility(View.GONE);
		} else {
			rlNoUsers.setVisibility(View.GONE);
			lvList.setVisibility(View.VISIBLE);
			mFollowingAdapter = new FollowingAdapter(this, mUsers);
			lvList.setAdapter(mFollowingAdapter);
		}
	}

	public void updateLikeUI(ArrayList<Like> likes, ArrayList<User> users) {
		mLikes = likes;
		mUsers = users;

		if (mUsers.size() == 0) {
			tvNoText.setText(getString(R.string.detail_no_likes));
			rlNoUsers.setVisibility(View.VISIBLE);
			lvList.setVisibility(View.GONE);
		} else {
			rlNoUsers.setVisibility(View.GONE);
			lvList.setVisibility(View.VISIBLE);
			mLikeAdapter = new LikeAdapter(this, likes, users);
			lvList.setAdapter(mLikeAdapter);
		}
	}

	public void updateGroups(ArrayList<Group> groups) {
		mGrouspAdapter = new GroupsAdapter(this, groups);
		lvList.setAdapter(mGrouspAdapter);
	}

	/*
	 * 
	 * Validate the comment: 1) Must be shorter than MAX_SIZE characters 2) Must
	 * not be empty 3) Must wait until the previous comment has been sent
	 */
	public boolean validateComment() {

		// WLog.d(this,"Tamanho: "+etComment.getText().toString().length());

		if (etComment.getText().toString().equals("")) {
			return false;
		}

		if (etComment.getText().toString().length() > Comment.MAX_SIZE) {
			getToast().setText(getString(R.string.toast_long_comment))
					.setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			return false;
		}

		return true;
	}

	public void postComment(View v) {
		if (validateComment()) {
			new SendCommentTask(app, etComment.getText().toString(), video);
			etComment.setText("");
		}
	}

	public void updatePlayersUI() {
		mUsers = new ArrayList<User>();

		for (int i = 0; i < video.getUsers().size(); i++) {

			User u = UserCache.getInstance(app).getUser(video.getUsers().get(i));

			if (u.exists()) {
				WLog.d(this, "Existe este mambo: " + u.getId());
			} else {
				WLog.d(this, "Nao existe este mambo: " + u.getId());
			}

			WLog.d(this, "E este é o toString: " + u.toString());

			if(video.getRepliedAts().get(i) == null || video.getRepliedAts().get(i).equals("null")){
			} else {
				mUsers.add(u);
			}
		}

		mPlayerAdapter = new PlayersAdapter(this, mUsers);
		lvList.setAdapter(mPlayerAdapter);
	}

	public void updateCommentUI() {
		if (mCursor != null)
			mCursor.close();

		mCursor = Comment.getComments(app, video.getId());

		if (mCursor.getCount() == 0) {
			// TODO no coments, be the first?
		} else {

			if (mCommentAdapter == null) {
				mCommentAdapter = new CommentAdapter(DetailActivity.this,
						mCursor);
				lvList.setAdapter(mCommentAdapter);
				lvList.setSelection(mCommentAdapter.getCount() - 1);
			} else {
				mCommentAdapter.changeCursor(mCursor);
				mCommentAdapter.notifyDataSetChanged();
				lvList.setSelection(mCommentAdapter.getCount() - 1);
			}

		}
	}

	public void drawPendingComments() {
		footerViewPendingComments.removeAllViews();
		Cursor c = app.getDB().getPendingComments(video.getId());
		if (c.getCount() > 0) {
			while (c.moveToNext()) {
				RelativeLayout row_comment = (RelativeLayout) getLayoutInflater().inflate(R.layout.row_comment_pending, null);

				Picasso.with(this).load(Me.getInstance(this).getAvatar()).into(((ImageView) row_comment.findViewById(R.id.ivAvatar)));

				SpannableString ss1 = new SpannableString(Me.getInstance(this).getUsername() + " " + c.getString(c.getColumnIndex(DBHelper.C_COMMENT)));

				ss1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.dark_grey)), 0, Me.getInstance(this).getUsername().length(), 0);

				((TextView) row_comment.findViewById(R.id.tvText)).setText(ss1);

				footerViewPendingComments.addView(row_comment);
			}

			if (mCommentAdapter != null)
				lvList.setSelection(mCommentAdapter.getCount());
		}
		c.close();
	}

	private class CommentAdapter extends CursorAdapter {

		LayoutInflater mInflater;

		public CommentAdapter(Context context, Cursor c) {
			super(context, c);
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public void bindView(View view, Context context, Cursor c) {

			ViewHolder holder = (ViewHolder) view.getTag();

			User u = UserCache.getInstance(app).getUser(c.getLong(c.getColumnIndex(DBHelper.C_USER_ID)));
			String sname = UserCache.getInstance(app).getUsername(u.getId());

			SpannableString ss1 = new SpannableString(sname + " " + c.getString(c.getColumnIndex(DBHelper.C_COMMENT)));

			ss1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.dark_grey)), 0, sname.length(), 0);

			holder.tvText.setText(ss1);
			holder.tvTime.setText(Utils.getTimePast(DetailActivity.this,c.getString(c.getColumnIndex(DBHelper.C_CREATED_AT))));

			AvatarDrawer drawer = new AvatarDrawer(app, holder.ivAvatar, u);
			drawer.draw();

			holder.rlAvatarContainer.setTag(u.getId());
			holder.rlAvatarContainer.setOnClickListener(openProfile);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
			ViewHolder holder = new ViewHolder();
			View v = mInflater.inflate(R.layout.row_list_comment, null);

			holder.tvText = (TextView) v.findViewById(R.id.tvText);
			holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
			holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
			holder.rlAvatarContainer = (RelativeLayout) v.findViewById(R.id.srlAvatarContainer);

			v.setTag(holder);
			return v;
		}

		private class ViewHolder {
			TextView tvText;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlAvatarContainer;
		}
	}

	private class LikeAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<Like> likes;
		ArrayList<User> users;

		public LikeAdapter(Context context, ArrayList<Like> values, ArrayList<User> users) {
			super(context, R.layout.row_list_like, values);
			mInflater = LayoutInflater.from(context);
			this.likes = values;
			this.users = users;
		}

		public void swapData() {
			this.likes = mLikes;
			this.users = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_like, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			Like l = likes.get(position);
			User u = users.get(position);

			new AvatarDrawer(app, holder.ivAvatar, u).draw();

			holder.tvName.setText(u.getUsername());
			holder.tvTime.setText(u.getName());

			holder.rlContainer.setTag(l.getUser_id());
			holder.rlContainer.setOnClickListener(openProfile);

			if (l.getUser_id() == Me.getInstance(DetailActivity.this).getId()) {
				holder.bAction.setVisibility(View.INVISIBLE);
			} else {
				holder.bAction.setVisibility(View.VISIBLE);

				new FollowingButtonDrawer(DetailActivity.this, mUsers.get(position).getFollow_status(), holder.bAction).draw();

				holder.bAction.setTag(position);
				holder.bAction.setOnClickListener(follow);
			}

			return v;
		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlContainer;
			ToggleButton bAction;
		}

	}

	private class PlayersAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<User> users;

		public PlayersAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.row_list_participant, values);
			mInflater = LayoutInflater.from(context);
			this.users = values;
		}

		public void swapData() {
			this.users = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_participant, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			User u = users.get(position);

			new AvatarDrawer(app, holder.ivAvatar, u).draw();
			holder.tvName.setText(u.getUsername());
			holder.tvTime.setText(u.getName());

			holder.rlContainer.setTag(u.getId());
			holder.rlContainer.setOnClickListener(openProfile);

			if (u.getId() == Me.getInstance(DetailActivity.this).getId()) {
				holder.bAction.setVisibility(View.INVISIBLE);
			} else {
				holder.bAction.setVisibility(View.VISIBLE);

				new FollowingButtonDrawer(DetailActivity.this, u.getFollow_status(), holder.bAction).draw();

				holder.bAction.setTag(position);
				holder.bAction.setOnClickListener(follow);
			}

			return v;
		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlContainer;
			ToggleButton bAction;
		}

	}

	private class FriendsAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<User> values;

		public FriendsAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.row_list_friend, values);
			mInflater = LayoutInflater.from(context);
			this.values = values;
		}

		public void swapData() {
			this.values = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_friend, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.ivAction = (ImageView) v.findViewById(R.id.ivAction);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.rlActionContainer = (RelativeLayout)v.findViewById(R.id.srlActionContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			if (user_id != Me.getInstance(DetailActivity.this).getId()) {
				holder.rlActionContainer.setVisibility(View.INVISIBLE);
			} else {
				holder.rlActionContainer.setVisibility(View.VISIBLE);
			}

			User user = values.get(position);

			AvatarDrawer drawer = new AvatarDrawer(app, holder.ivAvatar, user);
			drawer.draw();
			holder.rlContainer.setTag(user.getId());
			holder.rlContainer.setOnClickListener(openProfile);

			holder.tvName.setText(user.getUsername());
			holder.tvTime.setText(user.getName());

			if (user_id == Me.getInstance(DetailActivity.this).getId()) {

				if (user.getId() == Me.getInstance(DetailActivity.this).getId()) {
					holder.rlActionContainer.setVisibility(View.INVISIBLE);
				} else {
					holder.rlActionContainer.setVisibility(View.VISIBLE);
				}

				holder.bAction.setVisibility(View.GONE);

				holder.rlActionContainer.setTag(user.getId());
				holder.rlActionContainer.setOnClickListener(favorite);

				if (mFavoriteCache.contains(user.getId())) {
					holder.ivAction.setBackgroundResource(R.drawable.friend_favorite);
				} else {
					holder.ivAction.setBackgroundResource(R.drawable.friend_favorite_un);
				}

			} else {
				holder.rlActionContainer.setVisibility(View.INVISIBLE);

				if (user.getId() == Me.getInstance(DetailActivity.this).getId()) {
					holder.bAction.setVisibility(View.INVISIBLE);
				} else {
					holder.bAction.setVisibility(View.VISIBLE);

					new FollowingButtonDrawer(DetailActivity.this, user.getFollow_status(), holder.bAction).draw();

					holder.bAction.setTag(position);
					holder.bAction.setOnClickListener(follow);
				}

			}

			return v;

		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			ImageView ivAction;
			RelativeLayout rlContainer;
			RelativeLayout rlActionContainer;
			ToggleButton bAction;
		}

	}

	private class FollowingAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<User> values;

		public FollowingAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.row_list_following, values);
			mInflater = LayoutInflater.from(context);
			this.values = values;
		}

		public void swapData() {
			this.values = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_following, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			User user = values.get(position);

			AvatarDrawer drawer = new AvatarDrawer(app, holder.ivAvatar, user);
			drawer.draw();
			holder.rlContainer.setTag(user.getId());
			holder.rlContainer.setOnClickListener(openProfile);

			holder.tvName.setText(user.getUsername());
			holder.tvTime.setText(user.getName());

			if (user.getId() == Me.getInstance(DetailActivity.this).getId()) {
				holder.bAction.setVisibility(View.INVISIBLE);
			} else {
				holder.bAction.setVisibility(View.VISIBLE);

				new FollowingButtonDrawer(DetailActivity.this, user.getFollow_status(), holder.bAction).draw();

				holder.bAction.setTag(position);
				holder.bAction.setOnClickListener(follow);
			}

			return v;
		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlContainer;
			ToggleButton bAction;
		}

	}

	private class FollowersAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<User> values;

		public FollowersAdapter(Context context, ArrayList<User> values) {
			super(context, R.layout.row_list_follower, values);
			mInflater = LayoutInflater.from(context);
			this.values = values;
		}

		public void swapData() {
			this.values = mUsers;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_follower, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
				holder.bAction = (ToggleButton) v.findViewById(R.id.bAction);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			User user = values.get(position);

			AvatarDrawer drawer = new AvatarDrawer(app, holder.ivAvatar, user);
			drawer.draw();
			holder.rlContainer.setTag(user.getId());
			holder.rlContainer.setOnClickListener(openProfile);

			holder.tvName.setText(user.getUsername());
			holder.tvTime.setText(user.getName());

			if (user.getId() == Me.getInstance(DetailActivity.this).getId()) {
				holder.bAction.setVisibility(View.GONE);
			} else {
				holder.bAction.setVisibility(View.VISIBLE);

				new FollowingButtonDrawer(DetailActivity.this,
						user.getFollow_status(), holder.bAction).draw();

				holder.bAction.setTag(position);
				holder.bAction.setOnClickListener(follow);
			}

			return v;
		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlContainer;
			ToggleButton bAction;
		}

	}

	private class GroupsAdapter extends ArrayAdapter {

		LayoutInflater mInflater;
		ArrayList<Group> values;

		public GroupsAdapter(Context context, ArrayList<Group> values) {
			super(context, R.layout.row_list_group, values);
			mInflater = LayoutInflater.from(context);
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View v = convertView;
			ViewHolder holder; // to reference the child views for later actions

			if (v == null) {
				v = mInflater.inflate(R.layout.row_list_group, null);
				// cache view fields into the holder

				holder = new ViewHolder();
				holder.ivAvatar = (ImageView) v.findViewById(R.id.ivAvatar);
				holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
				holder.tvName = (TextView) v.findViewById(R.id.tvName);
				holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			Group group = values.get(position);

			holder.tvName.setText(group.getName());
			holder.tvTime.setText(" " + Utils.getTimePast(DetailActivity.this,group.getUpdatedAt()));

			new GroupAvatarDrawer(app, holder.ivAvatar, group).drawAvatar();

			holder.rlContainer.setTag(group.getId());
			holder.rlContainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(DetailActivity.this, GroupActivity.class);
					i.putExtra("group_id", ((Long) v.getTag()));
					startActivity(i);
				}
			});

			return v;

		}

		private class ViewHolder {
			TextView tvName;
			TextView tvTime;
			ImageView ivAvatar;
			RelativeLayout rlContainer;
		}

	}

	public void createNewGroup(View v) {
		Intent i = new Intent(this, CreateGroupActivity.class);
		startActivityForResult(i, TheMainActivity.GROUP_CREATE);
	}

	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event) {
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session),
				Toast.LENGTH_SHORT).show();
	}

	@Subscribe
	public void onAsyncTaskResult(FetchLikeEvent event) {
		loading.setVisibility(View.GONE);
		if (event.getId() == video.getId()) {
			updateLikeUI(event.getLikes(), event.getUsers());
		}
	}

	@Subscribe
	public void onAsyncTaskResult(SendCommentResultEvent event) {
		drawPendingComments();
		updateCommentUI();
	}

	@Subscribe
	public void onAsyncTaskResult(SendCommentInitEvent event) {
		drawPendingComments();
	}

	@Subscribe
	public void onAsyncTaskResult(NewCommentEvent event) {
		updateCommentUI();
	}

	@Subscribe
	public void onAsyncTaskResult(FetchFriendsEvent event) {
		loading.setVisibility(View.GONE);
		if (event.getSuccess()) {
			if (event.getId() == user_id) {
				updateFriends(event.getFriends());
			}
		} else {
			// Failed TODO show smth
		}
	}

	@Subscribe
	public void onAsyncTaskResult(FetchFollowingEvent event) {
		loading.setVisibility(View.GONE);
		if (event.getSuccess()) {
			if (event.getId() == user_id) {
				updateFollowing(event.getFollowing());
			}
		} else {
			// TODO failed, show smth
		}
	}

	@Subscribe
	public void onAsyncTaskResult(FetchFollowersEvent event) {
		loading.setVisibility(View.GONE);
		if (event.getSuccess()) {
			if (event.getId() == user_id) {
				updateFollowers(event.getFollowers());
			}
		} else {
			// TODO Falied, show smth
		}
	}

	@Subscribe
	public void onAsyncTaskResult(FetchGroupsEvent event) {
		loading.setVisibility(View.GONE);
		if (event.getSuccess()) {
			if (event.getGroups().size() > 0) {
				updateGroups(event.getGroups());
				findViewById(R.id.rlNoGroup).setVisibility(View.GONE);
				lvList.setVisibility(View.VISIBLE);
			} else {
				lvList.setVisibility(View.INVISIBLE);
				findViewById(R.id.rlNoGroup).setVisibility(View.VISIBLE);
			}
		} else {
			// TODO Failed, do something
		}
	}
	
	@Subscribe
	public void onAsyncTaskResult(UserNotExistEvent event){
		if(mUsers != null){
			switch (type) {
			case TYPE_FRIENDS:
				mFriendsAdapter.swapData();
				mFriendsAdapter.notifyDataSetChanged();
				break;
			case TYPE_PLAYERS:
				mPlayerAdapter.swapData();
				mPlayerAdapter.notifyDataSetChanged();
				break;
			case TYPE_LIKE:
				mLikeAdapter.swapData();
				mLikeAdapter.notifyDataSetChanged();
				break;
			case TYPE_FOLLOWERS:
				mFollowersAdapter.swapData();
				mFollowersAdapter.notifyDataSetChanged();
				break;
			case TYPE_FOLLOWING:
				mFollowingAdapter.swapData();
				mFollowingAdapter.notifyDataSetChanged();
				break;
			}
		}
		Toast.makeText(this, getString(R.string.user_not_exist), Toast.LENGTH_SHORT).show();
	}

	@Subscribe
	public void onAsyncTaskResult(FollowEvent event) {

		if (mUsers != null) {
			for (int i = 0; i < mUsers.size(); i++) {
				if (mUsers.get(i).getId() == event.getUser_id()) {
					mUsers.get(i).setFollow_status(event.getStatus());
					break;
				}
			}

			switch (type) {
			case TYPE_FRIENDS:
				mFriendsAdapter.swapData();
				mFriendsAdapter.notifyDataSetChanged();
				break;
			case TYPE_PLAYERS:
				mPlayerAdapter.swapData();
				mPlayerAdapter.notifyDataSetChanged();
				break;
			case TYPE_LIKE:
				mLikeAdapter.swapData();
				mLikeAdapter.notifyDataSetChanged();
				break;
			case TYPE_FOLLOWERS:
				mFollowersAdapter.swapData();
				mFollowersAdapter.notifyDataSetChanged();
				break;
			case TYPE_FOLLOWING:
				mFollowingAdapter.swapData();
				mFollowingAdapter.notifyDataSetChanged();
				break;
			}
		}
	}

	@Subscribe
	public void onAsyncTaskResult(UnfollowEvent event) {
		if (mUsers != null) {
			for (int i = 0; i < mUsers.size(); i++) {
				if (mUsers.get(i).getId() == event.getUser_id()) {
					mUsers.get(i).setFollow_status(User.NOTHING);
				}
			}

			switch (type) {
			case TYPE_FRIENDS:
				mFriendsAdapter.swapData();
				mFriendsAdapter.notifyDataSetChanged();
				break;
			case TYPE_PLAYERS:
				mPlayerAdapter.swapData();
				mPlayerAdapter.notifyDataSetChanged();
				break;
			case TYPE_LIKE:
				mLikeAdapter.swapData();
				mLikeAdapter.notifyDataSetChanged();
				break;
			case TYPE_FOLLOWERS:
				mFollowersAdapter.swapData();
				mFollowersAdapter.notifyDataSetChanged();
				break;
			case TYPE_FOLLOWING:
				mFollowingAdapter.swapData();
				mFollowingAdapter.notifyDataSetChanged();
				break;
			}
		}
	}

}
