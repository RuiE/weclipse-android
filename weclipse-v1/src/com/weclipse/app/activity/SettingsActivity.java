package com.weclipse.app.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.managers.StackManager;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DeleteAccountEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;

public class SettingsActivity extends BaseActivity {

	private static final String TAG = "SettingsAct";
	
	public static final String MAIN_TAG = "main";
	public static final String NOTIFICATION_TAG = "notification";
	public static final String WEBVIEW_TAG = "webview";
	
	public static final int PASSWORD = 2;
	
	public static final int PRIVACY = 1;
	public static final int TOS = 2;
	public static final int FEEDBACK = 3;
	
	WApp app;
	String currentTag;
	
	ArrayList<String> tags;
	public StackManager mStackManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		app = (WApp)getApplication();
		setContentView(R.layout.activity_settings);
		initVariables();
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			if(!mStackManager.getCurrentTag().equals(MAIN_TAG)){
				swapHeader(getString(R.string.header_settings_activity));
			}
			mStackManager.onBackPressed(); 
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void initVariables(){
		tags = new ArrayList<String>();
		tags.add(MAIN_TAG);
		tags.add(NOTIFICATION_TAG);
		tags.add(WEBVIEW_TAG);
		mStackManager = new StackManager(this, tags);
		mStackManager.init();
		
		getSupportActionBar().setTitle(getString(R.string.header_settings_activity));
		
		Tracker t = ((WApp)getApplication()).getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_SETTINGS);
	    t.send(new HitBuilders.AppViewBuilder().build());
		
	}
	
	public void swapHeader(String title){
		getSupportActionBar().setTitle(title);
	}
	
	public void logout(){
		app.clearDB();
		Intent returnIntent = new Intent();
		returnIntent.putExtra("logout",true);
		setResult(RESULT_OK,returnIntent);
		this.finish();
	}
	
	@Override
	public void onBackPressed() {
		mStackManager.onBackPressed();
		if(mStackManager.getCurrentTag().equals(MAIN_TAG)){
			swapHeader(getString(R.string.header_settings_activity));
		}
	}
	
	/*
	 * Subscribed
	 */
	@Subscribe
	public void onAsyncTaskResult(DeleteAccountEvent event) {
		WLog.d(this,"DeleteAccountEvent - event received!");
		this.logout();
	}
	
	@Subscribe
	public void onAsyncTaskResult(UnauthorizedEvent event){
		app.logout(false);
		app.clearDB();
		Intent i = new Intent(getBaseContext(), EntranceActivity.class);
		startActivity(i);
		this.finish();
		Toast.makeText(this, getString(R.string.toast_expired_session), Toast.LENGTH_SHORT).show();
	}

}
