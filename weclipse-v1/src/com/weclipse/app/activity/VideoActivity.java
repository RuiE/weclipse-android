package com.weclipse.app.activity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnInfoListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.WApp.TrackerName;
import com.weclipse.app.managers.CacheManager;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.tasks.DownloadVideoTask;
import com.weclipse.app.tasks.MarkViewedTask;
import com.weclipse.app.utils.AnalyticsHelper;
import com.weclipse.app.utils.PlayPack;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DownloadVideoCompleteEvent;
import com.weclipse.app.widget.VideoSurface;
/**
 * 
 * @author rsantos@weclipseapp.com
 * @deprecated This activity was replaced for the inline video in InlinveVideoPlayer class
 */
@Deprecated
public class VideoActivity extends BaseActivity implements OnCompletionListener,OnClickListener {

	private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private GestureDetector gestureDetector;
    View.OnTouchListener gestureListener;
	
	VideoSurface video;
	ImageView image;
	TextView caption;
	String path,zipPath;
	Long vid;
	int i;
	private MediaPlayer mediaPlayer;
    private int currentVideo = 0;
    View mask;
    RelativeLayout videoContainer;
    RelativeLayout errorContainer;
    int displayHeight,displayWidth;
    TimelineVideo v;
    int replied;
    SimpleDateFormat df;
    ArrayList<PlayPack> playpack;
    String totalCounter,actualCounter;
    int totalDuration,actualDuration,originalDuration;
    boolean hasZip;
    Runnable counterThread;
    long started;
    ActionBar actionBar;
    Handler updateCounter;
    
    ProgressBar loading;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().setFlags(LayoutParams.FLAG_SECURE,LayoutParams.FLAG_SECURE);
		setContentView(R.layout.activity_video);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		Display dip = getWindowManager().getDefaultDisplay();
		
		Point size = new Point();
		dip.getSize(size);
		displayHeight = size.y;
		displayWidth = size.x;
		
		int abHeight = getResources().getDimensionPixelSize(R.dimen.action_bar_size);
		
		mask = (View)findViewById(R.id.vMask);
		RelativeLayout.LayoutParams maskParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		maskParams.setMargins(0,(displayHeight/2)+(displayWidth/2)-abHeight,0,0);
		mask.setLayoutParams(maskParams);
		
		videoContainer = (RelativeLayout)findViewById(R.id.rlContainer);
		
		RelativeLayout.LayoutParams videoParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		videoParams.setMargins(0,(displayHeight/2)-(displayWidth/2)-abHeight,0,0);
		videoContainer.setLayoutParams(videoParams);
		
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(getString(R.string.video_activity_header));
		
		caption = (TextView)findViewById(R.id.tvCaption);
		
        video = (VideoSurface) findViewById(R.id.vVideo);
        image = (ImageView)findViewById(R.id.vImage);
        
        video.init();
        
        loading = (ProgressBar)findViewById(R.id.loading);
        loading.setIndeterminate(true);
    
        vid = getIntent().getExtras().getLong("vid");
	    
        //Download video...
		v = new TimelineVideo(this,vid);
		v.load();
		
		Picasso.with(this).load(v.getThumbnail_url()).into(image);
		new DownloadVideoTask(app,v);
		
		updateCounter = new Handler();
		
		counterThread = new Runnable() {
			@Override
			public void run() {
				long timePassed = System.currentTimeMillis()-started;
				actualDuration = (int) (timePassed/1000);
				
				if(actualDuration>9){
					actualCounter="00:"+actualDuration;
				} else {
					actualCounter="00:0"+actualDuration;
				}
				
				if(actualDuration <= totalDuration){
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							actionBar.setSubtitle(actualCounter+"/"+totalCounter);
						}
					});
				}
				
				if(timePassed < originalDuration){
					updateCounter.postDelayed(this,100);
				}
			}
		};
		
		gestureDetector = new GestureDetector(this, new MyGestureDetector());
		gestureListener = new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};
		
		findViewById(R.id.wholeContainer).setOnTouchListener(gestureListener);
		
		Tracker t = app.getTracker(TrackerName.APP_TRACKER);
	    t.setScreenName(AnalyticsHelper.S_VIDEO);
	    t.send(new HitBuilders.AppViewBuilder().build());   
	}
	
	public void unzipAndPlay(){
		new MarkViewedTask(app, v);
		
//		Lets see the stuff
//		Log.d(TAG,"Intensively logging the Video: ");
//		Log.d(TAG,v.toString());
		
		path = Environment.getExternalStorageDirectory().getAbsolutePath() + WApp.BASE_DIR+CacheManager.CACHE_DIR + vid + "/";
		zipPath = Environment.getExternalStorageDirectory().getAbsolutePath() + WApp.BASE_DIR+CacheManager.CACHE_DIR + vid + ".zip";
		
		hasZip = true;
		
		try {
			File zip = new File(zipPath);
			ZipFile zf = new ZipFile(zip);
			
			zf.extractAll(path);
			
		} catch (ZipException e1) {
			hasZip = false;
			e1.printStackTrace();
		}
		
		JSONObject json = null;
		
		if(hasZip){
			try {
				json = new JSONObject(WApp.getStringFromFile(path+vid+".json"));
			} catch (Exception e) {
				e.printStackTrace();
				setResult(RESULT_CANCELED);
				this.finish();
			}
	        
	        df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);
	        
			caption.setText(v.getTitle());
			
			playpack = new ArrayList<PlayPack>();
//			Log.d(TAG,"Logging durations...");
			try {
				JSONArray jarray = json.getJSONArray("clips");
				JSONObject clip;
				replied = jarray.length();
				
				for(int i = 0 ; i < replied ; i++ ){
					clip = jarray.getJSONObject(i);
					playpack.add(new PlayPack(clip.getString("filename"), clip.getInt("duration"),clip.getInt("media_type")));
					originalDuration += clip.getInt("duration");	
				}
				
			} catch (JSONException e) {
				if(WApp.DEBUG){
					e.printStackTrace();
				}
			}
			
//			Log.d(TAG,"Duracao total: "+originalDuration);
			totalDuration = (int)Math.floor((float)originalDuration/1000);
			actualDuration = 0;
			
//			Log.d(TAG,"New total duration: "+totalDuration);
			
			if(totalDuration > 9){
				totalCounter = "00:"+totalDuration;
			} else {
				totalCounter = "00:0"+totalDuration;
			}
			actualCounter = "00:00";
			
			actionBar.setSubtitle(actualCounter+"/"+totalCounter);
			
			currentVideo = 0;
		} else {
			videoContainer.setVisibility(View.GONE);
			errorContainer = (RelativeLayout)findViewById(R.id.rlErrorContainer);
			errorContainer.setVisibility(View.VISIBLE);
			actionBar.setSubtitle("00:00/00:00");
			Tracker t = app.getTracker(TrackerName.APP_TRACKER);
			t.send(new HitBuilders.EventBuilder()
					.setCategory(AnalyticsHelper.C_VIDEO)
					.setAction(AnalyticsHelper.A_VIDEO_NOT_FOUND)
					.build());
		}
		
		playNext();
		startCounter();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			this.onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
//		zipPath
		/* Closing the activity is too slow, lets thread the deleting */
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					File f = new File(path);
					File[] files = f.listFiles();
					for(int i = 0 ; i < files.length ; i++ ){
						files[i].delete();
					}
					f.delete();
				} catch(Exception e){
//					Log.d(TAG,"Exception on file delete");
				}
				
			}
		}).start();
		updateCounter.removeCallbacks(counterThread);
		video.setVisibility(View.GONE);
	}
	
	public void setResult(){
		setResult(RESULT_OK);
	}
	
	@Override
	public void onBackPressed() {
		setResult();
		this.finish();
	}
	
	public void playNext(){
//		Log.d(TAG,"CurrentVideo: "+currentVideo);
//		Log.d(TAG,"Get Repied: "+replied);
		if(currentVideo<replied){
			if(playpack.get(currentVideo).media_type==CameraActivity.MEDIA_TYPE_IMAGE){
				playImage(path+playpack.get(currentVideo).fileName);
			} else {
				playVideo(path+playpack.get(currentVideo).fileName);
			}
			currentVideo++;
		} else {
			onBackPressed();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
    	mediaPlayer.release();
    	playNext();
	}
		
	
	private void playImage(String imagePath){
		image.bringToFront();
		File imgFile = new  File(imagePath);
		if(imgFile.exists()){
			Bitmap tempBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
			image.setImageBitmap(tempBitmap);
		    image.postDelayed(new Runnable() {
				@Override
				public void run() {
					image.setImageBitmap(null);
					playNext();
				}
			},playpack.get(currentVideo).duration);
		}
	}
	
	private void playVideo(String videoPath) {
		WLog.d(this,"Playing path: "+videoPath);
        try {
        	
        	mediaPlayer = new MediaPlayer();
    		mediaPlayer.setOnCompletionListener(this);
    		mediaPlayer.setOnInfoListener(new OnInfoListener() {
				
				@Override
				public boolean onInfo(MediaPlayer mp, int what, int extra) {
					if(what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START){
						image.setVisibility(View.GONE);
					}
					return false;
				}
			});
    		mediaPlayer.setDisplay(video.getHolder());
            mediaPlayer.setDataSource(videoPath);
            mediaPlayer.prepare();
            
            int videoWidth = mediaPlayer.getVideoWidth();
    	    int videoHeight = mediaPlayer.getVideoHeight();
    	    @SuppressWarnings("deprecation")
    		int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
    	    
    	    android.view.ViewGroup.LayoutParams lp = video.getLayoutParams();

    	    lp.width = screenWidth;
    	    //Set the height of the SurfaceView to match the aspect ratio of the video 
    	    //be sure to cast these as floats otherwise the calculation will likely be 0
    	    lp.height = (int) (((float)videoHeight / (float)videoWidth) * (float)screenWidth);
    	    video.setLayoutParams(lp);
            
            mediaPlayer.start();

        } catch (IllegalArgumentException e) {
        	if(WApp.DEBUG)
        		Log.d("MEDIA_PLAYER", e.getMessage());
        } catch (IllegalStateException e) {
        	if(WApp.DEBUG)
        		Log.d("MEDIA_PLAYER", e.getMessage());
        } catch (SecurityException e) {
        	if(WApp.DEBUG)
        		e.printStackTrace();
		} catch (IOException e) {
			if(WApp.DEBUG)
				e.printStackTrace();
			
			// Bug do iOS
			currentVideo++;
			playNext();
		}
    }
	
	public void startCounter(){
		started = System.currentTimeMillis();
		updateCounter.postDelayed(counterThread, 100);
	}
	
	public void incrementCounter(){
		started -= mediaPlayer.getDuration()-mediaPlayer.getCurrentPosition();
	}
	
	class MyGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                
                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	incrementCounter();
                	VideoActivity.this.onCompletion(mediaPlayer);
                }
            } catch (Exception e) {
                // nothing
            }
            return false;
        }

            @Override
        public boolean onDown(MotionEvent e) {
              return true;
        }
    }
	
	@Subscribe
	public void onAsyncTaskResult(DownloadVideoCompleteEvent event){
		WLog.d(this,"Done download");
		if(vid == event.getId()){
			if(event.getSuccess()){
				loading.setVisibility(View.GONE);
				unzipAndPlay();
			} else {
				
			}
		}
	}
	
}
