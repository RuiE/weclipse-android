package com.weclipse.app.utils;

import android.support.v4.util.LongSparseArray;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;

public class UserCache {
	
	LongSparseArray<User> users;
	
	private static UserCache mUserCache;
	WApp app;
	
	private UserCache(WApp app){
		this.app = app;
		users = new LongSparseArray<User>();
	}
	
	public static UserCache getInstance(WApp app){
		if(mUserCache == null)
			mUserCache = new UserCache(app);
		
		return mUserCache;
	}
	
	public boolean containsUser(long id){
		return users.get(id) != null;
	}
	
	public String getUsername(long id){
		return getUser(id).getUsername();
	}
	
	public User getUser(long id){
		User u = users.get(id);
		
		if(id == Me.getInstance(app).getId()){
			u = Me.getInstance(app).getUser();
			updateUser(u);
		}
		
		if(u == null){
			u = new User(app, id);
			u.load();
		}
		
		return u;
	}
	
	public void updateUser(User u){
		users.put(u.getId(), u);
		u.create();
	}
	
}
