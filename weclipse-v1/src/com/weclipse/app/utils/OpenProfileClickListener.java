package com.weclipse.app.utils;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.weclipse.app.activity.ProfileActivity;

public class OpenProfileClickListener implements OnClickListener {
	
	Context ctx;
	
	public OpenProfileClickListener(Context ctx){
		this.ctx = ctx;
	}

	@Override
	public void onClick(View v) {
		Intent i = new Intent(ctx, ProfileActivity.class);
		i.putExtra("user_id", (Long) v.getTag());
		ctx.startActivity(i);
	}
	
	public void setTag(View v,long id){
		v.setTag(id);
	}


}
