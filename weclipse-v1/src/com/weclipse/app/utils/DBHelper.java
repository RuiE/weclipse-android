package com.weclipse.app.utils;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;
import android.util.Log;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.CacheRequestManager;
import com.weclipse.app.models.Comment;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;

public class DBHelper extends SQLiteOpenHelper {

	static final String TAG = "DBHelper";
	static final int DB_VERSION = 127;

	public static final String TABLE_USERS = "users";
	public static final String TABLE_VIDEOS = "videos";
	// public static final String TABLE_CONTACTS = "contacts";
	public static final String TABLE_GROUPS = "groups";
	public static final String TABLE_FRIENDS = "friends";

	public static final String TABLE_MY_VIDEOS = "my_videos";
	public static final String TABLE_LIKES_MY_VIDEOS = "my_video_likes";

	public static final String TABLE_TIMELINE = "timeline";
	public static final String TABLE_LIKES_TIMELINE = "likes_timeline";
	public static final String TABLE_LIKES_ACTIVITY = "likes_activity";

	public static final String TABLE_ACTIONS = "scheduled_actions";
	public static final String TABLE_PENDING_VIDEOS = "pending_videos";
	public static final String TABLE_REPLY_PENDING_VIDEOS = "pending_reply_videos";

	public static final String TABLE_COMMENTS = "comments";
	public static final String TABLE_COMMENTS_PENDING = "pending_comments";
	public static final String TABLE_COMMENT_NOTIFICATIONS = "comment_notifications";

	public static final String TABLE_FAVORITES = "favorites";
	public static final String TABLE_VIDEO_NOTIFICATIONS = "video_notifications";

	public static final String TABLE_ACTIVITY = "activity";

	public static final String C_ID = BaseColumns._ID;
	public static final String C_WECLIPSE_ID = "wid";
	public static final String C_TITLE = "title";
	public static final String C_PLAYERS = "players";
	public static final String C_ETA = "eta";
	public static final String C_USER_ID = "user_id";
	public static final String C_GROUP_ID = "group_id";
	public static final String C_TARGET = "target";
	public static final String C_REPLIED = "replied";
	public static final String C_STATUS = "status";
	public static final String C_USER_STATUS = "user_status";
	public static final String C_CREATED_AT = "created_at";
	public static final String C_UPDATED_AT = "updated_at";
	public static final String C_EXPIRES_AT = "expires_at";
	public static final String C_GROUP_NAME = "group_name";
	public static final String C_NAME = "name";
	public static final String C_USERNAME = "username";
	public static final String C_AVATAR_URL = "avatar_url";
	public static final String C_COVER_URL = "cover_url";
	public static final String C_FOLLOW_STATUS = "follow_status";
	public static final String C_PHONE = "phone";
	public static final String C_USERS = "users";
	public static final String C_GROUPS = "groups";
	public static final String C_STATUSES = "statuses";
	public static final String C_NR_VIDEOS = "nr_videos";
	public static final String C_MEDIA_TYPES = "media_types";
	public static final String C_DURATIONS = "durations";
	public static final String C_REPLIED_AT = "replied_at";
	public static final String C_VIDEO_STATUS = "video_status";
	public static final String C_INVITED = "invited";
	public static final String C_RECENT = "recent";
	public static final String C_READY_AT = "ready_at";
	public static final String C_PRIVATE_VIDEO = "private";
	public static final String C_EDITORS_CHOICE = "editors_choice";
	public static final String C_PRIORITY = "priority";
	public static final String C_PRIVATE_USER = "private_user";
	public static final String C_THUMBNAIL_URL = "thumbnail_url";
	public static final String C_SHARE_URL = "share_url";
	public static final String C_REWECLIPSED_BY = "reweclipsed_by_id";

	/* Columns for social media */
	public static final String C_N_LIKES = "n_likes";
	public static final String C_N_VIEWS = "n_views";
	public static final String C_N_COMMENTS = "n_comments";

	/* Additional columns for NetworkManager */
	public static final String C_MEDIA_TYPE = "media_type";
	public static final String C_PATH = "path";
	public static final String C_DURATION = "duration";
	public static final String C_PROVIDED_ID = "provided_id";
	public static final String C_TYPE = "type";
	public static final String C_VIDEO_ID = "vid";

	/* Additional columns for Comments feature */
	public static final String C_COMMENT = "comment";
	public static final String C_COMMENT_ID = "comment_id";
	public static final String C_NEW_COMMENTS = "new_comments";
	public static final String C_NOTIFICATION_ID = "notification_id";

	public static final String C_SORTABLE_NAME = "sortable_name";

	Context context;
	boolean isOpened;

	public DBHelper(Context context) {
		super(context, WApp.DB_NAME, null, DB_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createUsers(db);
		// createContacts(db);
		createGroups(db);
		createVideos(db);
		createPendingVideos(db);
		createActions(db);
		createComments(db);
		createFavorites(db);
		createPendingComments(db);
		createVideoNotifications(db);
		createTimeline(db);
		createActivityFeed(db);
		createMyVideos(db);
		// Log.d(TAG,"Created");
	}

	public void createUsers(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_USERS + " (" + C_ID + " integer primary key," + C_USERNAME + " text," + C_NAME + " text," + C_AVATAR_URL + " text, " + C_STATUS + " int," + C_FOLLOW_STATUS + " int," + C_PRIVATE_USER + " integer)");
		db.execSQL("create table " + TABLE_FRIENDS + " (" + C_ID + " integer primary key)");
	}

	// public void createContacts(SQLiteDatabase db){
	// db.execSQL("create table "+TABLE_CONTACTS+" ("+C_ID+" integer primary key autoincrement, "+C_WECLIPSE_ID+" integer, "+C_NAME+" text, "+C_PHONE+" text, "+C_USERNAME+" text,"+C_AVATAR+" int, "+C_STATUS+" int,"+C_INVITED+" int default 0,"+C_RECENT+" int default 0)");
	// }

	public void createFavorites(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_FAVORITES + " (" + C_ID + " integer primary key)");
	}

	public void createVideos(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_VIDEOS + " (" + C_ID + " integer primary key, " + C_TITLE + " text, " + C_PLAYERS + " int, " + C_ETA + " text, " + C_USER_ID + " integer, " + C_GROUP_ID + " integer, " + C_TARGET + " int, " + C_REPLIED + " int, " + C_STATUS + " int," + C_USER_STATUS + " int," + C_USERS + " text, " + C_STATUSES + " text," + C_REPLIED_AT + " text," + C_VIDEO_STATUS + " int default " + Video.CONFIRMED + "," + C_CREATED_AT + " date, " + C_UPDATED_AT + " date," + C_EXPIRES_AT + " date," + C_PRIVATE_VIDEO + " int," + C_N_LIKES + " int," + C_N_VIEWS + " int," + C_N_COMMENTS + " int," + C_THUMBNAIL_URL + " text," + C_SHARE_URL + " text)");
	}

	public void createGroups(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_GROUPS + " (" + C_ID + " integer primary key, " + C_NAME + " text, " + C_USER_ID + " integer, " + C_USERS + " text," + C_AVATAR_URL + " text," + C_COVER_URL + " text," + C_NR_VIDEOS + " int," + C_UPDATED_AT + " text)");
	}

	public void createPendingVideos(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_PENDING_VIDEOS + " (" + C_ID + " integer primary key," + C_TYPE + " int," + C_VIDEO_ID + " integer," + C_USER_ID + " integer," + C_MEDIA_TYPE + " int, " + C_PATH + " text, " + C_TARGET + " int, " + C_PLAYERS + " int," + C_USERS + " text," + C_GROUPS + " text," + C_ETA + " text," + C_TITLE + " text," + C_DURATION + " int)");
		db.execSQL("create table " + TABLE_REPLY_PENDING_VIDEOS + " (" + C_ID + " integer primary key)");
	}

	public void createActions(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_ACTIONS + " (" + C_ID + " integer primary key autoincrement," + C_TYPE + " int," + C_PROVIDED_ID + " integer)");
	}

	public void createComments(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_COMMENTS + " (" + C_ID + " integer primary key, " + C_VIDEO_ID + " integer," + C_USER_ID + " integer," + C_COMMENT + " text," + C_CREATED_AT + " date)");
		db.execSQL("create table " + TABLE_COMMENT_NOTIFICATIONS + " (" + C_VIDEO_ID + " integer primary key," + C_NEW_COMMENTS + " integer, " + C_NOTIFICATION_ID + " integer)");
	}

	public void createPendingComments(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_COMMENTS_PENDING + " (" + C_ID + " integer primary key, " + C_VIDEO_ID + " integer," + C_COMMENT + " text)");
	}

	public void createVideoNotifications(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_VIDEO_NOTIFICATIONS + " (" + C_ID + " integer primary key," + C_TYPE + " integer)");
	}

	public void createMyVideos(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_MY_VIDEOS + " (" + C_ID + " integer primary key, " + C_TITLE + " text, " + C_USER_ID + " integer, " + C_GROUP_ID + " integer," + C_USERS + " text," + C_REPLIED_AT + " text," + C_VIDEO_STATUS + " int default " + Video.CONFIRMED + "," + C_CREATED_AT + " date, " + C_UPDATED_AT + " date," + C_READY_AT + " date," + C_N_LIKES + " int," + C_N_VIEWS + " int," + C_N_COMMENTS + " int," + C_PRIVATE_VIDEO + " int," + C_EDITORS_CHOICE + " int," + C_THUMBNAIL_URL + " text," + C_SHARE_URL + " text,"+C_REWECLIPSED_BY+" integer)");
		db.execSQL("create table " + TABLE_LIKES_MY_VIDEOS + " (" + C_ID + " integer primary key)");
	}

	public void createTimeline(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_TIMELINE + " (" + C_ID + " integer primary key, " + C_TITLE + " text, " + C_USER_ID + " integer, " + C_GROUP_ID + " integer," + C_USERS + " text," + C_REPLIED_AT + " text," + C_VIDEO_STATUS + " int default " + Video.CONFIRMED + "," + C_CREATED_AT + " date, " + C_UPDATED_AT + " date," + C_READY_AT + " date," + C_N_LIKES + " int," + C_N_VIEWS + " int," + C_N_COMMENTS + " int," + C_PRIVATE_VIDEO + " int," + C_EDITORS_CHOICE + " int," + C_THUMBNAIL_URL + " text," + C_SHARE_URL + " text,"+C_REWECLIPSED_BY+" integer)");
		db.execSQL("create table " + TABLE_LIKES_ACTIVITY + " (" + C_ID + " integer primary key)");
		db.execSQL("create table " + TABLE_LIKES_TIMELINE + " (" + C_ID + " integer primary key)");
	}

	public void createActivityFeed(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_ACTIVITY + " (" + C_ID + " integer primary key, " + C_TYPE + " int," + C_UPDATED_AT + " date," + C_USER_ID + " integer," + C_USERNAME + " text," + C_NAME + " text," + C_AVATAR_URL + " text," + C_VIDEO_ID + " integer," + C_TITLE + " text," + C_GROUP_ID + " integer," + C_GROUP_NAME + " text," + C_THUMBNAIL_URL + " text," + C_EXPIRES_AT + " date," + C_PRIVATE_VIDEO + " int,"+C_PRIORITY+" int)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (newVersion != oldVersion) {
			WLog.d(this, "Updating from " + oldVersion + " to " + newVersion);
			
			CacheRequestManager.getInstance(context).reset();

			dropDatabase(db);
			onCreate(db);
		}
	}

	public void dropDatabase(SQLiteDatabase db) {
		db.execSQL("drop table if exists " + TABLE_ACTIONS);
		db.execSQL("drop table if exists " + TABLE_PENDING_VIDEOS);
		db.execSQL("drop table if exists " + TABLE_COMMENT_NOTIFICATIONS);
		db.execSQL("drop table if exists " + TABLE_COMMENTS);
		db.execSQL("drop table if exists " + TABLE_COMMENTS_PENDING);
		db.execSQL("drop table if exists " + TABLE_REPLY_PENDING_VIDEOS);
		// db.execSQL("drop table if exists "+TABLE_CONTACTS);
		db.execSQL("drop table if exists " + TABLE_VIDEO_NOTIFICATIONS);
		db.execSQL("drop table if exists " + TABLE_VIDEOS);
		db.execSQL("drop table if exists " + TABLE_FAVORITES);
		db.execSQL("drop table if exists " + TABLE_GROUPS);
		db.execSQL("drop table if exists " + TABLE_USERS);
		db.execSQL("drop table if exists " + TABLE_FRIENDS);
		db.execSQL("drop table if exists " + TABLE_TIMELINE);
		db.execSQL("drop table if exists " + TABLE_ACTIVITY);
		db.execSQL("drop table if exists " + TABLE_LIKES_ACTIVITY);
		db.execSQL("drop table if exists " + TABLE_LIKES_TIMELINE);

		db.execSQL("drop table if exists " + TABLE_MY_VIDEOS);
		db.execSQL("drop table if exists " + TABLE_LIKES_MY_VIDEOS);
	}

	public void rebasePendingComments() {
		WLog.d(this, "Rebased Pending Comments Table..");
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("drop table if exists " + TABLE_COMMENTS_PENDING);
		createPendingComments(db);
	}

	public Cursor getSendTo() {
		Cursor c;

		SQLiteDatabase myDatabase = this.getReadableDatabase();
		c = myDatabase.rawQuery("SELECT " + C_ID + "," + WApp.TYPE_FAVORITE + " as type,'aaa' as name FROM " + TABLE_FAVORITES
				+ " UNION "
				+ "SELECT " + C_ID + "," + WApp.TYPE_GROUPS + " as type," + C_NAME + " FROM " + TABLE_GROUPS
				+ " UNION "
				+ "SELECT " + TABLE_FRIENDS + "." + C_ID + "," + WApp.TYPE_FRIEND + " as type, " + C_USERNAME + " as name FROM " + TABLE_FRIENDS + " INNER JOIN " + TABLE_USERS + " ON " + TABLE_FRIENDS + "." + C_ID + "=" +
				TABLE_USERS + "." + C_ID
				+ " ORDER BY type,name COLLATE NOCASE", null);
		return c;
	}

	public Cursor getVideosId() {
		Cursor c;
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		c = myDatabase.rawQuery("SELECT " + C_ID + " FROM " + TABLE_VIDEOS, null);
		return c;
	}

	public Cursor getUsers(int type) {
		Cursor c;
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		c = myDatabase.rawQuery("SELECT * FROM " + TABLE_USERS + " WHERE " + C_STATUS + "=" + type, null);
		return c;
	}

	public Cursor getUsers() {
		Cursor c = this.getReadableDatabase().rawQuery("SELECT * FROM " + TABLE_USERS, null);
		return c;
	}

	public Cursor getPendingComments(long vid) {
		return getReadableDatabase().rawQuery("SELECT * FROM " + TABLE_COMMENTS_PENDING + " WHERE " + C_VIDEO_ID + "=" + vid, null);
	}

	public Cursor getPendingVideos() {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_PENDING_VIDEOS, null);
		return c;
	}

	public Cursor getPendingVideo(long vid) {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_PENDING_VIDEOS + " WHERE " + DBHelper.C_VIDEO_ID + "=" + vid, null);
		return c;
	}

	public Cursor getPendingVideo(int id) {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_PENDING_VIDEOS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		return c;
	}

	public Cursor getPendingVideos(int type) {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_PENDING_VIDEOS + " WHERE " + C_TYPE + "=" + type, null);
		return c;
	}

	public Cursor getOfflineActions() {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_ACTIONS, null);
		return c;
	}

	/*
	 * Getting Contacts in very different ways: a) With filter b) For section
	 * headers c) Simply getting contacts
	 */

	// public Cursor getContacts(int type){
	// Cursor c;
	// SQLiteDatabase myDatabase = this.getReadableDatabase();
	// c =
	// myDatabase.rawQuery("SELECT "+C_ID+","+C_WECLIPSE_ID+","+C_NAME+","+C_PHONE+","+C_USERNAME+","+C_AVATAR+","+C_STATUS+","+C_INVITED+" FROM "+TABLE_CONTACTS+" WHERE "+C_STATUS+"="+type+" ORDER BY "+C_USERNAME,
	// null);
	// return c;
	// }
	//
	// public Cursor getContacts(int type, ArrayList<Long> ids) {
	// String user_ids = "";
	// for(int i=0;i<ids.size();i++){
	// user_ids += ids.get(i);
	// user_ids += ",";
	// }
	// user_ids = user_ids.substring(0, user_ids.length()-1);
	// Cursor c;
	// SQLiteDatabase myDatabase = this.getReadableDatabase();
	// c=myDatabase.rawQuery("SELECT "+C_ID+","+C_WECLIPSE_ID+","+C_NAME+","+C_PHONE+","+C_USERNAME+","+C_AVATAR+","+C_STATUS+","+C_INVITED+" FROM "+TABLE_CONTACTS+" WHERE "+C_STATUS+"="+type+" AND "+C_WECLIPSE_ID+" NOT IN("+user_ids+") ORDER BY "+C_USERNAME,
	// null);
	// return c;
	// }

	public void clearPendingVideos(ArrayList<Long> ids) {
		if (ids.size() > 0) {
			String pids = "";
			for (int i = 0; i < ids.size(); i++) {
				pids += ids.get(i);
				pids += ",";
			}
			pids = pids.substring(0, pids.length() - 1);
			SQLiteDatabase myDatabase = this.getReadableDatabase();
			myDatabase.delete(TABLE_PENDING_VIDEOS, C_ID + " IN(" + pids + ")", null);
		}
	}

	public String[] getFriendsSectionArray(ArrayList<Long> ids) {
		String user_ids = "";

		Cursor c = getFriends(ids);

		if (ids != null) {
			for (int i = 0; i < ids.size(); i++) {
				user_ids += ids.get(i);
				user_ids += ",";
			}
			if (!user_ids.equals(""))
				user_ids = user_ids.substring(0, user_ids.length() - 1);
		}

		String res[] = new String[c.getCount()];
		int i = 0;
		if (c.moveToFirst()) {
			do {
				res[i++] = c.getString(c.getColumnIndex(DBHelper.C_USERNAME));
			} while (c.moveToNext());
		}
		return res;
	}

	public Cursor getFriends(ArrayList<Long> uids) {
		SQLiteDatabase mDb = this.getReadableDatabase();

		if (uids == null) {
			return mDb.rawQuery("SELECT " + TABLE_USERS + ".* FROM " + TABLE_FRIENDS + " INNER JOIN " + TABLE_USERS + " ON " + TABLE_FRIENDS + "." + C_ID + "=" + TABLE_USERS + "." + C_ID + " WHERE " + TABLE_USERS + "." + C_ID + " ORDER BY " + DBHelper.C_USERNAME, null);
		}

		String ids = "";

		for (int i = 0; i < uids.size(); i++) {
			ids += uids.get(i);
			ids += ",";
		}
		if (!ids.equals(""))
			ids = ids.substring(0, ids.length() - 1);

		WLog.d(this, "Numero de users: " + getUsers().getCount());
		// WLog.d(this,"Numero de friends: "+)

		Cursor c = mDb.rawQuery("SELECT " + TABLE_USERS + ".* FROM " + TABLE_FRIENDS + " INNER JOIN " + TABLE_USERS + " ON " + TABLE_FRIENDS + "." + C_ID + "=" + TABLE_USERS + "." + C_ID + " WHERE " + TABLE_USERS + "." + C_ID + " NOT IN (" + ids + ") ORDER BY " + DBHelper.C_USERNAME, null);

		return c;
	}

	// public String[] getContactsSectionArray(int type){
	// Cursor c = getContacts(type);
	// String res[] = new String[c.getCount()];
	// int i = 0;
	// if(c.moveToFirst()){
	// do{
	// // if(name==null || name.equals("")){
	// res[i++] = c.getString(c.getColumnIndex(DBHelper.C_USERNAME));
	// // } else {
	// // res[i++] = name;
	// // }
	//
	// }while(c.moveToNext());
	// }
	// return res;
	// }
	//
	// public String[] getContactsSectionArray(int type, ArrayList<Long> ids){
	// String user_ids = "";
	// for(int i=0;i<ids.size();i++){
	// user_ids += ids.get(i);
	// user_ids += ",";
	// }
	// user_ids = user_ids.substring(0, user_ids.length()-1);
	// Cursor c = getContacts(type, ids);
	// String res[] = new String[c.getCount()];
	// int i = 0;
	// if(c.moveToFirst()){
	// do {
	// // if(name==null || name.equals("")){
	// res[i++] = c.getString(c.getColumnIndex(DBHelper.C_USERNAME));
	// // } else {
	// // res[i++] = name;
	// // }
	//
	// } while(c.moveToNext());
	// }
	// return res;
	// }

	// public Cursor getContacts(String query){
	// Cursor c;
	// SQLiteDatabase myDatabase = this.getReadableDatabase();
	// c =
	// myDatabase.rawQuery("SELECT "+C_ID+","+C_WECLIPSE_ID+","+C_NAME+","+C_PHONE+","+C_USERNAME+","+C_AVATAR+","+C_STATUS+","+C_INVITED+" FROM "+TABLE_CONTACTS+
	// " WHERE "+C_NAME+" LIKE '"+query+"%' OR "+C_USERNAME+" LIKE '"+query+"%' AND ("+C_STATUS+"="+Contact.FOLLOWER+
	// " OR "+C_STATUS+"="+Contact.FOLLOWING+
	// " OR "+C_STATUS+"="+Contact.CONTACT+
	// " OR "+C_STATUS+"="+Contact.FRIEND+
	// " OR "+C_STATUS+"="+Contact.NOTHING+") ORDER BY "+C_STATUS+" DESC,"+C_USERNAME,
	// null);
	// return c;
	// }
	//
	// public Cursor getContactsForFind(){
	// Cursor c;
	// SQLiteDatabase myDatabase = this.getReadableDatabase();
	// c =
	// myDatabase.rawQuery("SELECT "+C_ID+","+C_WECLIPSE_ID+","+C_NAME+","+C_PHONE+","+C_USERNAME+","+C_AVATAR+","+C_STATUS+","+C_INVITED+","+C_RECENT+" FROM "+TABLE_CONTACTS+
	// " WHERE ("+C_STATUS+"="+Contact.FOLLOWER+
	// " OR "+C_STATUS+"="+Contact.FOLLOWING+
	// " OR "+C_STATUS+"="+Contact.CONTACT+
	// " OR "+C_STATUS+"="+Contact.NOTHING+") ORDER BY "+C_STATUS+" DESC,"+C_NAME,
	// null);
	// return c;
	// }

	public Cursor getComments() {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_COMMENTS, null);
		return c;
	}

	public long getLastComment(long vid) {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT " + C_ID + " FROM " + TABLE_COMMENTS + " WHERE " + C_VIDEO_ID + "=" + vid + " ORDER BY " + C_CREATED_AT + " DESC", null);
		// Log.d(TAG,"Encontrados (getLastComment): "+c.getCount());
		if (c.moveToFirst()) {
			return c.getLong(c.getColumnIndex(C_ID));
		} else {
			return 0;
		}
	}

	public void cleanComments(ArrayList<Long> vids) {
		if (vids.size() > 0) {
			String vid_ids = "";
			for (int i = 0; i < vids.size(); i++) {
				vid_ids += vids.get(i);
				vid_ids += ",";
			}
			vid_ids = vid_ids.substring(0, vid_ids.length() - 1);
			SQLiteDatabase myDatabase = this.getWritableDatabase();
			if (this.isOpened) {
				// Log.d(TAG,"It is opened");
			}
			myDatabase.delete(TABLE_COMMENTS, C_VIDEO_ID + " NOT IN (" + vid_ids + ")", null);
		} else {
			SQLiteDatabase myDatabase = this.getWritableDatabase();
			myDatabase.delete(TABLE_COMMENTS, null, null);
		}
	}

	// public Cursor getContactsForMy() {
	// Cursor c;
	// SQLiteDatabase myDatabase = this.getReadableDatabase();
	// c =
	// myDatabase.rawQuery("SELECT "+C_ID+","+C_WECLIPSE_ID+",IFNULL("+C_NAME+","+C_USERNAME+") as "+C_SORTABLE_NAME+","+C_NAME+","+C_PHONE+","+C_USERNAME+","+C_AVATAR+","+C_STATUS+","+C_INVITED+","+C_RECENT+" FROM "+TABLE_CONTACTS+" WHERE "+C_STATUS+"="+Contact.FRIEND+" ORDER BY "+C_SORTABLE_NAME+" COLLATE NOCASE",
	// null);
	// return c;
	// }
	//
	// public Cursor getWeclipseContacts(){
	// Cursor c;
	// c =
	// this.getReadableDatabase().rawQuery("SELECT "+C_ID+","+C_WECLIPSE_ID+","+C_NAME+","+C_PHONE+","+C_USERNAME+","+C_AVATAR+","+C_STATUS+","+C_INVITED+","+C_RECENT+" FROM "+TABLE_CONTACTS+" WHERE "+C_STATUS+">"+Contact.NOTHING,
	// null);
	// return c;
	// }

	public synchronized void clearVideos() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_VIDEOS, null, null);
	}

	public void clearMyVideos() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_MY_VIDEOS, null, null);
		myDatabase.delete(TABLE_LIKES_MY_VIDEOS, null, null);
	}

	public void clearFeed() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_VIDEOS, null, null);
	}

	public void clearUsers() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_USERS, null, null);
	}

	public void clearFriends() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_FRIENDS, null, null);
	}

	public void clearGroups() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_GROUPS, null, null);
	}

	public void clearComments() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_COMMENTS, null, null);
		myDatabase.delete(TABLE_COMMENT_NOTIFICATIONS, null, null);
	}

	public void clearPendingVideos() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_PENDING_VIDEOS, null, null);
	}

	public void clearActions() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_ACTIONS, null, null);
	}

	public void clearLikesActivity() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_LIKES_ACTIVITY, null, null);
	}

	public void clearLikesTimeline() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_LIKES_TIMELINE, null, null);
	}

	public void clearTimeline() {
		SQLiteDatabase myDatabase = this.getWritableDatabase();
		myDatabase.delete(TABLE_TIMELINE, null, null);
	}

	// public void dumpContacts() {
	// SQLiteDatabase myDatabase = this.getReadableDatabase();
	// Cursor c =
	// myDatabase.rawQuery("SELECT * FROM "+TABLE_CONTACTS+" ORDER BY "+C_STATUS+" DESC,"+C_NAME,
	// null);
	// if(c.moveToFirst()){
	// do{
	// Log.d(TAG,"Contact("+c.getInt(c.getColumnIndex(C_ID))+"): "+(new
	// Contact(context,
	// c.getLong(c.getColumnIndex(C_WECLIPSE_ID)),
	// c.getString(c.getColumnIndex(C_NAME)),
	// c.getString(c.getColumnIndex(C_PHONE)),
	// c.getString(c.getColumnIndex(C_USERNAME)),
	// c.getInt(c.getColumnIndex(C_AVATAR)),
	// c.getInt(c.getColumnIndex(C_STATUS))).toString()));
	// }while(c.moveToNext());
	// }
	// }

	public void dumpComments() {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_COMMENTS, null);
		if (c.moveToFirst()) {
			do {
				Log.d(TAG, "Comment(" + c.getLong(c.getColumnIndex(C_ID)) + "): " + (new Comment(context,
						c.getLong(c.getColumnIndex(C_ID)),
						c.getLong(c.getColumnIndex(C_VIDEO_ID)),
						c.getLong(c.getColumnIndex(C_USER_ID)),
						c.getString(c.getColumnIndex(C_COMMENT)),
						c.getString(c.getColumnIndex(C_CREATED_AT))))
						.toString());
			} while (c.moveToNext());
		}
	}

	public int getNotificationId(long vid) {
		int res = 0;
		SQLiteDatabase mDatabase = this.getReadableDatabase();
		Cursor c = mDatabase.rawQuery("SELECT " + C_NOTIFICATION_ID + " FROM " + TABLE_COMMENT_NOTIFICATIONS + " WHERE " + C_VIDEO_ID + "=" + vid, null);
		if (c.moveToFirst()) {
			res = c.getInt(0);
		}
		c.close();
		return res;
	}

	public int getMaxCommentNotificationId() {
		int res = 0;

		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT max(" + C_NOTIFICATION_ID + ") as max FROM " + TABLE_COMMENT_NOTIFICATIONS, null);
		if (c.moveToFirst()) {
			res = c.getInt(0);
		}
		c.close();

		return res + 101;
	}

	public int getFeedNotification() {
		return this.getReadableDatabase().rawQuery("SELECT * FROM " + TABLE_VIDEO_NOTIFICATIONS, null).getCount();
	}

	public int getFeedNotification(int type) {
		return this.getReadableDatabase().rawQuery("SELECT * FROM " + TABLE_VIDEO_NOTIFICATIONS + " WHERE " + C_TYPE + "=" + type, null).getCount();
	}

	public int getCommentNotificationsCount(long vid) {
		SQLiteDatabase myDatabase = this.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + TABLE_COMMENT_NOTIFICATIONS + " WHERE " + C_VIDEO_ID + "=" + vid, null);
		int res = 0;
		if (c.moveToFirst()) {
			res = c.getInt(c.getColumnIndex(C_NEW_COMMENTS));
		}
		c.close();
		return res;
	}

	public boolean isVideoPending(String name) {
		boolean res = false;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT " + C_ID + " FROM " + TABLE_PENDING_VIDEOS + " WHERE " + C_PATH + "='" + name + "'", null);
		res = c.moveToFirst();
		c.close();
		return res;
	}

	// public boolean userExist(String name){
	// boolean res = false;
	// SQLiteDatabase db = this.getReadableDatabase();
	// Cursor c =
	// db.rawQuery("SELECT "+C_USERNAME+" FROM "+TABLE_CONTACTS+" WHERE "+C_USERNAME+"='"+name+"'",null);
	// res = c.moveToFirst();
	// c.close();
	// return res;
	// }

	public boolean videoExists(long id) {
		boolean res = false;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT " + C_ID + " FROM " + TABLE_VIDEOS + " WHERE " + C_ID + "=" + id, null);
		res = c.moveToFirst();
		c.close();
		WLog.d(this, "Checking if this exists: " + id + " :" + res);
		return res;
	}

	public int getNumberOfFavorites() {
		return getReadableDatabase().rawQuery("SELECT " + C_ID + " FROM " + TABLE_FAVORITES, null).getCount();
	}

	// public void updateContatsRecent(int type) {
	// SQLiteDatabase db = this.getWritableDatabase();
	// ContentValues cv = new ContentValues();
	// cv.put(C_RECENT,Contact.NOT_RECENT);
	// db.update(TABLE_CONTACTS, cv, C_RECENT+"=? AND "+C_STATUS+"=?",new
	// String[]{""+Contact.RECENT,""+type});
	// }

	/*
	 * ***************************************************************************************************************************************************
	 * ***************************************************************************************************************************************************
	 * ***************************************************************************************************************************************************
	 * ************************************************* BULK / INSERT
	 * *************************************************
	 * ************************************************* OPERATIONS
	 * *************************************************
	 * *************************
	 * *************************************************
	 * *************************************************************************
	 * *
	 * *************************************************************************
	 * *************************************************************************
	 * *
	 * *************************************************************************
	 * *************************************************************************
	 */

	/*
	 * Bulk insert/update contacts
	 */
	// public void bulkInsertContacts(ArrayList<Contact> contacts){
	// String sql = "INSERT INTO "+TABLE_CONTACTS+" VALUES(?,?,?,?,?,?,?,?,?);";
	// SQLiteDatabase db = this.getWritableDatabase();
	// SQLiteStatement stmt = db.compileStatement(sql);
	// db.beginTransaction();
	// Contact c;
	// for(int i=0;i<contacts.size();i++){
	// c = contacts.get(i);
	// stmt.clearBindings();
	// stmt.bindLong(2,c.getWid());
	// if(c.getName()!=null)
	// stmt.bindString(3,c.getName());
	//
	// if(c.getPhone()!=null)
	// stmt.bindString(4, c.getPhone());
	//
	// if(c.getUsername() != null)
	// stmt.bindString(5, c.getUsername());
	//
	// stmt.bindLong(6, c.getAvatar());
	// stmt.bindLong(7, c.getStatus());
	// stmt.execute();
	// }
	// db.setTransactionSuccessful();
	// db.endTransaction();
	// }
	//
	// public void bulkUpdateContacts(ArrayList<Contact> contacts){
	// String sql = "UPDATE "+TABLE_CONTACTS+" SET "+C_WECLIPSE_ID+"=?,"
	// +C_USERNAME+"=?,"
	// +C_STATUS+"=?,"
	// +C_AVATAR+"=? WHERE "+C_WECLIPSE_ID+"=?";
	// SQLiteDatabase db = this.getWritableDatabase();
	// SQLiteStatement stmt = db.compileStatement(sql);
	// db.beginTransaction();
	// for(Contact c : contacts){
	// stmt.clearBindings();
	// stmt.bindLong(1, c.getWid());
	// stmt.bindString(2, c.getUsername());
	// stmt.bindLong(3, c.getStatus());
	// stmt.bindLong(4, c.getAvatar());
	// stmt.bindLong(5, c.getWid());
	// stmt.execute();
	// }
	// db.setTransactionSuccessful();
	// db.endTransaction();
	// }
	//
	// //This one is special because these contacts have the field unusableCode
	// to facilitate batch update
	// public void bulkUpdateConnections(ArrayList<Contact> contacts){
	// String sql = "UPDATE "+TABLE_CONTACTS+" SET "+C_WECLIPSE_ID+"=?,"
	// +C_USERNAME+"=?,"
	// +C_STATUS+"=?,"
	// +C_AVATAR+"=? WHERE "+C_PHONE+"=? OR "+C_PHONE+"=?";
	// SQLiteDatabase db = this.getWritableDatabase();
	// SQLiteStatement stmt = db.compileStatement(sql);
	// db.beginTransaction();
	// for(Contact c : contacts){
	// stmt.clearBindings();
	// stmt.bindLong(1,c.getWid());
	// stmt.bindString(2,c.getUsername());
	// stmt.bindLong(3,c.getStatus());
	// stmt.bindLong(4,c.getAvatar());
	// stmt.bindString(5,c.getPhone());
	// stmt.bindString(6,c.unusableCode+c.getPhone());
	// stmt.execute();
	// }
	// db.setTransactionSuccessful();
	// db.endTransaction();
	// }

}
