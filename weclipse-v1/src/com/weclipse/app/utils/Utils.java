package com.weclipse.app.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import com.weclipse.app.WApp;
import com.weclipse.app.R;

public class Utils {
	
	/*
	 * Timers
	 */
	public static final String TIMER_LOGIN = "login";
	public static final String TIMER_SIGNUP = "signup";
	
	/*
	 * Values for the progress wheel
	 */
	public static final int PROGRESS_FILE_UPLOAD = 60;
	public static final int PROGRESS_COMPLETE = 100;
	public static final int PROGRESS_PROCESS_FULL_OUTPUT = 90;
	public static final int PROGRESS_PROCESS_VIDEO = 95;
	
	public static final int PROGRESS_STEP = 4;
	
	/*
	 * Generic values for avatars
	 */
	public static final int AVATAR_NOT_SET = 0;
	public static final int AVATAR_SET = 1;
	public static final int AVATAR_CHANGED = 2;

	private static final String TAG = "Utils Class";
	
    // Prevents instantiation.
    private Utils() {}

    /**
     * Enables strict mode. This should only be called when debugging the application and is useful
     * for finding some potential bugs or best practice violations.
     */
    @TargetApi(11)
    public static void enableStrictMode() {
        // Strict mode is only available on gingerbread or later
        if (Utils.hasGingerbread()) {

            // Enable all thread strict mode policies
            StrictMode.ThreadPolicy.Builder threadPolicyBuilder =
                    new StrictMode.ThreadPolicy.Builder()
                            .detectAll()
                            .penaltyLog();

            // Enable all VM strict mode policies
            StrictMode.VmPolicy.Builder vmPolicyBuilder =
                    new StrictMode.VmPolicy.Builder()
                            .detectAll()
                            .penaltyLog();

            // Honeycomb introduced some additional strict mode features
            if (Utils.hasHoneycomb()) {
                // Flash screen when thread policy is violated
                threadPolicyBuilder.penaltyFlashScreen();
                // For each activity class, set an instance limit of 1. Any more instances and
                // there could be a memory leak.
//                vmPolicyBuilder.
//                        .setClassInstanceLimit(ContactsListActivity.class, 1)
//                        .setClassInstanceLimit(ContactDetailActivity.class, 1);
            }

            // Use builders to enable strict mode policies
            StrictMode.setThreadPolicy(threadPolicyBuilder.build());
            StrictMode.setVmPolicy(vmPolicyBuilder.build());
        }
    }

    /**
     * Uses static final constants to detect if the device's platform version is Gingerbread or
     * later.
     */
    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    /**
     * Uses static final constants to detect if the device's platform version is Honeycomb or
     * later.
     */
    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * Uses static final constants to detect if the device's platform version is Honeycomb MR1 or
     * later.
     */
    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    /**
     * Uses static final constants to detect if the device's platform version is ICS or
     * later.
     */
    public static boolean hasICS() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }
    
    public static String getMinifiedName(String name){
    	String[] aux = name.split(" ");
		 if(aux.length>1)
			 return (aux[0].substring(0,1)+aux[aux.length-1].substring(0,1)).toUpperCase();
		 else
			 return (aux[0].substring(0,1)+aux[0].substring(aux[0].length()-1, aux[0].length())).toUpperCase();
    }
    
    public static byte[] createChecksum(String filename) throws Exception {
        InputStream fis =  new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    public static String getMD5Checksum(String filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";

        for (int i=0; i < b.length; i++) {
            result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }
    
    public static int getTimezoneDifference(){
    	DateTime client = new DateTime();
    	DateTime server = new DateTime(DateTimeZone.UTC);
    	return server.getHourOfDay()-client.getHourOfDay();
    }
    
    public static boolean isAfterNow(String expires_at){
    	return DateTime.parse(expires_at,DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss").withZoneUTC()).isAfterNow();
    }
    
    public static boolean isAfter(String date1,String date2){
    	return DateTime.parse(date1, DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss").withZoneUTC()).isAfter(DateTime.parse(date2, DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss").withZoneUTC()));
    }
    
    public static String getTimeLeft(Context context,String ends_at,boolean long_version){
    	DateTime end = DateTime.parse(ends_at,DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss").withZoneUTC());
		DateTime now = new DateTime();
		
		int minutes = end.minus(now.getMillis()).getMinuteOfHour();
		int hours = end.minus(now.getMillis()).getHourOfDay();
		String _min,_hours;
		if(minutes==0)
			_min = "";
		else if(minutes<10)
			_min = "0"+minutes+"m";
		else
			_min = minutes+"m";
		
		if(hours==0)
			_hours="";
		else
			_hours=hours+"h";
		
		if((_hours+_min).equals("")){
			if(long_version){
				return context.getString(R.string.time_left).replace("_time_","24h");
			} else {
				return "24h";
			}
		} else {
			if(long_version){
				return context.getString(R.string.time_left).replace("_time_",_hours+_min);
			} else {
				return _hours+_min;
			}
		}
    }

    public static String getTimeLeft(Context context,String ends_at){
		return getTimeLeft(context,ends_at, true);
    }
    
    public static String getRealTimeLeft(Context context,String ends_at){
		DateTime end = DateTime.parse(ends_at,DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss").withZoneUTC());
		DateTime now = new DateTime();
		
		int seconds = end.minus(now.getMillis()).getSecondOfMinute();
		int minutes = end.minus(now.getMillis()).getMinuteOfHour();
		int hours = end.minus(now.getMillis()).getHourOfDay();
		String _min,_hours,_seconds;
		if(minutes==0)
			_min = "";
		else if(minutes<10)
			_min = "0"+minutes+":";
		else
			_min = minutes+":";
		
		if(hours==0)
			_hours="";
		else
			_hours=hours+":";
		
		if(seconds==0){
			_seconds = "00";
		} else if(seconds<10){
			_seconds = "0"+seconds;
		} else {
			_seconds = seconds+"";
		}
		
		return context.getString(R.string.time_left).replace("_time_", _hours+_min+_seconds);
    }
    
    public static String getTimePast(Context context,String created_at){
    	DateTime start = DateTime.parse(created_at,DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss").withZoneUTC());
    	DateTime now = new DateTime();
    	
    	long millis = now.minus(start.getMillis()).getMillis();
    	
    	long minutes = (millis/1000/60);
    	long hours = minutes/60;
    	long days = hours/24;
    	hours = (hours%24);
    	minutes = (minutes%60);
    	
    	if(minutes == 0 && hours == 0 && days == 0){
    		return context.getString(R.string.time_just_now);
    	}
    	
    	if(days==1){
    		return context.getString(R.string.time_past).replace("_time_", days+"d");
    	} else if(days>1){
    		return context.getString(R.string.time_past).replace("_time_", days+"d");
    	}
    	
    	if(hours>=1)
    		return context.getString(R.string.time_past).replace("_time_", hours+"h");
    	else
    		return context.getString(R.string.time_past).replace("_time_", minutes+"m");
    	
    }
    
    public static String etaToTime(String eta){
    	String res = "";
    	if(eta.equals("0.5")){
    		res = "30m";
    	} else {
    		res = eta+"h";
    	}
    	return res;
    }
    
    public static String convertTime(Context context,String eta){
    	return context.getString(R.string.time_left).replace("_time_", etaToTime(eta));
    }
    
    public static String getResendText(long lastCodeRequestTimestamp){
    	DateTime display = new DateTime(WApp.MESSAGE_REQUEST_THRESHOLD+(lastCodeRequestTimestamp-System.currentTimeMillis()));
    	
    	int seconds = display.getSecondOfMinute();
    	
    	return display.getMinuteOfHour()+":"+(seconds<10?"0"+seconds:""+seconds);
    }
    
    public static void copyFile(String file,String newFile){
		File source = new File(file);
		File destiny = new File(newFile);
		
		try {
			
			InputStream in = new FileInputStream(source);
		    OutputStream out = new FileOutputStream(destiny);
	
		    // Transfer bytes from in to out
		    byte[] buf = new byte[1024];
		    int len;
		    while ((len = in.read(buf)) > 0) {
		        out.write(buf, 0, len);
		    }
		    in.close();
		    out.close();
		} catch(IOException e){
			e.printStackTrace();
		}
    }
    
    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
	public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    
    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
            String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    
    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    
}