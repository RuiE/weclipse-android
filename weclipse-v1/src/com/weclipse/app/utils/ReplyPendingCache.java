package com.weclipse.app.utils;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.v4.util.LongSparseArray;

import com.weclipse.app.WApp;
import com.weclipse.app.models.User;
import com.weclipse.app.models.UserActivity;

public class ReplyPendingCache {
	
	ArrayList<Long> pending;
	
	private static ReplyPendingCache mReplyCache;
	WApp app;
	
	private ReplyPendingCache(WApp app){
		this.app = app;
		pending = new ArrayList<Long>();
	}
	
	public static ReplyPendingCache getInstance(WApp app){
		if(mReplyCache == null)
			mReplyCache = new ReplyPendingCache(app);
		
		return mReplyCache;
	}
	
	public boolean contains(long id){
		return pending.contains(id);
	}
	
	public void load(){
		if(pending != null){
			pending.clear();
			pending = null;
		}
		
		pending = new ArrayList<Long>();
		
		Cursor c = app.getDB().getReadableDatabase().rawQuery("SELECT * FROM "+DBHelper.TABLE_REPLY_PENDING_VIDEOS,null);
		if(c.moveToFirst()){
			do {
				pending.add(c.getLong(c.getColumnIndex(DBHelper.C_ID)));
			} while(c.moveToNext());
		}
		
		c.close();
	}
	
	public void save(){
		new Runnable() {
			public void run() {
				String sql = "INSERT INTO "+DBHelper.TABLE_REPLY_PENDING_VIDEOS+" VALUES(?)";
				SQLiteDatabase db = app.getDB().getWritableDatabase();
				SQLiteStatement stmt = db.compileStatement(sql);
				db.beginTransaction();
				db.delete(DBHelper.TABLE_REPLY_PENDING_VIDEOS,null, null);
				
				for(int i = 0 ; i < pending.size() ; i++){
					stmt.clearBindings();
					stmt.bindLong(1,pending.get(i));
					stmt.execute();
				}
				
				db.setTransactionSuccessful();
				db.endTransaction();
			}
		}.run();
	}
	
	public void addPending(long id){
		if(!pending.contains(id))
			pending.add(id);
		
		save();
	}
	
	public void removePending(long id){
		pending.remove(id);
		removeFromDB(id);
	}
	
	private void removeFromDB(final long id){
		new Runnable() {
			
			@Override
			public void run() {
				SQLiteDatabase db = app.getDB().getWritableDatabase();
				db.delete(DBHelper.TABLE_REPLY_PENDING_VIDEOS, DBHelper.C_ID+"="+id, null);
				db.close();
			}
		}.run();
	}
	
	public static Cursor getPending(WApp app){
        return app.getDB().getReadableDatabase().rawQuery("SELECT * FROM "+DBHelper.TABLE_REPLY_PENDING_VIDEOS,null);
	}
	

	public static void clear(WApp app){
		app.getDB().getWritableDatabase().delete(DBHelper.TABLE_REPLY_PENDING_VIDEOS, null, null);
	}
	
}
