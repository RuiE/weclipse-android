package com.weclipse.app.utils;

import java.util.HashMap;

public class DefaultHashMap<K,V> extends HashMap<K, V> {

	protected V defaultValue;
	
	public DefaultHashMap(V def){
		this.defaultValue = def;
	}
	
	@Override
	public V get(Object key) {
		return containsKey(key) ? super.get(key) : defaultValue;
	}
	
}
