package com.weclipse.app.utils;

import android.util.Log;

import com.weclipse.app.WApp;

public class WLog {
	
	/*
	 * The order in terms of verbosity, from least to most is ERROR, WARN, INFO, DEBUG, VERBOSE.
	 * Verbose should never be compiled into an application except during development.
	 * Debug logs are compiled in but stripped at runtime. Error, warning and info logs are always kept. 
	 *
	 * d - DEBUG (Blue)
	 * e - ERROR (Red)
	 * i - INFO (Green)
	 * v - VERBOSE ()
	 * w - WARN (Orange)
	 */
	
	public static void d(Object obj,String msg){
		if(WApp.DEBUG){
			Log.d(obj.getClass().getSimpleName(),msg);
		}
	}
	
	public static void e(Object obj,String msg){
		if(WApp.DEBUG){
			Log.e(obj.getClass().getSimpleName(),msg);
		}
	}
	
	public static void i(Object obj,String msg){
		if(WApp.DEBUG){
			Log.i(obj.getClass().getSimpleName(),msg);
		}
	}
	
	public static void v(Object obj,String msg){
		if(WApp.DEBUG){
			Log.v(obj.getClass().getSimpleName(), msg);
		}
	}
	
	public static void w(Object obj,String msg){
		if(WApp.DEBUG){
			Log.w(obj.getClass().getSimpleName(),msg);
		}
	}
	
}
