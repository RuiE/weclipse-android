package com.weclipse.app.utils;

import com.weclipse.app.activity.CameraActivity;

public class PlayPack {

	public int duration;
	public String fileName;
	public int media_type;
	
	public PlayPack(String fileName,int duration,int media_type){
		this.fileName=fileName;
		this.duration=duration;
		this.media_type=media_type;
	}
	
	public String toString(){
		if(media_type==CameraActivity.MEDIA_TYPE_IMAGE){
			return "IMAGEM Duration: "+duration+" Nome: "+fileName;
		} else {
			return "VIDEO Duration: "+duration+" Nome: "+fileName;
		}
	}
	
}