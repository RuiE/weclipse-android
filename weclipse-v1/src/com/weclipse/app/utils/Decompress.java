package com.weclipse.app.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Decompress {
	private String _zipFile;
	private String _location;

	public Decompress(String zipFile, String location) {
		_zipFile = zipFile;
		_location = location;
		_dirChecker("");
	}

	public void unzip() {
		try {
			FileInputStream fin = new FileInputStream(_zipFile);
			ZipInputStream zin = new ZipInputStream(fin);
			ZipEntry ze = null;
			while ((ze = zin.getNextEntry()) != null) {
//				Log.v("Decompress", "Unzipping " + ze.getName());

				if (ze.isDirectory()) {
					_dirChecker(ze.getName());
				} else {
					FileOutputStream fout = new FileOutputStream(_location + ze.getName());
					
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
		            byte[] buffer = new byte[1024];
		            int count;
					
		            while((count=zin.read(buffer)) != -1){
		            	baos.write(buffer,0,count);
		            	byte[] bytes = baos.toByteArray();
		                fout.write(bytes);    
		                baos.reset();
		            }
		            
					zin.closeEntry();
					fout.close();
				}
			}
			zin.close();
		} catch (Exception e) {
//			Log.e("Decompress", "unzip", e);
		}
	}
	
	public void decompress(){
		InputStream is;
	     ZipInputStream zis;
	     try 
	     {
	         String filename;
	         is = new FileInputStream(_zipFile);
	         zis = new ZipInputStream(new BufferedInputStream(is));          
	         ZipEntry ze;
	         byte[] buffer = new byte[1024];
	         int count;

	         while ((ze = zis.getNextEntry()) != null) 
	         {
	             // zapis do souboru
	             filename = ze.getName();

	             // Need to create directories if not exists, or
	             // it will generate an Exception...
	             if (ze.isDirectory()) {
	                File fmd = new File(_location + filename);
	                fmd.mkdirs();
	                continue;
	             }

	             FileOutputStream fout = new FileOutputStream(_location + filename);

	             // cteni zipu a zapis
	             while ((count = zis.read(buffer)) != -1){
	                 fout.write(buffer, 0, count);             
	             }

	             fout.close();               
	             zis.closeEntry();
	         }

	         zis.close();
	     } catch(IOException e) {
	         e.printStackTrace();
	     }

	}

	private void _dirChecker(String dir) {
		File f = new File(_location + dir);

		if (!f.isDirectory()) {
			f.mkdirs();
		}
	}
}
