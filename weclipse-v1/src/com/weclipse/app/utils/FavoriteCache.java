package com.weclipse.app.utils;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.v4.util.LongSparseArray;

import com.weclipse.app.WApp;
import com.weclipse.app.models.User;
import com.weclipse.app.models.UserActivity;

public class FavoriteCache {
	
	ArrayList<Long> favorites;
	
	private static FavoriteCache mFavoriteCache;
	WApp app;
	
	private FavoriteCache(WApp app){
		this.app = app;
		favorites = new ArrayList<Long>();
	}
	
	public static FavoriteCache getInstance(WApp app){
		if(mFavoriteCache == null)
			mFavoriteCache = new FavoriteCache(app);
		
		return mFavoriteCache;
	}
	
	public boolean contains(long id){
		return favorites.contains(id);
	}
	
	public void load(){
		if(favorites != null){
			favorites.clear();
			favorites = null;
		}
		
		favorites = new ArrayList<Long>();
		
		Cursor c = app.getDB().getReadableDatabase().rawQuery("SELECT * FROM "+DBHelper.TABLE_FAVORITES,null);
		if(c.moveToFirst()){
			do {
				favorites.add(c.getLong(c.getColumnIndex(DBHelper.C_ID)));
			} while(c.moveToNext());
		}
		
		c.close();
	}
	
	public void save(){
		new Runnable() {
			public void run() {
				String sql = "INSERT INTO "+DBHelper.TABLE_FAVORITES+" VALUES(?)";
				SQLiteDatabase db = app.getDB().getWritableDatabase();
				SQLiteStatement stmt = db.compileStatement(sql);
				db.beginTransaction();
				db.delete(DBHelper.TABLE_FAVORITES,null, null);
				
				for(int i = 0 ; i < favorites.size() ; i++){
					stmt.clearBindings();
					stmt.bindLong(1,favorites.get(i));
					stmt.execute();
				}
				
				db.setTransactionSuccessful();
				db.endTransaction();
			}
		}.run();
	}
	
	public void addFavorite(long id){
		if(!favorites.contains(id))
			favorites.add(id);
		
	}
	
	public void removeFavorite(long id){
		favorites.remove(id);
	}
	
	public static Cursor getFavorites(WApp app){
        return app.getDB().getReadableDatabase().rawQuery("SELECT * FROM "+DBHelper.TABLE_FAVORITES,null);
	}
	

	public static void clearFavorites(WApp app){
		app.getDB().getWritableDatabase().delete(DBHelper.TABLE_FAVORITES, null, null);
	}
	
}
