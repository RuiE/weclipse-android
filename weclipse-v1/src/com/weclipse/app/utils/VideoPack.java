package com.weclipse.app.utils;

public class VideoPack {
	
	public String filePath;
	public String eta;
	public int players;
	public String caption;
	public long duration;
	public int media_type;
	public int private_video;

	public VideoPack(int players,String eta,String filePath,String caption,long duration,int media_type,int private_video){
		this.filePath = filePath;
		this.eta = eta;
		this.players = players;
		this.caption = caption;
		this.duration = duration;
		this.media_type = media_type;
		this.private_video = private_video;
	}
	
}
