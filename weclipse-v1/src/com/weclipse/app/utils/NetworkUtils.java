package com.weclipse.app.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.weclipse.app.WApp;

public class NetworkUtils {

	public static boolean isInternetConnected(WApp app){
		ConnectivityManager cm = (ConnectivityManager)app.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if(activeNetwork != null && activeNetwork.isConnected()){
			return true;
		}
		return false;
	}
	
	public static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }
	
	public static boolean isConnectedToMobile(WApp app){
		NetworkInfo info = getNetworkInfo(app);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
	}
	
	public static boolean isConnectedToWifi(WApp app){
		NetworkInfo info = getNetworkInfo(app);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
	}
	
}
