package com.weclipse.app.utils;

import android.app.Activity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weclipse.app.R;

public class WToast {
	
	public static final int FLASH = 500;
	public static final int VERY_SHORT = 1000;
	public static final int SHORT = 2000;
	public static final int MEDIUM = 3000;
	public static final int LONG = 4000;
	private static final int FADE_TIME = 750;
	
	Activity activity;
	TextView tvLegend;
	RelativeLayout background;
	AlphaAnimation anim;
	
	public WToast(Activity activity){
		this.activity=activity;
		tvLegend = (TextView)activity.findViewById(R.id.tvToast);
		background = (RelativeLayout)activity.findViewById(R.id.rlToast);
	}
	
	public WToast(Activity activity,String message,int color){
		this.activity=activity;
		tvLegend = (TextView)activity.findViewById(R.id.tvToast);
		background = (RelativeLayout)activity.findViewById(R.id.rlToast);
		tvLegend.setText(message);
		background.setBackgroundColor(color);
	}
	
	/*
	 * Loads the animations to fade the toast
	 */
	public Animation loadFadeInAnimation(){
		anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(FADE_TIME);
		anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				background.setAnimation(null);
			}
		});
		return anim;
	}
	
	/*
	 * Shows the toast
	 */
	public void show(){
		activity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				background.setVisibility(View.VISIBLE);
				background.setAnimation(loadFadeInAnimation());
				//To be sure its on top of everything
				background.bringToFront();				
			}
		});
	}
	
	/*
	 * Show the Toast for a certain duration
	 */
	public void show(final int mills){
		activity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				background.setVisibility(View.VISIBLE);
				background.setAnimation(loadFadeInAnimation());
				//To be sure its on top of everything
				background.bringToFront();
				background.postDelayed(new Runnable() {
					@Override
					public void run() {
						background.setVisibility(View.GONE);
					}
				},mills);
			}
		});
	}
	
	/*
	 * Hides the toast
	 */
	public void hide(){
		if(background!=null)
			background.setVisibility(View.GONE);
	}
	
	/*
	 * Method to toggle the view
	 */
	public void toggle(){
		if(background.isShown()){
			background.setVisibility(View.GONE);
		} else {
			background.setAnimation(anim);
			background.setVisibility(View.VISIBLE);
		}
	}
	
	/*
	 * Sets the message for the toast
	 */
	public WToast setText(String message){
		tvLegend.setText(message);
		return this;
	}
	
	/*
	 * Sets the message for the toast, specified from the resources
	 */
	public WToast setText(int message){
		tvLegend.setText(activity.getString(message));
		return this;
	}
	
	/*
	 * Sets the background color for the toast
	 */
	public WToast setBackground(int color){
		background.setBackgroundResource(color);
		return this;
	}
	
	/*
	 * booleans the toast visibility
	 */
	public boolean isShowing(){
		return background.isShown();
	}
	
	/*
	 * Specific Toast showing there is no internet
	 */
	public void showNoInternet(){
		this.setText(activity.getString(R.string.toast_no_internet)).setBackground(R.color.dark_grey_75).show();
	}
	

}
