package com.weclipse.app.utils;

public class AnalyticsHelper {
	
	/* Screens */
	public static final String S_FEED = "Feed";
	public static final String S_VIDEO = "Video Playback";
	public static final String S_SETTINGS = "Settings";
	public static final String S_LOGIN = "Login";
	public static final String S_SEND_CLIP = "Send Clip";
	public static final String S_SEARCH = "Friends Search";
	public static final String S_ADD_FRIEND = "Add Friends";
	public static final String S_CREATE_GROUP = "Create Group";
	public static final String S_RECOVER_PASSWORD = "Recover Password";
	public static final String S_SIGNUP = "Signup";
	
	public static final String S_VIDEO_DETAIL = "Video Detail";
	public static final String S_PROFILE = "Profile";
	
	public static final String S_FOLLOWERS = "Followers";
	public static final String S_FOLLOWING = "Following";
	public static final String S_FRIENDS = "Friends";
	public static final String S_COMMENTS = "Comments";
	public static final String S_LIKES = "Followers";
	public static final String S_PARTICIPANTS = "Followers";
	public static final String S_GROUPS = "Groups";
	
	public static final String S_CONTACTS = "Contacts";
	public static final String S_FACEBOOK = "Facebook";
	
	public static final String S_CAMERA = "Camera";
	
	/* Categories */
	public static final String C_CAMERA = "camera";
	public static final String C_VIDEO = "video";
	
	public static final String C_TIMED_EVENT = "timed_event";
	
	/* Actions */
	public static final String A_VIDEO_RECORD_FAIL = "video_record_fail";
	public static final String A_VIDEO_NOT_FOUND = "video_not_found_on_device";
	public static final String A_LOGIN = "login";
	
	/* Values */

}
