package com.weclipse.app.utils;

public class ReplyPack {

	public long user_id;
	public long video_id;
	public long group_id;
	public int type;
	
	public ReplyPack(long video_id,long user_id,long group_id,int type){
		this.video_id=video_id;
		this.user_id=user_id;
		this.group_id=group_id;
		this.type = type;
	}
	
}
