package com.weclipse.app.utils;

public class MyHeader {
	String name;
	String value;
	
	public MyHeader(String name,String value){
		this.name = name;
		this.value = value;
	}
	
	public String getName(){
		return name;
	}
	
	public String getValue(){
		return value;
	}
}