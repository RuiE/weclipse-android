package com.weclipse.app.utils.eventbus;

public class ProgressVideoDownload {

	long id;
	int progress;
	
	public ProgressVideoDownload(long id,int progress){
		this.id = id;
		this.progress = progress;
	}
	
	public long getId(){
		return this.id;
	}
	
	public int getProgress(){
		return this.progress;
	}
	
}
