package com.weclipse.app.utils.eventbus;

public class ReplyPendingVideoFinishedEvent {
	
	long vid;
	boolean success;
	
	public ReplyPendingVideoFinishedEvent(long vid,boolean success){
		this.vid = vid;
		this.success = success;
	}
	
	public long getVid(){
		return this.vid;
	}
	
	public boolean getSuccess(){
		return this.success;
	}

}
