package com.weclipse.app.utils.eventbus;

public class RequestSMSEvent {

	private boolean success;
	
	public RequestSMSEvent(boolean success){
		this.success = success;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
}
