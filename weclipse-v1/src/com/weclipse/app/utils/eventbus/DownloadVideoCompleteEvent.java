package com.weclipse.app.utils.eventbus;

public class DownloadVideoCompleteEvent {
	
	long vid;
	boolean success;
	
	public DownloadVideoCompleteEvent(long vid,boolean success){
		this.vid = vid;
		this.success = success;
	}
	
	public long getId(){
		return vid;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
