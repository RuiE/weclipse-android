package com.weclipse.app.utils.eventbus;

public class UnlikedVideoEvent {
	
	long vid;
	
	public UnlikedVideoEvent(long vid){
		this.vid = vid;
	}
	
	public long getId(){
		return vid;
	}

}
