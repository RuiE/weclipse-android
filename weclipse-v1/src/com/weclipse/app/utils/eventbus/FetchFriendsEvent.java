package com.weclipse.app.utils.eventbus;

import java.util.ArrayList;

import com.weclipse.app.models.User;

public class FetchFriendsEvent {
	
	private long id;
	ArrayList<User> users;
	boolean success;
	
	public FetchFriendsEvent(long id,ArrayList<User> users,boolean success){
		this.id = id;
		this.users = users;
		this.success = success;
	}
	
	public long getId(){
		return this.id;
	}
	
	public ArrayList<User> getFriends(){
		return users;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
