package com.weclipse.app.utils.eventbus;

public class InlineVideoStopEvent {

	public static int NORMAL = 1;
	public static int CORRUPTED = 2;
	
	int type;
	
	public InlineVideoStopEvent(int type){
		this.type = type;
	}
	
	public int getType(){
		return type;
	}
	
}
