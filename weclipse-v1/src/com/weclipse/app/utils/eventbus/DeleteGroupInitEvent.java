package com.weclipse.app.utils.eventbus;

public class DeleteGroupInitEvent {

	private long id;
	
	public DeleteGroupInitEvent(long id){
		this.id = id;
	}
	
	public long getId(){
		return id;
	}
	
}
