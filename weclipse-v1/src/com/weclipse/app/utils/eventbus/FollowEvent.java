package com.weclipse.app.utils.eventbus;

public class FollowEvent {
	
	long user_id;
	int status;
	boolean success;
	
	int n_followers;
	
	public FollowEvent(long user_id,int status,boolean success,int n_followers){
		this.user_id = user_id;
		this.status = status;
		this.success = success;
		this.n_followers = n_followers;
	}
	
	public long getUser_id(){
		return user_id;
	}
	
	public int getStatus(){
		return status;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
	public int getN_followers(){
		return n_followers;
	}

}
