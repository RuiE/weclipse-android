package com.weclipse.app.utils.eventbus;

import com.weclipse.app.models.Video;

public class FetchVideoEvent {

	Video v;
	
	public FetchVideoEvent(Video v){
		this.v = v;
	}
	
	public Video getVideo(){
		return v;
	}
	
}
