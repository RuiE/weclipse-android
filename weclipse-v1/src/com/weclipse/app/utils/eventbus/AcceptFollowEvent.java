package com.weclipse.app.utils.eventbus;

public class AcceptFollowEvent {
	
	private boolean success;
	
	public AcceptFollowEvent(boolean success){
		this.success = success;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
