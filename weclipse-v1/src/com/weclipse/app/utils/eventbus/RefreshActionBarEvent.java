package com.weclipse.app.utils.eventbus;

public class RefreshActionBarEvent {
	
	private boolean refreshing;
	
	public RefreshActionBarEvent(boolean refreshing){
		this.refreshing = refreshing;
	}
	
	public boolean getRefreshing(){
		return refreshing;
	}

}
