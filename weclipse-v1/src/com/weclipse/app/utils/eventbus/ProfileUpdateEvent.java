package com.weclipse.app.utils.eventbus;

import java.util.ArrayList;

import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.Video;

public class ProfileUpdateEvent {

	boolean success;
	ArrayList<TimelineVideo> videos;
	ArrayList<Long> likes;
	long user_id;
	int n_friends;
	int n_followers;
	int n_following;
	int n_videos;
	String name;
	String username;
	boolean private_user;
	String avatar_url;
	int following_status;
	
	int code;
	
	public ProfileUpdateEvent(boolean success,ArrayList<TimelineVideo> videos,ArrayList<Long> likes,long user_id,int n_friends,int n_followers,int n_following,String name,String username,int n_videos,boolean private_user,String avatar_url,int following_status,int code){
		this.success = success;
		this.videos = videos;
		this.likes = likes;
		this.user_id = user_id;
		this.n_friends = n_friends;
		this.n_followers = n_followers;
		this.n_following = n_following;
		this.name = name;
		this.username = username;
		this.n_videos = n_videos;
		this.private_user = private_user;
		this.avatar_url = avatar_url;
		this.following_status = following_status;
		this.code = code;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	
	public int getCode(){
		return code;
	}

	/**
	 * @return the videos
	 */
	public ArrayList<TimelineVideo> getVideos() {
		return videos;
	}

	/**
	 * @return the user_id
	 */
	public long getUser_id() {
		return user_id;
	}

	/**
	 * @return the n_friends
	 */
	public int getN_friends() {
		return n_friends;
	}

	/**
	 * @return the n_followers
	 */
	public int getN_followers() {
		return n_followers;
	}

	/**
	 * @return the n_following
	 */
	public int getN_following() {
		return n_following;
	}
	
	/**
	 * @return the n_videos
	 */
	public int getN_videos() {
		return n_videos;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	public boolean getPrivate(){
		return private_user;
	}
	
	public String getAvatar_url(){
		return avatar_url;
	}
	
	public ArrayList<Long> getLikes(){
		return likes;
	}
	
	public int getFollowing_status(){
		return following_status;
	}
	
	
}
