package com.weclipse.app.utils.eventbus;


public class FetchTimelineEvent {

	private boolean success;
	private int method;
	
	public FetchTimelineEvent(boolean success,int method){
		this.success = success;
		this.method = method;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
	public int getMethod(){
		return method;
	}
	
}
