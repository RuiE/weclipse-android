package com.weclipse.app.utils.eventbus;


public class FetchMyVideosEvent {
	
	int method;
	boolean success;
	
	public FetchMyVideosEvent(boolean success,int method){
		this.method = method;
		this.success = success;
	}
	
	public int getMethod(){
		return method;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
