package com.weclipse.app.utils.eventbus;

public class GroupLeaveEvent {
	
	private boolean success;
	private long id;
	
	public GroupLeaveEvent(boolean success,long id){
		this.success = success;
		this.id = id;
	}
	
	public boolean getSuccess(){
		return this.success;
	}
	
	public long getId(){
		return this.id;
	}

}
