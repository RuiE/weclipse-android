package com.weclipse.app.utils.eventbus;

public class ProgressPendingCreateVideoEvent {

	long id;
	int progress;
	
	public ProgressPendingCreateVideoEvent(long id,int progress){
		this.id = id;
		this.progress = progress;
	}
	
	public long getId(){
		return id;
	}
	
	public int getProgress(){
		return progress;
	}
	
}
