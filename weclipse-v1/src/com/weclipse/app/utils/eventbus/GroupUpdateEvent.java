package com.weclipse.app.utils.eventbus;

public class GroupUpdateEvent {
	
	long gid;
	
	public GroupUpdateEvent(long gid){
		this.gid = gid;
	}
	
	public long getId(){
		return gid;
	}

}
