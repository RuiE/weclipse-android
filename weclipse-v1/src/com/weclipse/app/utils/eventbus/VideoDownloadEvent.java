package com.weclipse.app.utils.eventbus;

public class VideoDownloadEvent {
	
	private long id;
	
	public VideoDownloadEvent(long id){
		this.id = id;
	}
	
	public long getId(){
		return this.id;
	}

}
