package com.weclipse.app.utils.eventbus;

import java.util.ArrayList;

import com.weclipse.app.models.User;

public class FetchFacebookFriendsEvent {

	ArrayList<User> mUsers;
	boolean sex;
	
	public FetchFacebookFriendsEvent(ArrayList<User> mUsers,boolean sex){
		this.sex = sex;
		this.mUsers = mUsers;
	}
	
	public ArrayList<User> getUsers(){
		return mUsers;
	}
	
	public boolean getSuccess(){
		return sex;
	}
	
}
