package com.weclipse.app.utils.eventbus;

public class UnfollowEvent {

	long user_id;
	boolean success;
	
	int n_followers;
	
	public UnfollowEvent(long user_id,boolean success,int n_followers){
		this.user_id = user_id;
		this.success = success;
		this.n_followers = n_followers;
	}
	
	public long getUser_id(){
		return user_id;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
	public int getN_followers(){
		return n_followers;
	}
	
}
