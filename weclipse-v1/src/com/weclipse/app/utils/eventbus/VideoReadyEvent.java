package com.weclipse.app.utils.eventbus;

public class VideoReadyEvent {

	private long id;
	
	public VideoReadyEvent(long id){
		this.id = id;
	}
	
	public long getId(){
		return this.id;
	}
	
}
