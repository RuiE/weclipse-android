package com.weclipse.app.utils.eventbus;

public class ReplyPendingVideoStartedEvent {
	
	long vid;
	
	public ReplyPendingVideoStartedEvent(long vid){
		this.vid = vid;
	}
	
	public long getVid(){
		return this.vid;
	}

}
