package com.weclipse.app.utils.eventbus;

public class FacebookLoginEvent {

	public static final int ERROR = 5;
	public static final int NEW_ACCOUNT = 1;
	public static final int LOGIN = 2;
	
	int action;
	
	public FacebookLoginEvent(int action){
		this.action = action;
	}
	
	public int getAction(){
		return action;
	}
	
}
