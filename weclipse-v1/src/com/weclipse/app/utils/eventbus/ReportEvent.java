package com.weclipse.app.utils.eventbus;

public class ReportEvent {

	private boolean success;
	
	public ReportEvent(boolean success){
		this.success = success;
	}
	
	public boolean getSuccess(){
		return this.success;
	}
	
}
