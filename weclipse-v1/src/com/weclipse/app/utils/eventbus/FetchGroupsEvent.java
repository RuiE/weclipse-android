package com.weclipse.app.utils.eventbus;

import java.util.ArrayList;

import com.weclipse.app.models.Group;

public class FetchGroupsEvent {
	
	ArrayList<Group> groups;
	boolean success;
	
	public FetchGroupsEvent(ArrayList<Group> groups,boolean success){
		this.groups = groups;
		this.success = success;
	}
	
	public ArrayList<Group> getGroups(){
		return groups;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
