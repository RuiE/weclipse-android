package com.weclipse.app.utils.eventbus;

public class ProgressCreateVideoEvent {

	Integer progress;
	int id;
	
	public ProgressCreateVideoEvent(int id,Integer progress){
		this.id = id;
		this.progress = progress;
	}
	
	public int getId(){
		return this.id;
	}
	
	public int getProgress(){
		return this.progress;
	}
	
}
