package com.weclipse.app.utils.eventbus;

public class DeleteVideoEvent {

	long vid;
	boolean sex;
	
	public DeleteVideoEvent(boolean sex,long vid){
		this.sex = sex;
		this.vid = vid;
	}
	
	public boolean getSuccess(){
		return sex;
	}
	
	public long getId(){
		return vid;
	}
	
}
