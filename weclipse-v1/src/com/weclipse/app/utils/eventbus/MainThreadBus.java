package com.weclipse.app.utils.eventbus;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.squareup.otto.Bus;

public class MainThreadBus extends Bus {
	
	private final Bus mBus;
	private final Handler mainThread = new Handler(Looper.getMainLooper());

	public MainThreadBus(final Bus bus){
		if(bus==null){
			throw new NullPointerException("Bus must not be null");
		}
		mBus = bus;
	}
	
	@Override
	public void register(Object obj) {
		mBus.register(obj);
	}
	
	@Override
	public void unregister(Object obj) {
		mBus.unregister(obj);
	}
	
	@Override
	public void post(final Object event) {
		if (Looper.myLooper() == Looper.getMainLooper()) {
			mBus.post(event);
		} else {
			mainThread.post(new Runnable() {
				@Override
				public void run() {
					mBus.post(event);
				}
			});
		}
	}
	
}
