package com.weclipse.app.utils.eventbus;

public class NewCommentEvent {
	
	private long video_id;
	private int source;
	
	public NewCommentEvent(long video_id,int source){
		this.video_id=video_id;
		this.source = source;
	}
	
	public long getVideoId(){
		return video_id;
	}
	
	public int getSource(){
		return source;
	}

}
