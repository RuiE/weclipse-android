package com.weclipse.app.utils.eventbus;

public class RemoveVideoEventComplete {
	
	private long vid;
	private boolean success;
	
	public RemoveVideoEventComplete(long vid,boolean success){
		this.vid = vid;
		this.success = success;
	}
	
	public long getVid(){
		return vid;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
