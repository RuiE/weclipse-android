package com.weclipse.app.utils.eventbus;

public class InsertUsernameEvent {
	
	private int code;

	public InsertUsernameEvent(int code){
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
	
}
