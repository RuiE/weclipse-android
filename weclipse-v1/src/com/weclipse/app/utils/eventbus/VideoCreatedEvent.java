package com.weclipse.app.utils.eventbus;

public class VideoCreatedEvent {

	boolean success;
	
	public VideoCreatedEvent(boolean success){
		this.success = success;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
}
