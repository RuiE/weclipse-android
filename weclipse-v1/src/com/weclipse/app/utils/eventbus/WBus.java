package com.weclipse.app.utils.eventbus;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class WBus {
	
	private static final MainThreadBus BUS = new MainThreadBus(new Bus(ThreadEnforcer.ANY));
	
	public static MainThreadBus getInstance() {
		return BUS;
	}

}
