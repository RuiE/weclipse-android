package com.weclipse.app.utils.eventbus;

public class FetchPossiblePlayersEvent {

	private boolean success;
	
	public FetchPossiblePlayersEvent(boolean success){
		this.success = success;
	}
	
	public boolean getSuccess(){
		return this.success;
	}
	
}
