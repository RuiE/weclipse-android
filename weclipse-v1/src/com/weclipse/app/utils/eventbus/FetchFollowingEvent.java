package com.weclipse.app.utils.eventbus;

import java.util.ArrayList;

import com.weclipse.app.models.User;

public class FetchFollowingEvent {
	
	private long id;
	ArrayList<User> users;
	boolean success;
	
	public FetchFollowingEvent(long id,ArrayList<User> users,boolean success){
		this.id = id;
		this.users = users;
		this.success = success;
	}
	
	public long getId(){
		return this.id;
	}
	
	public ArrayList<User> getFollowing(){
		return users;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
