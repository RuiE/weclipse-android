package com.weclipse.app.utils.eventbus;

public class CreatePendingVideoStartedEvent {

	private long pid;
	
	public CreatePendingVideoStartedEvent(long pid){
		this.pid=pid;
	}
	
	public long getPid(){
		return pid;
	}
	
}
