package com.weclipse.app.utils.eventbus;

public class LoginEvent {
	
	public static final int TYPE_SUCCESS = 1;
	public static final int TYPE_UNAUTHTORIZED = 2;
	public static final int TYPE_TOO_MANY_TIMES = 3;
	public static final int TYPE_FAIL = 4;
	
	private int type;
	
	public LoginEvent(int type){
		this.type = type;
	}
	
	public int getType(){
		return type;
	}

}
