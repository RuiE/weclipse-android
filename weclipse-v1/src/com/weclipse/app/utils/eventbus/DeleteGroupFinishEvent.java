package com.weclipse.app.utils.eventbus;

public class DeleteGroupFinishEvent {

	long id;
	boolean success;
	
	public DeleteGroupFinishEvent(long id,boolean success){
		this.id = id;
		this.success = success;
	}
	
	public long getId(){
		return id;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
}
