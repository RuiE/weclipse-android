package com.weclipse.app.utils.eventbus;

public class UpdateMeEvent {
	
	private int field;
	private boolean success;
	
	public UpdateMeEvent(int field,boolean success){
		this.field = field;
		this.success = success;
	}
	
	public int getField(){
		return field;
	}
	
	public boolean getSuccess(){
		return success;
	}

}
