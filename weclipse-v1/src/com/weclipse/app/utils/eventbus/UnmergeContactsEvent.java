package com.weclipse.app.utils.eventbus;

public class UnmergeContactsEvent {
	
	boolean sex;
	
	public UnmergeContactsEvent(boolean sex){
		this.sex = sex;
	}
	
	public boolean getSuccess(){
		return sex;
	}

}
