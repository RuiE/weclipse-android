package com.weclipse.app.utils.eventbus;

public class ValidateSMSEvent {

	private boolean success;
	
	public ValidateSMSEvent(boolean success){
		this.success = success;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
}
