package com.weclipse.app.utils.eventbus;

import com.weclipse.app.utils.DefaultHashMap;

public class ActivityUpdateEvent {

	boolean success;
	DefaultHashMap<Long, Integer> mStatuses;
	
	public ActivityUpdateEvent(boolean success,DefaultHashMap<Long, Integer> mStatuses){
		this.success = success;
		this.mStatuses = mStatuses;
	}
	
	public boolean getSuccess(){
		return this.success;
	}
	
	public DefaultHashMap<Long, Integer> getStatuses(){
		return mStatuses;
	}
	
}
