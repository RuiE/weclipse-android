package com.weclipse.app.utils.eventbus;

import java.util.ArrayList;

import com.weclipse.app.models.User;

public class SearchEvent {

	boolean success;
	ArrayList<User> mUsers;
	
	public SearchEvent(boolean success,ArrayList<User> mUsers){
		this.success = success;
		this.mUsers = mUsers;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
	public ArrayList<User> getUsers(){
		return mUsers;
	}
	
}
