package com.weclipse.app.utils.eventbus;

public class CreatePendingVideoFinishedEvent {

	private long vid;
	private long pid;
	private boolean success;
	
	public CreatePendingVideoFinishedEvent(long vid,long pid,boolean success){
		this.vid=vid;
		this.pid=pid;
		this.success = success;
	}
	
	public long getVid(){
		return vid;
	}
	
	public long getPid(){
		return pid;
	}
	
	public boolean getSuccess(){
		return this.success;
	}
	
}
