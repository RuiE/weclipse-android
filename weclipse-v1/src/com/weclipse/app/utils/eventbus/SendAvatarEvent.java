package com.weclipse.app.utils.eventbus;

public class SendAvatarEvent {

	boolean success;
	
	public SendAvatarEvent(boolean success){
		this.success = success;
	}
	
	public boolean getSuccess(){
		return success;
	}
	
}
