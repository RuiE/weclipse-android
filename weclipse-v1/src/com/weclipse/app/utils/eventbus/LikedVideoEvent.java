package com.weclipse.app.utils.eventbus;

public class LikedVideoEvent {
	
	public long vid;
	
	public LikedVideoEvent(long vid){
		this.vid = vid;
	}
	
	public long getId(){
		return vid;
	}

}
