package com.weclipse.app.utils.eventbus;

import java.util.ArrayList;

import com.weclipse.app.models.Like;
import com.weclipse.app.models.User;

public class FetchLikeEvent {
	
	private long id;
	ArrayList<Like> likes;
	ArrayList<User> users;
	
	public FetchLikeEvent(long id,ArrayList<Like> likes,ArrayList<User> users){
		this.id = id;
		this.likes = likes;
		this.users = users;
	}
	
	public long getId(){
		return this.id;
	}
	
	public ArrayList<Like> getLikes(){
		return likes;
	}
	
	public ArrayList<User> getUsers(){
		return users;
	}

}
