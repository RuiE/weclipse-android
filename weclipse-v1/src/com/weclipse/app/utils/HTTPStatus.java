package com.weclipse.app.utils;

public class HTTPStatus {
	
	public static final int CONTINUE = 100;
	
	public static final int OK = 200;
	public static final int CREATED = 201;
	
	public static final int REDIRECT_FOUND = 302;
	public static final int NOT_MODIFIED = 304;
	
	public static final int BAD_REQUEST = 400;
	public static final int UNAUTHORIZED = 401;
	public static final int NOT_FOUND = 404;
	
	public static final int INTERNAL_SERVER_ERROR = 500;
	
	/* Custom WECLIPSE HTTP CODES */
	public static final int PASSWORD_TOO_MANY_TIMES = 430;
	
	public static final int CLIP_NOT_PROVIDED = 431;

}
