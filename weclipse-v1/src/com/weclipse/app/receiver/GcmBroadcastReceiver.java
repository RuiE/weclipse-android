package com.weclipse.app.receiver;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.weclipse.app.service.GcmIntentService;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
	private static final String TAG ="GCM Receiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        // Explicitly specify that GcmIntentService will handle the intent.
//    	Log.i(TAG, "Received Broadcast");
        ComponentName comp = new ComponentName(context.getPackageName(),GcmIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
//        Log.i(TAG, "Started Shizzle");
    }
}