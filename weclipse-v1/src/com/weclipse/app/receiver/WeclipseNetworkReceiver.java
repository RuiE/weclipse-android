package com.weclipse.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.weclipse.app.WApp;
import com.weclipse.app.utils.WLog;

public class WeclipseNetworkReceiver extends BroadcastReceiver {
	
	WApp app;
	
	public static final int CONNECTION_WIFI = 1;
	public static final int CONNECITON_DATA = 2;
	
	@Override
    public void onReceive(Context context, Intent intent) {
		app = (WApp)context.getApplicationContext();
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE );

        NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        boolean isConnected = (wifi != null && wifi.isConnected()) || (mobile != null && mobile.isConnected());

        if (isConnected){
        	WLog.d(this,"connected to WIFI, also doing nothing");
        	app.connectedToInternet(wifi != null && wifi.isConnected()?CONNECTION_WIFI:CONNECITON_DATA);
        } else {
//        	app.showNoInternet();
        	WLog.d(this,"Broadcast showing no internet connection, doing nothing");
        }
    }
	
}
