package com.weclipse.app.models;

import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.WLog;

public abstract class AbstractVideo implements Storable {
	
	Context context;
	DBHelper mDatabase;
	
	long id;
	String title;
	long owner_id,group_id;
	int video_status;
	int private_video;
	
	int n_likes;
	int n_views;
	int n_comments;
	
	ArrayList<Long> users;
	ArrayList<String> replied_ats;
	
	String created_at,updated_at;
	
	public static final int SOURCE_TIMELINE = 1;
	public static final int SOURCE_PROFILE = 2;
	public static final int SOURCE_DETAIL = 3;
	
	public abstract boolean isTimelineVideo();
	public abstract boolean isMyVideo();
	public abstract void load();
	
	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return the owner_id
	 */
	public long getOwner_id() {
		return owner_id;
	}

	/**
	 * @param owner_id the owner_id to set
	 */
	public void setOwner_id(long owner_id) {
		this.owner_id = owner_id;
	}
	
	/**
	 * @return the group_id
	 */
	public long getGroup_id() {
		return group_id;
	}

	/**
	 * @param group_id the group_id to set
	 */
	public void setGroup_id(long group_id) {
		this.group_id = group_id;
	}
	
	/**
	 * @return the video_status
	 */
	public int getVideo_status() {
		return video_status;
	}

	/**
	 * @param video_status the video_status to set
	 */
	public void setVideo_status(int video_status) {
		this.video_status = video_status;
	}
	
	/**
	 * @return the updated_at
	 */
	public String getUpdated_at() {
		return updated_at;
	}

	/**
	 * @param updated_at the updated_at to set
	 */
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	/**
	 * @return the created_at
	 */
	public String getCreated_at() {
		return created_at;
	}

	/**
	 * @param created_at the created_at to set
	 */
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	
	/**
	 * @return the users
	 */
	public ArrayList<Long> getUsers() {
		return users;
	}
	
	public String getUsersText(){
		String user_ids = "";
		for(int i = 0 ; i < users.size() ; i++){
			user_ids += users.get(i);
			user_ids += ",";
		}
		user_ids = user_ids.substring(0, user_ids.length()-1);
		return user_ids;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(ArrayList<Long> users) {
		this.users = users;
	}

	/**
	 * @return the private_video
	 */
	public int getPrivate_video() {
		return private_video;
	}
	/**
	 * @param private_video the private_video to set
	 */
	public void setPrivate_video(int private_video) {
		this.private_video = private_video;
	}
	
	/**
	 * @return the n_likes
	 */
	public int getN_likes() {
		return n_likes;
	}

	/**
	 * @param n_likes the n_likes to set
	 */
	public void setN_likes(int n_likes) {
		this.n_likes = n_likes;
	}

	/**
	 * @return the n_views
	 */
	public int getN_views() {
		return n_views;
	}

	/**
	 * @param n_views the n_views to set
	 */
	public void setN_views(int n_views) {
		this.n_views = n_views;
	}

	/**
	 * @return the n_comments
	 */
	public int getN_comments() {
		return n_comments;
	}

	/**
	 * @param n_comments the n_comments to set
	 */
	public void setN_comments(int n_comments) {
		this.n_comments = n_comments;
	}
	
	public ArrayList<String> getRepliedAts(){
		return replied_ats;
	}
	
	public String getRepliedAtsText(){
		String res = "";
		for(int i = 0 ; i < replied_ats.size() ; i++){
			res += replied_ats.get(i)+",";
		}
		res = res.substring(0, res.length() - 1);
		return res;
	}
	
	public boolean equals(AbstractVideo v) {
		return v.getId() == this.getId();
	}
	
	public void updateStats(int n_likes, int n_views, int n_comments,int source) {
		WLog.d(this,"up the dating");
		String table = "";
		
		switch(source){
		case SOURCE_DETAIL:
			table = DBHelper.TABLE_VIDEOS;
			break;
		case SOURCE_PROFILE:
			table = DBHelper.TABLE_MY_VIDEOS;
			break;
		case SOURCE_TIMELINE:
			table = DBHelper.TABLE_TIMELINE;
			break;
		case 0:
			if(isTimelineVideo()){
				table = DBHelper.TABLE_TIMELINE;
			} else {
				table = DBHelper.TABLE_VIDEOS;
			}
			break;
		}
		
		WLog.d(this,"Updating stats on "+table);

		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.execSQL("UPDATE "+table+" SET "+DBHelper.C_N_VIEWS+"="+n_views+","
				+DBHelper.C_N_COMMENTS+"="+n_comments + ","
				+DBHelper.C_N_LIKES+"="+n_likes+" WHERE "+DBHelper.C_ID+"="+id);
		
		if(source == 0 && isTimelineVideo() && isMyVideo()){
			WLog.d(this,"And also on "+DBHelper.TABLE_MY_VIDEOS);
			myDatabase.execSQL("UPDATE "+DBHelper.TABLE_MY_VIDEOS+" SET "+DBHelper.C_N_VIEWS+"="+n_views+","
					+DBHelper.C_N_COMMENTS+"="+n_comments + ","
					+DBHelper.C_N_LIKES+"="+n_likes+" WHERE "+DBHelper.C_ID+"="+id);
		}
		
		
		myDatabase.close();
	}
	
}
