package com.weclipse.app.models;

import android.content.Context;

import com.weclipse.app.R;

public class Country {
	
	public String code;
	public String country;
	
	public Country(String country,String code){
		this.country=country;
		this.code=code;
	}
	
	public static String getCountry(int code,Context context){
		for(String item : context.getResources().getStringArray(R.array.countries)){
			if(Integer.valueOf(item.split(";")[1])==code)
				return item.split(";")[0];
		}
		return context.getString(R.string.choose_your_country);
	}
	
	public static String getCountry(String iso,Context context){
		for(String item : context.getResources().getStringArray(R.array.countries)){
			if(item.split(";")[2].equals(iso))
				return item.split(";")[0];
		}
		return context.getString(R.string.choose_your_country);
	}

}
