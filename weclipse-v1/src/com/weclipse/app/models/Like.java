package com.weclipse.app.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.sqlite.SQLiteDatabase;

import com.weclipse.app.WApp;

public class Like {
	
	long vid;
	long user_id;
	String created_at;
	
	public Like(long vid,long user_id,String created_at){
		this.vid = vid;
		this.user_id = user_id;
		this.created_at = created_at;
	}
	
	public long getId(){
		return vid;
	}
	
	public String getCreated_at(){
		return created_at;
	}
	
	public long getUser_id(){
		return user_id;
	}

	public static String getLastId(WApp app) {
		SQLiteDatabase mDatabase = app.getDB().getReadableDatabase();
		return null;
	}
	
	public static Like parse(JSONObject jLike) throws JSONException {
		return new Like(Long.valueOf(jLike.getString("id")),
						Long.valueOf(jLike.getString("user_id")),
						jLike.getString("created_at"));
	}

}
