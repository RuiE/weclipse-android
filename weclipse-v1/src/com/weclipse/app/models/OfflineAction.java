package com.weclipse.app.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.weclipse.app.utils.DBHelper;

public class OfflineAction implements Storable {
	
	public static final int VIDEO_VIEWED = 1;
	
	public static final int SEND_VIDEO = 10;
	public static final int CLEAR_FEED = 20;
	public static final int CLEAR_VIDEO = 21;
	
	int id;
	long provided_id;
	int type;
	Context context;
	DBHelper mDatabase;
	
	public OfflineAction(Context context, int id){
		this.context=context;
		mDatabase= new DBHelper(context);
		this.id=id;
	}

	public OfflineAction(Context context,long provided_id,int type){
		this.context=context;
		mDatabase = new DBHelper(context);
		this.provided_id=provided_id;
		this.type=type;
	}

	@Override
	public void create() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM "+DBHelper.TABLE_ACTIONS+" WHERE "+DBHelper.C_ID+"="+id, null);
		if(c.moveToFirst()){
			c.close();
			db.close();
		} else {
			c.close();
			try {
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_PROVIDED_ID,provided_id);
				values.put(DBHelper.C_TYPE, type);
				db.insert(DBHelper.TABLE_ACTIONS, null,values);
				db.close();
			} catch (SQLException e) {
				//Já existe
			}
		}
	}

	@Override
	public void update(String key, String value) {
		// TODO Auto-generated method stub
	}

	@Override
	public void update(String key, int value) {
		// TODO Auto-generated method stub
	}

	@Override
	public void destroy() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_ACTIONS, DBHelper.C_ID+"="+id, null);
		db.close();
	}

	@Override
	public void load() {
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_ACTIONS+" WHERE "+DBHelper.C_ID+"="+id, null);
		c.moveToFirst();
		provided_id = c.getLong(c.getColumnIndex(DBHelper.C_PROVIDED_ID));
		type = c.getInt(c.getColumnIndex(DBHelper.C_TYPE));
		c.close();
		myDatabase.close();
	}
	
	public long getProvidedId(){
		return provided_id;
	}
	
	public int getType(){
		return type;
	}

}
