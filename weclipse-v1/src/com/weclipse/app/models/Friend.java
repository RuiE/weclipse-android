package com.weclipse.app.models;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.weclipse.app.WApp;
import com.weclipse.app.utils.DBHelper;

public class Friend implements Storable {
	
	private long id;
	private Context context;
	DBHelper mDatabase;
	
	public Friend(Context context,long id){
		this.context = context;
		this.id = id;
		mDatabase = new DBHelper(context);
		
	}
	
	public long getId(){
		return id;
	}



	@Override
	public void create() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM "+DBHelper.TABLE_FRIENDS+" WHERE "+DBHelper.C_ID+"="+id, null);
		if(c.moveToFirst()){
			c.close();
			db.close();
		} else {
			c.close();
			try {
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID, id);
				db.insert(DBHelper.TABLE_ACTIVITY, null,values);
				db.close();
			} catch (SQLException e) {
				
			}
		}
	}

	@Override
	public void update(String key, String value) {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.execSQL("UPDATE "+DBHelper.TABLE_FRIENDS+" SET "+key+"='"+value+"' WHERE "+DBHelper.C_ID+"="+id);
		db.close();
	}

	@Override
	public void update(String key, int value) {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.execSQL("UPDATE "+DBHelper.TABLE_FRIENDS+" SET "+key+"="+value+" WHERE "+DBHelper.C_ID+"="+id);
		db.close();
	}

	@Override
	public void destroy() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_FRIENDS, DBHelper.C_ID+"="+id, null);
		db.close();
	}

	@Override
	public void load() {
		
	}
	
	public static Cursor getFriends(WApp app){
		Cursor c;
		SQLiteDatabase myDatabase = app.getDB().getReadableDatabase();
		c = myDatabase.rawQuery("SELECT "+DBHelper.TABLE_USERS+".* FROM "+DBHelper.TABLE_FRIENDS+" INNER JOIN "+DBHelper.TABLE_USERS+" ON "+DBHelper.TABLE_FRIENDS+"."+DBHelper.C_ID+"="+
																																			DBHelper.TABLE_USERS+"."+DBHelper.C_ID,null);
		return c;
	}
	
	public static void bulkInsert(WApp app,ArrayList<Long> friends){
		String sql = "INSERT INTO "+DBHelper.TABLE_FRIENDS+" VALUES(?)";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();
		
		db.delete(DBHelper.TABLE_FRIENDS,null, null);
		
		for(int i = 0 ; i < friends.size() ; i++){
			stmt.clearBindings();
			stmt.bindLong(1,friends.get(i));
			stmt.execute();
		}
		
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
}
