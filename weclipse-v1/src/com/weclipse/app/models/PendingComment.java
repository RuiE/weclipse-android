package com.weclipse.app.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.weclipse.app.utils.DBHelper;

public class PendingComment implements Storable {

	private static final String TAG = "PendingCommentModel";
	
	Context context;
	DBHelper mDatabase;
	long id;
	long vid;
	String comment;
	
	public PendingComment(Context context,long id){
		this.context = context;
		mDatabase = new DBHelper(context);
		this.id=id;
	}
	
	public PendingComment(Context context,long id,long vid,String comment){
		this.context=context;
		mDatabase = new DBHelper(context);
		this.id=id;
//		Log.d(TAG,"Created in vid: "+vid);
		this.vid=vid;
		this.comment=comment;
	}

	@Override
	public void create() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM "+DBHelper.TABLE_COMMENTS_PENDING+" WHERE "+DBHelper.C_ID+"="+id, null);
		if(c.moveToFirst()){
			c.close();
			db.close();
		} else {
			c.close();
			try {
//				Log.d(TAG,"Vou criar: "+vid);
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID,id);
				values.put(DBHelper.C_VIDEO_ID,vid);
				values.put(DBHelper.C_COMMENT,comment);
				db.insert(DBHelper.TABLE_COMMENTS_PENDING, null,values);
				db.close();
			} catch (SQLException e) {
				//J?? existe
			}
		}
	}

	@Override
	public void update(String key, String value) {
		// Comments are not updated
	}

	@Override
	public void update(String key, int value) {
		// Comments are not updated
	}

	@Override
	public void destroy() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_COMMENTS_PENDING, DBHelper.C_ID+"="+id, null);
		db.close();
	}

	@Override
	public void load() {
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_COMMENTS_PENDING+" WHERE "+DBHelper.C_ID+"="+id, null);
		c.moveToFirst();
		vid = c.getLong(c.getColumnIndex(DBHelper.C_VIDEO_ID));
		comment = c.getString(c.getColumnIndex(DBHelper.C_COMMENT));
		c.close();
		myDatabase.close();
	}
	
	public String toString(){
		return "[Vid: "+vid+"] I said: "+comment+" at "+id;
	}
	
	public String getComment(){
		return comment;
	}
	
}
