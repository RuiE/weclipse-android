package com.weclipse.app.models;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.TheMainActivity;
import com.weclipse.app.managers.AlertManager;
import com.weclipse.app.managers.NotificationBarManager;

public class WNotification extends NotificationCompat {
	
	private static final String TAG = "WNotification";
	
	/* Test */
	public static final int CHUCK_TESTA = 0;
	
	/* Videos */
	public static final int VIDEO_READY = 1;
    public static final int VIDEO_INVITE = 2;
    public static final int VIDEO_UPDATE = 7;
    public static final int VIDEO_COMMENT = 9;
    public static final int VIDEO_LIKE = 16;
    
    /* Friends */
    public static final int USER_FOLLOW = 3;
    public static final int USER_UNFOLLOW = 4;
    public static final int USER_FRIENDS = 32;
    public static final int USER_REQUEST_FOLLOW = 30;
    public static final int USER_ACCEPT_FOLLOW = 31;
    
    public static final int FRIEND_DELETE_ACCOUNT = 10;
    public static final int CONTACTS_READY = 5;
    public static final int CONTACT_JOINED_WECLIPSE = 12;
    
    /* Groups */
    public static final int GROUP_UPDATE = 6;
    public static final int GROUP_DELETE = 11;
    public static final int GROUP_INVITE = 14;
    
    /* Misc */
    public static final int LOGOUT = 8;
    
    /* Notification bar ids */
    public static final int NOTIFICATION_DEFAULT_ID = 1;
    public static final int NOTIFICATION_FRIEND_ID = 2; 
    public static final int NOTIFICATION_VIDEO_READY_ID = 3;
    public static final int NOTIFICATION_VIDEO_INVITE_ID = 4;
    public static final int NOTIFICATION_COMMENT_ID = 5;
    
    /* Feed Notification Types */
    public static final int FEED_VIDEO_READY = 1;
    public static final int FEED_VIDEO_INVITE = 2;
    
    /* Users and Friends */
    
    
    /*
     * Video Ready
	 * Video Invite
	 * Comment
	 * Friend Updates
    */
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    WApp app;
    
    public WNotification(WApp app,int type,String text){
    	
    	NotificationBarManager.getInstance(app).savePush(type);

    	if(app.isActive)
    		return;
    	
    	AlertManager mAlertManager = AlertManager.getInstance(app);
    	
    	//Se é para notificar ou não
    	if(type == VIDEO_LIKE || type == VIDEO_COMMENT){
    		if(!mAlertManager.getAlertPreference(AlertManager.COMMENT_ALERT)){
        		return;
    		}
    	} else if(type == VIDEO_READY || type == VIDEO_INVITE){
    		if(!mAlertManager.getAlertPreference(AlertManager.VIDEO_ALERT))
        		return;
    	} else { //Friends ou groups
    		if(!mAlertManager.getAlertPreference(AlertManager.FRIEND_ALERT))
        		return;
    	}
    	
    	//É para notificar...
    	
    	this.app = app;
    	
    	Intent resultIntent = new Intent(app,TheMainActivity.class);
    	resultIntent.putExtra("from_push", true);
    	//É para ir para a activity
    	
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(app);
        stackBuilder.addNextIntent(resultIntent);
    	PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(type, PendingIntent.FLAG_UPDATE_CURRENT);
    	
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(app);
        mBuilder.setContentIntent(resultPendingIntent);
        
        
        mBuilder
	        .setAutoCancel(true)
	        .setSmallIcon(R.drawable.notification_bar_icon)
	        .setContentTitle(app.getString(R.string.notification_title));
	    
        if(NotificationBarManager.getInstance(app).getNPush(type) > 1){
        	text = NotificationBarManager.getInstance(app).getMultipleText(type);
        }
        
        mBuilder.setTicker(text)
        	.setContentText(text);
        
        //Validações de diferentes tipos de preferencias, com som, sem som, com vibração sem vibraçao...
        if(type == VIDEO_LIKE || type == VIDEO_COMMENT){
    		if(mAlertManager.getAlertPreference(AlertManager.COMMENT_SOUND)){
    			mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
    		}
    	} else if(type == VIDEO_READY || type == VIDEO_INVITE){
    		if(mAlertManager.getAlertPreference(AlertManager.VIDEO_SOUND))
    			mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
    	} else { //Friends ou groups
    		if(mAlertManager.getAlertPreference(AlertManager.FRIEND_SOUND))
    			mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
    	}
        
        if(type == VIDEO_LIKE || type == VIDEO_COMMENT){
    		if(mAlertManager.getAlertPreference(AlertManager.COMMENT_VIBRATION)){
    			mBuilder.setVibrate(new long[]{0,500,300,500,300,500});
    		}
    	} else if(type == VIDEO_READY || type == VIDEO_INVITE){
    		if(mAlertManager.getAlertPreference(AlertManager.VIDEO_VIBRATION))
    			mBuilder.setVibrate(new long[]{0,500,300,500,300,500});
    	} else { //Friends ou groups
    		if(mAlertManager.getAlertPreference(AlertManager.FRIEND_VIBRATION))
    			mBuilder.setVibrate(new long[]{0,500,300,500,300,500});
    	}
    	
    	if(type == VIDEO_LIKE || type == VIDEO_COMMENT){
    		if(mAlertManager.getAlertPreference(AlertManager.COMMENT_LED)){
    			mBuilder.setLights(Color.YELLOW,1000,3000);
    		}
    	} else if(type == VIDEO_READY || type == VIDEO_INVITE){
    		if(mAlertManager.getAlertPreference(AlertManager.VIDEO_LED))
    			mBuilder.setLights(Color.YELLOW,1000,3000);
    	} else { //Friends ou groups
    		if(mAlertManager.getAlertPreference(AlertManager.FRIEND_LED))
    			mBuilder.setLights(Color.YELLOW,1000,3000);
    	}
    	
		//Construir a notificação
    	mNotificationManager = (NotificationManager)app.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(type, mBuilder.build());
    	
    }
    
//    public WNotification(WApp app,int type,String title,String caption,long vid){
//    	
//    	if(app.isActive)
//    		return;
//    	
//    	AlertManager mAlertManager = AlertManager.getInstance(app);
//    	
//    	if(!mAlertManager.getAlertPreference(AlertManager.COMMENT_ALERT))
//    		return;
//    	
//    	int number = app.getDB().getCommentNotificationsCount(vid);
//    	int id = app.getDB().getNotificationId(vid);
//    	
//    	//Ver quantas notificações existem.
//    	String msg = "";
//    	if(number==1){
//    		msg = app.getString(R.string.notification_new_comment)+" "+caption;
//    	} else if(number>1){
//    		msg = app.getString(R.string.notification_new_comment_plural)+" "+caption;
//    	}
//    	
//    	this.app = app;
//    	Intent resultIntent = new Intent(app,VideoDetailActivity.class);
//    	resultIntent.putExtra("vid",vid);
//    	resultIntent.putExtra("comment",true);
//    	
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(app);
//        stackBuilder.addNextIntent(resultIntent);
//    	PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT);
//    	
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(app);
//        mBuilder.setContentIntent(resultPendingIntent);
//        
//        mBuilder
//	        .setAutoCancel(true)
//	        .setSmallIcon(R.drawable.notification_bar_icon) //TODO smaller icon
//	        .setContentTitle(title)
//	        .setNumber(number)
//	        .setTicker(msg)
//	        .setContentText(msg);
//        
//        if(mAlertManager.getAlertPreference(AlertManager.COMMENT_SOUND))
//        	mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//        
//        if(mAlertManager.getAlertPreference(AlertManager.COMMENT_VIBRATION))
//        	mBuilder.setVibrate(new long[] {1000,150,300,150,300,150});
//        	
//    	if(mAlertManager.getAlertPreference(AlertManager.COMMENT_LED))
//    		mBuilder.setLights(Color.YELLOW,1000,3000);
//    	
//    	mNotificationManager = (NotificationManager)app.getSystemService(Context.NOTIFICATION_SERVICE);
//		mNotificationManager.notify(id, mBuilder.build());
//	
//    }
//    
//    public WNotification(WApp app,int type,String title,String msg){
//    	
//    	AlertManager mAlertManager = AlertManager.getInstance(app);
//    	
//    	int mId = NOTIFICATION_DEFAULT_ID;
//    	int number = 0;
//    	
//    	switch(type){
//    	case VIDEO_READY:
//    		number = app.getDB().getFeedNotification(FEED_VIDEO_READY)+1;
//    		if(number > 1){
//    			msg = app.getString(R.string.notification_video_ready_plural).replace("__num__", ""+number);
//    		}
//    		mId = NOTIFICATION_VIDEO_READY_ID;
//    		break;
//    	case VIDEO_INVITE:
//    		number = app.getDB().getFeedNotification(FEED_VIDEO_INVITE)+1;
//    		if(number > 1){
//    			msg = app.getString(R.string.notification_video_invite_plural).replace("__num__", ""+number);
//    		}
//    		mId = NOTIFICATION_VIDEO_INVITE_ID;
//    		break;
//    	case FRIEND_REQUEST:
//    	case CONTACT_JOINED_WECLIPSE:
//			msg = app.getString(R.string.notification_friend_updates).replace("__num__","bananas");
//    		mId = NOTIFICATION_FRIEND_ID;
//    		break;
//    	}
//    	
//    	if((type == VIDEO_INVITE || type == VIDEO_READY) && !mAlertManager.getAlertPreference(AlertManager.VIDEO_ALERT))
//    		return;
//    	
//    	if((type == FRIEND_REQUEST || type == CONTACT_JOINED_WECLIPSE) && !mAlertManager.getAlertPreference(AlertManager.FRIEND_ALERT))
//    		return;
//    	
//    	this.app = app;
//    	Intent resultIntent;
//    	if(type == CONTACT_JOINED_WECLIPSE){
////    		resultIntent = new Intent(app,AddFriendActivity.class);
//    		resultIntent = new Intent(app,TheMainActivity.class);
//    	} else {
//    		resultIntent = new Intent(app,TheMainActivity.class);	
//    	}
//    	
//    	if(type == FRIEND_REQUEST){
//    		resultIntent.putExtra("navigate_to_friends",true);
//    	}
//    	
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(app);
//        stackBuilder.addNextIntent(resultIntent);
//    	PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(mId, PendingIntent.FLAG_UPDATE_CURRENT);
//    	
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(app);
//        mBuilder.setContentIntent(resultPendingIntent);
//        
//        mBuilder
//        .setAutoCancel(true)
//        .setSmallIcon(R.drawable.notification_bar_icon)
//        .setContentTitle(title)
//        .setTicker(msg)
//        .setContentText(msg);
//        
//        if(type==VIDEO_READY || type==VIDEO_INVITE){
//        	mBuilder.setNumber(number);
//        }
//        
//        if((type == VIDEO_READY || type == VIDEO_INVITE) && mAlertManager.getAlertPreference(AlertManager.VIDEO_SOUND))
//        	mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//        
//        if((type == VIDEO_READY || type == VIDEO_INVITE) && mAlertManager.getAlertPreference(AlertManager.VIDEO_VIBRATION))
//        	mBuilder.setVibrate(new long[] {1000,150,300,150,300,150});
//        	
//    	if((type == VIDEO_READY || type == VIDEO_INVITE) && mAlertManager.getAlertPreference(AlertManager.VIDEO_LED))
//    		mBuilder.setLights(Color.YELLOW,1000,3000);
//    	
//    	if((type == FRIEND_REQUEST || type == CONTACT_JOINED_WECLIPSE) && mAlertManager.getAlertPreference(AlertManager.FRIEND_SOUND))
//        	mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//        
//        if((type == FRIEND_REQUEST || type == CONTACT_JOINED_WECLIPSE) && mAlertManager.getAlertPreference(AlertManager.FRIEND_VIBRATION))
//        	mBuilder.setVibrate(new long[] {1000,150,300,150,300,150});
//        	
//    	if((type == FRIEND_REQUEST || type == CONTACT_JOINED_WECLIPSE) && mAlertManager.getAlertPreference(AlertManager.FRIEND_LED))
//    		mBuilder.setLights(Color.YELLOW,1000,3000);
//    	
//    	mNotificationManager = (NotificationManager)app.getSystemService(Context.NOTIFICATION_SERVICE);
//		mNotificationManager.notify(mId, mBuilder.build());
//		
//    	if(app.isActive)
//    		cancel(app,mId);
//    	
//    }
    
    public static void cancelAll(Context context){
        NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
        context.getSharedPreferences("notification_preference",Activity.MODE_PRIVATE).edit().remove("notification_bar_video_ready")
    																						.remove("notification_bar_video_invite")
    																						.remove("notification_bar_friend_updates")
    																						.commit();
    }
    
    public static void cancel(Context context,int id){
    	NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(id);
        switch(id){
        case NOTIFICATION_FRIEND_ID:
        	context.getSharedPreferences("notification_preference",Activity.MODE_PRIVATE).edit().remove("notification_bar_friend_updates").commit();
        	break;
        case NOTIFICATION_VIDEO_INVITE_ID:
        	context.getSharedPreferences("notification_preference",Activity.MODE_PRIVATE).edit().remove("notification_bar_video_invite").commit();
        	break;
        case NOTIFICATION_VIDEO_READY_ID:
        	context.getSharedPreferences("notification_preference",Activity.MODE_PRIVATE).edit().remove("notification_bar_video_ready").commit();
        	break;
        }
    }

}
