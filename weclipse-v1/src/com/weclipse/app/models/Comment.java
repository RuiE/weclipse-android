package com.weclipse.app.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.weclipse.app.WApp;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.WLog;

public class Comment implements Storable {
	
	private static final String TAG = "CommentModel";
	public static final int MAX_SIZE = 140;
	
	Context context;
	DBHelper mDatabase;
	long id;
	long vid;
	long uid;
	String created_at;
	String comment;
	
	public Comment(Context context,long id){
		this.context = context;
		mDatabase = new DBHelper(context);
		this.id=id;
	}
	
	public Comment(Context context,long id,long vid,long uid,String comment,String created_at){
		this.context=context;
		mDatabase = new DBHelper(context);
		this.id = id;
		this.vid = vid;
		this.uid = uid;
		this.comment = comment;
		this.created_at = created_at;
	}

	@Override
	public void create() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM "+DBHelper.TABLE_COMMENTS+" WHERE "+DBHelper.C_ID+"="+id, null);
		if(c.moveToFirst()){
			c.close();
			db.close();
		} else {
			c.close();
			try {
//				Log.d(TAG,"Vou criar: "+vid);
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID,id);
				values.put(DBHelper.C_VIDEO_ID,vid);
				values.put(DBHelper.C_USER_ID,uid);
				values.put(DBHelper.C_COMMENT,comment);
				values.put(DBHelper.C_CREATED_AT,created_at);
				db.insert(DBHelper.TABLE_COMMENTS, null,values);
				db.close();
			} catch (SQLException e) {
				//J?? existe
			}
		}
	}

	@Override
	public void update(String key, String value) {
		// Comments are not updated
	}

	@Override
	public void update(String key, int value) {
		// Comments are not updated
	}

	@Override
	public void destroy() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_COMMENTS, DBHelper.C_ID+"="+id, null);
		db.close();
	}

	@Override
	public void load() {
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_COMMENTS+" WHERE "+DBHelper.C_ID+"="+id, null);
		c.moveToFirst();
		vid = c.getLong(c.getColumnIndex(DBHelper.C_VIDEO_ID));
		uid = c.getLong(c.getColumnIndex(DBHelper.C_USER_ID));
		comment = c.getString(c.getColumnIndex(DBHelper.C_COMMENT));
		created_at = c.getString(c.getColumnIndex(DBHelper.C_CREATED_AT));
		c.close();
		myDatabase.close();
	}
	
	public String toString(){
		return "[Vid: "+vid+"] User "+uid+" said "+comment+" at "+created_at;
	}
	
	public long getUserId(){
		return uid;
	}
	
	public String getComment(){
		return comment;
	}
	
	public static Cursor getComments(WApp app,long id){
		SQLiteDatabase myDatabase = app.getDB().getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_COMMENTS+" WHERE "+DBHelper.C_VIDEO_ID+"="+id+" ORDER BY "+DBHelper.C_CREATED_AT+" ASC", null);
		return c;
	}

}
