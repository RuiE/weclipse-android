package com.weclipse.app.models;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.fetch.FetchTimelineTask;
import com.weclipse.app.utils.DBHelper;

public class MyLike {

	Context context;
	DBHelper mDatabase;
	
	long vid;
	
	public static final int TYPE_TIMELINE = 1;
	public static final int TYPE_ACTIVITY = 2;
	
	String table;
	
	public MyLike(Context context,long vid,String table){
		this.context = context;
		mDatabase = new DBHelper(context);
		this.vid = vid;
		this.table = table;
	}
	
	public void create(){
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM "+table+" WHERE "+DBHelper.C_ID+"="+vid, null);
		if(c.moveToFirst()){
			//Já deu like, fazer nada
			c.close();
			db.close();
		} else {
			c.close();
			try {
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID, vid);
				db.insert(table, null,values);
				db.close();
			} catch (SQLException e) {
				
			}
		}
	}
	
	public void destroy(){
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(table, DBHelper.C_ID+"="+vid, null);
		db.close();
	}
	
	public long getId(){
		return vid;
	}
	
	public boolean exists(){
		return mDatabase.getReadableDatabase().rawQuery("SELECT * FROM "+table+" WHERE "+DBHelper.C_ID+"="+vid, null).moveToFirst();
	}
	
	public static void dump(WApp app){
		SQLiteDatabase db = app.getDB().getReadableDatabase();
		
		Cursor c = db.rawQuery("SELECT * FROM "+DBHelper.TABLE_LIKES_TIMELINE, null);
		if(WApp.DEBUG){
			Log.d("MyLike","---Dumping My Likes---");
		}
		while(c.moveToNext()){
			if(WApp.DEBUG){
				Log.d("MyLike","Gostei do: "+c.getLong(c.getColumnIndex(DBHelper.C_ID)));
			}
		}
		
		db.close();
	}
	
	public static void bulkInsert(WApp app,ArrayList<MyLike> likes,int method,String table){
		String sql = "INSERT INTO "+table+" VALUES(?)";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();
		
		if(method == FetchTimelineTask.FRESH){
			db.delete(table,null, null);
		} else if(method == FetchTimelineTask.LOAD_MORE){

			String ids = "";
			int i = 0;
			for(MyLike l : likes){
				ids += l.getId()+",";
				i++;
			}
			
			if(i > 0)
				ids = ids.substring(0, ids.length() - 1);
			
			db.delete(table,DBHelper.C_ID+" IN ("+ids+")", null);
		}
		
		db.setTransactionSuccessful();
		db.endTransaction();
		db.beginTransaction();
		
		MyLike l;
		
		for(int i = 0 ; i < likes.size() ; i++){
			l = likes.get(i);
			stmt.clearBindings();
			stmt.bindLong(1,l.getId());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
}
