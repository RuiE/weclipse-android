package com.weclipse.app.models;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.GroupUpdateEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class Group implements Storable {

	private static final String TAG = "GroupModel";

	public static final int PICTURE_BANNER = 1;
	public static final int PICTURE_THUMB = 2;

	public static final int MAX_GROUP_NAME = 20;
	public static final int MAX_GROUP_SIZE = 20;
	public static final int MIN_GROUP_SIZE = 2;

	public static final int COMPARE_DOES_NOT_EXIST = 0;
	public static final int COMPARE_EXIST_DONT_UPDATE = 1;
	public static final int COMPARE_EXIST_UPDATE = 2;

	public static final int GROUP_BANNER_WIDTH = 640;
	public static final int GROUP_BANNER_HEIGHT = 360;

	Context context;
	DBHelper mDatabase;
	long id;
	String name;
	String updated_at;
	long user_id;
	String avatar_url;
	String cover_url;
	int nrVideos;
	ArrayList<Long> users;

	public Group(Context context, long id, String name, long user_id, ArrayList<Long> users, String avatar_url, String cover_url, int nrVideos, String updated_at) {
		this.context = context;
		mDatabase = new DBHelper(context);
		this.id = id;
		this.name = name;
		this.user_id = user_id;
		this.updated_at = updated_at;
		this.users = users;
		this.avatar_url = avatar_url;
		this.cover_url = cover_url;
		this.nrVideos = nrVideos;
	}

	public Group(Context context, long id) {
		this.context = context;
		mDatabase = new DBHelper(context);
		this.id = id;
	}

	@Override
	public void create() {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + DBHelper.TABLE_GROUPS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		if (c.moveToFirst()) {
			c.close();
			updateAll();
			myDatabase.close();
			return;
		}
		String user_ids = "";
		for (int i = 0; i < users.size(); i++) {
			user_ids += users.get(i);
			user_ids += ",";
		}
		user_ids = user_ids.substring(0, user_ids.length() - 1);
		try {
			ContentValues values = new ContentValues();
			values.put(DBHelper.C_ID, id);
			values.put(DBHelper.C_NAME, name);
			values.put(DBHelper.C_USER_ID, user_id);
			values.put(DBHelper.C_USERS, user_ids);
			values.put(DBHelper.C_AVATAR_URL, avatar_url);
			values.put(DBHelper.C_COVER_URL, cover_url);
			values.put(DBHelper.C_NR_VIDEOS, nrVideos);
			values.put(DBHelper.C_UPDATED_AT, updated_at);
			myDatabase.insert(DBHelper.TABLE_GROUPS, null, values);
			myDatabase.close();
		} catch (SQLException e) {
			// Já existe
		}
	}

	@Override
	public void update(String key, String value) {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.execSQL("UPDATE " + DBHelper.TABLE_GROUPS + " SET " + key + "='" + value + "' WHERE " + DBHelper.C_ID + "=" + id);
		db.close();
	}

	@Override
	public void update(String key, int value) {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.execSQL("UPDATE " + DBHelper.TABLE_GROUPS + " SET " + key + "=" + value + " WHERE " + DBHelper.C_ID + "=" + id);
		db.close();
	}

	public void updateAll() {
		String user_ids = "";
		for (int i = 0; i < users.size(); i++) {
			user_ids += users.get(i);
			user_ids += ",";
		}
		user_ids = user_ids.substring(0, user_ids.length() - 1);
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.execSQL("UPDATE " + DBHelper.TABLE_GROUPS + " SET " + DBHelper.C_NR_VIDEOS + "=" + nrVideos + ","
				+ DBHelper.C_NAME + "='" + name + "',"
				+ DBHelper.C_USER_ID + "=" + user_id + ","
				+ DBHelper.C_UPDATED_AT + "='" + updated_at + "',"
				+ DBHelper.C_USERS + "='" + user_ids + "',"
				+ DBHelper.C_COVER_URL + "='" + cover_url + "',"
				+ DBHelper.C_AVATAR_URL + "='" + avatar_url + "' WHERE " + DBHelper.C_ID + "=" + id);
		db.close();
	}

	@Override
	public void destroy() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_GROUPS, DBHelper.C_ID + "=" + id, null);
		db.close();
	}

	public boolean exists() {
		boolean res;
		Cursor c = mDatabase.getReadableDatabase().rawQuery("SELECT * FROM " + DBHelper.TABLE_GROUPS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		res = c.moveToFirst();
		c.close();
		mDatabase.getReadableDatabase().close();
		return res;
	}

	public int exists(Group g) {
		int res = 0;
		Cursor c = mDatabase.getReadableDatabase().rawQuery("SELECT * FROM " + DBHelper.TABLE_GROUPS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		if (c.moveToFirst()) {
			String updatedAt = c.getString(c.getColumnIndex(DBHelper.C_UPDATED_AT));
			if (updatedAt.equals(g.getUpdatedAt())) {
				res = COMPARE_EXIST_DONT_UPDATE;
			} else {
				res = COMPARE_EXIST_UPDATE;
			}
		} else {
			res = COMPARE_DOES_NOT_EXIST;
		}

		c.close();
		mDatabase.getReadableDatabase().close();

		return res;
	}

	@Override
	public void load() {
		SQLiteDatabase db = mDatabase.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_GROUPS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		c.moveToFirst();
		name = c.getString(c.getColumnIndex(DBHelper.C_NAME));
		user_id = c.getLong(c.getColumnIndex(DBHelper.C_USER_ID));
		avatar_url = c.getString(c.getColumnIndex(DBHelper.C_AVATAR_URL));
		cover_url = c.getString(c.getColumnIndex(DBHelper.C_COVER_URL));
		updated_at = c.getString(c.getColumnIndex(DBHelper.C_UPDATED_AT));
		nrVideos = c.getInt(c.getColumnIndex(DBHelper.C_NR_VIDEOS));
		String[] user_ids = c.getString(c.getColumnIndex(DBHelper.C_USERS)).split(",");
		users = new ArrayList<Long>();
		for (int i = 0; i < user_ids.length; i++)
			users.add(Long.valueOf(user_ids[i]));

		c.close();
		db.close();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUserId() {
		return user_id;
	}

	public String getUpdatedAt() {
		return updated_at;
	}

	public long getId() {
		return id;
	}

	public ArrayList<Long> getUsers() {
		return users;
	}

	public String getUsersText() {
		String res = "";
		for (int i = 0; i < users.size(); i++) {
			res += users.get(i);
			res += ",";
		}
		res = res.substring(0, res.length() - 1);
		return res;
	}

	public void setUsers(ArrayList<Long> users) {
		this.users = users;
	}

	public String getAvatar() {
		return avatar_url;
	}

	public String getCover() {
		return cover_url;
	}

	public int getNrVideos() {
		return nrVideos;
	}

	public void addMembers(ArrayList<Long> ids) {
		users.addAll(ids);
		String user_ids = "";
		for (int i = 0; i < users.size(); i++) {
			user_ids += users.get(i);
			user_ids += ",";
		}
		user_ids = user_ids.substring(0, user_ids.length() - 1);
		// Log.d(TAG,"added users: "+users.toString());
		update(DBHelper.C_USERS, user_ids);
	}

	public void removeMember(long id) {
		users.remove(id);
		String user_ids = "";
		for (int i = 0; i < users.size(); i++) {
			user_ids += users.get(i);
			user_ids += ",";
		}
		user_ids = user_ids.substring(0, user_ids.length() - 1);
		update(DBHelper.C_USERS, user_ids);
	}

	public int getMaxPlayers(int defaultPlayers) {
		return users.size() < defaultPlayers ? users.size() : defaultPlayers;
	}

	public boolean imInGroup() {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + DBHelper.TABLE_GROUPS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		if (c.moveToFirst()) {
			c.close();
			myDatabase.close();
			return true;
		} else {
			c.close();
			myDatabase.close();
			return false;
		}
	}

	public static Cursor getGroups(WApp app) {
		return app.getDB().getReadableDatabase().rawQuery("SELECT * FROM " + DBHelper.TABLE_GROUPS + " ORDER BY " + DBHelper.C_UPDATED_AT + " DESC", null);
	}

	public void updateName(final WApp app) {
		new AsyncTask<String, Integer, String>() {

			int code;

			@Override
			protected String doInBackground(String... uri) {
				String result = "";
				String endpoint = WApp.BASE + WApp.EDIT_GROUP;
				endpoint = endpoint.replace("__gid__", "" + id);
				MyHttpClient httpclient = new MyHttpClient(app);
				HttpPost post = new HttpPost(endpoint);
				HttpResponse response;
				try {
					MultipartEntity postEntity = new MultipartEntity();
					postEntity.addPart("android_version", new StringBody(WApp.APP_VERSION));
					postEntity.addPart("name", new StringBody(name, Charset.forName("UTF-8")));
					post.setEntity(postEntity);
					post.setHeader("Authorization", "access_token=" + app.getAccessToken());

					response = httpclient.execute(post);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if (entity != null) {
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream);
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return result;
			}

			protected void onPostExecute(String result) {
				WBus.getInstance().post(new GroupUpdateEvent(id));
			}

		}.execute();
	}

	public void updateAvatar(final WApp app) {
		new AsyncTask<String, String, String>() {

			int code;

			protected String doInBackground(String... params) {
				String endpoint = WApp.BASE + WApp.EDIT_GROUP;
				endpoint = endpoint.replace("__gid__", "" + id);
				// Log.d(TAG,"Endpoint: "+endpoint);
				code = 0;
				String result = null;
				MyHttpClient httpclient = new MyHttpClient(app);
				HttpPost post = new HttpPost(endpoint);
				HttpResponse response;
				try {
					File f = new File(WApp.TEMP_GROUP_LOC);
					MultipartEntity postEntity = new MultipartEntity();
					postEntity.addPart("android_version", new StringBody(WApp.APP_VERSION));
					postEntity.addPart("avatar", new FileBody(f, "image/jpeg"));
					post.setHeader("Authorization", "access_token=" + app.getAccessToken());
					post.setEntity(postEntity);

					response = httpclient.execute(post);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if (entity != null) {
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream);
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}

				WLog.d(this, "Result update avatar(" + code + "): <<");
				// WLog.d(this,"Result update avatar("+code+"): "+result);

				if (code == HTTPStatus.OK) {

					try {
						JSONObject group = new JSONObject(result);

						Group g = Group.parse(app, group);
						g.updateAll();

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				return result;
			}

			protected void onPostExecute(String result) {
				// Log.d(TAG,"Result do updateAvatar: "+result);
			};

		}.execute();
	}

	public void updateMembers(final WApp app) {
		new AsyncTask<String, String, String>() {

			int code;

			protected String doInBackground(String... params) {
				String endpoint = WApp.BASE + WApp.EDIT_GROUP;
				endpoint = endpoint.replace("__gid__", "" + id);
				// Log.d(TAG,"Endpoint: "+endpoint);
				code = 0;
				String result = null;
				MyHttpClient httpclient = new MyHttpClient(app);
				HttpPost post = new HttpPost(endpoint);
				HttpResponse response;
				try {
					MultipartEntity postEntity = new MultipartEntity();
					postEntity.addPart("android_version", new StringBody(WApp.APP_VERSION));

					JSONArray jarray = new JSONArray();
					// Log.d(TAG,"Updating with: "+users.toString());
					for (int i = 0; i < users.size(); i++) {
						jarray.put(users.get(i));
					}

					postEntity.addPart("user_ids", new StringBody("" + jarray.toString()));
					post.setEntity(postEntity);
					post.setHeader("Authorization", "access_token=" + app.getAccessToken());

					response = httpclient.execute(post);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if (entity != null) {
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream);
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}

				if (code == HTTPStatus.CREATED) {
					String user_ids = "";
					for (int i = 0; i < users.size(); i++) {
						user_ids += users.get(i);
						user_ids += ",";
					}
					user_ids = user_ids.substring(0, user_ids.length() - 1);
					update(DBHelper.C_USERS, user_ids);
				}

				return result;
			}

			protected void onPostExecute(String result) {
				// Log.d(TAG,"Result do updateMembers("+code+"): "+result);
				if (code == HTTPStatus.CREATED) {
					WBus.getInstance().post(new GroupUpdateEvent(id));
				}
			};

		}.execute();
	}

	public boolean equals(Group g) {

		if (id != g.getId())
			return false;

		if (!updated_at.equals(g.getUpdatedAt()))
			return false;

		return true;
	}

	public static Group parse(WApp app, JSONObject json) throws JSONException {

		ArrayList<Long> users = new ArrayList<Long>();

		for (int i = 0; i < json.getJSONArray("members").length(); i++) {
			users.add(Long.valueOf(json.getJSONArray("members").getJSONObject(i).getString("id")));
		}

		return new Group(app,
				Long.valueOf(json.getString("id")),
				json.getString("name"),
				Long.valueOf(json.getString("user_id")),
				users,
				json.getString("avatar_url"),
				json.getString("cover_url"),
				json.getInt("nrvideos"),
				json.getString("updated_at"));
	}

	public static void bulkInsertGroups(WApp app, ArrayList<Group> groups) {
		String sql = "INSERT INTO " + DBHelper.TABLE_GROUPS + " VALUES(?,?,?,?,?,?,?,?)";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();
		db.delete(DBHelper.TABLE_GROUPS, null, null);
		for (Group g : groups) {
			stmt.clearBindings();
			stmt.bindLong(1, g.getId());
			stmt.bindString(2, g.getName());
			stmt.bindLong(3, g.getUserId());
			stmt.bindString(4, g.getUsersText());
			stmt.bindString(5, g.getAvatar());
			stmt.bindString(6, g.getCover());
			stmt.bindLong(7, g.getNrVideos());
			stmt.bindString(8, g.getUpdatedAt());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public static void bulkUpdateGroups(WApp app, ArrayList<Group> groups) {
		String sql = "UPDATE " + DBHelper.TABLE_GROUPS + " SET "
				+ DBHelper.C_NR_VIDEOS + "=?,"
				+ DBHelper.C_NAME + "=?,"
				+ DBHelper.C_USER_ID + "=?,"
				+ DBHelper.C_UPDATED_AT + "=?,"
				+ DBHelper.C_USERS + "=?,"
				+ DBHelper.C_COVER_URL + "=?,"
				+ DBHelper.C_AVATAR_URL + "=? WHERE " + DBHelper.C_ID + "=?";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();
		for (Group g : groups) {
			stmt.clearBindings();
			stmt.bindLong(1, g.getNrVideos());
			stmt.bindString(2, g.getName());
			stmt.bindLong(3, g.getUserId());
			stmt.bindString(4, g.getUpdatedAt());
			stmt.bindString(5, g.getUsersText());
			stmt.bindString(6, g.getAvatar());
			stmt.bindString(7, g.getCover());
			stmt.bindLong(8, g.getId());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

}
