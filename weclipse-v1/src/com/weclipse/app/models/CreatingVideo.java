package com.weclipse.app.models;

public class CreatingVideo {

	int id;
	String caption;
	int players;
	int target;
	String eta;
	
	public CreatingVideo(int id,String caption,int players,int target,String eta){
		this.id=id;
		this.caption=caption;
		this.players=players;
		this.target=target;
		this.eta=eta;
	}
	
	public int getId(){
		return this.id;
	}
	
	public int getTarget(){
		return this.target;
	}
	
	public int getPlayers(){
		return this.players;
	}
	
	public String getEta(){
		return this.eta;
	}
	
	public String getCaption(){
		return this.caption;
	}
	
	@Override
	public boolean equals(Object o) {
		return ((CreatingVideo)o).getId()==this.getId();
	}
	
}
