package com.weclipse.app.models;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.WLog;

public class PendingVideo implements Storable {

	public static final int CREATE = 1;
	public static final int REPLY = 2;
	
	Context context;
	DBHelper mDatabase;
	long id;
	
	String title;
	String path;
	ArrayList<Long> users;
	ArrayList<Long> groups;
	int type;
	long uid;
	long vid;
	int media_type;
	int target;
	int players;
	String eta;
	long duration;
	
	public PendingVideo(Context context,long id){
		this.context=context;
		mDatabase=new DBHelper(context);
		this.id=id;
	}
	
	public PendingVideo(Context context,long id,String title,String path,ArrayList<Long> users,ArrayList<Long> groups,int type,long vid,long uid,int media_type,int target,int players,String eta,long duration){
		this.context=context;
		mDatabase = new DBHelper(context);
		this.id = id;
		this.title=title;
		this.path=path;
		this.users=users;
		this.groups=groups;
		this.type=type;
		this.media_type=media_type;
		this.target=target;
		this.players=players;
		this.eta=eta;
		this.duration=duration;
		this.vid=vid;
		this.uid=uid;
	}
	
	@Override
	public void create() {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		ContentValues values = new ContentValues();
		if(type==CREATE){
			String ids = "";
			String group_ids= "";
			for(int i=0;i<users.size();i++){
				ids += users.get(i);
				ids += ",";
			}
			
			for(int i=0;i<groups.size();i++){
				group_ids += groups.get(i);
				group_ids += ",";
			}
			
			if(!ids.equals("")){
				ids=ids.substring(0,ids.length()-1);
			}
			
			if(!group_ids.equals("")){
				group_ids.substring(0,group_ids.length()-1);
			}
			
			values.put(DBHelper.C_ID,id);
			values.put(DBHelper.C_TYPE,type);
			values.put(DBHelper.C_VIDEO_ID,0);
			values.put(DBHelper.C_USER_ID,uid);
			values.put(DBHelper.C_MEDIA_TYPE, media_type);
			values.put(DBHelper.C_PATH, path);
			values.put(DBHelper.C_TARGET,target);
			values.put(DBHelper.C_PLAYERS,players);
			values.put(DBHelper.C_USERS,ids);
			values.put(DBHelper.C_GROUPS,group_ids);
			values.put(DBHelper.C_ETA,eta);
			values.put(DBHelper.C_TITLE, title);
			values.put(DBHelper.C_DURATION,duration);
			myDatabase.insert(DBHelper.TABLE_PENDING_VIDEOS, null,values);
		} else {
			values.put(DBHelper.C_ID,id);
			values.put(DBHelper.C_TYPE,type);
			values.put(DBHelper.C_VIDEO_ID,vid);
			values.put(DBHelper.C_USER_ID,uid);
			values.put(DBHelper.C_MEDIA_TYPE, media_type);
			values.put(DBHelper.C_PATH, path);
			values.put(DBHelper.C_TARGET,0);
			values.put(DBHelper.C_PLAYERS,0);
			values.put(DBHelper.C_USERS,"");
			values.put(DBHelper.C_GROUPS,"");
			values.put(DBHelper.C_ETA,0);
			values.put(DBHelper.C_TITLE,"");
			values.put(DBHelper.C_DURATION,duration);
			myDatabase.insert(DBHelper.TABLE_PENDING_VIDEOS, null,values);
		}
		myDatabase.close();
	}

	@Override
	public void update(String key, String value) {
		//No need to update
	}

	@Override
	public void update(String key, int value) {
		//No need to update
	}

	@Override
	public void destroy() {
		WLog.d(this,"Im destroying");
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_PENDING_VIDEOS, DBHelper.C_ID+"="+id, null);
		db.close();
	}

	@Override
	public void load() {
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_PENDING_VIDEOS+" WHERE "+DBHelper.C_ID+"="+id, null);
		c.moveToFirst();
		title = c.getString(c.getColumnIndex(DBHelper.C_TITLE));
		path = c.getString(c.getColumnIndex(DBHelper.C_PATH));
		type = c.getInt(c.getColumnIndex(DBHelper.C_TYPE));
		vid = c.getLong(c.getColumnIndex(DBHelper.C_VIDEO_ID));
		uid = c.getLong(c.getColumnIndex(DBHelper.C_USER_ID));
		media_type=c.getInt(c.getColumnIndex(DBHelper.C_MEDIA_TYPE));
		target = c.getInt(c.getColumnIndex(DBHelper.C_TARGET));
		players = c.getInt(c.getColumnIndex(DBHelper.C_PLAYERS));
		eta = c.getString(c.getColumnIndex(DBHelper.C_ETA));
		duration = c.getLong(c.getColumnIndex(DBHelper.C_DURATION));
		String[] ids = c.getString(c.getColumnIndex(DBHelper.C_USERS)).split(",");
		String[] gids = c.getString(c.getColumnIndex(DBHelper.C_GROUPS)).split(",");
		
		if(type==CREATE){
			users = new ArrayList<Long>();
			groups = new ArrayList<Long>();
			for(int i=0;i<ids.length;i++){
				if(!ids[i].equals("")){
					users.add(Long.valueOf(ids[i]));
				}
			}
			
			for(int i=0;i<gids.length;i++){
				if(!gids[i].equals("")){
					groups.add(Long.valueOf(gids[i]));
				}
			}
		} else {
			users = new ArrayList<Long>();
			groups = new ArrayList<Long>();
		}
		c.close();
		myDatabase.close();
	}
	
	public boolean exists(){
		boolean res;
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_PENDING_VIDEOS+" WHERE "+DBHelper.C_ID+"="+id, null);
		res = c.moveToFirst();
		c.close();
		myDatabase.close();
		return res;
		
	}
	
	public int getType(){
		return type;
	}
	
	public int getMediaType(){
		return media_type;
	}
	
	public long getDuration(){
		return duration;
	}
	
	public String getFilePath(){
		return path;
	}
	
	public long getVideoId(){
		return vid;
	}
	
	public long getUserId(){
		return uid;
	}
	
	public String getTitle(){
		return title;
	}
	
	public int getPlayers(){
		return players;
	}
	
	public String getEta(){
		return eta;
	}
	
	public int getTarget(){
		return target;
	}
	
	public ArrayList<Long> getUsers(){
		return users;
	}
	
	public ArrayList<Long> getGroups(){
		return groups;
	}
	
	public long getId(){
		return id;
	}
	
}
