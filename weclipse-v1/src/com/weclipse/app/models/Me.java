package com.weclipse.app.models;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.client.ClientProtocolException;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.weclipse.app.WApp;
import com.weclipse.app.utils.Utils;

public class Me implements Storable {

	public long id;
	public String email;
	public String phone;
	public String code;
	SharedPreferences myPrefs;
	Context context;
	String username;
	String name;
	String avatar;

	/* New Attributes */
	int n_videos;
	int n_friends;
	int n_following;
	int n_followers;
	int private_user;
	int n_groups;

	private static Me mMe;

	public static Me getInstance(Context context) {
		if (mMe == null) {
			mMe = new Me(context);
			mMe.load();
		}

		return mMe;
	}

	public Me(Context context, String username, String name, String email,String phone, String code, long id, String avatar, int n_videos,int n_friends, int n_followers, int n_following, int n_groups,int private_user) {
		this.context = context;
		myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		this.username = username;
		this.name = name;
		this.email = email;
		this.id = id;
		this.phone = phone;
		this.avatar = avatar;
		this.code = code;

		/* new attributes */
		this.name = name;
		this.n_videos = n_videos;
		this.n_followers = n_followers;
		this.n_friends = n_friends;
		this.n_following = n_following;
		this.private_user = private_user;
		this.n_groups = n_groups;
	}
	
	public Me(Context context) {
		this.context = context;
		myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
	}

	@Override
	public void create() {
		SharedPreferences.Editor editor = myPrefs.edit();
		editor.putString("username", username);
		editor.putString("name", name);
		editor.putString("email", email);
		editor.putLong("id", id);
		editor.putString("phone_number", phone);
		editor.putString("phone_code", code);
		editor.putString("avatar", avatar);
		/* new attributes */
		editor.putInt("n_videos", n_videos);
		editor.putInt("n_friends", n_friends);
		editor.putInt("n_followers", n_followers);
		editor.putInt("n_following", n_following);
		editor.putInt("private_user", private_user);
		editor.putInt("n_groups", n_groups);
		editor.commit();

		mMe = this;
	}
	
	public void save(){
		create();
	}

	@Override
	public void update(String key, String value) {
		SharedPreferences.Editor editor = myPrefs.edit();
		editor.putString(key, value);
		editor.commit();
	}

	@Override
	public void update(String key, int value) {
		SharedPreferences.Editor editor = myPrefs.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	@Override
	public void destroy() {
		// do not forget to also clear the avatar
		SharedPreferences.Editor editor = myPrefs.edit();
		editor.remove("username");
		editor.remove("name");
		editor.remove("id");
		editor.remove("email");
		editor.remove("avatar");
		editor.remove("phone_number");
		editor.remove("phone_code");
		editor.remove("addable");
		editor.remove("private_user");
		editor.remove("n_videos");
		editor.remove("n_friends");
		editor.remove("n_following");
		editor.remove("n_followers");
		editor.remove("n_groups");
		editor.commit();
	}

	@Override
	public void load() {
		this.id = myPrefs.getLong("id", 0);
		this.username = myPrefs.getString("username", "no_username_available");
		this.name = myPrefs.getString("name", "no_name_available");
		this.email = myPrefs.getString("email", "no_email_available");
		this.phone = myPrefs.getString("phone", "no_phone_available");
		this.code = myPrefs.getString("code", "no_code_available");
		this.avatar = myPrefs.getString("avatar", "no_avatar_available");

		this.n_friends = myPrefs.getInt("n_friends", 0);
		this.n_following = myPrefs.getInt("n_following", 0);
		this.n_followers = myPrefs.getInt("n_followers", 0);
		this.n_videos = myPrefs.getInt("n_videos", 0);
		this.private_user = myPrefs.getInt("private_user", 0);
		this.n_groups = myPrefs.getInt("n_groups", 0);
	}

	public String getEmail() {
		return email;
	}

	public long getId() {
		return id;
	}

	public String getPhone() {
		return phone;
	}

	/**
	 * @return the n_videos
	 */
	public int getN_videos() {
		return n_videos;
	}

	/**
	 * @return the n_friends
	 */
	public int getN_friends() {
		return n_friends;
	}

	/**
	 * @return the n_following
	 */
	public int getN_following() {
		return n_following;
	}

	/**
	 * @return the n_followers
	 */
	public int getN_followers() {
		return n_followers;
	}

	/**
	 * @return the private_user
	 */
	public int getPrivate_user() {
		return private_user;
	}

	/**
	 * @return the n_groups
	 */
	public int getN_groups() {
		return n_groups;
	}

	/**
	 * @param n_videos
	 *            the n_videos to set
	 */
	public void setN_videos(int n_videos) {
		this.n_videos = n_videos;
	}

	/**
	 * @param n_friends
	 *            the n_friends to set
	 */
	public void setN_friends(int n_friends) {
		this.n_friends = n_friends;
	}

	/**
	 * @param n_following
	 *            the n_following to set
	 */
	public void setN_following(int n_following) {
		this.n_following = n_following;
	}

	/**
	 * @param n_followers
	 *            the n_followers to set
	 */
	public void setN_followers(int n_followers) {
		this.n_followers = n_followers;
	}

	/**
	 * @param private_user
	 *            the private_user to set
	 */
	public void setPrivate_user(int private_user) {
		this.private_user = private_user;
	}

	/**
	 * @param n_groups
	 *            the n_groups to set
	 */
	public void setN_groups(int n_groups) {
		this.n_groups = n_groups;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public String getMinifiedName(){
		return Utils.getMinifiedName(username);
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public User getUser(){
		return new User(context, id, username, name, avatar, 0,private_user==User.PRIVATE);
	}

}
