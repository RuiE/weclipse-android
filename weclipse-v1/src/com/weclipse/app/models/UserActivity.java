package com.weclipse.app.models;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.fetch.FetchTimelineTask;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.WLog;

public class UserActivity implements Storable {

	// video activity types
	public static final int CHALLENGED = 1;
	public static final int CHALLENGED_GROUP = 6;
	public static final int REPLIED = 2;
	public static final int VIDEO_READY = 3;
	public static final int VIDEO_EXPIRED = 12;
	public static final int LIKED = 4;
	public static final int COMMENTED = 5;
	// users activity types
	public static final int REQUEST_FOLLOW = 7;
	public static final int B_FOLLOWS = 8;
	public static final int FRIENDED = 9;
	public static final int GROUP_ADDED = 10;
	public static final int ACCEPT_FOLLOW = 11;

	// user challenge
	public static final int CHALLENGE = 12;
	public static final int CHALLENGE_GROUP = 13;

	public static final int JOINED_WECLIPSE_FB = 14;
	public static final int JOINED_WECLIPSE_CONTACT = 15;

	int type;
	long id;

	DBHelper mDatabase;
	Context context;

	String updated_at;

	/* Metadata */
	long from_id;
	String from_name;
	String from_username;
	String from_avatar_url;
	long video_id;
	String video_name;
	long group_id;
	int private_video;
	String group_name;
	String thumbnail_url;
	String video_expires_at;
	int priority;

	public UserActivity(Context context, long id, int type, String updated_at,
			long from_id, String from_username, String from_name,String from_avatar_url,
			long video_id, String video_name, long group_id, String group_name,
			String thumbnail_url,String video_expires_at,int private_video,int priority) {
		this.context = context;
		mDatabase = new DBHelper(context);

		this.id = id;
		this.type = type;

		this.updated_at = updated_at;

		this.from_id = from_id;
		this.from_name = from_name;
		this.from_username = from_username;
		this.from_avatar_url = from_avatar_url;
		this.video_id = video_id;
		this.video_name = video_name;
		this.group_id = group_id;
		this.group_name = group_name;
		this.thumbnail_url = thumbnail_url;
		this.video_expires_at = video_expires_at;
		this.private_video = private_video;
		this.priority = priority;
	}

	@Override
	public void create() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_ACTIVITY
				+ " WHERE " + DBHelper.C_ID + "=" + id, null);
		if (c.moveToFirst()) {
			c.close();
			db.close();
		} else {
			c.close();
			try {
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID, id);
				values.put(DBHelper.C_TYPE, type);
				values.put(DBHelper.C_UPDATED_AT, updated_at);
				/* Metadata */
				values.put(DBHelper.C_USER_ID, from_id);
				values.put(DBHelper.C_USERNAME, from_username);
				values.put(DBHelper.C_AVATAR_URL,from_avatar_url);
				values.put(DBHelper.C_NAME, from_name);
				values.put(DBHelper.C_VIDEO_ID, video_id);
				values.put(DBHelper.C_TITLE, video_name);
				values.put(DBHelper.C_THUMBNAIL_URL, thumbnail_url);
				values.put(DBHelper.C_EXPIRES_AT, video_expires_at);
				values.put(DBHelper.C_PRIORITY, priority);
				db.insert(DBHelper.TABLE_ACTIVITY, null, values);
				db.close();
			} catch (SQLException e) {

			}
		}
	}

	@Override
	public void update(String key, String value) {

	}

	@Override
	public void update(String key, int value) {

	}

	@Override
	public void destroy() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_ACTIVITY, DBHelper.C_ID + "=" + id, null);
		db.close();
	}

	/**
	 * @return the group_id
	 */
	public long getGroup_id() {
		return group_id;
	}

	/**
	 * @param group_id
	 *            the group_id to set
	 */
	public void setGroup_id(long group_id) {
		this.group_id = group_id;
	}

	/**
	 * @return the group_name
	 */
	public String getGroup_name() {
		return group_name;
	}

	/**
	 * @param group_name
	 *            the group_name to set
	 */
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	
	public String getVideoExpiresAt(){
		return this.video_expires_at;
	}

	@Override
	public void load() {

	}

	public static Cursor getActivity(WApp app) {
		Cursor c = app
				.getDB()
				.getReadableDatabase()
				.rawQuery("SELECT * FROM " + DBHelper.TABLE_ACTIVITY + " ORDER BY "+DBHelper.C_PRIORITY+" DESC," + DBHelper.C_UPDATED_AT + " DESC", null);
		// WLog.d(TimelineVideo.class,"---------- Returning Cursor -----------");
		// for(int i = 0 ; i < c.getColumnCount() ; i++ ){
		// WLog.d(TimelineVideo.class,"Coluna n"+i+": "+c.getColumnNames()[i]);
		// }
		return c;
	}

	public static UserActivity parse(WApp app, JSONObject activity)
			throws JSONException {

		// ArrayList<Video> videos = new ArrayList<Video>();

		int type = activity.getInt("type");

		JSONObject metadata = activity.getJSONObject("metadata");

		long from_id = 0;
		String from_name = "";
		String from_avatar_url = "";
		long video_id = 0;
		String video_name = "";
		long group_id = 0;
		int private_video = 0;
		String group_name = "";
		String thumbnail_url = "";
		String from_username = "";
		String video_expires_at = "";

		switch (type) {
		case UserActivity.CHALLENGED:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			from_avatar_url = metadata.getString("from_avatar_url");
			video_id = Long.valueOf(metadata.getString("video_id"));
			video_name = metadata.getString("video_title");
			video_expires_at = metadata.getString("video_expires_at");
			private_video = metadata.getInt("video_private");
			break;
		case UserActivity.REPLIED:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			from_avatar_url = metadata.getString("from_avatar_url");
			video_id = Long.valueOf(metadata.getString("video_id"));
			video_name = metadata.getString("video_title");
			break;
		case UserActivity.VIDEO_READY:
		case UserActivity.LIKED:
		case UserActivity.COMMENTED:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			from_avatar_url = metadata.getString("from_avatar_url");
			video_id = Long.valueOf(metadata.getString("video_id"));
			video_name = metadata.getString("video_title");
			thumbnail_url = metadata.getString("video_thumbnail");
			break;
		case UserActivity.CHALLENGED_GROUP:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			video_id = Long.valueOf(metadata.getString("video_id"));
			video_name = metadata.getString("video_title");
			group_id = Long.valueOf(metadata.getString("group_id"));
			group_name = metadata.getString("group_name");
			video_expires_at = metadata.getString("video_expires_at");
			private_video = metadata.getInt("video_private");
			break;
		case UserActivity.CHALLENGE:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			video_id = Long.valueOf(metadata.getString("video_id"));
			video_name = metadata.getString("video_title");
			break;
		case UserActivity.CHALLENGE_GROUP:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			video_id = Long.valueOf(metadata.getString("video_id"));
			video_name = metadata.getString("video_title");
			group_id = Long.valueOf(metadata.getString("group_id"));
			group_name = metadata.getString("group_name");
			break;
		/* User Messages */
		case UserActivity.REQUEST_FOLLOW:
			from_username = metadata.getString("from_username");
			from_avatar_url = metadata.getString("from_avatar_url");
			from_id = Long.valueOf(metadata.getString("from_id"));
			break;
		case UserActivity.ACCEPT_FOLLOW:
			break;
		case UserActivity.B_FOLLOWS:
			from_username = metadata.getString("from_username");
			from_avatar_url = metadata.getString("from_avatar_url");
			from_id = Long.valueOf(metadata.getString("from_id"));
			break;
		case UserActivity.FRIENDED:
			from_username = metadata.getString("from_username");
			from_avatar_url = metadata.getString("from_avatar_url");
			from_id = Long.valueOf(metadata.getString("from_id"));
			break;
		case UserActivity.GROUP_ADDED:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			from_avatar_url = metadata.getString("from_avatar_url");
			group_id = Long.valueOf(metadata.getString("group_id"));
			group_name = metadata.getString("group_name");
			break;
		case UserActivity.JOINED_WECLIPSE_CONTACT:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			from_name = metadata.getString("from_name");
			from_avatar_url = metadata.getString("from_avatar_url");
			break;
		case UserActivity.JOINED_WECLIPSE_FB:
			from_id = Long.valueOf(metadata.getString("from_id"));
			from_username = metadata.getString("from_username");
			from_name = metadata.getString("from_name");
			from_avatar_url = metadata.getString("from_avatar_url");
			break;
		}

		return new UserActivity(app, Long.valueOf(activity.getString("id")),
				type, activity.getString("updated_at"), from_id, from_username,
				from_name, from_avatar_url,video_id, video_name, group_id, group_name,
				thumbnail_url,video_expires_at,private_video,activity.getInt("priority"));
	}

	public static void bulkInsert(WApp app, ArrayList<UserActivity> activity,
			int method) {
		String sql = "INSERT INTO " + DBHelper.TABLE_ACTIVITY
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();

		if (method == FetchTimelineTask.FRESH) {
			db.delete(DBHelper.TABLE_ACTIVITY, null, null);
		} else {
			String ids = "";
			int i = 0;
			for (UserActivity a : activity) {
				ids += a.getId() + ",";
				i++;
			}

			if (i > 0)
				ids = ids.substring(0, ids.length() - 1);

			db.delete(DBHelper.TABLE_ACTIVITY, DBHelper.C_ID + " IN (" + ids
					+ ")", null);
		}

		UserActivity a;

		for (int i = 0; i < activity.size(); i++) {
			a = activity.get(i);
			stmt.clearBindings();
			stmt.bindLong(1, a.getId());
			stmt.bindLong(2, a.getType());
			stmt.bindString(3, a.getUpdated_at());
			stmt.bindLong(4, a.getFrom_id());
			stmt.bindString(5, a.getFrom_username());
			stmt.bindString(6, a.getFrom_name());
			stmt.bindString(7, a.getFrom_avatar_url());
			stmt.bindLong(8, a.getVideo_id());
			stmt.bindString(9, a.getVideo_name());
			stmt.bindLong(10, a.getGroup_id());
			stmt.bindString(11, a.getGroup_name());
			stmt.bindString(12, a.getThumbnail_url());
			stmt.bindString(13, a.getVideoExpiresAt());
			stmt.bindLong(14, a.getPrivate_video());
			stmt.bindLong(15,a.getPriority());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public static long getLastId(WApp app) {
		Cursor c = app
				.getDB()
				.getReadableDatabase()
				.rawQuery(
						"SELECT * FROM " + DBHelper.TABLE_ACTIVITY
								+ " ORDER BY " + DBHelper.C_UPDATED_AT + " ASC",
						null);
		c.moveToFirst();
		return c.getLong(c.getColumnIndex(DBHelper.C_ID));
	}
	
	public int getPriority(){
		return priority;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the updated_at
	 */
	public String getUpdated_at() {
		return updated_at;
	}

	/**
	 * @param updated_at
	 *            the updated_at to set
	 */
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	/**
	 * @return the from_id
	 */
	public long getFrom_id() {
		return from_id;
	}

	/**
	 * @param from_id
	 *            the from_id to set
	 */
	public void setFrom_id(long from_id) {
		this.from_id = from_id;
	}

	/**
	 * @return the from_name
	 */
	public String getFrom_name() {
		return from_name;
	}

	/**
	 * @return the from_username
	 */
	public String getFrom_username() {
		return from_username;
	}
	
	public String getFrom_avatar_url(){
		return from_avatar_url;
	}
	
	public int getPrivate_video(){
		return private_video;
	}

	/**
	 * @param from_name
	 *            the from_name to set
	 */
	public void setFrom_name(String from_name) {
		this.from_name = from_name;
	}

	/**
	 * @return the video_id
	 */
	public long getVideo_id() {
		return video_id;
	}

	/**
	 * @param video_id
	 *            the video_id to set
	 */
	public void setVideo_id(long video_id) {
		this.video_id = video_id;
	}

	/**
	 * @return the video_name
	 */
	public String getVideo_name() {
		return video_name;
	}

	/**
	 * @param video_name
	 *            the video_name to set
	 */
	public void setVideo_name(String video_name) {
		this.video_name = video_name;
	}

	/**
	 * @return the thumbnail_url
	 */
	public String getThumbnail_url() {
		if (thumbnail_url == null)
			return "null";

		return thumbnail_url;
	}

	/**
	 * @param thumbnail_url
	 *            the thumbnail_url to set
	 */
	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public String toString() {
		switch (type) {
		case CHALLENGE:
			return from_name + " fez-t um desafio";
		case CHALLENGE_GROUP:
			return "Grupo " + group_name + " desafiou.t";
		case REQUEST_FOLLOW:
			return from_name + " quer ser teu amigo!";
		case B_FOLLOWS:
			return from_name + " está te a seguir!";
		}

		return "";
	}

}
