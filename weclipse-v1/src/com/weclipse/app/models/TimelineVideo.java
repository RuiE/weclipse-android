package com.weclipse.app.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.fetch.FetchTimelineTask;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;

public class TimelineVideo extends AbstractVideo implements Storable {

	String ready_at;

	String thumbnail_url;
	String share_url;

	int editors_choice;
	long reweclipsed_id;

	public static final int TIMELINE = 1;
	public static final int TIMELINE_MY_VIDEOS = 2;

	public TimelineVideo(Context context, long id) {
		this.context = context;
		mDatabase = new DBHelper(context);

		this.id = id;
	}

	public TimelineVideo(Context context, long id, String title, long owner_id, long group_id, int video_status, String ready_at, String updated_at, String created_at, ArrayList<Long> users, ArrayList<String> replied_ats, int n_likes, int n_views, int n_comments, int private_video, int editors_choice, String thumbnail_url, String share_url,long reweclipsed_id) {
		this.context = context;
		mDatabase = new DBHelper(context);

		this.id = id;
		this.owner_id = owner_id;
		this.group_id = group_id;
		this.video_status = video_status;
		this.title = title;
		this.ready_at = ready_at;
		this.updated_at = updated_at;
		this.created_at = created_at;

		this.users = users;
		this.replied_ats = replied_ats;

		this.n_likes = n_likes;
		this.n_views = n_views;
		this.n_comments = n_comments;

		this.private_video = private_video;
		this.editors_choice = editors_choice;
		this.reweclipsed_id = reweclipsed_id;
		this.thumbnail_url = thumbnail_url;
		this.share_url = share_url;
	}

	@Override
	public void create() {
		Cursor c = null;
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		c = myDatabase.rawQuery("SELECT " + DBHelper.C_ID + " FROM " + DBHelper.TABLE_TIMELINE + " WHERE " + DBHelper.C_ID + "=" + id, null);
		if (c.moveToFirst()) {
			updateAll();
		} else {
			String user_ids = "";
			String replieds = "";
			for (int i = 0; i < users.size(); i++) {
				user_ids += users.get(i) + ",";
				replieds += replied_ats.get(i) + ",";
			}
			user_ids = user_ids.substring(0, user_ids.length() - 1);
			replieds = replieds.substring(0, replieds.length() - 1);
			try {
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID, id);
				values.put(DBHelper.C_USER_ID, owner_id);
				values.put(DBHelper.C_GROUP_ID, group_id);
				values.put(DBHelper.C_VIDEO_STATUS, video_status);
				values.put(DBHelper.C_TITLE, title);
				values.put(DBHelper.C_CREATED_AT, created_at);
				values.put(DBHelper.C_UPDATED_AT, updated_at);
				values.put(DBHelper.C_EXPIRES_AT, ready_at);

				values.put(DBHelper.C_USERS, user_ids);
				values.put(DBHelper.C_REPLIED_AT, replieds);

				values.put(DBHelper.C_N_LIKES, n_likes);
				values.put(DBHelper.C_N_VIEWS, n_views);
				values.put(DBHelper.C_N_COMMENTS, n_comments);

				values.put(DBHelper.C_PRIVATE_VIDEO, private_video);
				values.put(DBHelper.C_EDITORS_CHOICE, editors_choice);
				values.put(DBHelper.C_REWECLIPSED_BY,reweclipsed_id);
				values.put(DBHelper.C_THUMBNAIL_URL, thumbnail_url);

				myDatabase.insert(DBHelper.TABLE_TIMELINE, null, values);
				myDatabase.close();
			} catch (SQLException e) {
				// � existe?
			}
		}
		c.close();
		myDatabase.close();
	}

	@Override
	public void update(String key, String value) {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.execSQL("UPDATE " + DBHelper.TABLE_TIMELINE + " SET " + key + "='" + value + "' WHERE " + DBHelper.C_ID + "=" + id);
		myDatabase.close();
	}

	@Override
	public void update(String key, int value) {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.execSQL("UPDATE " + DBHelper.TABLE_TIMELINE + " SET " + key + "=" + value + " WHERE " + DBHelper.C_ID + "=" + id);
		myDatabase.close();
	}

	@Override
	public void destroy() {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.delete(DBHelper.TABLE_TIMELINE, DBHelper.C_ID + "=" + id, null);
		myDatabase.close();
	}

	@Override
	public void load() {
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM " + DBHelper.TABLE_TIMELINE + " WHERE " + DBHelper.C_ID + "=" + id + " UNION SELECT * FROM " + DBHelper.TABLE_MY_VIDEOS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		if (c.moveToFirst()) {
			title = c.getString(c.getColumnIndex(DBHelper.C_TITLE));

			owner_id = c.getLong(c.getColumnIndex(DBHelper.C_USER_ID));
			group_id = c.getLong(c.getColumnIndex(DBHelper.C_GROUP_ID));

			video_status = c.getInt(c.getColumnIndex(DBHelper.C_VIDEO_STATUS));

			updated_at = c.getString(c.getColumnIndex(DBHelper.C_UPDATED_AT));
			created_at = c.getString(c.getColumnIndex(DBHelper.C_CREATED_AT));
			ready_at = c.getString(c.getColumnIndex(DBHelper.C_READY_AT));
			video_status = c.getInt(c.getColumnIndex(DBHelper.C_VIDEO_STATUS));

			n_likes = c.getInt(c.getColumnIndex(DBHelper.C_N_LIKES));
			n_views = c.getInt(c.getColumnIndex(DBHelper.C_N_VIEWS));
			n_comments = c.getInt(c.getColumnIndex(DBHelper.C_N_COMMENTS));

			private_video = c.getInt(c.getColumnIndex(DBHelper.C_PRIVATE_VIDEO));
			editors_choice = c.getInt(c.getColumnIndex(DBHelper.C_EDITORS_CHOICE));

			thumbnail_url = c.getString(c.getColumnIndex(DBHelper.C_THUMBNAIL_URL));
			share_url = c.getString(c.getColumnIndex(DBHelper.C_SHARE_URL));

			String[] user_ids = c.getString(c.getColumnIndex(DBHelper.C_USERS)).split(",");
			String[] replieds = c.getString(c.getColumnIndex(DBHelper.C_REPLIED_AT)).split(",");

			users = new ArrayList<Long>();
			replied_ats = new ArrayList<String>();

			for (int i = 0; i < user_ids.length; i++) {
				users.add(Long.valueOf(user_ids[i]));
				replied_ats.add(replieds[i]);
			}
		}
		c.close();
		myDatabase.close();
	}

	public void updateAll() {

	}

	public static Cursor getMyVideos(WApp app) {
		Cursor c;
		SQLiteDatabase myDatabase = app.getDB().getReadableDatabase();
		c = myDatabase.rawQuery("SELECT " + DBHelper.TABLE_MY_VIDEOS + ".*," + DBHelper.TABLE_LIKES_MY_VIDEOS + "." + DBHelper.C_ID + " as like FROM " + DBHelper.TABLE_MY_VIDEOS + " LEFT OUTER JOIN " + DBHelper.TABLE_LIKES_MY_VIDEOS
				+ " ON " + DBHelper.TABLE_MY_VIDEOS + "." + DBHelper.C_ID + "=" + DBHelper.TABLE_LIKES_MY_VIDEOS + "." + DBHelper.C_ID
				+ " ORDER BY " + DBHelper.C_READY_AT + " DESC," + DBHelper.C_ID + " DESC", null);

		return c;
	}

	public static Cursor getTimeline(WApp app) {
		Cursor c;
		SQLiteDatabase myDatabase = app.getDB().getReadableDatabase();
		c = myDatabase.rawQuery("SELECT " + DBHelper.TABLE_TIMELINE + ".*," + DBHelper.TABLE_LIKES_TIMELINE + "." + DBHelper.C_ID + " as like FROM " + DBHelper.TABLE_TIMELINE + " LEFT OUTER JOIN " + DBHelper.TABLE_LIKES_TIMELINE
				+ " ON " + DBHelper.TABLE_TIMELINE + "." + DBHelper.C_ID + "=" + DBHelper.TABLE_LIKES_TIMELINE + "." + DBHelper.C_ID
				+ " ORDER BY " + DBHelper.C_READY_AT + " DESC," + DBHelper.C_ID + " DESC", null);

		return c;
	}

	public static void destroy(WApp app, long id) {
		SQLiteDatabase myDatabase = app.getDB().getWritableDatabase();
		myDatabase.delete(DBHelper.TABLE_MY_VIDEOS, DBHelper.C_ID + "=" + id, null);
	}

	public static TimelineVideo parse(WApp app, JSONObject json) throws JSONException {

		ArrayList<Long> users = new ArrayList<Long>();
		ArrayList<String> replied_ats = new ArrayList<String>();

		JSONArray playersList = json.getJSONArray("playerslist");

		JSONObject juser;

		for (int i = 0; i < playersList.length(); i++) {
			juser = playersList.getJSONObject(i);
			users.add(Long.valueOf(juser.getString("id")));
			replied_ats.add(juser.getString("replied_at"));
			UserCache.getInstance(app).updateUser(User.parse(app, juser));
		}

		int editors_choice = 0;
		long reweclipsed_id = 0;

		String ready_at = json.getString("ready_at");

		if (json.has("reweclipsed_by")) {
			editors_choice = Video.EDITORS_CHOICE;
			ready_at = json.getJSONObject("reweclipsed_by").getString("reweclipsed_at");
			reweclipsed_id = Long.valueOf(json.getJSONObject("reweclipsed_by").getString("id"));
		}

		return new TimelineVideo(app,
				Long.valueOf(json.getString("id")),
				json.getString("title"),
				Long.valueOf(json.getString("user_id")),
				Long.valueOf(json.getString("group_id")),
				Video.CONFIRMED,
				ready_at,
				json.getString("updated_at"),
				json.getString("created_at"),
				users,
				replied_ats,
				json.getInt("nr_likes"),
				json.getInt("nr_views"),
				json.getInt("nr_comments"),
				json.getInt("private"),
				editors_choice,
				json.getString("thumbnail"),
				json.getString("share_url"),
				reweclipsed_id);
	}

	public static String getLastId(WApp app) {
		String res = "";

//		dump(app);

		SQLiteDatabase mDatabase = app.getDB().getReadableDatabase();
		Cursor c = mDatabase.rawQuery("SELECT " + DBHelper.C_ID + " FROM " + DBHelper.TABLE_TIMELINE + " ORDER BY " + DBHelper.C_READY_AT + "," + DBHelper.C_ID + " LIMIT 1", null);

		if (c.moveToFirst())
			res = "" + c.getLong(c.getColumnIndex(DBHelper.C_ID));

		c.close();
		return res;
	}

	public static String getLastIdMyVideos(WApp app) {
		String res = "";
		SQLiteDatabase mDatabase = app.getDB().getReadableDatabase();
		Cursor c = mDatabase.rawQuery("SELECT " + DBHelper.C_ID + " FROM " + DBHelper.TABLE_MY_VIDEOS + " ORDER BY " + DBHelper.C_ID + " LIMIT 1", null);

		if (c.moveToFirst())
			res = "" + c.getLong(c.getColumnIndex(DBHelper.C_ID));

		c.close();
		return res;
	}

	public void bulkInsert(WApp app, ArrayList<TimelineVideo> timelines, int target) {
		bulkInsert(app, timelines, 0, target);
	}

	public static ArrayList<Long> getIds(WApp app) {
		ArrayList<Long> res = new ArrayList<Long>();

		Cursor vids = app.getDB().getReadableDatabase().rawQuery("SELECT " + DBHelper.C_ID + " FROM " + DBHelper.TABLE_TIMELINE, null);
		while (vids.moveToNext()) {
			res.add(vids.getLong(vids.getColumnIndex(DBHelper.C_ID)));
		}

		return res;
	}

	public static void dump(WApp app) {
		SQLiteDatabase db = app.getDB().getReadableDatabase();

		int i = 0;
		Cursor c = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_TIMELINE + " ORDER BY _ID", null);
		if (WApp.DEBUG) {
			Log.d("TimelineVideo", "---Dumping Timeline Videos---");
		}
		while (c.moveToNext()) {
			i++;
			WLog.d(TimelineVideo.class, i + " - TimelineVideo: " + c.getLong(c.getColumnIndex(DBHelper.C_ID)) + " title: " + c.getString(c.getColumnIndex(DBHelper.C_TITLE)));
		}

		db.close();
	}

	public static void bulkInsert(WApp app, ArrayList<TimelineVideo> timelines, int method, int target) {

		String table = "";

		if (target == TIMELINE) {
			table = DBHelper.TABLE_TIMELINE;
		} else {
			table = DBHelper.TABLE_MY_VIDEOS;
		}

		String sql = "INSERT INTO " + table + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();

		if (method == FetchTimelineTask.FRESH) {
			db.delete(table, null, null);
		} else if (method == FetchTimelineTask.LOAD_MORE) {
			String ids = "";
			int i = 0;
			for (TimelineVideo v : timelines) {
				ids += v.getId() + ",";
				i++;
			}

			if (i > 0)
				ids = ids.substring(0, ids.length() - 1);

			db.delete(table, DBHelper.C_ID + " IN (" + ids + ")", null);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.beginTransaction();

		TimelineVideo v;

		for (int i = 0; i < timelines.size(); i++) {
			v = timelines.get(i);
			stmt.clearBindings();
			stmt.bindLong(1, v.getId());
			stmt.bindString(2, v.getTitle());
			stmt.bindLong(3, v.getOwner_id());
			stmt.bindLong(4, v.getGroup_id());
			stmt.bindString(5, v.getUsersText());
			stmt.bindString(6, v.getRepliedAtsText());
			stmt.bindLong(7, v.getVideo_status());
			stmt.bindString(8, v.getCreated_at());
			stmt.bindString(9, v.getUpdated_at());
			stmt.bindString(10, v.getReady_at());
			stmt.bindLong(11, v.getN_likes());
			stmt.bindLong(12, v.getN_views());
			stmt.bindLong(13, v.getN_comments());
			stmt.bindLong(14, v.getPrivate_video());
			stmt.bindLong(15, v.getEditors_choice());
			stmt.bindString(16, v.getThumbnail_url());
			stmt.bindString(17, v.getShare_url());
			stmt.bindLong(18,v.getReweclipsedBy());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public int getEditors_choice() {
		return editors_choice;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the ready_at
	 */
	public String getReady_at() {
		return ready_at;
	}

	/**
	 * @param ready_at
	 *            the ready_at to set
	 */
	public void setReady_at(String ready_at) {
		this.ready_at = ready_at;
	}

	@Override
	public boolean isTimelineVideo() {
		return true;
	}

	@Override
	public boolean isMyVideo() {
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		return myDatabase.rawQuery("SELECT " + DBHelper.C_ID + " FROM " + DBHelper.TABLE_MY_VIDEOS, null).moveToFirst();
	}

	/**
	 * @return the thumbnail_url
	 */
	public String getThumbnail_url() {
		return thumbnail_url;
	}

	/**
	 * 
	 * @return the share_url
	 */
	public String getShare_url() {
		return share_url;
	}

	/**
	 * @param thumbnail_url
	 *            the thumbnail_url to set
	 */
	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}
	
	public long getReweclipsedBy(){
		return reweclipsed_id;
	}

}
