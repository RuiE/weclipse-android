package com.weclipse.app.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.weclipse.app.WApp;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.WLog;

public class Video extends AbstractVideo implements Storable {

	/* Privacy */
	public static final int PUBLIC = 0;
	public static final int PRIVATE = 1;
	/* Editors */
	public static final int EDITORS_CHOICE = 2;
	/* Active states */
	public static final int ACTIVE = 1;
	public static final int INVITED = 0;
	public static final int REPLIED = 1;
	public static final int READY = 2;
	public static final int DOWNLOADED = 4;
	/* Inactive states */
	public static final int EXPIRED = 8;
	public static final int VIEWED = 9;
	public static final int DELETED = 10;
	/* Pending State */ 
	public static final int REPLY_FAILED = 2;
	/* Confirmed */
	public static final int CONFIRMED = 0;
	public static final int READY_TO_DOWNLOAD = 1;
	public static final int DOWNLOAD_FAIL = 2;
	
	public static final int PRINTSCREEN = 1;
	
	public static final int EXPIRED_DEFAULT = 5;
	
	protected static final String TAG = "VideoModel";
	
	String eta;
	int players;
	int target,replied,status,user_status;
	ArrayList<Integer> statuses;
	String expires_at;
	String thumbnail;
	String share_url;
	
	public Video(Context context,long id,String title,int players,String eta,long user_id,long group_id,int target,int replied,int status,int user_status,ArrayList<Long> users,ArrayList<Integer> statuses,ArrayList<String> replied_ats,String updated_at, String created_at,String expires_at,int private_video,int n_likes,int n_views,int n_comments,String thumbnail,String share_url){
		mDatabase = new DBHelper(context);
		this.id = id;
		this.title = title;
		this.players = players;
		this.eta = eta;
		this.owner_id = user_id;
		this.target = target;
		this.replied = replied;
		this.status = status;
		this.user_status = user_status;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.users = users;
		this.statuses = statuses;
		this.replied_ats = replied_ats;
		this.group_id = group_id;
		this.expires_at = expires_at;
		this.video_status = CONFIRMED;
		this.private_video = private_video;
		this.n_likes = n_likes;
		this.n_views = n_views;
		this.n_comments = n_comments;
		this.thumbnail = thumbnail;
		this.share_url = share_url;
	}
	
	public Video(Context context,long id){
		mDatabase = new DBHelper(context);
		this.id = id;
	}

	@Override
	public void create() {
		Cursor c = null;
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		c = myDatabase.rawQuery("SELECT "+DBHelper.C_ID+" FROM "+DBHelper.TABLE_VIDEOS+" WHERE "+DBHelper.C_ID+"="+id, null);
		if(c.moveToFirst()){
			updateAll();
		} else {
			String user_ids = "";
			String users_status = "";
			String replied_temp = "";
			for(int i = 0 ; i < users.size();i++){
				user_ids += users.get(i)+",";
				users_status += statuses.get(i)+",";
				replied_temp += replied_ats.get(i)+",";
			}
			user_ids = user_ids.substring(0, user_ids.length()-1);
			users_status = users_status.substring(0,users_status.length()-1);
			replied_temp = replied_temp.substring(0,replied_temp.length()-1);
			try {
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID, id);
				values.put(DBHelper.C_TITLE, title);
				values.put(DBHelper.C_PLAYERS,players);
				values.put(DBHelper.C_ETA, eta);
				values.put(DBHelper.C_USER_ID,owner_id);
				values.put(DBHelper.C_GROUP_ID, group_id);
				values.put(DBHelper.C_TARGET,target);
				values.put(DBHelper.C_REPLIED, replied);
				values.put(DBHelper.C_STATUS,status);
				values.put(DBHelper.C_USER_STATUS, user_status);
				values.put(DBHelper.C_USERS,user_ids);
				values.put(DBHelper.C_REPLIED_AT,replied_temp);
				values.put(DBHelper.C_CREATED_AT,created_at);
				values.put(DBHelper.C_UPDATED_AT,updated_at);
				values.put(DBHelper.C_STATUSES,users_status);
				values.put(DBHelper.C_EXPIRES_AT, expires_at);
				values.put(DBHelper.C_PRIVATE_VIDEO,private_video);
				
				values.put(DBHelper.C_N_LIKES,n_likes);
				values.put(DBHelper.C_N_VIEWS,n_views);
				values.put(DBHelper.C_N_COMMENTS,n_comments);
				values.put(DBHelper.C_THUMBNAIL_URL,thumbnail);
				myDatabase.insert(DBHelper.TABLE_VIDEOS, null,values);
				myDatabase.close();
			} catch (SQLException e) {
				//J?? existe? prolly not
			}
		}
		c.close();
		myDatabase.close();
	}
	
	@Override
	public void update(String key, String value) {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.execSQL("UPDATE "+DBHelper.TABLE_VIDEOS+" SET "+key+"='"+value+"' WHERE "+DBHelper.C_ID+"="+id);
		myDatabase.close();
	}
	
	@Override
	public void update(String key, int value) {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.execSQL("UPDATE "+DBHelper.TABLE_VIDEOS+" SET "+key+"="+value+" WHERE "+DBHelper.C_ID+"="+id);
		myDatabase.close();
	}
	
	public boolean replied(long uid){
		for(int i = 0 ; i < users.size() ; i++){
			if(users.get(i) == uid){
				return !replied_ats.get(i).equals("null");  
			}
		}
		return false;
	}
	
	public void updateAll(){
		String user_ids = "";
		String users_status = "";
		String replied_temp = "";
		for(int i = 0 ; i < users.size() ; i++ ){
			user_ids += users.get(i)+",";
			users_status += statuses.get(i)+",";
			replied_temp += replied_ats.get(i)+",";
		}
		user_ids = user_ids.substring(0, user_ids.length()-1);
		users_status = users_status.substring(0, users_status.length()-1);
		replied_temp = replied_temp.substring(0,replied_temp.length()-1);
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.execSQL("UPDATE "+DBHelper.TABLE_VIDEOS+" SET "+DBHelper.C_REPLIED+"="+replied+","
																	+DBHelper.C_STATUS+"="+status + ","
																	+DBHelper.C_USERS+"='"+user_ids+"',"
																	+DBHelper.C_USER_STATUS+"="+user_status+","
																	+DBHelper.C_THUMBNAIL_URL+"='"+thumbnail+"',"
																	+DBHelper.C_STATUSES+"='"+users_status+"',"
																	+DBHelper.C_UPDATED_AT+"='"+updated_at+"',"
																	+DBHelper.C_N_COMMENTS+"="+n_comments+","
																	+DBHelper.C_N_LIKES+"="+n_likes+","
																	+DBHelper.C_N_VIEWS+"="+n_views+","
																	+DBHelper.C_REPLIED_AT+"='"+replied_temp+"' WHERE "+DBHelper.C_ID+"="+id);
		myDatabase.close();
	}
	
	@Override
	public void destroy() {
		SQLiteDatabase myDatabase = mDatabase.getWritableDatabase();
		myDatabase.delete(DBHelper.TABLE_VIDEOS,DBHelper.C_ID+"="+id, null);
		myDatabase.close();
	}
	
	public String getStringTarget(){
		switch(target){
		case WApp.TARGET_WORLD:
			return "World";
		case WApp.TARGET_FRIENDS:
			return "Friends";
		case WApp.TARGET_GROUPS:
			return "Groups";
		}
		return "";
	}
	
	public int getTarget(){
		return target;
	}
	
	public boolean exists(){
		Cursor c = mDatabase.getReadableDatabase().rawQuery("SELECT * FROM "+DBHelper.TABLE_VIDEOS+" WHERE "+DBHelper.C_ID+"="+id,null);
		boolean res = c.moveToFirst();
		c.close();
		mDatabase.getReadableDatabase().close();
		return res;
	}

	@Override
	public synchronized void load() {
		SQLiteDatabase myDatabase = mDatabase.getReadableDatabase();
		Cursor c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_VIDEOS+" WHERE "+DBHelper.C_ID+"="+id, null);
		if(c.moveToFirst()){
			title = c.getString(c.getColumnIndex(DBHelper.C_TITLE)); 
			players = c.getInt(c.getColumnIndex(DBHelper.C_PLAYERS));
			eta = c.getString(c.getColumnIndex(DBHelper.C_ETA));
			owner_id = c.getLong(c.getColumnIndex(DBHelper.C_USER_ID));
			group_id = c.getLong(c.getColumnIndex(DBHelper.C_GROUP_ID));
			target = c.getInt(c.getColumnIndex(DBHelper.C_TARGET));
			replied = c.getInt(c.getColumnIndex(DBHelper.C_REPLIED));
			status = c.getInt(c.getColumnIndex(DBHelper.C_STATUS));
			updated_at = c.getString(c.getColumnIndex(DBHelper.C_UPDATED_AT));
			created_at = c.getString(c.getColumnIndex(DBHelper.C_CREATED_AT));
			expires_at = c.getString(c.getColumnIndex(DBHelper.C_EXPIRES_AT));
			video_status = c.getInt(c.getColumnIndex(DBHelper.C_VIDEO_STATUS));
			private_video = c.getInt(c.getColumnIndex(DBHelper.C_PRIVATE_VIDEO));
			String[] user_ids = c.getString(c.getColumnIndex(DBHelper.C_USERS)).split(",");
			String[] users_status = c.getString(c.getColumnIndex(DBHelper.C_STATUSES)).split(",");
			String[] replied_temp = c.getString(c.getColumnIndex(DBHelper.C_REPLIED_AT)).split(",");
			users = new ArrayList<Long>();
			statuses = new ArrayList<Integer>();
			replied_ats = new ArrayList<String>();
			for(int i = 0 ; i < user_ids.length ; i++){
				users.add(Long.valueOf(user_ids[i]));
				statuses.add(Integer.valueOf(users_status[i]));
				replied_ats.add(replied_temp[i]);
			}
			
			n_likes = c.getInt(c.getColumnIndex(DBHelper.C_N_LIKES));
			n_views = c.getInt(c.getColumnIndex(DBHelper.C_N_VIEWS));
			n_comments = c.getInt(c.getColumnIndex(DBHelper.C_N_COMMENTS));
			thumbnail = c.getString(c.getColumnIndex(DBHelper.C_THUMBNAIL_URL));
		}
		c.close();
		myDatabase.close();
	}

	public int getPlayers() {
		return players;
	}

	public void setPlayers(int players) {
		this.players = players;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public int getReplied() {
		return replied;
	}

	public void setReplied(int replied) {
		this.replied = replied;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setTarget(int target) {
		this.target = target;
	}
	
	public long getSecondPlayer(){
		for(int i = 0 ; i < users.size();i++){
			if(users.get(i) != getOwner_id())
				return users.get(i);
		}
		return 0;
	}
	
	public ArrayList<Integer> getStatuses(){
		return statuses;
	}
	
	public String getStatusesText(){
		String res = "";
		for(int i = 0 ; i < statuses.size();i++){
			res += statuses.get(i)+",";
		}
		res = res.substring(0, res.length()-1);
		return res;
	}
	
	public String getExpiresAt(){
		return expires_at;
	}
	
	/**
	 * @return the thumbnail
	 */
	public String getThumbnail() {
		if(thumbnail == null)
			return "null";
		
		return thumbnail;
	}
	
	public String getShare_url(){
		if(share_url == null){
			return "null";
		}
		
		return share_url;
	}

	/**
	 * @param thumbnail the thumbnail to set
	 */
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	public int getUser_status(){
		return user_status;
	}
	
	public String toString(){
		return "Video: "+title+"("+id+") Owner: "+owner_id
				+"\n"+" Replieds: "+replied_ats.toString()
				+"\n"+" status: "+status;
	}
	
	public static Video parse(WApp app,JSONObject json) throws JSONException {
		JSONObject juser;
		int user_status = 0;
		ArrayList<Long> users = new ArrayList<Long>();
		ArrayList<Integer> statuses = new ArrayList<Integer>();
		ArrayList<String> replied_ats = new ArrayList<String>();
		JSONArray jarrayPlayers = json.getJSONArray("playerslist");
		for(int j = 0 ; j < jarrayPlayers.length() ; j++ ){
			juser = jarrayPlayers.getJSONObject(j);
			long id = Long.valueOf(juser.getString("id"));
			users.add(id);
			statuses.add((juser.getString("replied_at") != null && juser.getString("replied_at") != "null")?1:0);
			replied_ats.add(juser.getString("replied_at"));
			User.parse(app, juser).create();
			
			if(id == app.getId())
				user_status = juser.getInt("status");
		}
		
		int likes = 0;
		int views = 0;
		int comments = 0;
		
		if(json.has("nr_likes"))
			likes = json.getInt("nr_likes");
		
		if(json.has("nr_views"))
			views = json.getInt("nr_views");
		
		if(json.has("nr_comments"))
			comments = json.getInt("nr_comments");
		
//		WLog.d(Video.class,"Thumbnail: "+json.getString("thumbnail"));
//		WLog.d(Video.class,"Status. "+json.getInt("status"));
		
		return new Video(app,
							Long.valueOf(json.getString("id")),
							json.getString("title"),
							json.getInt("nr_players"),
							json.getString("eta"),
							Long.valueOf(json.getString("user_id")),
							Long.valueOf(json.getString("group_id")),
							json.getInt("target"),
							json.getInt("replied"),
							json.getInt("status"),
							user_status,
							users,
							statuses,
							replied_ats,
							json.getString("updated_at"),
							json.getString("created_at"),
							json.getString("expires_at"),
							json.getInt("private"),
							likes,
							views,
							comments,
							json.getString("thumbnail"),
							json.getString("share_url"));
	}
	
	/*
	 * Bulk insert/update videos
	 */
	public static void bulkInsert(WApp app,ArrayList<Video> videos){
		String sql = "INSERT INTO "+DBHelper.TABLE_VIDEOS+" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();
		db.delete(DBHelper.TABLE_VIDEOS,null, null);
		Video v;
		for(int i = 0 ; i < videos.size() ; i++){
			v = videos.get(i);
			stmt.clearBindings();
			stmt.bindLong(1,v.getId());
			stmt.bindString(2, v.getTitle());
			stmt.bindLong(3,v.getPlayers());
			stmt.bindString(4,v.getEta());
			stmt.bindLong(5,v.getOwner_id());
			stmt.bindLong(6,v.getGroup_id());
			stmt.bindLong(7,v.getTarget());
			stmt.bindLong(8,v.getReplied());
			stmt.bindLong(9,v.getStatus());
			stmt.bindLong(10, v.getUser_status());
			stmt.bindString(10,v.getUsersText());
			stmt.bindString(11,v.getStatusesText());
			stmt.bindString(12,v.getRepliedAtsText());
			stmt.bindString(14,v.getCreated_at());
			stmt.bindString(14,v.getUpdated_at());
			stmt.bindString(16,v.getExpiresAt());
			stmt.bindLong(17,v.getPrivate_video());
			stmt.bindLong(18, v.getN_likes());
			stmt.bindLong(19, v.getN_views());
			stmt.bindLong(20, v.getN_comments());
			stmt.bindString(21, v.getThumbnail());
			stmt.bindString(22, v.getShare_url());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
	public static void bulkUpdate(WApp app,ArrayList<Video> videos){
		String sql = "UPDATE "+DBHelper.TABLE_VIDEOS+" SET "+DBHelper.C_REPLIED+"=?,"
				+DBHelper.C_STATUS+"=?,"
				+DBHelper.C_USERS+"=?,"
				+DBHelper.C_STATUSES+"=?,"
				+DBHelper.C_UPDATED_AT+"=?,"
				+DBHelper.C_REPLIED_AT+"=?,"
				+DBHelper.C_N_LIKES+"=?,"
				+DBHelper.C_N_VIEWS+"=?,"
				+DBHelper.C_N_COMMENTS+"=?,"
				+DBHelper.C_SHARE_URL+"=?,"
				+DBHelper.C_THUMBNAIL_URL+"=? WHERE "+DBHelper.C_ID+"=?";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();
		for(Video video : videos){
			stmt.clearBindings();
			stmt.bindLong(1, video.getReplied());
			stmt.bindLong(2, video.getStatus());
			stmt.bindString(3, video.getUsersText());
			stmt.bindString(4, video.getStatusesText());
			stmt.bindString(5, video.getUpdated_at());
			stmt.bindString(6, video.getRepliedAtsText());
			stmt.bindLong(7,video.getN_likes());
			stmt.bindLong(8,video.getN_views());
			stmt.bindLong(9,video.getN_comments());
			stmt.bindString(10, video.getShare_url());
			stmt.bindString(11, video.getThumbnail());
			stmt.bindLong(112, video.getId());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	public static Cursor getVideos(WApp app){
		Cursor c;
        SQLiteDatabase myDatabase = app.getDB().getReadableDatabase();
        c = myDatabase.rawQuery("SELECT * FROM "+DBHelper.TABLE_VIDEOS+" ORDER BY "+DBHelper.C_UPDATED_AT+" DESC",null);
        return c;
	}

	@Override
	public boolean isTimelineVideo() {
		return false;
	}
	
	@Override
	public boolean isMyVideo(){
		return false;
	}
	
}
