package com.weclipse.app.models;

public interface Storable {
	
	/** Save the instance into the database */
	public void create();
	
	/** Save the value into the database */
	public void update(String key,String value);
	public void update(String key,int value);
	
	/** Delete the instance from the database */
	public void destroy();
	
	/** Load this instance */
	public void load();

}
