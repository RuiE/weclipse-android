package com.weclipse.app.models;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.weclipse.app.WApp;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;

public class User implements Storable {

	private static final String TAG = "User Model";

	public static final int FRIENDS = 1;
	public static final int GROUPS = 2;
	public static final int WORLD = 3;

	public static final int NOTHING = -1;
	public static final int CLEAN_FOLLOW = 0;
	public static final int PENDING_ACCEPT = 1;
	
	public static final int PRIVATE  = 1;
	public static final int PUBLIC = 0;

	Context context;
	long id;
	DBHelper mDatabase;
	int follow_status;
	String username;
	String name;
	String avatar;
	boolean private_user;

	public User(Context context, long id, String username, String name,String avatar_url, int follow_status,boolean private_user) {
		mDatabase = new DBHelper(context);
		this.context = context;
		this.id = id;
		this.name = name;
		this.username = username;
		this.avatar = avatar_url;
		this.follow_status = follow_status;
		this.private_user = private_user;
	}

	public User(Context ctx, long id) {
		mDatabase = new DBHelper(ctx);
		this.context = ctx;
		this.id = id;
	}

	@Override
	public void create() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_USERS + " WHERE " + DBHelper.C_ID + "=" + id, null);
		if (c.moveToFirst()) {
			c.close();
			db.close();
			updateAll();
		} else {
			c.close();
			try {
				ContentValues values = new ContentValues();
				values.put(DBHelper.C_ID, id);
				values.put(DBHelper.C_NAME, name);
				values.put(DBHelper.C_USERNAME, username);
				values.put(DBHelper.C_AVATAR_URL, avatar);
				values.put(DBHelper.C_FOLLOW_STATUS, follow_status);
				values.put(DBHelper.C_PRIVATE_USER, private_user?User.PRIVATE:User.PUBLIC);
				db.insert(DBHelper.TABLE_USERS, null, values);
				db.close();
			} catch (SQLException e) {
				// J?? existe
			}
		}
	}

	public boolean exists() {
		boolean res;
		SQLiteDatabase db = mDatabase.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_USERS
				+ " WHERE " + DBHelper.C_ID + "=" + id, null);
		res = c.moveToFirst();
		c.close();
		db.close();
		return res;
	}

	@Override
	public void update(String key, String value) {

	}

	public void updateAll() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		
		String sql = "UPDATE "+DBHelper.TABLE_USERS+" SET "
				+DBHelper.C_AVATAR_URL+"=?,"
				+DBHelper.C_USERNAME+"=?,"
				+DBHelper.C_NAME+"=?,"
				+DBHelper.C_FOLLOW_STATUS+"=? WHERE "+DBHelper.C_ID+"=?";
		
		SQLiteStatement stmt = db.compileStatement(sql);
		
		stmt.clearBindings();
		stmt.bindString(1, avatar);
		stmt.bindString(2, username);
		stmt.bindString(3, name);
		stmt.bindLong(4, follow_status);
		stmt.bindLong(4, id);
		stmt.execute();
		
		db.close();
	}

	@Override
	public void update(String key, int value) {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.execSQL("UPDATE " + DBHelper.TABLE_USERS + " SET " + key + "="
				+ value + " WHERE " + DBHelper.C_ID + "=" + id);
		db.close();
	}

	@Override
	public void destroy() {
		SQLiteDatabase db = mDatabase.getWritableDatabase();
		db.delete(DBHelper.TABLE_USERS, DBHelper.C_ID + "=" + id, null);
		db.close();
	}

	@Override
	public void load() {
		SQLiteDatabase db = mDatabase.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + DBHelper.TABLE_USERS + " WHERE " + DBHelper.C_ID + "=" + id, null);
//		WLog.d(this,"Loading...:"+c.getCount());
		if (c.moveToFirst()) {
//			WLog.d(this,"Move to first!!!");
			username = c.getString(c.getColumnIndex(DBHelper.C_USERNAME));
			name = c.getString(c.getColumnIndex(DBHelper.C_NAME));
			avatar = c.getString(c.getColumnIndex(DBHelper.C_AVATAR_URL));
			follow_status = c.getInt(c.getColumnIndex(DBHelper.C_FOLLOW_STATUS));
			private_user = c.getInt(c.getColumnIndex(DBHelper.C_PRIVATE_USER)) == User.PRIVATE;
//			WLog.d(this,"Tostring to load:" +toString());
		} else {
			WLog.d(this,"Vou por o username a not available... WHAT?----------");
			username = "not_available";
		}
		c.close();
		db.close();
	}

	public long getId() {
		return id;
	}

	public int getFollow_status() {
		return follow_status;
	}

	public void setFollow_status(int follow_status) {
		this.follow_status = follow_status;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public boolean isPrivate(){
		return private_user;
	}
	
	public int getPrivate(){
		return private_user?User.PRIVATE:User.PUBLIC;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public String getMinifiedName(){
		return Utils.getMinifiedName(username);
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public String toString() {
		return name + "(" + username + ")" + " - " + follow_status + " --- "+id;
	}

	public static User parse(WApp app, JSONObject json) throws JSONException {
		
		int follow_status = User.CLEAN_FOLLOW;
		
		if(json.has("follow_status")){
			follow_status = json.getInt("follow_status");
		}
		
		User u = new User(app, Long.valueOf(json.getString("id")),
				json.getString("username"), json.getString("name"),
				json.getString("avatar_url"),
				follow_status,
				json.getBoolean("private"));
		
		UserCache.getInstance(app).updateUser(u);
		
		return u;
	}
	
	public static void bulkInsert(WApp app,ArrayList<User> users){
		bulkInsert(app, users, true);
	}
	
	/**
	 * 
	 * @param app link to the application, which serves as a general context
	 * @param users list of users to populate the database with
	 * @param delete whether to delete the previous user base
	 */
	public static void bulkInsert(WApp app,ArrayList<User> users,boolean delete){
		String sql = "INSERT INTO "+DBHelper.TABLE_USERS+" VALUES(?,?,?,?,?,?,?);";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		
		db.beginTransaction();
		
		if(delete)
			db.delete(DBHelper.TABLE_USERS,null, null);
		
		for(User u : users){
			stmt.clearBindings();
			stmt.bindLong(1,u.getId());
			stmt.bindString(2, u.getUsername());	
			stmt.bindString(3, u.getName());
			stmt.bindString(4, u.getAvatar());
			stmt.bindLong(6,u.getFollow_status());
			stmt.bindLong(7, u.getPrivate());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	
	public static void bulkUpdate(WApp app,ArrayList<User> users){
		String sql = "UPDATE "+DBHelper.TABLE_USERS+" SET "
				+DBHelper.C_FOLLOW_STATUS+"=?,"
				+DBHelper.C_PRIVATE_USER+"=?,"
				+DBHelper.C_AVATAR_URL+"=? WHERE "+DBHelper.C_ID+"=?";
		SQLiteDatabase db = app.getDB().getWritableDatabase();
		SQLiteStatement stmt = db.compileStatement(sql);
		db.beginTransaction();
		for(User u : users){
			stmt.clearBindings();
			stmt.bindLong(1,u.getFollow_status());
			stmt.bindLong(2,u.getPrivate());
			stmt.bindString(3, u.getAvatar());
			stmt.bindLong(4, u.getId());
			stmt.execute();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

}
