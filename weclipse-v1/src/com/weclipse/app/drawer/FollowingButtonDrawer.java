package com.weclipse.app.drawer;

import android.content.Context;
import android.widget.ToggleButton;

import com.weclipse.app.R;
import com.weclipse.app.models.User;

public class FollowingButtonDrawer {

	int status;
	ToggleButton button;
	Context context;
	
	public FollowingButtonDrawer(Context context,int status,ToggleButton button){
		this.status = status;
		this.button = button;
		this.context = context;
	}
	
	public void draw(){
		button.setTag(status);
		
		switch(status){
		case User.CLEAN_FOLLOW:
			button.setChecked(true);
			button.setText(context.getString(R.string.profile_button_follow_pressed));
			button.setBackgroundResource(R.drawable.button_following);
			break;
		case User.PENDING_ACCEPT:
			button.setChecked(true);
			button.setText(context.getString(R.string.profile_button_follow_pending));
			button.setBackgroundResource(R.drawable.button_following_pending);
			break;
		case User.NOTHING:
			button.setChecked(false);
			button.setText(context.getString(R.string.profile_button_follow));
			button.setBackgroundResource(R.drawable.button_following);
			break;
		}
			
	}
	
}
