package com.weclipse.app.drawer;

import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.weclipse.app.WApp;
import com.weclipse.app.models.User;
import com.weclipse.app.utils.CircleTransform;
import com.weclipse.app.utils.Utils;

public class AvatarDrawer {

	ImageView ivAvatar;
	User u;
	WApp app;
	
	public AvatarDrawer(WApp app,ImageView ivAvatar,User u){
		this.ivAvatar = ivAvatar;
		this.u = u;
		this.app = app;
	}
	public void draw(){
		Picasso.with(app)
				.load(u.getAvatar())
				.transform(new CircleTransform())
				.into(ivAvatar);
	}
	
}
