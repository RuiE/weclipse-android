package com.weclipse.app.drawer;

import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.weclipse.app.WApp;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.User;
import com.weclipse.app.utils.CircleTransform;
import com.weclipse.app.utils.Utils;

public class GroupAvatarDrawer {

	ImageView ivAvatar;
	Group g;
	WApp app;
	
	public GroupAvatarDrawer(WApp app,ImageView ivAvatar,Group g){
		this.ivAvatar = ivAvatar;
		this.g = g;
		this.app = app;
	}
	public void drawAvatar(){
		Picasso.with(app)
				.load(g.getAvatar())
				.transform(new CircleTransform())
				.into(ivAvatar);
	}
	
	public void drawCover(){
		Picasso.with(app)
		.load(g.getCover())
		.into(ivAvatar);
	}
	
}
