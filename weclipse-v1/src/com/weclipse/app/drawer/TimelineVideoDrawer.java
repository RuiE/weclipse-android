package com.weclipse.app.drawer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.widget.FacebookDialog;
import com.squareup.picasso.Picasso;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.DetailActivity;
import com.weclipse.app.activity.ProfileActivity;
import com.weclipse.app.activity.TheMainActivity;
import com.weclipse.app.activity.VideoActivity;
import com.weclipse.app.R;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.DeleteVideoTask;
import com.weclipse.app.tasks.LikeVideoTask;
import com.weclipse.app.tasks.ReportTask;
import com.weclipse.app.tasks.UnlikeVideoTask;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;

public class TimelineVideoDrawer {

	TimelineVideo video;
	ViewHolderVideo holder;
	WApp app;
	Activity context;
	int source;
	
	public boolean liked;
	
	public TimelineVideoDrawer(Activity act,WApp app,TimelineVideo video,ViewHolderVideo holder,int source){
		this.video = video;
		this.holder = holder;
		this.context = act;
		this.app = app;
		this.source = source;
	}
	
	public void draw(){
		holder.caption.setText(video.getTitle());
		//TODO getString
		holder.tvLikes.setText(video.getN_likes() +" "+app.getString(R.string.timeline_video_likes));
		holder.tvViews.setText(video.getN_views() +" "+app.getString(R.string.timeline_video_views));
		holder.tvComments.setText(video.getN_comments()+" "+app.getString(R.string.timeline_video_comments));
		
		int participants = video.getRepliedAts().size()-1;
		holder.tvHost.setText(UserCache.getInstance(app).getUsername(video.getOwner_id())+((participants > 0)?" + "+participants:""));
		
//		WLog.d(this,"Drawing :"+UserCache.getInstance(app).getUsername(video.getOwner_id()));
		
		if(video.getReady_at() != null && !video.getReady_at().equals("null")){
			holder.tvTime.setText(Utils.getTimePast(context,video.getReady_at()));
		} else {
			holder.tvTime.setText(Utils.getTimePast(context,video.getCreated_at()));
			WLog.d(this,"Ready at a null: "+video.getReady_at());
		}
		
		Picasso.with(app)
				.load(video.getThumbnail_url())
				.into(holder.ivImage);
		
		User host = UserCache.getInstance(app).getUser(video.getOwner_id());
		
		new AvatarDrawer(app, holder.ivHost, host).draw();
		
		if(video.getPrivate_video() == Video.PRIVATE){
			holder.ivPrivate.setVisibility(View.VISIBLE);
		} else if(video.getPrivate_video() == Video.PUBLIC) {
			holder.ivPrivate.setVisibility(View.GONE);
		}
		
		if(video.getEditors_choice() == Video.EDITORS_CHOICE){
			holder.ivEditor.setVisibility(View.VISIBLE);
			holder.ivEditor.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(context, ProfileActivity.class);
					i.putExtra("user_id", video.getReweclipsedBy());
					context.startActivity(i);
				}
			}); 
		} else {
			holder.ivEditor.setVisibility(View.GONE);
			holder.ivEditor.setOnClickListener(null);
		}
		
		if(liked){
			holder.ivLikes.setBackgroundResource(R.drawable.timeline_likes_pink);
		} else {
			holder.ivLikes.setBackgroundResource(R.drawable.timeline_likes_white);
		}
			
		/* Clickers and shizzle */
		if(!WApp.INLINE){
			holder.rlContainer.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent i = new Intent(context, VideoActivity.class);
					i.putExtra("vid", video.getId());
					i.putExtra("caption",video.getTitle());
					context.startActivity(i);
				}
			});
		}
		
		holder.rlLikeContainer.setTag(liked);
		holder.rlLikeContainer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean liked = (Boolean)v.getTag();
				if(liked){
					v.findViewById(R.id.ivLikes).setBackgroundResource(R.drawable.timeline_likes_white);
					new UnlikeVideoTask(app,video,source);
				} else {
					v.findViewById(R.id.ivLikes).setBackgroundResource(R.drawable.timeline_likes_pink);
					new LikeVideoTask(app, video,source);
				}
			}
		});
		
		OnClickListener ocl = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(context,DetailActivity.class);
				i.putExtra("type",DetailActivity.TYPE_COMMENT);
				i.putExtra("vid",video.getId());
				i.putExtra("caption",video.getTitle());
				i.putExtra("is_timeline",true);
				context.startActivity(i);
			}
		};
		
		holder.rlCommentsContainer.setOnClickListener(ocl);
		holder.tvComments.setOnClickListener(ocl);
		
		holder.tvLikes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(context,DetailActivity.class);
				i.putExtra("type",DetailActivity.TYPE_LIKE);
				i.putExtra("vid", video.getId());
				i.putExtra("caption",video.getTitle());
				i.putExtra("is_timeline",true);
				context.startActivity(i);
			}
		});
		
		holder.rlHost.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(context,DetailActivity.class);
				i.putExtra("type",DetailActivity.TYPE_PLAYERS);
				i.putExtra("vid", video.getId());
				i.putExtra("caption",video.getTitle());
				i.putExtra("is_timeline",true);
				context.startActivity(i);
			}
		});
		
		holder.rlShareContainer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				timelineVideoOptions(video).show();
			}
		});
		
	}
	
	public static class ViewHolderVideo {
		public TextView caption;
		public TextView tvHost;
		public TextView tvTime;
		public TextView tvLikes;
		public TextView tvViews;
		public TextView tvComments;
		/* Icons */
		public ImageView ivHost;
		public ImageView ivImage;
		public ImageView ivPrivate;
		public ImageView ivLikes;
		public ImageView ivEditor;
		/* containers */
		public RelativeLayout rlLikeContainer;
		public RelativeLayout rlCommentsContainer;
		public RelativeLayout rlShareContainer;
		public RelativeLayout rlHost;
		public RelativeLayout rlContainer;
	}
	
	public Dialog timelineVideoOptions(final TimelineVideo video) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    
	    String[] options = null;
	    
	    if(source == AbstractVideo.SOURCE_TIMELINE){
	    	
	    	if(video.getPrivate_video() == Video.PUBLIC){//pubblico no timeline
	    		options = context.getResources().getStringArray(R.array.timeline_options);
	    	} else {
	    		options = context.getResources().getStringArray(R.array.timeline_options_private);
	    	}
	    	
	    	builder.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
             	   switch(which){
             	   case 0:// Report
             		   new ReportTask(app, video.getId());
             		   break;
             	   case 1:// Share on the facebook
             		   FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
             	        .setLink(video.getShare_url())
             	        .build();
             		   if(context instanceof TheMainActivity)
             			   (((TheMainActivity)context).uiHelper).trackPendingDialogCall(shareDialog.present());
             		   else
             			   (((ProfileActivity)context).uiHelper).trackPendingDialogCall(shareDialog.present());
             		   
             		   break;
             	   }
         	   }
    		});
	    	
	    } else if(source == AbstractVideo.SOURCE_PROFILE){
	    	
	    	if(video.getPrivate_video() == Video.PUBLIC ){ //publico no mru profile
	    		options = context.getResources().getStringArray(R.array.timeline_my_options);
	    	} else {//private no meu profile
	    		options = context.getResources().getStringArray(R.array.timeline_my_options_private);
	    	}
	    	
	    	builder.setItems(options, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
             	   switch(which){
             	   case 0:// Report
             		   new ReportTask(app, video.getId());
             		   break;
             	   case 1://Delete
             		   new DeleteVideoTask(app, video.getId());
             		   break;
             	   case 2:// Share on the facebook
             		   FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
             	        .setLink(video.getShare_url())
             	        .build();
             		   if(context instanceof TheMainActivity)
             			   (((TheMainActivity)context).uiHelper).trackPendingDialogCall(shareDialog.present());
             		   else
             			   (((ProfileActivity)context).uiHelper).trackPendingDialogCall(shareDialog.present());
             		   
             		   break;
             	   }
         	   }
    		});
	    	
	    }
	    
	    
	    builder.setTitle(video.getTitle());
	    return builder.create();
	}
	
	public static View newView(View v){
		ViewHolderVideo holder = new ViewHolderVideo();
		/* Texts */
		holder.caption = (TextView) v.findViewById(R.id.tvCaption);
		holder.tvHost = (TextView) v.findViewById(R.id.tvHost);
		holder.tvTime = (TextView) v.findViewById(R.id.tvTime);
		holder.tvLikes = (TextView) v.findViewById(R.id.tvLikes);
		holder.tvViews = (TextView) v.findViewById(R.id.tvViews);
		holder.tvComments = (TextView) v.findViewById(R.id.tvComments);
		/* Icons */
		holder.ivHost = (ImageView) v.findViewById(R.id.ivHost);
		holder.ivImage = (ImageView) v.findViewById(R.id.ivImage);
		holder.ivPrivate = (ImageView) v.findViewById(R.id.ivPrivate);
		holder.ivLikes = (ImageView) v.findViewById(R.id.ivLikes);
		holder.ivEditor = (ImageView) v.findViewById(R.id.ivEditor);
		/* Containers */
		holder.rlLikeContainer = (RelativeLayout) v.findViewById(R.id.rlLikeContainer);
		holder.rlContainer = (RelativeLayout) v.findViewById(R.id.rlContainer);
		holder.rlCommentsContainer = (RelativeLayout) v.findViewById(R.id.rlCommentsContainer);
		holder.rlShareContainer = (RelativeLayout) v.findViewById(R.id.rlShareContainer);
		holder.rlHost = (RelativeLayout) v.findViewById(R.id.rlHost);

		v.setTag(holder);
		return v;
	}
	
}
