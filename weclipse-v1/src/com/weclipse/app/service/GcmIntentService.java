package com.weclipse.app.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.weclipse.app.WApp;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.WNotification;
import com.weclipse.app.receiver.GcmBroadcastReceiver;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.VideoUpdateTask;
import com.weclipse.app.tasks.fetch.FetchActivityTask;
import com.weclipse.app.tasks.fetch.FetchFriendsTask;
import com.weclipse.app.tasks.fetch.FetchGroupsTask;
import com.weclipse.app.tasks.fetch.FetchMyVideosTask;
import com.weclipse.app.tasks.fetch.FetchPossiblePlayers;
import com.weclipse.app.tasks.fetch.FetchTimelineTask;
import com.weclipse.app.utils.WLog;

public class GcmIntentService extends IntentService {
	private static final String TAG = "GCM Intent Service";
	Context ctx;
	WApp app;

	public GcmIntentService() {
		super("GcmIntentService");
		ctx = this;
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		app = (WApp)getApplication();
		
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("error", "Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("deleted", "Deleted messages on server: " + extras.toString());
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

				// Post notification of received message.
				WLog.i(this, extras.toString());
				int type = Integer.parseInt(extras.getString("type"));

				if (!((WApp) getApplication()).isLogged()) {
					if (type == WNotification.CONTACTS_READY) {
						// Log.d(TAG,"Received the shizzle");
					} else {
						return;
					}
				}

				// Log.d(TAG,"TYPE: "+type);

				String msg = "";

				switch (type) {
				case WNotification.CHUCK_TESTA: // 0
					msg = extras.getString("alert");
					new WNotification((WApp) getApplication(), type, msg);
					break;
				case WNotification.VIDEO_READY:// 1
				case WNotification.VIDEO_UPDATE:// 7
				case WNotification.VIDEO_INVITE:// 2
					new FetchActivityTask((WApp) getApplication(),SimpleTask.FRESH,0);
					if(type == WNotification.VIDEO_READY){
						new FetchTimelineTask(app, FetchTimelineTask.FRESH);
						new FetchMyVideosTask(app, FetchMyVideosTask.FRESH);
					}
					
					if(type == WNotification.VIDEO_INVITE || type == WNotification.VIDEO_READY){
						new VideoUpdateTask(app, Long.valueOf(extras.getString("video_id")));
						new WNotification((WApp)getApplication(), type, extras.getString("alert"));
					}
					
					break;
				case WNotification.VIDEO_COMMENT: // 9
				case WNotification.VIDEO_LIKE: // 16
				case WNotification.USER_FOLLOW: //3
				case WNotification.USER_ACCEPT_FOLLOW: // 31
				case WNotification.CONTACT_JOINED_WECLIPSE: // 12
				case WNotification.USER_REQUEST_FOLLOW: //30
					try {
						new FetchActivityTask(app, FetchActivityTask.FRESH, 0);
						new WNotification(app,type,extras.getString("alert"));
					} catch (NumberFormatException e2) {
						e2.printStackTrace();
					}
					break;
				case WNotification.USER_FRIENDS:// 32
					new FetchActivityTask((WApp) getApplication(), FetchTimelineTask.FRESH, 0);
					new FetchPossiblePlayers(app);
					break;
				case WNotification.USER_UNFOLLOW:// 4
					//Do nothing?
					WLog.d(this,"Some guy unfollowed");
					break;
				case WNotification.GROUP_UPDATE:
				case WNotification.GROUP_INVITE:
				case WNotification.GROUP_DELETE:
					new FetchGroupsTask((WApp) getApplication(), FetchGroupsTask.FRESH, 0);
					if(type == WNotification.GROUP_INVITE){
						new WNotification((WApp)getApplication(), type, extras.getString("alert"));
					}
					break;
				case WNotification.LOGOUT:
					WLog.d(this,"logging out");
					((WApp) getApplication()).logout(false);
					((WApp) getApplication()).clearDB();
					((WApp) getApplication()).activelyLogout();
					break;
				case WNotification.FRIEND_DELETE_ACCOUNT:
					new FetchFriendsTask((WApp)getApplication(), Me.getInstance((WApp)getApplication()).getId(), FetchFriendsTask.FRESH, 0);
					break;
				default:
					// Do nothing
					break;
				}
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendNotification(String title, String msg) {
		// Log.d(TAG, title);
		// Log.d(TAG, msg);
	}
}