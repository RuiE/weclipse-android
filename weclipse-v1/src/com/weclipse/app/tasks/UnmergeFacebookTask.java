package com.weclipse.app.tasks;

import com.facebook.Session;
import com.weclipse.app.WApp;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.UnmergeFacebookEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class UnmergeFacebookTask {
	
	public UnmergeFacebookTask(final WApp app){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new UnmergeFacebookEvent(code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Result unlink fb("+code+"): "+result);
				if(code == HTTPStatus.OK){
					PreferenceManager.getInstance(app).putPreference(PreferenceManager.FACEBOOK,false);
					if(Session.getActiveSession() != null)
						Session.getActiveSession().closeAndClearTokenInformation();
				}
			}
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"unlink_fb", SimpleTask.TYPE_POST).execute();
		
	}

}
