package com.weclipse.app.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.WLog;

public class VideoUpdateTask {
	
	public VideoUpdateTask(final WApp app,final long video_id){
			
		SimpleTaskHelper mTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"result de video update("+code+"): ");
				WLog.d(this,"result de video update("+code+"): "+result);
				try {
					Video.parse(app, new JSONObject(result)).create();
				} catch(JSONException e){
					if(WApp.DEBUG){
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
			}
		};
		
		new SimpleTask(app,mTaskHelper,WApp.BASE+"videos/"+video_id,SimpleTask.TYPE_GET).execute();
	}
	
}