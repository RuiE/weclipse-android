package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.models.OfflineAction;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.eventbus.RemoveVideoEventComplete;
import com.weclipse.app.utils.eventbus.RemoveVideoEventInit;
import com.weclipse.app.utils.eventbus.WBus;

public class RemoveVideoTask {

	int code;
	WApp app;
	long vid;
	
	public RemoveVideoTask(final WApp app,final long vid){
		this.app = app;
		this.vid = vid;
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new RemoveVideoEventComplete(vid, code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				if(code == 0){
					app.getOfflineManager().addAction(new OfflineAction(app,vid,OfflineAction.CLEAR_VIDEO));
				}
			}

			@Override
			public void onPreExecute() {
				new Video(app,vid).destroy();
				WBus.getInstance().post(new RemoveVideoEventInit());
			}
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"clear_video/"+vid, SimpleTask.TYPE_GET).execute();
		
	}
	
}
