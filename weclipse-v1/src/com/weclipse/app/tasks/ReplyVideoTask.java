package com.weclipse.app.tasks;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.activity.BaseActivity;
import com.weclipse.app.models.PendingVideo;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.CustomMultiPartEntity;
import com.weclipse.app.utils.CustomMultiPartEntity.ProgressListener;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.NetworkUtils;
import com.weclipse.app.utils.ReplyPendingCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.FeedUpdateEvent;
import com.weclipse.app.utils.eventbus.ProgressReplyVideoEvent;
import com.weclipse.app.utils.eventbus.VideoDownloadEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class ReplyVideoTask extends AsyncTask<String, Integer, String> {

	WApp app;
	long user_id;
	long video_id;
	String filePath;
	long duration;
	int media_type;
	BaseActivity activity;
	
	int code = 0;
	long totalSize;
	long time = 0;
	
	public ReplyVideoTask(WApp app,BaseActivity activity,long user_id,long video_id,String filePath,long duration,int media_type){
		this.app = app;
		this.activity = activity;
		this.user_id = user_id;
		this.video_id = video_id;
		this.filePath = filePath;
		this.duration = duration;
		this.media_type = media_type;
		WLog.d(this,"Created, going to execute");
		this.execute();
	}
	
	protected void onProgressUpdate(Integer... values) {
		WBus.getInstance().post(new ProgressReplyVideoEvent(video_id,values[0]));
	};
	
	protected void onPreExecute() {
		app.getFeedLoadingManager().addLoading(video_id);
		WBus.getInstance().post(new VideoDownloadEvent(video_id));
	};

	@Override
	protected String doInBackground(String... params) {
		
		String endpoint = WApp.BASE;
		endpoint += WApp.REPLY_CLIP;
		endpoint = endpoint.replace("__uid__", "" + user_id);
		endpoint = endpoint.replace("__vid__", "" + video_id);
//		Log.d(TAG, "Endpoint: " + endpoint);
		String result = null;
		MyHttpClient httpclient = new MyHttpClient(app);
		HttpPost post = new HttpPost(endpoint);
		HttpResponse response;
		try {
			CustomMultiPartEntity postEntity = new CustomMultiPartEntity(new ProgressListener() {
				@Override
				public void transferred(long num) {
					Integer value =  (int) ((num/(float)totalSize)*100);
					if(System.currentTimeMillis()-time >= 75 || value==100){
						time = System.currentTimeMillis();
						publishProgress((int)((float)value*((float)Utils.PROGRESS_FILE_UPLOAD/(float)100)));
					}
				}
			});
			File f = new File(filePath);
//			Log.d(TAG,"Duration: "+duration);
//			Log.d(TAG,"Media type: "+media_type);
			postEntity.addPart("clip", new FileBody(f));
			postEntity.addPart("media_type", new StringBody(""+media_type));
			postEntity.addPart("duration",new StringBody("" + duration));
			postEntity.addPart("android_version",new StringBody(WApp.APP_VERSION));
			
			totalSize = postEntity.getContentLength();
			post.setHeader("Authorization", "access_token="+app.getAccessToken());
			post.setEntity(postEntity);
			
//			WeclipseTimer.getInstance(app).startTimer("upload_timer");
			response = httpclient.execute(post);
//			WeclipseTimer.getInstance(app).stopTimer("upload_timer");
			
			HttpEntity entity = response.getEntity();
			code = response.getStatusLine().getStatusCode();
			if (entity != null) {
				InputStream inStream = entity.getContent();
				result = WApp.convertStreamToString(inStream);
			}
		} catch (ClientProtocolException e) {
			if(WApp.DEBUG)
				e.printStackTrace();
		} catch (IOException e) {
			if(WApp.DEBUG)
				e.printStackTrace();
		}
		
		publishProgress(Utils.PROGRESS_PROCESS_FULL_OUTPUT);
		
		if(code == HTTPStatus.CREATED){
			try {
				
				//Creates the video
				Video.parse(app, new JSONObject(result)).create();
				
			} catch (JSONException e) {
				if(WApp.DEBUG){
					e.printStackTrace();
				}
			}
			WApp.deleteMediaFile(filePath);
		} else if(code == 0 || code == HTTPStatus.CLIP_NOT_PROVIDED){
			ReplyPendingCache.getInstance(app).addPending(video_id);
			app.getOfflineManager().addVideo(new PendingVideo(app,
																System.currentTimeMillis(),
																null,
																filePath,
																null,
																null,
																PendingVideo.REPLY,
																video_id,
																user_id,
																media_type,
																0,
																0,
																null,
																duration));
			Video v = new Video(app, video_id);
			v.update(DBHelper.C_VIDEO_STATUS,Video.REPLY_FAILED);
		} else if(code == HTTPStatus.BAD_REQUEST){
			WApp.deleteMediaFile(filePath);
			String endpointVideo=WApp.BASE+WApp.VIDEO;
			endpointVideo = endpoint.replace("__vid__",""+video_id);
			endpointVideo = endpoint.replace("__uid__",""+user_id);
			endpointVideo+="?access_token="+(app).getAccessToken();
			endpointVideo+="&android_version="+WApp.APP_VERSION;
			//Log.d(TAG,"Endpoint: "+endpoint);
			String resultVideo=null;
			HttpGet get = new HttpGet(endpointVideo);
			HttpResponse responseVideo;
			try {
				responseVideo = httpclient.execute(get);
				HttpEntity entity = responseVideo.getEntity();
				if(entity!=null){
					InputStream inStream = entity.getContent();
					resultVideo = WApp.convertStreamToString(inStream); 
				}
			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			//Log.d(TAG,"Result: "+result);
			try{
				
				Video.parse(app, new JSONObject(resultVideo)).updateAll();
				
			} catch(JSONException e){
				if(WApp.DEBUG){
					e.printStackTrace();
				}
			}
		}
		
		if(code == HTTPStatus.CREATED)
			publishProgress(Utils.PROGRESS_COMPLETE);
		
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
//		Log.d(TAG, "Result from server: " + result);
		app.getFeedLoadingManager().removeLoading(video_id);
		
		if(code == HTTPStatus.CREATED){
			
		} else if(code == HTTPStatus.BAD_REQUEST){
			activity.getToast().setText(R.string.toast_video_expired).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
		} else if(code == 0){
			
		}
		
		WBus.getInstance().post(new FeedUpdateEvent());
	}
	

}
