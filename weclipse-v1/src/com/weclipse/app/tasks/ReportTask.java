package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.ReportEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class ReportTask {

	public ReportTask(final WApp app,final long id){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new ReportEvent(code==HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this, "Result Report("+code+"): "+result);
			}
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"videos/"+id+"/report", SimpleTask.TYPE_POST).execute();
		
	}
	
}
