package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.AcceptFollowEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class AcceptFollowTask {
	
	public AcceptFollowTask(final WApp app,final long user_id){
	
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WLog.d(this,"Done accept folow("+code+"): "+result);
				
				WBus.getInstance().post(new AcceptFollowEvent(code == HTTPStatus.OK));
				
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
			}

			@Override
			public void onPreExecute() {
				
			}

			
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"users/"+user_id+"/accept_follow", SimpleTask.TYPE_POST).execute();
	}

}
