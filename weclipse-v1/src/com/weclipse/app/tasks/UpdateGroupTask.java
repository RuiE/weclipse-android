package com.weclipse.app.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.User;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.Utils;

import android.os.AsyncTask;

public class UpdateGroupTask extends AsyncTask<String, String, String> {

	int code;
	long group_id;
	WApp app;
	boolean avatar_changed;
	
	public UpdateGroupTask(WApp app,long group_id,boolean avatar_changed){
		this.app = app;
		this.group_id = group_id;
		this.avatar_changed = avatar_changed;
		this.execute();
	}
	
	@Override
	protected String doInBackground(String... params) {
		try {
    		code = 0;
    		String endpointGroup=WApp.BASE+WApp.GROUP;
    		endpointGroup += "?android_version="+WApp.APP_VERSION;
    		endpointGroup = endpointGroup.replace("__gid__",""+group_id);
			//Log.d(TAG,"Endpoint Connections: "+endpointGroup);
			String resultGroup=null;
			MyHttpClient httpclientGroup = new MyHttpClient(app);
			HttpGet getGroup = new HttpGet(endpointGroup);
			getGroup.setHeader("Authorization", "access_token="+app.getAccessToken());
			HttpResponse  responseGroup;
			responseGroup = httpclientGroup.execute(getGroup);
			HttpEntity entity = responseGroup.getEntity();
			code = responseGroup.getStatusLine().getStatusCode();
			if(entity!=null){
				InputStream inStream = entity.getContent();
				resultGroup = WApp.convertStreamToString(inStream); 
			}
			//Log.d(TAG,"DONE Request Group profile: "+resultGroup);
			//Log.d(TAG,"Code: "+codeGroup);
			if(code == HTTPStatus.OK){
				try {
					JSONObject group = new JSONObject(resultGroup);
					JSONArray members;
					ArrayList<Long> membs = new ArrayList<Long>();
					members = group.getJSONArray("members");
					JSONObject user;
					for(int j = 0 ; j < members.length() ; j++){
						user = members.getJSONObject(j);
						User.parse(app,user).create();
						membs.add(Long.valueOf(user.getString("id")));
					}
					
					Group g = Group.parse(app,group);
					g.create();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
