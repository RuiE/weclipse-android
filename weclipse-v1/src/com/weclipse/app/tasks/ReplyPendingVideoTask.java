package com.weclipse.app.tasks;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.weclipse.app.R;
import com.weclipse.app.WApp;
import com.weclipse.app.models.PendingVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.CustomMultiPartEntity;
import com.weclipse.app.utils.CustomMultiPartEntity.ProgressListener;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.ReplyPendingCache;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WToast;
import com.weclipse.app.utils.eventbus.ProgressReplyVideoEvent;
import com.weclipse.app.utils.eventbus.ReplyPendingVideoFinishedEvent;
import com.weclipse.app.utils.eventbus.ReplyPendingVideoStartedEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class ReplyPendingVideoTask extends AsyncTask<String, Integer, String> {
	
	public String check_pin_reply = "videos/__vid__/check_retry";

	WApp app;
	int code,codeUpload;
	PendingVideo pv;
	long time = 0 ;
	long totalSize = 0;
	
	public ReplyPendingVideoTask(WApp app,PendingVideo pv){
		this.app = app;
		this.pv = pv;
		app.getOfflineManager().setWorking(true);
		app.getFeedLoadingManager().getPendingLoadingIdsReply().add(pv.getVideoId());
		this.execute();
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		WBus.getInstance().post(new ProgressReplyVideoEvent(pv.getVideoId(), values[0]));
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		WBus.getInstance().post(new ReplyPendingVideoStartedEvent(pv.getVideoId()));
	}

	@Override
	protected String doInBackground(String... params) {
		codeUpload = 0 ;
		code = 0;
		String endpointUpload=WApp.BASE+check_pin_reply;
		endpointUpload = endpointUpload.replace("__vid__",""+pv.getVideoId());
		endpointUpload += "?android_version="+WApp.APP_VERSION;
		
		publishProgress(0);
		
		String resultUploadPin = null;
		MyHttpClient uploadPinClient = new MyHttpClient(app);
		HttpGet get = new HttpGet(endpointUpload);
		get.setHeader("Authorization", "access_token="+app.getAccessToken());
		try {
			HttpResponse response = uploadPinClient.execute(get);
			HttpEntity entity = response.getEntity();
			codeUpload = response.getStatusLine().getStatusCode();
			if(entity!=null){
				InputStream inStream = entity.getContent();
				resultUploadPin = WApp.convertStreamToString(inStream); 
			}
		} catch (ClientProtocolException e) {
			if(WApp.DEBUG)
				e.printStackTrace();
		} catch (IOException e) {
			if(WApp.DEBUG)
				e.printStackTrace();
		}
		
//		WLog.d(this,"Code CheckRetry: "+codeUpload);
		
		if(codeUpload == HTTPStatus.OK){
			processAnswer(resultUploadPin);
			ReplyPendingCache.getInstance(app).removePending(pv.getVideoId());
			WApp.deleteMediaFile(pv.getFilePath());
			pv.destroy();
		} else if(codeUpload == HTTPStatus.NOT_FOUND){
			String endpoint = WApp.BASE;
			endpoint += WApp.REPLY_CLIP;
			endpoint = endpoint.replace("__uid__", "" + pv.getUserId());
			endpoint = endpoint.replace("__vid__", "" + pv.getVideoId());
			String result = null;
			MyHttpClient httpclient = new MyHttpClient(app);
			HttpPost post = new HttpPost(endpoint);
			HttpResponse response;
			try {
				CustomMultiPartEntity postEntity = new CustomMultiPartEntity(new ProgressListener() {
					@Override
					public void transferred(long num) {
						Integer value =  (int) ((num/(float)totalSize)*100);
						if(System.currentTimeMillis()-time >= 75 || value==100){
							time = System.currentTimeMillis();
							publishProgress((int)((float)value*((float)Utils.PROGRESS_FILE_UPLOAD/(float)100)));
						}
					}
				});
				
				File f = new File(pv.getFilePath());
				
				if(f.exists()){
					postEntity.addPart("clip", new FileBody(f));
					postEntity.addPart("media_type", new StringBody(""+pv.getMediaType()));
					postEntity.addPart("duration",new StringBody("" +pv.getDuration()));
					postEntity.addPart("android_version",new StringBody(WApp.APP_VERSION));
					
					totalSize = postEntity.getContentLength();
					
					post.setEntity(postEntity);
					post.setHeader("Authorization", "access_token="+app.getAccessToken());
					response = httpclient.execute(post);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if (entity != null) {
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream);
					}
				} else {
					code = -1;
				}
				
				publishProgress(Utils.PROGRESS_PROCESS_FULL_OUTPUT);
				
				if(code == HTTPStatus.CREATED){
					processAnswer(result);
					WApp.deleteMediaFile(pv.getFilePath());
					ReplyPendingCache.getInstance(app).removePending(pv.getVideoId());
					pv.destroy();
				} else if(code == HTTPStatus.BAD_REQUEST){
					WApp.deleteMediaFile(pv.getFilePath());
//					Video v = new Video(app,pv.getVideoId());
//					v.update(DBHelper.C_USER_STATUS,Video.EXPIRED);
//					v.update(DBHelper.C_EXPIRED,Video.EXPIRED_DEFAULT);
					ReplyPendingCache.getInstance(app).removePending(pv.getVideoId());
					pv.destroy();
				} else if(code == -1) {
					pv.destroy();
				} else {
					
				}
				
			} catch (ClientProtocolException e) {
				//Client Protocol Exception, also do nothing
				if(WApp.DEBUG){
					e.printStackTrace();
				}
			} catch (IOException e) {
				//IOException, do nothing
				if(WApp.DEBUG){
					e.printStackTrace();
				}
			}
			
		} else if(codeUpload==HTTPStatus.BAD_REQUEST){
			processAnswer(resultUploadPin);
			ReplyPendingCache.getInstance(app).removePending(pv.getVideoId());
			WApp.deleteMediaFile(pv.getFilePath());
			pv.destroy();
		}
		
		if(code == HTTPStatus.CREATED)
			publishProgress(Utils.PROGRESS_COMPLETE);
		
		app.getOfflineManager().clearVideos();
		
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		app.getOfflineManager().setWorking(false);
		app.getOfflineManager().removeWorkingId(pv.getId());
		app.getFeedLoadingManager().getPendingLoadingIdsReply().remove(pv.getVideoId());
		boolean success = false;
//		Log.d(TAG, "Result from server when retrying upload: " + result);
		if(code==HTTPStatus.CREATED){
			//Do nothing, the update will update the UI
			success = true;
		} else if(code==HTTPStatus.BAD_REQUEST){
			if(app.getBaseActivity()==null){
				//do nothing
			} else {
				app.getBaseActivity().getToast().hide();
				app.getBaseActivity().getToast().setText(R.string.toast_reply_fail).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
			}
		} else if(code == 0){
			
			
		} else if(code == -1){
			//File was not found, video was not sent, do nothing
		} else if(code == HTTPStatus.UNAUTHORIZED){
			WBus.getInstance().post(new UnauthorizedEvent());
		}
		
		WBus.getInstance().post(new ReplyPendingVideoFinishedEvent(pv.getVideoId(),success));
	}

	public void processAnswer(String result){
		try {
			JSONObject jason = new JSONObject(result);
			JSONArray jarrayPlayers = jason.getJSONArray("playerslist");
			JSONObject juser;
			int user_status = 0;
			ArrayList<Long> users = new ArrayList<Long>();
			ArrayList<Integer> statuses = new ArrayList<Integer>();
			ArrayList<String> replied_ats = new ArrayList<String>();
			for (int j = 0; j < jarrayPlayers.length(); j++) {
				juser = jarrayPlayers.getJSONObject(j);
				users.add(Long.valueOf(juser.getString("id")));
				statuses.add((juser.getString("replied_at") != null && juser.getString("replied_at")!="null")?1:0);
				if(app.getId() == Long.valueOf(juser.getString("id"))){
					user_status = juser.getInt("status");
				}
				replied_ats.add(juser.getString("replied_at"));
				User.parse(app,juser).create();
			}
			
			publishProgress(Utils.PROGRESS_PROCESS_VIDEO);
			
			Video.parse(app, jason).create();
			
		} catch (JSONException e) {
			if(WApp.DEBUG){
				e.printStackTrace();
			}
		}
	}

	
}
