package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DeleteVideoEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class DeleteVideoTask {
	
	public DeleteVideoTask(final WApp app,final long vid){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				
				if(code == HTTPStatus.OK){
					TimelineVideo.destroy(app,vid);
				}
				
				WBus.getInstance().post(new DeleteVideoEvent(code == HTTPStatus.OK,vid));
				
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Responde Delete video("+code+"): "+result);
			}
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"videos/"+vid+"/delete", SimpleTask.TYPE_POST).execute();
		
	}

}
