package com.weclipse.app.tasks;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.models.CreatingVideo;
import com.weclipse.app.models.PendingVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.CustomMultiPartEntity;
import com.weclipse.app.utils.CustomMultiPartEntity.ProgressListener;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.VideoPack;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.ProgressCreateVideoEvent;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.VideoCreatedEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class CreateVideoTask extends AsyncTask<Integer, Integer, String> {
	
	int code;
	private VideoPack vp;
	private ArrayList<Long> groupIds;
	private ArrayList<Long> contactIds;
	int target;
	long timeOfCreation;
	CreatingVideo cv;
	long totalSize;
	long time;
	WApp app;

	public CreateVideoTask(WApp app,VideoPack vp,ArrayList<Long> groupIds,ArrayList<Long> contactIds,int target) {
		this.app = app;
		this.vp = vp;
		this.target = target;
		this.groupIds = groupIds;
		this.contactIds = contactIds;
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		WBus.getInstance().post(new ProgressCreateVideoEvent(cv.getId(),values[0]));
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		int max = 0;
		if(target == WApp.TARGET_FRIENDS){
			max =  contactIds.size()+1;
		} else {
			max = vp.players;
		}
		
		cv = new CreatingVideo(app.getFeedLoadingManager().generateId(),vp.caption,max,target,vp.eta);
		app.getFeedLoadingManager().getCreatingVideos().add(cv);
	}

	@Override
	protected String doInBackground(Integer... params) {
		
		timeOfCreation = System.currentTimeMillis();
		
		String endpoint = WApp.BASE+WApp.CREATE_CLIP;
		endpoint = endpoint.replace("__uid__", "" + app.getUID());
		code = 0;
		String result = null;
		MyHttpClient httpclient = new MyHttpClient(app);
		HttpPost post = new HttpPost(endpoint);
		HttpResponse response;
		
		try {
			CustomMultiPartEntity postEntity = new CustomMultiPartEntity(new ProgressListener() {
				@Override
				public void transferred(long num) {
					int progress_val = (int) ((num/(float)totalSize)*100);
					if(System.currentTimeMillis()-time >= 75 || progress_val == 100){
						time = System.currentTimeMillis();
						publishProgress((int)((float)progress_val*((float)Utils.PROGRESS_FILE_UPLOAD/(float)100)));
					}
				}
			});
			
			File f = new File(vp.filePath);
			postEntity.addPart("clip", new FileBody(f));
			postEntity.addPart("media_type", new StringBody("" +vp.media_type));
			postEntity.addPart("duration", new StringBody(""+ vp.duration));
			postEntity.addPart("android_version",new StringBody(WApp.APP_VERSION));
			postEntity.addPart("title", new StringBody("" + vp.caption,Charset.forName("UTF-8")));
			postEntity.addPart("nr_players",new StringBody("" + vp.players));
			postEntity.addPart("upload_pin",new StringBody(""+timeOfCreation));
			postEntity.addPart("eta",new StringBody(vp.eta));
			postEntity.addPart("target", new StringBody("" + target));
			
			if(vp.private_video == Video.PRIVATE)
				postEntity.addPart("private",new StringBody(""+Video.PRIVATE));
			
			JSONArray jarrayGroups = new JSONArray();
			JSONArray jarrayContacts = new JSONArray();
			
			for (int i = 0; i < groupIds.size(); i++)
				jarrayGroups.put(groupIds.get(i));
			
			for (int i = 0; i < contactIds.size(); i++)
				jarrayContacts.put(contactIds.get(i));
			
			if(target == WApp.TARGET_BOTH){
				postEntity.addPart("user_ids", new StringBody(""+ jarrayContacts.toString()));
				postEntity.addPart("group_ids",new StringBody("" + jarrayGroups.toString()));
			} else if (target == WApp.TARGET_FRIENDS) {	
				postEntity.addPart("user_ids", new StringBody(""+ jarrayContacts.toString()));
			} else if (target == WApp.TARGET_GROUPS) {
				postEntity.addPart("group_ids",new StringBody("" + jarrayGroups.toString()));
			}
			
			totalSize = postEntity.getContentLength();
			post.setHeader("Authorization","access_token="+app.getAccessToken());
			post.setEntity(postEntity);

			response = httpclient.execute(post);
			
			HttpEntity entity = response.getEntity();
			code = response.getStatusLine().getStatusCode();
			if (entity != null) {
				InputStream inStream = entity.getContent();
				result = WApp.convertStreamToString(inStream);
			}
			
		} catch (ClientProtocolException e) {
			if(WApp.DEBUG){
				e.printStackTrace();
			}
		} catch (IOException e) {
			if(WApp.DEBUG){
				e.printStackTrace();
			}
		}
		
		publishProgress(Utils.PROGRESS_PROCESS_FULL_OUTPUT);
		
		WLog.d(this, "-------------------- --------------------");
		WLog.d(this, "Creating Video Response!!");
		WLog.d(this, "Code: "+code);
		WLog.d(this, "Result from the post of the video:"+result);
		WLog.d(this, "-------------------- --------------------");
		
		if(code == HTTPStatus.CREATED){
			try {
				if(target==WApp.TARGET_GROUPS || target==WApp.TARGET_BOTH){
					JSONArray jArray = new JSONArray(result);
					JSONObject jason;
					
					int total = jArray.length();
					
					for(int i = 0 ; i < jArray.length() ; i++){
						jason = jArray.getJSONObject(i);
						JSONArray jarrayPlayers = jason.getJSONArray("playerslist");
						JSONObject juser;
						ArrayList<Long> users = new ArrayList<Long>();
						ArrayList<Integer> statuses = new ArrayList<Integer>();
						ArrayList<String> replied_ats = new ArrayList<String>();
						for (int j = 0; j < jarrayPlayers.length(); j++) {
							juser = jarrayPlayers.getJSONObject(j);
							users.add(Long.valueOf(juser.getString("id")));
							statuses.add((juser.getString("replied_at")!=null && juser.getString("replied_at")!="null")?1:0);
							replied_ats.add(juser.getString("replied_at"));
							User.parse(app,juser).create();
						}
						
						publishProgress((int)((Utils.PROGRESS_PROCESS_FULL_OUTPUT)+((float)((2*Utils.PROGRESS_COMPLETE-Utils.PROGRESS_PROCESS_FULL_OUTPUT-Utils.PROGRESS_PROCESS_VIDEO)/total)*(float)i)));
						
						Video.parse(app, jason).create();
					}
				} else {
					Video.parse(app, new JSONObject(result)).create();
				}
			} catch (JSONException e) {
				if(WApp.DEBUG)
					e.printStackTrace();	
			}
			
			WApp.deleteMediaFile(vp.filePath);
//			lastFile = null;
		} else if(code == 0 || code == HTTPStatus.CLIP_NOT_PROVIDED){
			app.getOfflineManager().addVideo(new PendingVideo(app,
					timeOfCreation,
					vp.caption,
					vp.filePath,
					contactIds,
					groupIds,
					PendingVideo.CREATE,
					0,
					app.getId(),
					vp.media_type,
					target,
					app.getDefaultPlayers(),
					vp.eta,
					vp.duration));

		} else if(code == HTTPStatus.BAD_REQUEST){
			WApp.deleteMediaFile(vp.filePath);
		}

		publishProgress(Utils.PROGRESS_COMPLETE);
		
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
//				Log.d(TAG, "Result from server: " + result);
		
		app.getFeedLoadingManager().removeCreating(cv);
		
		//TODO clear header create videos
//		if(mStackManager.getCurrentTag().equals(FEED_TAG)){
//			Fragment f = getSupportFragmentManager().findFragmentByTag(FEED_TAG);
//			((FeedsFragment) f).clearHeaderCreateVideos();
//		}
		
		if(code == HTTPStatus.CREATED){
//			updateFeedLV();
		} else if(code == HTTPStatus.BAD_REQUEST){
			WLog.d(this,"Create Video Taks: Bad request bruv");
//			getToast().setText(R.string.toast_video_expired).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
		} else if(code == HTTPStatus.UNAUTHORIZED){
			WBus.getInstance().post(new UnauthorizedEvent());
		} else if(code == 0){
//			updateFeedHeader();
//					if(!app.isInternetConnected()){
//	        			getToast().showNoInternet();
//	        		} else {
//	        			
//	        			getToast().showNoServer();
//	        		}
		} else {
//					Log.d(TAG,"-------------------"+code);
//			getToast().setText(R.string.toast_create_fail).setBackground(R.color.weclipse_red_75).show(WToast.MEDIUM);
		}
		
		WBus.getInstance().post(new VideoCreatedEvent(code == HTTPStatus.CREATED));
		
	}

}
