package com.weclipse.app.tasks;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Comment;
import com.weclipse.app.models.OfflineAction;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.MarkViewedEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class MarkViewedTask {

	WApp app;
	int code;
	AbstractVideo v;
	
	public MarkViewedTask(final WApp app,final AbstractVideo v){
		this.app = app;
		this.v = v;
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new MarkViewedEvent());
			}
			
			@Override
			public void backgroundPreExecute() {
				v.update(DBHelper.C_VIDEO_STATUS, Video.VIEWED);
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
				WLog.d(this,"Result("+code+") "+result);
				
				if(code == HTTPStatus.OK){
					
					try {
						JSONObject stats = new JSONObject(result);
						v.updateStats(stats.getInt("nr_likes"), stats.getInt("nr_views"), stats.getInt("nr_comments"),0);	
					} catch(JSONException e){
						e.printStackTrace();
					}
					
				} else if(code == HTTPStatus.BAD_REQUEST){
//					Log.d(TAG,"NOT Marked as viewed");
				} else {
					app.getOfflineManager().addAction(new OfflineAction(app,v.getId(),OfflineAction.VIDEO_VIEWED));
				}
				
				
			}
		};
		
		new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"videos/"+v.getId()+"/viewed",SimpleTask.TYPE_POST).execute();
		
	}
	
}
