package com.weclipse.app.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.MyLike;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.LikedVideoEvent;
import com.weclipse.app.utils.eventbus.TimelineUpdateEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class LikeVideoTask {
	
	public LikeVideoTask(final WApp app,final AbstractVideo video,final int source){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				
				if(code == HTTPStatus.OK){
					WBus.getInstance().post(new LikedVideoEvent(video.getId()));
				}
				
				WBus.getInstance().post(new TimelineUpdateEvent());
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Resultado("+code+"): "+result);
				if(code == HTTPStatus.OK){
					
					String table ="";
					
					switch (source) {
					case AbstractVideo.SOURCE_DETAIL:
						table = DBHelper.TABLE_LIKES_ACTIVITY;
						break;
					case AbstractVideo.SOURCE_PROFILE:
						table = DBHelper.TABLE_LIKES_MY_VIDEOS;
						break;
					case AbstractVideo.SOURCE_TIMELINE:
						table = DBHelper.TABLE_LIKES_TIMELINE;
						break;
					}
					
					if(source != 0){
						new MyLike(app, video.getId(),table).create();
					}
					
					try {
						JSONObject stats = new JSONObject(result);
						
						video.updateStats(stats.getInt("nr_likes"), stats.getInt("nr_views"), stats.getInt("nr_comments"),source);
						
					} catch(JSONException e){
						e.printStackTrace();
					}
				}
			}
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"videos/"+video.getId()+"/like", SimpleTask.TYPE_POST).execute();
		
	}

}
