package com.weclipse.app.tasks;

import java.util.HashMap;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.GCMManager;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.WLog;

public class RegisterDeviceTask {

	public RegisterDeviceTask(final WApp app){
		
		WLog.d(this,"Registering device i guess");
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				// TODO Auto-generated method stub
				WLog.d(this,"Did it("+code+"): "+result);
			}
		};
		
		HashMap<String, String> mParams = new HashMap<String,String>();
		mParams.put("type","1");
		mParams.put("device_id",GCMManager.getInstance(app).getRegistrationId(app));
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"register_device", SimpleTask.TYPE_POST, mParams).execute();
	}
	
}
