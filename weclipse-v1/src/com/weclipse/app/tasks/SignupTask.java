package com.weclipse.app.tasks;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.weclipse.app.WApp;
import com.weclipse.app.managers.GCMManager;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.models.Friend;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.MyLike;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.UserActivity;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.tasks.fetch.FetchTimelineTask;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.LoginEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class SignupTask {

	public SignupTask(final WApp app,final String name,final String username,final String email,final String pass){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
//				getToast().setText(R.string.toast_signingup).setBackground(R.color.dark_grey_75).show();
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				// TODO Auto-generated method stub
				int type = 0;

				if(code == HTTPStatus.CREATED){
					type = LoginEvent.TYPE_SUCCESS;
				} else if(code == HTTPStatus.UNAUTHORIZED){
					type = LoginEvent.TYPE_UNAUTHTORIZED;
				} else if(code == 0){
				} else if(code == HTTPStatus.PASSWORD_TOO_MANY_TIMES){
					type = LoginEvent.TYPE_TOO_MANY_TIMES;
				}
				
				WBus.getInstance().post(new LoginEvent(type));	
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				// TODO Auto-generated method stub
				WLog.d(this,"Restultado signup("+code+"): "+result);
				if(code == HTTPStatus.CREATED){
					JSONObject json;
					try {
						json = new JSONObject(result);
						JSONObject user = json.getJSONObject("user");
						app.putStringDetail("access_token",user.getString("access_token"));
						
						long fb_id = 0;
						String facebook_id = user.getString("facebook_id");
						if(facebook_id == null || facebook_id.equals("null")){
							
						} else {
							Long.valueOf(facebook_id);
						}
						
						String phone_number = user.getString("phone_number");
						
						PreferenceManager.getInstance(app).putPreference(PreferenceManager.FACEBOOK, !(fb_id == 0));
						PreferenceManager.getInstance(app).putPreference(PreferenceManager.HAS_PHONE, !(phone_number == null || phone_number.equals("null")));
						PreferenceManager.getInstance(app).putPreference(PreferenceManager.PHONE_CODE, user.getString("phone_code"));

						new Me(app, user.getString("username"),
								user.getString("name"),
								user.getString("email"),
								phone_number,
								user.getString("phone_code"),
								Long.valueOf(user.getString("id")),
								user.getString("avatar_url"),
								user.getInt("nr_videos"),
								user.getInt("nr_friends"),
								user.getInt("nr_followers"),
								user.getInt("nr_following"),
								user.getInt("nr_groups"),
								user.getBoolean("private") ? 1 : 0).create();

						JSONArray friends = json.getJSONArray("friends");
						JSONArray groups = json.getJSONArray("groups");
						JSONArray timeline = json.getJSONArray("timeline");
						JSONObject activity = json.getJSONObject("activity");

						ArrayList<User> mUsers = new ArrayList<User>();
						ArrayList<Long> mFriends = new ArrayList<Long>();
						ArrayList<Group> mGroups = new ArrayList<Group>();
						ArrayList<UserActivity> mActivities = new ArrayList<UserActivity>();
						ArrayList<TimelineVideo> mTimeline = new ArrayList<TimelineVideo>();
						ArrayList<MyLike> mLikes = new ArrayList<MyLike>();

						for (int i = 0; i < friends.length(); i++) {
							User u = User.parse(app, friends.getJSONObject(i));
							mUsers.add(u);
							mFriends.add(u.getId());
						}
						
						mUsers.add(Me.getInstance(app).getUser());
						
						User.bulkInsert(app, mUsers);
						

						for (int i = 0; i < groups.length(); i++) {
							mGroups.add(Group.parse(app,groups.getJSONObject(i)));
						}

						for (int i = 0; i < timeline.length(); i++) {
							mTimeline.add(TimelineVideo.parse(app,timeline.getJSONObject(i)));
						}
						
						JSONArray activities = activity.getJSONArray("activities");
						JSONArray video_likes = activity.getJSONArray("video_likes");

						for (int i = 0; i < activities.length(); i++) {
							mActivities.add(UserActivity.parse(app,activities.getJSONObject(i)));
						}

						// Sacando os likes
						if (video_likes.length() > 0) {
							for (int i = 0; i < video_likes.length(); i++) {
								mLikes.add(new MyLike(app, Long.valueOf(video_likes.getString(i)),DBHelper.TABLE_LIKES_ACTIVITY));
							}
						}

//						JSONObject temp;
//						for (int j = 0; j < statuses.length(); j++) {
//							temp = statuses.getJSONObject(j);
//							mStatuses.put(Long.valueOf(temp.getString("id")),temp.getInt("status"));
//						}

						UserActivity.bulkInsert(app, mActivities,FetchTimelineTask.FRESH);
						MyLike.bulkInsert(app, mLikes, FetchTimelineTask.FRESH,DBHelper.TABLE_LIKES_ACTIVITY);
						Friend.bulkInsert(app, mFriends);
						Group.bulkInsertGroups(app, mGroups);
						TimelineVideo.bulkInsert(app, mTimeline,FetchTimelineTask.FRESH,TimelineVideo.TIMELINE);
					
//					try {
//		        		JSONObject me = new JSONObject(result);
//		        		app.putStringDetail("access_token",me.getString("access_token"));
//						
//						new Me(app,
//								me.getString("username"),
//								me.getString("name"),
//								me.getString("email"),
//								null,
//								null,
//								Long.valueOf(me.getString("id")), 
//								me.getString("avatar_url"),
//								0,
//								0,
//								0,
//								0,
//								0,
//								User.PUBLIC).create();
						
						if (GCMManager.getInstance(app).checkPlayServices()) {
							GCMManager.getInstance(app).gcm = GoogleCloudMessaging.getInstance(app);
							GCMManager.getInstance(app).regid = GCMManager.getInstance(app).getRegistrationId(app);
				            if(GCMManager.getInstance(app).regid.isEmpty()) {//register and send
				            	GCMManager.getInstance(app).registerInBackground();
				            } else {
				            	new RegisterDeviceTask(app);
				            }
			    		} else {
				            WLog.i(this, "No valid Google Play Services APK found.");
				        }
						
					} catch(JSONException e){
						if(WApp.DEBUG)
							e.printStackTrace();
					}
				}
			}
		};
		
		HashMap<String, String> mParams = new HashMap<String, String>();
		mParams.put("name",name);
		mParams.put("username", username);
		mParams.put("email", email);
		mParams.put("password",pass);
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"users",SimpleTask.TYPE_POST,mParams).execute();
		
		
	}
	
}
