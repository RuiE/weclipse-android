package com.weclipse.app.tasks;

import java.util.HashMap;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.RequestSMSEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class RequestSMSCodeTask {
	
	public RequestSMSCodeTask(final WApp app,final String code,final String phone){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new RequestSMSEvent(code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
//				WLog.d(this, "Resultado do request sms("+code+"): "+result);
				WLog.d(this, "Resultado do request sms("+code+"): ");
			}
		};
		
		HashMap<String, String> mParams = new HashMap<String, String>();
		mParams.put("phone_code" , code);
		mParams.put("phone_number" , phone);
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"request_sms", SimpleTask.TYPE_POST,mParams).execute();
	}

}
