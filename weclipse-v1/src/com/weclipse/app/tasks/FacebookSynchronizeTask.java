package com.weclipse.app.tasks;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchFacebookFriendsEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FacebookSynchronizeTask {

	public FacebookSynchronizeTask(final WApp app,final String token){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				
				ArrayList<User> mUsers = new ArrayList<User>();

				if(code == HTTPStatus.OK){
					try {
						 
						 JSONArray users = new JSONArray(result);
						 JSONObject juser;
						 
						 
						 
						 for(int i = 0 ; i < users.length() ; i++ ){
							 juser = users.getJSONObject(i);
							 
							 mUsers.add(new User(app,
									 				Long.valueOf(juser.getString("id")),
									 				juser.getString("username"),
									 				juser.getString("name"),
									 				juser.getString("avatar_url"),
									 				juser.getInt("follow_status"),
									 				juser.getBoolean("private")));
						 }
						 
						 WLog.d(this,"Posting event...(FetchFacebookFriends)");
						 
						 WLog.d(this,"Putting FB a true");
						 
					} catch(JSONException e){
						if(WApp.DEBUG){
							e.printStackTrace();
						}
					}
					
					WBus.getInstance().post(new FetchFacebookFriendsEvent(mUsers,code == HTTPStatus.OK));
					
				}
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
				if(code == HTTPStatus.OK){
					PreferenceManager.getInstance(app).putPreference(PreferenceManager.FACEBOOK, true);
				}
				WLog.d(this,"Result sync fb("+code+"): "+result);
			}
		};
		
		HashMap<String, String> mParams = new HashMap<String,String>();
		mParams.put("fb_access_token",""+token);
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"sync_fb", SimpleTask.TYPE_POST,mParams).execute();
		
	}
	
}
