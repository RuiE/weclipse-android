package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Group;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.eventbus.GroupLeaveEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class LeaveGroupTask {
	
	public LeaveGroupTask(final WApp app,final Group g){
		
		SimpleTaskHelper mTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new GroupLeaveEvent(code == HTTPStatus.OK,g.getId()));
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
			}

			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
			}
		};
		
		new SimpleTask(app,mTaskHelper,WApp.BASE+"groups/"+g.getId()+"/leave",SimpleTask.TYPE_POST).execute();
	}
	
}
