package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.UnmergeContactsEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class UnmergeContactsTask {
	
	public UnmergeContactsTask(final WApp app){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new UnmergeContactsEvent(code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Result unlink contacts("+code+"): "+result);
				if(code == HTTPStatus.OK){
					PreferenceManager.getInstance(app).putPreference(PreferenceManager.HAS_PHONE,false);
					PreferenceManager.getInstance(app).putPreference(PreferenceManager.PHONE_CODE, "");
				}
			}
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"unlink_contact", SimpleTask.TYPE_POST).execute();
		
	}

}
