package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.CacheRequestManager;
import com.weclipse.app.models.MyLike;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.UserActivity;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.MyHeader;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.ActivityUpdateEvent;
import com.weclipse.app.utils.eventbus.ProfileUpdateEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchWallTask {

	ProfileUpdateEvent event;
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;

	public FetchWallTask(final WApp app, final long uid,final int method,final long last_video_id) {

		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {

			@Override
			public void mainPostExecute(int code, String result) {

				WBus.getInstance().post(event);

			}

			@Override
			public void backgroundPreExecute() {

			}

			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this, "Reuslt FetchWall(" + code + "): ");
//				 WLog.d(this,"Result FeetchWall("+code+"): "+result);

				
				if(code == HTTPStatus.OK){
					try {
	
						JSONObject wall = new JSONObject(result);
	
						ArrayList<TimelineVideo> videos = new ArrayList<TimelineVideo>();
						ArrayList<Long> likes = new ArrayList<Long>();
	
						JSONArray jarray_vids = wall.getJSONArray("videos");
	
						for (int i = 0; i < wall.getJSONArray("videos").length(); i++) {
							videos.add(TimelineVideo.parse(app, jarray_vids.getJSONObject(i)));
							if (jarray_vids.getJSONObject(i).getInt("liked") == 1) {
								likes.add(Long.valueOf(jarray_vids.getJSONObject(i).getString("id")));
							}
						}
	
						event = new ProfileUpdateEvent(code == HTTPStatus.OK,
								videos,
								likes,
								uid,
								wall.getInt("nr_friends"),
								wall.getInt("nr_followers"),
								wall.getInt("nr_following"),
								wall.getString("name"),
								wall.getString("username"),
								wall.getInt("nr_videos"),
								wall.getBoolean("private"),
								wall.getString("avatar_url"),
								wall.getInt("follow_status"),
								code);
						
//						User u = new User(app, uid, event.getUsername(), event.getName(), event.getAvatar_url(), event.getFollowing_status(), event.getPrivate());
//						u.create();
	
					} catch (JSONException e) {
						if (WApp.DEBUG)
							e.printStackTrace();
					}
				} else {
					event = new ProfileUpdateEvent(code == HTTPStatus.OK,
													null,
													null, 
													0,
													0,
													0,
													0,
													null,
													null,
													0,
													true,
													null,
													0,
													code);
				}
			}

			@Override
			public void onPreExecute() {

			}

		};

		if(method == LOAD_MORE){
			HashMap<String, String> mParams = new HashMap<String,String>();
			mParams.put("before_id",""+last_video_id);
			
			new SimpleTask(app, mSimpleTaskHelper, WApp.BASE + "users/" + uid, SimpleTask.TYPE_GET,mParams).execute();
		} else {
			new SimpleTask(app, mSimpleTaskHelper, WApp.BASE + "users/" + uid, SimpleTask.TYPE_GET).execute();
		}
		
		
		

	}

}
