package com.weclipse.app.tasks.fetch;

import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Video;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchVideoEvent;
import com.weclipse.app.utils.eventbus.WBus;


public class FetchVideoTask {
	
	public FetchVideoTask(final WApp app,final long vid){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
//				WLog.d(this,"Result("+code+"): "+result);
				try {
					Video v = Video.parse(app,new JSONObject(result));
					v.create();
					WBus.getInstance().post(new FetchVideoEvent(v));
					
				} catch (JSONException e){
					if(WApp.DEBUG)
						e.printStackTrace();
				}
			}
			
			@Override
			public void backgroundPreExecute() {
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Video JSON("+code+"): ");
//				WLog.d(this,"Video JSON("+code+"): "+result);
			}

		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"videos/"+vid, SimpleTask.TYPE_GET).execute();
		
	}

}
