package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchGroupsEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchGroupsTask {

	public static int FRESH = 0;
	public static int LOAD_MORE = 1;
	
	ArrayList<Group> mGroups;
	
	public FetchGroupsTask(final WApp app,final int method,final long before_id){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new FetchGroupsEvent(mGroups,code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub	
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Groups("+code+"): ");
//				WLog.d(this,"Groups("+code+"): "+result);
				
				if(code == HTTPStatus.OK){
					try {
						mGroups = new ArrayList<Group>();
						
						JSONArray groups = new JSONArray(result);
						
						for(int i = 0 ; i < groups.length() ; i++){
							Group g = Group.parse(app,groups.getJSONObject(i));
							g.create();
							mGroups.add(g);
							
							JSONArray members = groups.getJSONObject(i).getJSONArray("members");
							for(int j = 0 ; j < members.length() ; j++ ){
								UserCache.getInstance(app).updateUser(User.parse(app,members.getJSONObject(j)));
							}
							
						}
						
						Me.getInstance(app).setN_groups(mGroups.size());
						Me.getInstance(app).save();
						
						Group.bulkInsertGroups(app, mGroups);
						
					} catch(JSONException e){
						if(WApp.DEBUG)
							e.printStackTrace();
					}
				}
			}

		};
		
		if(method == LOAD_MORE){
			HashMap<String, String> mParams = new HashMap<String,String>();
			mParams.put("before_id",""+before_id);
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"groups",SimpleTask.TYPE_GET,mParams).execute();
		} else {
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"groups",SimpleTask.TYPE_GET).execute();
		}
		
	}
	
}
