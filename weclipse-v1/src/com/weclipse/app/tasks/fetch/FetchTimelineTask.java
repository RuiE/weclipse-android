package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.CacheRequestManager;
import com.weclipse.app.models.MyLike;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.CacheHelper;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.MyHeader;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchTimelineEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchTimelineTask {
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;
	
	public FetchTimelineTask(final WApp app,final int method){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new FetchTimelineEvent(code == HTTPStatus.OK,method));
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
				WLog.d(this,"Result fetch timeline("+code+")");
//				WLog.d(this,"Result fetch timeline("+code+"): "+result);
				
				if(code == HTTPStatus.OK){
					try {
						JSONArray jarray = new JSONArray(result);
						
						ArrayList<TimelineVideo> timelines = new ArrayList<TimelineVideo>();
						ArrayList<MyLike> likes = new ArrayList<MyLike>();
						
//						WLog.d(this,"Devolvendo "+jarray.length()+" novos registos");
						for(int i = 0 ; i < jarray.length() ; i++){
							timelines.add(TimelineVideo.parse(app, jarray.getJSONObject(i)));
							if(jarray.getJSONObject(i).getInt("liked") == 1){
								likes.add(new MyLike(app, Long.valueOf(jarray.getJSONObject(i).getString("id")), DBHelper.TABLE_LIKES_TIMELINE));
							}
							
						}
						
						if(timelines.size() > 0 ){
							TimelineVideo.bulkInsert(app, timelines, method,TimelineVideo.TIMELINE);
						} else {
							
							if(method == FRESH)
								app.getDB().clearTimeline();
						}
						
						if(likes.size() > 0 ){
							MyLike.bulkInsert(app, likes, method,DBHelper.TABLE_LIKES_TIMELINE);
						} else {
							
							if(method == FRESH)
								app.getDB().clearLikesTimeline();
							
						}
						
					} catch (JSONException e){
						if(WApp.DEBUG)
							e.printStackTrace();
					}
					
		        }
			}

			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
			}

		};
		
		CacheHelper mCacheHelper = new CacheHelper() {
			
			@Override
			public void updateTag(String tag) {
				CacheRequestManager.getInstance(app).putKey(CacheRequestManager.REQUEST_KEY_TIMELINE, tag);
			}
		};
		
		if(method == LOAD_MORE){
			HashMap<String, String> mParams = new HashMap<String,String>();
			
			String before_id = TimelineVideo.getLastId(app);
			
			mParams.put("before_id",before_id);
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"timeline",SimpleTask.TYPE_GET,mParams).execute();
		} else {
			MyHeader h = new MyHeader("If-None-Match", CacheRequestManager.getInstance(app).getValue(CacheRequestManager.REQUEST_KEY_TIMELINE));
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"timeline",SimpleTask.TYPE_GET,mCacheHelper,h).execute();
		}
		
		
	}

}
