package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchFollowingEvent;
import com.weclipse.app.utils.eventbus.FetchFriendsEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchFollowingTask {
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;
	
	public FetchFollowingTask(final WApp app,final long user_id,final int method,final long before_id){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				
//				WBus.getInstance().post(new FetchLikeEvent(video.getId(),likes));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
//				WLog.d(this, "Done Following: "+code+" :"+result);
				WLog.d(this, "Done Following: "+code);
				
				try {
					JSONArray jarray = new JSONArray(result);
					
					ArrayList<User> users = new ArrayList<User>();
					ArrayList<Integer> statuses = new ArrayList<Integer>();
					
					for(int i = 0 ; i < jarray.length() ; i++ ){
						users.add(User.parse(app, jarray.getJSONObject(i)));
					}
				
					WBus.getInstance().post(new FetchFollowingEvent(user_id, users,code == HTTPStatus.OK));
					
				} catch(JSONException e){
					if(WApp.DEBUG)
						e.printStackTrace();
				}
			}

			@Override
			public void onPreExecute() {
				
			}

		};
		
		if(method == LOAD_MORE){
			HashMap<String, String> mParams = new HashMap<String,String>();
			mParams.put("before_id",""+before_id);
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"users/"+user_id+"/following",SimpleTask.TYPE_GET,mParams).execute();
		} else {
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"users/"+user_id+"/following",SimpleTask.TYPE_GET).execute();
		}
		
	}

}
