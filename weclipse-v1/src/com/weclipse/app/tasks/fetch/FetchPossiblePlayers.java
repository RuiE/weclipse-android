package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Friend;
import com.weclipse.app.models.Group;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchPossiblePlayersEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchPossiblePlayers {
	
	public FetchPossiblePlayers(final WApp app){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Resultado possible players("+code+"): "+result);
				
				try {
					JSONObject jarray = new JSONObject(result);
					
					ArrayList<Long> mFriends = new ArrayList<Long>();
					JSONArray friends = jarray.getJSONArray("friends");
					JSONArray groups = jarray.getJSONArray("groups");
					
					for(int i = 0 ; i < friends.length() ; i++){
						mFriends.add(Long.valueOf(friends.getJSONObject(i).getString("id")));
						User.parse(app, friends.getJSONObject(i)).create();
					}
					
					for(int i = 0 ; i < groups.length() ; i++){
						Group.parse(app, groups.getJSONObject(i)).create();
					}
					
					Friend.bulkInsert(app,mFriends);
					
				} catch (JSONException e) {
					if(WApp.DEBUG)
						e.printStackTrace();
				}
				
				WBus.getInstance().post(new FetchPossiblePlayersEvent(code == HTTPStatus.OK));
			}

		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"possible_players", SimpleTask.TYPE_GET).execute();
	}

}
