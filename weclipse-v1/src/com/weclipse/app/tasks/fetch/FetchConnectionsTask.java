package com.weclipse.app.tasks.fetch;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.eventbus.RefreshActionBarEvent;
import com.weclipse.app.utils.eventbus.UpdateFriendsEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchConnectionsTask {
	
	int code;
	WApp app;
	
	public FetchConnectionsTask(final WApp app){
		this.app = app;
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				WBus.getInstance().post(new RefreshActionBarEvent(true));
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				if(code==HTTPStatus.OK){
					WBus.getInstance().post(new UpdateFriendsEvent());
				}
				
				WBus.getInstance().post(new RefreshActionBarEvent(false));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				//TODO Fetch connections
//				if(code==HTTPStatus.OK){
//					
//					WLog.d(this,"Result: "+result);
//					
//					ArrayList<Contact> contactsToInsert = new ArrayList<Contact>();
//					ArrayList<Contact> contactsToUpdate = new ArrayList<Contact>();
//					
//					try {
//						JSONArray jasonArray = new JSONArray(result);
//						JSONObject user;
//						for(int n=0;n<jasonArray.length();n++){
//							user = jasonArray.getJSONObject(n);
//							Contact c = new Contact(app,
//									Long.valueOf(user.getString("id")),
//									null,
//									null,
//									user.getString("username"),
//									user.getInt("avatar"),
//									user.getInt("status"));
//							
//							if(c.exists()){
//								contactsToUpdate.add(c);
//							} else {
//								contactsToInsert.add(c);
//							}
//							
//						}
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
//					
//					if(contactsToInsert.size()>0){
//						app.getDB().bulkInsertContacts(contactsToInsert);
//					}
//					
//					if(contactsToUpdate.size()>0){
//						app.getDB().bulkUpdateContacts(contactsToUpdate);
//					}
//					
//				}
			}

		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"connections" , SimpleTask.TYPE_GET).execute();
		
	}
}
