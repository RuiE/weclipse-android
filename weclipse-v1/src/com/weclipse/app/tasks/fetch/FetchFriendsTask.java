package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Friend;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchFriendsEvent;
import com.weclipse.app.utils.eventbus.FetchLikeEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchFriendsTask {
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;
	
	public FetchFriendsTask(final WApp app,final long user_id,final int method,final long before_id){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
				WLog.d(this, "Done Friends: "+code+" :");
//				WLog.d(this, "Done Friends: "+code+" :"+result);
				ArrayList<User> users = new ArrayList<User>();
				
				if(code == HTTPStatus.OK){
					
					try {
						JSONArray jarray = new JSONArray(result);
						ArrayList<Long> friends = new ArrayList<Long>();
						
						for(int i = 0 ; i < jarray.length() ; i++ ){
							users.add(User.parse(app, jarray.getJSONObject(i)));
							friends.add(users.get(i).getId());
						}
					
						Friend.bulkInsert(app,friends);
						
					} catch(JSONException e){
						if(WApp.DEBUG)
							e.printStackTrace();
					}
					
				}
				
				WBus.getInstance().post(new FetchFriendsEvent(user_id, users, code==HTTPStatus.OK));
				
			}

			@Override
			public void onPreExecute() {
				
			}

			
		};
		
		if(method == LOAD_MORE){
			HashMap<String, String> mParams = new HashMap<String,String>();
			mParams.put("before_id",""+before_id);
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"users/"+user_id+"/friends",SimpleTask.TYPE_GET,mParams).execute();
		} else {
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"users/"+user_id+"/friends",SimpleTask.TYPE_GET).execute();
		}
		
	}

}
