package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.CacheRequestManager;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.MyLike;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.CacheHelper;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.MyHeader;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchMyVideosEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchMyVideosTask {
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;
	
	public FetchMyVideosTask(final WApp app,final int method){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new FetchMyVideosEvent(code == HTTPStatus.OK,method));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
				WLog.d(this,"Acabou Fetch My Videos Task: "+code);
//				WLog.d(this,"Acabou("+code+"): "+result);
				
				if(code == HTTPStatus.OK){
					try {
					
						JSONObject json = new JSONObject(result);
						
						//Update my fields ye
						Me m = Me.getInstance(app);
						
						m.setN_followers(json.getInt("nr_followers"));
						m.setN_following(json.getInt("nr_following"));
						m.setN_friends(json.getInt("nr_friends"));
						m.setN_videos(json.getInt("nr_videos"));
						m.setN_groups(json.getInt("nr_groups"));
						
						m.save();
						
						JSONArray videos = json.getJSONArray("videos");
						
						ArrayList<TimelineVideo> mTimelines = new ArrayList<TimelineVideo>();
						ArrayList<MyLike> mLikes = new ArrayList<MyLike>();
						
						for(int i = 0 ; i < videos.length() ; i++ ){
							mTimelines.add(TimelineVideo.parse(app, videos.getJSONObject(i)));
							if(videos.getJSONObject(i).getInt("liked") == 1){
								mLikes.add(new MyLike(app, Long.valueOf(videos.getJSONObject(i).getString("id")), DBHelper.TABLE_LIKES_MY_VIDEOS));
							}
						}
						
						if(mTimelines.size() > 0){
							TimelineVideo.bulkInsert(app, mTimelines,method,TimelineVideo.TIMELINE_MY_VIDEOS);
						} else {
							if(method == FRESH){
								app.getDB().clearMyVideos();
							}
						}
						
						if(mLikes.size() > 0)
							MyLike.bulkInsert(app, mLikes, method, DBHelper.TABLE_LIKES_MY_VIDEOS);
						
					} catch (JSONException e){
						if(WApp.DEBUG){
							e.printStackTrace();
						}
					}
				} else {
					
				}
				
			}

			@Override
			public void onPreExecute() {
				
			}

			
		};
		
		CacheHelper mCacheHelper = new CacheHelper() {
			
			@Override
			public void updateTag(String tag) {
				WLog.d(this,"gravar no profile: "+tag);
				CacheRequestManager.getInstance(app).putKey(CacheRequestManager.REQUEST_KEY_PROFILE, tag);
			}
		};
		
		if(method == LOAD_MORE){
			HashMap<String, String> mParams = new HashMap<String,String>();
			mParams.put("before_id",TimelineVideo.getLastIdMyVideos(app));
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"profile/",SimpleTask.TYPE_GET,mParams).execute();
		} else {
			
			MyHeader h = new MyHeader("If-None-Match", CacheRequestManager.getInstance(app).getValue(CacheRequestManager.REQUEST_KEY_PROFILE));
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"profile/",SimpleTask.TYPE_GET,mCacheHelper,h).execute();
		}
		
	}

}
