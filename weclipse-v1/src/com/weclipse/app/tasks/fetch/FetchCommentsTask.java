package com.weclipse.app.tasks.fetch;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Comment;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.NewCommentEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchCommentsTask extends AsyncTask<String, String, String> {
	
	public static int SOURCE_PUSH = 1;
	public static int SOURCE_FEED_DETAIL = 2;
	
	int source;
	long video_id;
	WApp app;
	
	int code;
	int total;
	
	public FetchCommentsTask(WApp app,long video_id,int source){
		this.video_id = video_id;
		this.source=source;
		this.app=app;
		this.execute();
	}
	
	@Override
	protected String doInBackground(String... params) {
		
		String resultComments = "";
		code = 0;
		total = 0;
		String endpointComments = WApp.BASE+WApp.COMMENTS;
		endpointComments = endpointComments.replace("__vid__", ""+video_id);
		endpointComments += "?last_id="+app.getDB().getLastComment(video_id);
		endpointComments += "&android_version="+WApp.APP_VERSION;
		
		MyHttpClient httpclt = new MyHttpClient(app);
		HttpGet getComments = new HttpGet(endpointComments);
		getComments.setHeader("Authorization", "access_token="+app.getAccessToken());
		try {
			HttpResponse cmtResponse = httpclt.execute(getComments);
			HttpEntity cmtEntity = cmtResponse.getEntity();
			code = cmtResponse.getStatusLine().getStatusCode();
			if(cmtEntity!=null){
				InputStream inStream = cmtEntity.getContent();
				resultComments = WApp.convertStreamToString(inStream); 
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		WLog.d(this,"Fetching Comments Result ("+code+"): "+resultComments);
		
		if(code == HTTPStatus.OK){
			try {
				JSONArray jComments = new JSONArray(resultComments);
				JSONObject comment;
				total = jComments.length();
				
//				WLog.d(this,total+" new comments to update on the UI! (from source: "+source+")");
				
				for(int i = 0 ; i < jComments.length() ; i++){
					comment = jComments.getJSONObject(i);
					
					long uid =Long.valueOf(comment.getString("user_id")); 
					new Comment(app,
							Long.valueOf(comment.getString("id")),
							Long.valueOf(comment.getString("video_id")),
							uid,
							comment.getString("comment"),
							comment.getString("created_at")).create();
					
					User u = UserCache.getInstance(app).getUser(uid);
					if(u.getUsername().equals("not_available")){
						u.setUsername(comment.getString("username"));
						u.setAvatar(comment.getString("avatar_url"));
						u.setName(comment.getString("name"));
						UserCache.getInstance(app).updateUser(u);
					}
					
					if(source == SOURCE_PUSH){
						Video v = new Video(app, Long.valueOf(comment.getString("video_id")));
						v.load();
						v.update(DBHelper.C_UPDATED_AT, comment.getString("created_at"));
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		if(code == HTTPStatus.OK && total > 0){
			WBus.getInstance().post(new NewCommentEvent(video_id,source));
		}
		
	}
	
}
