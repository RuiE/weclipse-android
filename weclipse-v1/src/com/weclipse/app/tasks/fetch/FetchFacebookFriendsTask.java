package com.weclipse.app.tasks.fetch;

import org.json.JSONArray;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchFacebookFriendsEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchFacebookFriendsTask {
	
	public FetchFacebookFriendsTask(final WApp app){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			ArrayList<User> mUsers;
			
			@Override
			public void mainPostExecute(int code, String result) {
				
				WBus.getInstance().post(new FetchFacebookFriendsEvent(mUsers,code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Result fb_friends("+code+"): ");
//				WLog.d(this,"Result fb_friends("+code+"): "+result);
				
				if(code == HTTPStatus.OK){
					try {
						 JSONArray users = new JSONArray(result);
						 JSONObject juser;
						 
						 mUsers = new ArrayList<User>();
						 
						 for(int i = 0 ; i < users.length() ; i++ ){
							 juser = users.getJSONObject(i);
							 
							 User u = new User(app,
						 				Long.valueOf(juser.getString("id")),
						 				juser.getString("username"),
						 				juser.getString("name"),
						 				juser.getString("avatar_url"),
						 				juser.getInt("follow_status"),
						 				juser.getBoolean("private"));
							 
							 UserCache.getInstance(app).updateUser(u);
							 mUsers.add(u);
						 }
						 
						 
					 } catch(JSONException e){
						 if(WApp.DEBUG){
							 e.printStackTrace();
						 }
					 }
					PreferenceManager.getInstance(app).putPreference(PreferenceManager.FACEBOOK, true);
				}
				
			}

			@Override
			public void onPreExecute() {
				
			}
			
		};
		
		new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"fb_friends",SimpleTask.TYPE_GET).execute();
	}

}
