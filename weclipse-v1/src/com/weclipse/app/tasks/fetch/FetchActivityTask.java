package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.CacheRequestManager;
import com.weclipse.app.models.MyLike;
import com.weclipse.app.models.UserActivity;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.CacheHelper;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.DefaultHashMap;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.MyHeader;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.ActivityUpdateEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchActivityTask {
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;

	DefaultHashMap<Long, Integer> mStatuses;

	public FetchActivityTask(final WApp app, final int method,final long before_id) {

		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {

			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new ActivityUpdateEvent(code == HTTPStatus.OK,mStatuses));
			}

			@Override
			public void backgroundPreExecute() {

			}

			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Result Fetch Activity("+code+")");
//				WLog.d(this, "Result(" + code + "): " + result);
				if(code == HTTPStatus.OK){
					try {
	
						JSONObject stuff = new JSONObject(result);
	
						ArrayList<UserActivity> array = new ArrayList<UserActivity>();
						ArrayList<MyLike> likes = new ArrayList<MyLike>();
						mStatuses = new DefaultHashMap<Long, Integer>(-1);
	
						JSONArray activities = stuff.getJSONArray("activities");
						JSONArray video_likes = stuff.getJSONArray("video_likes");
						JSONArray statuses = stuff.getJSONArray("follow_statuses");
	
						for (int i = 0; i < activities.length(); i++) {
							array.add(UserActivity.parse(app,activities.getJSONObject(i)));
						}
	
						// Sacando os likes
						if (video_likes.length() > 0) {
							for (int i = 0; i < video_likes.length(); i++) {
								likes.add(new MyLike(app, Long.valueOf(video_likes.getString(i)),DBHelper.TABLE_LIKES_ACTIVITY));
							}
						}
	
						JSONObject temp;
						for (int j = 0; j < statuses.length(); j++) {
							temp = statuses.getJSONObject(j);
							mStatuses.put(Long.valueOf(temp.getString("id")),temp.getInt("status"));
						}
	
						UserActivity.bulkInsert(app, array,method);
						MyLike.bulkInsert(app, likes, method,DBHelper.TABLE_LIKES_ACTIVITY);
	
					} catch (JSONException e) {
						if (WApp.DEBUG)
							e.printStackTrace();
					}
				} else if(code == HTTPStatus.NOT_MODIFIED){
					//Do nothing
				}

			}

			@Override
			public void onPreExecute() {

			}

		};
		
//		CacheHelper mCacheHelper = new CacheHelper() {
//			
//			@Override
//			public void updateTag(String tag) {
//				CacheRequestManager.getInstance(app).putKey(CacheRequestManager.REQUEST_KEY_ACTIVITY, tag);
//			}
//		};

		if (method == SimpleTask.LOAD_MORE) {
			HashMap<String, String> mParams = new HashMap<String, String>();
			mParams.put("before_id", "" + before_id);
			new SimpleTask(app, mSimpleTaskHelper, WApp.BASE + "activity",SimpleTask.TYPE_GET, mParams).execute();
		} else {
//			MyHeader d = new MyHeader("If-None-Match", CacheRequestManager.getInstance(app).getValue(CacheRequestManager.REQUEST_KEY_ACTIVITY));
			
//			new SimpleTask(app, mSimpleTaskHelper, WApp.BASE + "activity",SimpleTask.TYPE_GET,mCacheHelper,d).execute();
			new SimpleTask(app, mSimpleTaskHelper, WApp.BASE + "activity",SimpleTask.TYPE_GET).execute();
		}

	}

}
