package com.weclipse.app.tasks.fetch;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Like;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchLikeEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class FetchLikesTask {
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;
	
	public FetchLikesTask(final WApp app,final int method,final AbstractVideo video){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				
				WLog.d(this,"Likes Fetch("+code+"): "+result);
				
				ArrayList<Like> likes = new ArrayList<Like>();
				ArrayList<User> users = new ArrayList<User>();
				
				try {
					JSONArray jarray_likes = new JSONArray(result);
					JSONObject juser;
					for(int i = 0 ; i < jarray_likes.length() ; i++ ){
						juser = jarray_likes.getJSONObject(i);
						likes.add(new Like(Long.valueOf(juser.getString("like_id")),
											Long.valueOf(juser.getString("id")),
											juser.getString("liked_at")));
						
						users.add(User.parse(app, juser));
					}
					
					
				} catch (JSONException e){
					if(WApp.DEBUG)
						e.printStackTrace();
				}
				
				WBus.getInstance().post(new FetchLikeEvent(video.getId(),likes,users));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
			}

			@Override
			public void onPreExecute() {
				
			}

		};
		
		if(method == LOAD_MORE){
			HashMap<String, String> mParams = new HashMap<String,String>();
			mParams.put("before_id",Like.getLastId(app));
			
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"videos/"+video.getId()+"/likes",SimpleTask.TYPE_GET,mParams).execute();
		} else {
			new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"videos/"+video.getId()+"/likes",SimpleTask.TYPE_GET).execute();
		}
		
	}

}
