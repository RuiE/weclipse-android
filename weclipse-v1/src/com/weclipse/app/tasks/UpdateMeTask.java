package com.weclipse.app.tasks;

import java.io.File;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

import com.weclipse.app.WApp;
import com.weclipse.app.activity.EditProfileActivity;
import com.weclipse.app.activity.SettingsActivity;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.UpdateMeEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class UpdateMeTask {

	public UpdateMeTask(final WApp app,final int field,final String value){
		
		SimpleTaskHelper mTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new UpdateMeEvent(field,code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Result udpate me task("+code+"): "+result);
				if(code == HTTPStatus.OK){
		        	switch(field){
					case SettingsActivity.PASSWORD:
						break;
					case EditProfileActivity.PRIVATE:
						Me.getInstance(app).setPrivate_user(Integer.valueOf(value));
						Me.getInstance(app).save();
						break;
					case EditProfileActivity.NAME:
						Me.getInstance(app).setName(value);
						Me.getInstance(app).save();
						break;
					}
		        }
			}

			@Override
			public void onPreExecute() {
				
			}
		};
		

		HashMap<String, String> mParams = new HashMap<String,String>();
		
		switch(field){
		case SettingsActivity.PASSWORD:
			mParams.put("password", value);
			break;
		case EditProfileActivity.PRIVATE:
			mParams.put("private", value);
			break;
		case EditProfileActivity.NAME:
			mParams.put("name",value);
			break;
		}
			
		new SimpleTask(app,mTaskHelper,WApp.BASE+"update_profile",SimpleTask.TYPE_POST,mParams).execute();
		
	}
	
}
