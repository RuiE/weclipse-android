package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.WLog;

public class UnlinkContactsTask {

	public UnlinkContactsTask(final WApp app){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Resultado unlink contact("+code+"): "+result);
			}
		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"unlink_contact", SimpleTask.TYPE_POST).execute();
		
	}
	
}
