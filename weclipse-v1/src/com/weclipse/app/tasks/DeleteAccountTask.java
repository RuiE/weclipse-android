package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DeleteAccountEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class DeleteAccountTask {

	WApp app;
	int code;
	
	public DeleteAccountTask(final WApp app){
		this.app=app;
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WLog.d(this,"Deleted Account("+code+")");
				if(code==HTTPStatus.OK){
					app.logout(false);
					WBus.getInstance().post(new DeleteAccountEvent());
				}
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
			}

		};
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"delete_account", SimpleTask.TYPE_POST).execute();
	}
	
}
