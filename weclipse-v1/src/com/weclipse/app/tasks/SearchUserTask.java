package com.weclipse.app.tasks;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import com.weclipse.app.WApp;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FetchFollowersEvent;
import com.weclipse.app.utils.eventbus.SearchEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class SearchUserTask {
	
	public SearchUserTask(final WApp app,String query){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				
				try {
					JSONArray jarray = new JSONArray(result);
					
					ArrayList<User> mUsers = new ArrayList<User>();
					
					for(int i = 0 ; i < jarray.length() ; i++ ){
						mUsers.add(User.parse(app, jarray.getJSONObject(i)));
					}
					
					WBus.getInstance().post(new SearchEvent(code == HTTPStatus.OK, mUsers));
					
				} catch(JSONException e){
					if(WApp.DEBUG)
						e.printStackTrace();
				}
				
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Result search users("+code+"): "+result);
				
				
				
			}
		};
		
		HashMap<String, String> mParams = new HashMap<String, String>();
		mParams.put("query",query);
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"search_users", SimpleTask.TYPE_GET,mParams).execute();
		
	}

}
