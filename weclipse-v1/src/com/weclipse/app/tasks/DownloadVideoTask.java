package com.weclipse.app.tasks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Environment;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.CacheManager;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.TimelineVideo;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.DownloadVideoCompleteEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class DownloadVideoTask extends AsyncTask<String, String, String> {
	
	WApp app;
	AbstractVideo video;
	int code;
	
	boolean canceled = false;

	public DownloadVideoTask(WApp app,AbstractVideo video){
		this.app = app;
		this.video = video;
		this.execute();
	}
	
	@Override
	protected void onCancelled() {
		WLog.d(this,"Download canceled!!");
		canceled = true;
	}
	
	@Override
	protected String doInBackground(String... arg0) {
	
		code = 0;
		
		if(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+CacheManager.CACHE_DIR+video.getId()+".zip").exists()){
			WLog.d(this,"Já existe o zip, nem vou tocar pq já se deve ter visto o checksum");
			code = HTTPStatus.OK;
			return null;
		}
		
		//Verificar se por acaso já está sacado
		
		String original = WApp.BASE_NOSSL+WApp.VIDEO_DOWNLOAD;
		String endpoint = original.replace("__vid__",""+video.getId());
		
		endpoint += "?android_version="+WApp.APP_VERSION;
		
//		WLog.d(this,"Endpoint: "+endpoint);
		
		try {
			URL url = new URL(endpoint);
            HttpURLConnection temp = (HttpURLConnection) url.openConnection();
            temp.setRequestProperty("Authorization", "access_token="+app.getAccessToken());
            temp.setInstanceFollowRedirects(false);
            code = temp.getResponseCode();
            
	        if(code == HTTPStatus.REDIRECT_FOUND){
	            URL secondURL = new URL(temp.getHeaderField("Location"));
	            URLConnection urlConnection = secondURL.openConnection();
	            urlConnection.connect();
	//            File folder = new File(Environment.getExternalStorageDirectory().getCanonicalPath() + "/weclipse/" + id);
	//            folder.mkdirs();
	            String path = Environment.getExternalStorageDirectory().getAbsolutePath()+WApp.BASE_DIR+CacheManager.CACHE_DIR+video.getId()+".zip";
	            File myFile = new File(path);
	            if(myFile.exists()){
	            	WLog.d(this,"File already present, lets not corrupt it");
	            	
	            	String endpointVideo = WApp.BASE+WApp.VIDEO;
		            endpointVideo = endpointVideo.replace("__vid__",""+video.getId());
		            endpointVideo = endpointVideo.replace("__uid__",""+video.getOwner_id());
		            endpointVideo += "?android_version="+WApp.APP_VERSION;
		            
	//				Log.d(TAG,"Endpoint: "+endpointVideo);
					String result = null;
					MyHttpClient httpclient = new MyHttpClient(app);
					HttpGet get = new HttpGet(endpointVideo);
					get.setHeader("Authorization", "access_token="+app.getAccessToken());
					HttpResponse response;
					try {
						response = httpclient.execute(get);
						HttpEntity entity = response.getEntity();
						code = response.getStatusLine().getStatusCode();
						if(entity!=null){
							InputStream inStream = entity.getContent();
							result = WApp.convertStreamToString(inStream); 
						}
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
	//				Log.d(TAG,"Result: "+result);
					JSONObject video = new JSONObject(result);
					
					if(video.getString("checksum").equals(Utils.getMD5Checksum(path))){
	//					Log.d(TAG,"CHECK SUM CHECK on already exists!");
						//probably already said i downsloaded it!
						WLog.d(this,"Returning, no download");
						return null;
					} else {
	//					Log.d(TAG,"CHECK SUM NOOT CHECK!, needs to download it again");
						myFile.delete();
					}
	            }
	            
	            myFile.createNewFile();
	            FileOutputStream fos = new FileOutputStream(myFile);
	            
	            InputStream inputStream = secondURL.openStream();
	            
//	            int fileLength = urlConnection.getContentLength();
	            
	            byte[] buffer = new byte[1024];
	            int bufferLength = 0;
	            
//	            int total = 0;
//	            long time = 0;
//	            int progress_val = 0;
	            
//	            publishProgress(progress_val);
	            while((bufferLength = inputStream.read(buffer)) > 0 ) {
	            	if(isCancelled()){
	            		inputStream.close();
	            		fos.close();
	            		myFile.delete();
	            		WLog.d(this,"Canceling this one yeh");
	            		return null;
	                }
//	            	total += bufferLength;
//	            	progress_val = (int)(total*100/fileLength);
//	            	if(System.currentTimeMillis() - time >= 100 || progress_val == 100){
//	            		time = System.currentTimeMillis();
//	            		publishProgress(progress_val);
//	            	}
	                fos.write(buffer, 0, bufferLength);
	            }
	            
	            //close the output stream when complete //
	            fos.close();
	            
	            //get the video details to check if the checksum fits.
	            
	            String endpointVideo = WApp.BASE+WApp.VIDEO;
	            endpointVideo = endpointVideo.replace("__vid__",""+video.getId());
	            endpointVideo = endpointVideo.replace("__uid__",""+video.getOwner_id());
	            endpointVideo += "?android_version="+WApp.APP_VERSION;
	            
	//			Log.d(TAG,"Endpoint: "+endpointVideo);
				String result=null;
				MyHttpClient httpclient = new MyHttpClient(app);
				HttpGet get = new HttpGet(endpointVideo);
				get.setHeader("Authorization", "access_token="+app.getAccessToken());
				HttpResponse response;
				try {
					response = httpclient.execute(get);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if(entity!=null){
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream); 
					}
				} catch (ClientProtocolException e) {
					if(WApp.DEBUG)
						e.printStackTrace();
				} catch (IOException e) {
					if(WApp.DEBUG)
						e.printStackTrace();
				}
	//			Log.d(TAG,"Result: "+result);
				
				if(code == HTTPStatus.OK){
					JSONObject video = new JSONObject(result);
					
					if(video.getString("checksum").equals(Utils.getMD5Checksum(path))){
		//				Log.d(TAG,"CHECK SUM CHECK!");
						
					} else {
		//				Log.d(TAG,"CHECK SUM NOOT CHECK!");
						this.video.update(DBHelper.C_VIDEO_STATUS,Video.READY_TO_DOWNLOAD);
						myFile.delete();
					}
				} else {
					//Do something about it !?
				}
	        } else {
	        	
	        }
	        	
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if(!canceled){
			WLog.d(this, "Posting Downlaod Video Event Complete!!");
			WBus.getInstance().post(new DownloadVideoCompleteEvent(video.getId(),code == HTTPStatus.OK));
		}
	}
	
}
