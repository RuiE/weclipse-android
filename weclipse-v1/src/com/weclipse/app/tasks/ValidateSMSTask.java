package com.weclipse.app.tasks;

import java.util.HashMap;

import com.weclipse.app.WApp;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.ValidateSMSEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class ValidateSMSTask {
	
	public ValidateSMSTask(final WApp app,final String code){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new ValidateSMSEvent(code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Resultado validate_code("+code+"): "+result);
				
				if(code == HTTPStatus.OK){
					PreferenceManager.getInstance(app).putPreference(PreferenceManager.HAS_PHONE, true);
				}
				
			}
		};
		
		HashMap<String,String> mParams = new HashMap<String, String>();
		
		mParams.put("code", code);
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"validate_code", SimpleTask.TYPE_POST,mParams).execute();
		
	}

}
