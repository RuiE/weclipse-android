package com.weclipse.app.tasks;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.os.AsyncTask;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.weclipse.app.WApp;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.MyHeader;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.UnauthorizedEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class SimpleTask extends AsyncTask<String, String, String> {
	
	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	WApp app;
	int code;
	String result;
	SimpleTaskHelper sth;
	CacheHelper ch;
	File f;
	
	String endpoint;
	int type;
	HashMap<String, String> mParams;
	MyHeader header;
	
	public static final int TYPE_GET = 1;
	public static final int TYPE_POST = 2;
	
	public static int FRESH = 0;
	public static int LOAD_MORE = 1;
	
	public SimpleTask(WApp app,SimpleTaskHelper simpleTaskHelper,String endpoint,int type,HashMap<String, String> params){
		this.app = app;
		this.sth = simpleTaskHelper;
		this.endpoint = endpoint;
		this.type = type;
		this.mParams = params;
	}
	
	public SimpleTask(WApp app,SimpleTaskHelper simpleTaskHelper,String endpoint,int type,HashMap<String, String> params,File f){
		this.app = app;
		this.sth = simpleTaskHelper;
		this.endpoint = endpoint;
		this.type = type;
		this.mParams = params;
		this.f = f;
	}
	
	public SimpleTask(WApp app,SimpleTaskHelper simpleTaskHelper,String endpoint,int type){
		this.app = app;
		this.sth = simpleTaskHelper;
		this.endpoint = endpoint;
		this.type = type;
	}
	
	public SimpleTask(WApp app,SimpleTaskHelper simpleTaskHelper,String endpoint,int type,CacheHelper mCacheHelper,MyHeader header){
		this.app = app;
		this.sth = simpleTaskHelper;
		this.endpoint = endpoint;
		this.type = type;
		this.header = header;
		this.ch = mCacheHelper;
	}
	
	public SimpleTask(WApp app,SimpleTaskHelper simpleTaskHelper,String endpoint,int type,File f){
		this.app = app;
		this.sth = simpleTaskHelper;
		this.endpoint = endpoint;
		this.type = type;
		this.f = f;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		if(sth != null)
			sth.onPreExecute();
	}
	
	@Override
	protected String doInBackground(String... params) {
		
		if(sth != null)
			sth.backgroundPreExecute();
		
		code = 0;
		result = "";
		
		RequestBody requestBody = null;
		
		//General task here
		if(type == TYPE_GET){
			
			endpoint += "?android_version="+WApp.APP_VERSION;
			
			//Add parameters
			if(mParams != null){
				for(Map.Entry<String, String> entry : mParams.entrySet()){
					try {
						endpoint += "&"+entry.getKey()+"="+URLEncoder.encode(entry.getValue(),"utf-8");
					} catch(UnsupportedEncodingException e){
						if(WApp.DEBUG)
							e.printStackTrace();
					}
				}
			}	
		} else if(type == TYPE_POST) {
			
			if(f == null){
				FormEncodingBuilder builder = new FormEncodingBuilder();
				
				builder.add("android_version", WApp.APP_VERSION);
				if(mParams != null){
					for(Map.Entry<String, String> entry : mParams.entrySet()){
						builder.add(entry.getKey(), entry.getValue());
					}
				}
				
				requestBody = builder.build();
			} else {
				
				MultipartBuilder builder = new MultipartBuilder();
				
				builder.type(MultipartBuilder.FORM);
				builder.addFormDataPart("android_version", WApp.APP_VERSION);
				
				if(mParams != null){
					for(Map.Entry<String, String> entry : mParams.entrySet()){
						builder.addFormDataPart(entry.getKey(), entry.getValue());
					}
				}
				
				builder.addFormDataPart("avatar", "avatar", RequestBody.create(MediaType.parse("image/png"), f));
			}
			
		}
		
		OkHttpClient client = new OkHttpClient();
		
		client.setConnectTimeout(10, TimeUnit.SECONDS);
		client.setReadTimeout(10, TimeUnit.SECONDS);
		
		Request.Builder builder = new Request.Builder();
		
		builder.url(endpoint);
		builder.addHeader("Authorization", "access_token="+app.getAccessToken());
		builder.addHeader("Connection", "close");
		
		if(header != null){
			builder.header(header.getName(), header.getValue());
		}
		
		if(type == TYPE_POST)
			builder.post(requestBody);

		
		Request request = builder.build();
		
		Response response;
		try {
			response = client.newCall(request).execute();
			result = response.body().string();
			code = response.code();
			
			if(code != HTTPStatus.NOT_MODIFIED && response.header("Etag") != null){
				if(ch != null){
					ch.updateTag(response.header("Etag"));
				}
			}
			
			
			
		} catch (IOException e) {
			if(WApp.DEBUG)
				e.printStackTrace();
		}
		
		if(sth != null)
			sth.backgroundPostExecute(code, result);
		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		/* Do the 'must do' code */
		if(code == HTTPStatus.UNAUTHORIZED){
			WLog.d(this,"O Access token foi: "+app.getAccessToken());
			WBus.getInstance().post(new UnauthorizedEvent());
		}
		
		if(sth != null)
			sth.mainPostExecute(code, result);
	}
	
	public interface SimpleTaskHelper {
		public void onPreExecute();
		public void backgroundPreExecute();
		public void backgroundPostExecute(int code,String result);
		public void mainPostExecute(int code,String result);
	}
	
	public interface CacheHelper {
		public void updateTag(String tag);
	}

}
