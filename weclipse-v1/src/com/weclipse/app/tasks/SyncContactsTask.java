package com.weclipse.app.tasks;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.weclipse.app.WApp;
import com.weclipse.app.managers.PreferenceManager;
import com.weclipse.app.models.Me;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.SyncContactsEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class SyncContactsTask {

	JSONArray jarray;
	WApp app;
	
	public SyncContactsTask(WApp app){
		
		jarray = new JSONArray();
		this.app = app;
		
		a();
		
	}
	
	public void a(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				ContentResolver cr = app.getContentResolver();
    	        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
    	        if (cur.getCount() > 0) {
    	        	PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    	        	PhoneNumber phoneNumber;
    	            while (cur.moveToNext()) {
    	                  String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
    	                  if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
    	                     Cursor pCur = cr.query(
    	                               ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
    	                               ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{id}, null);
    	                     JSONArray number;
    	                     while (pCur.moveToNext()) {
	                        	 String phoneNo=pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	                        	 try{
	                        		 phoneNumber = phoneUtil.parse(phoneNo,phoneUtil.getRegionCodeForCountryCode(Integer.valueOf(PreferenceManager.getInstance(app).getStringPreference(PreferenceManager.PHONE_CODE))));
    	                        	 phoneNo = phoneNo.replace(" ","");
    	                        	 number = new JSONArray();
    	                        	 number.put(phoneNumber.getCountryCode()+"");
    	                        	 number.put(phoneNumber.getNationalNumber()+"");
    	                        	 jarray.put(number);
	                        	 } catch(NumberParseException npe){
	                        		 ;
	                        	 }
	                        	 
    	                    }
    	                    pCur.close();
    	                }
    	            }
    	        }
    	        
    	        sync();
    	        
			}
		}).run();
	}
	
	public void sync(){
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void mainPostExecute(int code, String result) {
				
				ArrayList<User> mUsers = new ArrayList<User>();
				
				try {
					JSONArray jarray_users = new JSONArray(result);
					
					for(int i = 0 ; i < jarray_users.length() ; i++){
						mUsers.add(User.parse(app, jarray_users.getJSONObject(i)));
					}
					
				} catch (JSONException e){
					if(WApp.DEBUG){
						e.printStackTrace();
					}
				}
				
				WBus.getInstance().post(new SyncContactsEvent(code==HTTPStatus.OK,mUsers));
			}
			
			@Override
			public void backgroundPreExecute() {
        		
    			/* Iterate over all contacts */
    			
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				WLog.d(this,"Resultado Sync contacts("+code+"): "+result);
			}
		};
		
		HashMap<String,String> mParams = new HashMap<String,String>();
		WLog.d(this, "Putting these number of contacts: "+jarray.length()+"<");
		mParams.put("contacts", jarray.toString());
		
		
		new SimpleTask(app, mSimpleTaskHelper, WApp.BASE+"sync_contacts", SimpleTask.TYPE_POST ,mParams).execute();
	}
	
}
