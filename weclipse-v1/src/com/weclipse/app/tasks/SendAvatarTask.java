package com.weclipse.app.tasks;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Me;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.SendAvatarEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class SendAvatarTask extends AsyncTask<String, String, String> {

	int code;
	WApp app;
	
	public SendAvatarTask(WApp app){
		this.app = app;
	}

	@Override
	protected String doInBackground(String... params) {
		
		String endpoint = WApp.BASE + "update_profile";
		// Log.d(TAG,"Endpoint: "+endpoint);
		code = 0;
		String result = null;
		MyHttpClient httpclient = new MyHttpClient(app);
		HttpPost post = new HttpPost(endpoint);
		HttpResponse response;
		File f = new File(WApp.TEMP_AVA_LOC);
		try {
			MultipartEntity postEntity = new MultipartEntity();
			postEntity.addPart("android_version", new StringBody(WApp.APP_VERSION));
			postEntity.addPart("avatar", new FileBody(f, "image/jpeg"));
			post.setHeader("Authorization", "access_token=" + app.getAccessToken());
			post.setEntity(postEntity);

			response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			code = response.getStatusLine().getStatusCode();
			if (entity != null) {
				InputStream inStream = entity.getContent();
				result = WApp.convertStreamToString(inStream);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}

		WLog.d(this, "Result update avatar(" + code + "): <<");
//		 WLog.d(this,"Result update avatar("+code+"): "+result);
		if(code == HTTPStatus.OK){
			try {
				
				JSONObject me = new JSONObject(result);
				Me.getInstance(app).setAvatar(me.getString("avatar_url"));
				Me.getInstance(app).save();
				
				f.delete();
				
			} catch (JSONException e) {
				if(WApp.DEBUG)
					e.printStackTrace();
			}
		}
		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		WBus.getInstance().post(new SendAvatarEvent(code == HTTPStatus.OK));
		
	}
	
}
