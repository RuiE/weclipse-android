package com.weclipse.app.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.models.AbstractVideo;
import com.weclipse.app.models.Comment;
import com.weclipse.app.models.PendingComment;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.DBHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.NetworkUtils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.SendCommentInitEvent;
import com.weclipse.app.utils.eventbus.SendCommentResultEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class SendCommentTask extends AsyncTask<String, String, String> {

	WApp app;
	int code;
	long last_id;
	AbstractVideo video;
	String comment;
	PendingComment pcomment;
	
	public SendCommentTask(WApp app,String comment,AbstractVideo video){
		this.app = app;
		this.comment = comment;
		this.video = video;
		this.execute();
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pcomment = new PendingComment(app,System.currentTimeMillis(),video.getId(),comment);
		pcomment.create();
		WBus.getInstance().post(new SendCommentInitEvent());
	}
	
	@Override
	protected String doInBackground(String... params) {
		String endpoint = WApp.BASE+WApp.COMMENTS;
		endpoint = endpoint.replace("__vid__", ""+video.getId());
		code = 0;
		String result = null;
		MyHttpClient httpclient = new MyHttpClient(app);
		HttpPost post = new HttpPost(endpoint);
		HttpResponse response;
		try {
			MultipartEntity postEntity = new MultipartEntity();
			postEntity.addPart("comment", new StringBody("" + comment,Charset.forName("UTF-8")));
			postEntity.addPart("android_version",new StringBody(WApp.APP_VERSION));
			post.setEntity(postEntity);
			post.setHeader("Authorization", "access_token="+app.getAccessToken());
			
			response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			code = response.getStatusLine().getStatusCode();
			if (entity != null) {
				InputStream inStream = entity.getContent();
				result = WApp.convertStreamToString(inStream);
			}
			
			WLog.d(this,"Result: "+result);
			
			if(code == HTTPStatus.CREATED){
				JSONObject comment = new JSONObject(result);
				new Comment(app,
							Long.valueOf(comment.getString("id")),
							Long.valueOf(comment.getString("video_id")),
							Long.valueOf(comment.getString("user_id")),
							comment.getString("comment"),
							comment.getString("created_at")).create();
				
				JSONObject stats = comment.getJSONObject("video_stats");
				
				video.update(DBHelper.C_UPDATED_AT, comment.getString("created_at"));
				
				video.updateStats(stats.getInt("nr_likes"),
									stats.getInt("nr_views"),
									stats.getInt("nr_comments"),
									0);
				
			} else {
			
			}
			
			/*
			 * Always destroying for the sake of not showing it.
			 * Merely serves for the purpose of drawing right away.
			 */
			pcomment.destroy();
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if(code == HTTPStatus.CREATED){
			WBus.getInstance().post(new SendCommentResultEvent());
		} else if(code == 0){
			if(!NetworkUtils.isInternetConnected(app)){
				//TODO Event to show no internet
//    			getToast().showNoInternet();
    		} else {
    			//TODO EVent to show no server
//    			getToast().showNoServer();
    		}
		} else {
			
		}
		
	}

}
