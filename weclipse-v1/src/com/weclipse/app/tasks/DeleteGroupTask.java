package com.weclipse.app.tasks;

import com.weclipse.app.WApp;
import com.weclipse.app.models.Group;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.eventbus.DeleteGroupFinishEvent;
import com.weclipse.app.utils.eventbus.DeleteGroupInitEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class DeleteGroupTask {
	
	public DeleteGroupTask(final WApp app,final Group g){
		
		SimpleTaskHelper mDeleteGroupTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WBus.getInstance().post(new DeleteGroupFinishEvent(g.getId(), code == HTTPStatus.OK));
			}
			
			@Override
			public void backgroundPreExecute() {
				
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				
			}

			@Override
			public void onPreExecute() {
				WBus.getInstance().post(new DeleteGroupInitEvent(g.getId()));
			}

		};
		
		new SimpleTask(app,mDeleteGroupTaskHelper,WApp.BASE+"groups/"+g.getId()+"/remove",SimpleTask.TYPE_POST).execute();
	}

}
