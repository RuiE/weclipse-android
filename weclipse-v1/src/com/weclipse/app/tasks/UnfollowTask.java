package com.weclipse.app.tasks;

import org.json.JSONException;
import org.json.JSONObject;

import com.weclipse.app.WApp;
import com.weclipse.app.models.User;
import com.weclipse.app.tasks.SimpleTask.SimpleTaskHelper;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.UserCache;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.FollowEvent;
import com.weclipse.app.utils.eventbus.UnfollowEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class UnfollowTask {
	
	public UnfollowTask(final WApp app,final long user_id){
		
		SimpleTaskHelper mSimpleTaskHelper = new SimpleTaskHelper() {
			
			@Override
			public void mainPostExecute(int code, String result) {
				WLog.d(this,"Fiz unfollow("+code+"):"+result);
				
				try {
					
					JSONObject json = new JSONObject(result);
					
					User u = User.parse(app, json);
					u.setFollow_status(User.NOTHING);
					
					UserCache.getInstance(app).updateUser(u);
					
					WBus.getInstance().post(new UnfollowEvent(user_id,code == HTTPStatus.OK,json.getInt("nr_followers")));
					
				} catch(JSONException e){
					if(WApp.DEBUG)
						e.printStackTrace();
					
					WBus.getInstance().post(new UnfollowEvent(user_id,code == HTTPStatus.OK,0));
				}
			}
			
			@Override
			public void backgroundPreExecute() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void backgroundPostExecute(int code, String result) {
				if(code == HTTPStatus.OK){
					
		        }
			}

			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
			}
		};
					
		new SimpleTask(app,mSimpleTaskHelper,WApp.BASE+"users/"+user_id+"/unfollow",SimpleTask.TYPE_POST).execute();
		
	}


}
