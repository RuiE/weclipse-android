package com.weclipse.app.tasks;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.weclipse.app.WApp;
import com.weclipse.app.models.PendingVideo;
import com.weclipse.app.models.User;
import com.weclipse.app.models.Video;
import com.weclipse.app.tls.MyHttpClient;
import com.weclipse.app.utils.CustomMultiPartEntity;
import com.weclipse.app.utils.CustomMultiPartEntity.ProgressListener;
import com.weclipse.app.utils.HTTPStatus;
import com.weclipse.app.utils.Utils;
import com.weclipse.app.utils.WLog;
import com.weclipse.app.utils.eventbus.CreatePendingVideoFinishedEvent;
import com.weclipse.app.utils.eventbus.CreatePendingVideoStartedEvent;
import com.weclipse.app.utils.eventbus.ProgressPendingCreateVideoEvent;
import com.weclipse.app.utils.eventbus.VideoCreatedEvent;
import com.weclipse.app.utils.eventbus.WBus;

public class CreatePendingVideoTask extends AsyncTask<Integer, Integer, String> {

	int code;
	int codeUpload;
	PendingVideo pv;
	long vid;
	WApp app;
	boolean success;
	long time;
	long totalSize;

	public CreatePendingVideoTask(WApp app,PendingVideo pv) {
		this.pv=pv;
		this.app = app;
		this.execute();
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		app.getOfflineManager().addWorkingId(pv.getId());
		app.getFeedLoadingManager().getPendingLoadingIds().add(pv.getId());
		WBus.getInstance().post(new CreatePendingVideoStartedEvent(pv.getId()));
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		WBus.getInstance().post(new ProgressPendingCreateVideoEvent(pv.getId(),values[0]));
	}

	@Override
	protected String doInBackground(Integer... params) {
		
		codeUpload = 0 ;
		code = 0;
		success = false;
		
		String endpointUpload = WApp.BASE+WApp.UPLOAD_PIN;
		endpointUpload = endpointUpload.replace("__pin__",""+pv.getId());
		endpointUpload += "?android_version="+WApp.APP_VERSION;
//		Log.d(TAG,"Endpoint: "+endpoint);
		String resultUploadPin=null;
		MyHttpClient uploadPinClient = new MyHttpClient(app);
		HttpGet get = new HttpGet(endpointUpload);
		get.setHeader("Authorization", "access_token="+app.getAccessToken());
		try {
			HttpResponse response = uploadPinClient.execute(get);
			HttpEntity entity = response.getEntity();
			codeUpload = response.getStatusLine().getStatusCode();
			if(entity!=null){
				InputStream inStream = entity.getContent();
				resultUploadPin = WApp.convertStreamToString(inStream); 
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		WLog.d(this,"Code of the check_retry: "+codeUpload);
		
		if(codeUpload==HTTPStatus.OK){
			success = true;
			try {
				JSONArray jArray = new JSONArray(resultUploadPin);
				JSONObject jason;
				for(int i=0;i<jArray.length();i++){
					jason = jArray.getJSONObject(i);
					JSONArray jarrayPlayers = jason.getJSONArray("playerslist");
					JSONObject juser;
					int user_status = 0;
					ArrayList<Long> users = new ArrayList<Long>();
					ArrayList<Integer> statuses = new ArrayList<Integer>();
					ArrayList<String> replied_ats = new ArrayList<String>();
					for (int j = 0; j < jarrayPlayers.length(); j++) {
						juser = jarrayPlayers.getJSONObject(j);
						users.add(Long.valueOf(juser.getString("id")));
						statuses.add((juser.getString("replied_at")!=null && juser.getString("replied_at")!="null")?1:0);
						if(app.getId()==Long.valueOf(juser.getString("id"))){
							user_status = juser.getInt("status");
						}
						replied_ats.add(juser.getString("replied_at"));
						User.parse(app, juser).create();
					}
					
					if(i == 0)
						vid = Long.valueOf(jason.getString("id"));
					
					Video.parse(app, jason).create();
				}
			} catch (JSONException e){
				if(WApp.DEBUG){
					e.printStackTrace();
				}
			}
			
			WApp.deleteMediaFile(pv.getFilePath());
			pv.destroy();
		} else if(codeUpload == HTTPStatus.NOT_FOUND){
			String endpoint = WApp.BASE;
			endpoint += WApp.CREATE_CLIP;
			endpoint = endpoint.replace("__uid__", "" + app.getUID());
			//Log.d(TAG, "Endpoint: " + endpoint);
			
			String result = null;
			MyHttpClient httpclient = new MyHttpClient(app);
			HttpPost post = new HttpPost(endpoint);
			post.setHeader("Authorization", "access_token="+app.getAccessToken());
			HttpResponse response;
			try {	
				CustomMultiPartEntity postEntity = new CustomMultiPartEntity(new ProgressListener() {
					@Override
					public void transferred(long num) {
						int progress_val = (int) ((num/(float)totalSize)*100);
						if(System.currentTimeMillis()-time >= 75 || progress_val == 100){
							time = System.currentTimeMillis();
							publishProgress((int)((float)progress_val*((float)Utils.PROGRESS_FILE_UPLOAD/(float)100)));
						}
					}
				});
				
				File f = new File(pv.getFilePath());
				
				if(f.exists()){
					postEntity.addPart("clip", new FileBody(f));
					postEntity.addPart("media_type", new StringBody("" +pv.getMediaType()));
					postEntity.addPart("duration", new StringBody(""+ pv.getDuration()));
					postEntity.addPart("android_version",new StringBody(WApp.APP_VERSION));
					postEntity.addPart("title", new StringBody("" + pv.getTitle(),Charset.forName("UTF-8")));
					postEntity.addPart("nr_players",new StringBody("" + pv.getPlayers()));
					postEntity.addPart("eta", new StringBody(pv.getEta()));
					postEntity.addPart("upload_pin",new StringBody(""+pv.getId()));
					postEntity.addPart("target", new StringBody("" + pv.getTarget()));
					
					JSONArray jarrayGroups = new JSONArray();
					JSONArray jarrayContacts = new JSONArray();
					
					for (int i = 0; i < pv.getGroups().size(); i++)
						jarrayGroups.put(pv.getGroups().get(i));
					
					for (int i = 0; i < pv.getUsers().size(); i++)
						jarrayContacts.put(pv.getUsers().get(i));
					
					if(pv.getTarget() == WApp.TARGET_BOTH){
						postEntity.addPart("user_ids", new StringBody(""+ jarrayContacts.toString()));
						postEntity.addPart("group_ids",new StringBody("" + jarrayGroups.toString()));
					} else if (pv.getTarget() == WApp.TARGET_FRIENDS) {	
						postEntity.addPart("user_ids", new StringBody(""+ jarrayContacts.toString()));
					} else if (pv.getTarget() == WApp.TARGET_GROUPS) {
						postEntity.addPart("group_ids",new StringBody("" + jarrayGroups.toString()));
					}
					
//					Log.d(TAG,"Post Entity: "+postEntity.toString());
//					Log.d(TAG,"Jarray Contacts: "+jarrayContacts.toString());
//					Log.d(TAG,"Jarray Groups: "+jarrayGroups.toString());
					
					totalSize = postEntity.getContentLength();
					post.setEntity(postEntity);
					response = httpclient.execute(post);
					HttpEntity entity = response.getEntity();
					code = response.getStatusLine().getStatusCode();
					if (entity != null) {
						InputStream inStream = entity.getContent();
						result = WApp.convertStreamToString(inStream);
					}
				} else {
					//Video file does not exist? Need to clean it, nothing we can do...
					code = -1;
				}
				
				
			} catch (ClientProtocolException e) {
				if(WApp.DEBUG)
					e.printStackTrace();
			} catch (IOException e) {
				if(WApp.DEBUG)
					e.printStackTrace();
			}
			
			publishProgress(Utils.PROGRESS_PROCESS_FULL_OUTPUT);
			
			WLog.d(this,"Code from the post the video: "+code);
			WLog.d(this,"Result from the post of the video:"+result);
			
			if(code == HTTPStatus.CREATED){
				success = true;
				processAnswer(result);
				WApp.deleteMediaFile(pv.getFilePath());
				pv.destroy();
			} else if(code == HTTPStatus.BAD_REQUEST){
				WApp.deleteMediaFile(pv.getFilePath());
				pv.destroy();
			} else if(code == -1){
				pv.destroy();
			}
		} else {
//			WLog.d(this,"Code not 200 nor 404... What is this? Nothing happens..");
		}
		
		app.getOfflineManager().clearVideos();
		
		if(code == HTTPStatus.CREATED)
			publishProgress(Utils.PROGRESS_COMPLETE);
		
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		app.getOfflineManager().removeWorkingId(pv.getId());
		app.getFeedLoadingManager().getPendingLoadingIds().remove(pv.getId());
		
//		Log.d(TAG, "Result from server: ("+code+")" + result);
		
		WBus.getInstance().post(new CreatePendingVideoFinishedEvent(vid, pv.getId(), success));
	}
	
	public void processAnswer(String result){
		try {
			if(pv.getTarget()==WApp.TARGET_GROUPS || pv.getTarget()==WApp.TARGET_BOTH){
				JSONArray jArray = new JSONArray(result);
				JSONObject jason;
				int total = jArray.length();
				for(int i = 0 ; i < total ; i++){
					jason = jArray.getJSONObject(i);
					JSONArray jarrayPlayers = jason.getJSONArray("playerslist");
					JSONObject juser;
					int user_status = 0;
					ArrayList<Long> users = new ArrayList<Long>();
					ArrayList<Integer> statuses = new ArrayList<Integer>();
					ArrayList<String> replied_ats = new ArrayList<String>();
					for (int j = 0 ; j < jarrayPlayers.length(); j++) {
						juser = jarrayPlayers.getJSONObject(j);
						users.add(Long.valueOf(juser.getString("id")));
						statuses.add((juser.getString("replied_at")!=null && juser.getString("replied_at")!="null")?1:0);
						if(app.getId()==Long.valueOf(juser.getString("id"))){
							user_status = juser.getInt("status");
						}
						replied_ats.add(juser.getString("replied_at"));
						User.parse(app, juser).create();
					}
					
					if(i == 0)
						vid = Long.valueOf(jason.getString("id"));
					
					publishProgress((int)((Utils.PROGRESS_PROCESS_FULL_OUTPUT)+((float)((2*Utils.PROGRESS_COMPLETE-Utils.PROGRESS_PROCESS_FULL_OUTPUT-Utils.PROGRESS_PROCESS_VIDEO)/total)*(float)i)));
					
					Video.parse(app,jason).create();
				}
			} else {
				JSONObject jason = new JSONObject(result);
				JSONArray jarrayPlayers = jason.getJSONArray("playerslist");
				JSONObject juser;
				ArrayList<Long> users = new ArrayList<Long>();
				ArrayList<Integer> statuses = new ArrayList<Integer>();
				ArrayList<String> replied_ats = new ArrayList<String>();
				for (int j = 0; j < jarrayPlayers.length(); j++) {
					juser = jarrayPlayers.getJSONObject(j);
					users.add(Long.valueOf(juser.getString("id")));
					statuses.add((juser.getString("replied_at")!=null && juser.getString("replied_at")!="null")?1:0);
					replied_ats.add(juser.getString("replied_at"));
					User.parse(app, juser).create();
				}
				
				vid = Long.valueOf(jason.getString("id"));
				
				publishProgress(Utils.PROGRESS_PROCESS_VIDEO);
				
				Video.parse(app,jason).create();
			}
			
		} catch (JSONException e) {
			if(WApp.DEBUG)
				e.printStackTrace();
		}
	}

}
