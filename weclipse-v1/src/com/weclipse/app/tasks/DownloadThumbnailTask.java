package com.weclipse.app.tasks;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.weclipse.app.utils.WLog;

public class DownloadThumbnailTask extends AsyncTask<String, Void, Bitmap> {
	
    ImageView bmImage;
    long id;
    String url;

    public DownloadThumbnailTask(ImageView bmImage,long id,String url) {
        this.bmImage = bmImage;
        this.url = url;
        this.id = id;
    }

    protected Bitmap doInBackground(String... urls) {
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
    	if(((Long)bmImage.getTag()) == id){
//    		WLog.d(this,"Setting image: "+id);
    		bmImage.setImageBitmap(result);
    	}
    }
    
}
